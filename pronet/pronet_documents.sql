--------------------------------------------------------
--  DDL for Table PRONET_RELATIONS
--------------------------------------------------------
CREATE TABLE PRONET_RELATIONS (	
   ID NUMBER, 
   FILE_NAME VARCHAR2(200 BYTE), 
   DATE_INSERTED DATE DEFAULT sysdate
)  ;

--------------------------------------------------------
--  DDL for Table PRONET_DOCUMENTS
--------------------------------------------------------

CREATE TABLE FERBIT.PRONET_DOCUMENTS (
   ID NUMBER, 
   STATUS NUMBER, 
   DATE_ADDED DATE, 
   DATE_FINISHED DATE, 
   PARTNER VARCHAR2(1000 BYTE), 
   INVOICE_NUMBER VARCHAR2(1000 BYTE), 
   INVOICE_DATE VARCHAR2(1000 BYTE), 
   INVOICE_AMOUNT VARCHAR2(1000 BYTE), 
   STATUS_DESCRIPTION VARCHAR2(1000 BYTE)
) 
--------------------------------------------------------
--  DDL for Index PRO_DOKPRIMARY
--------------------------------------------------------
CREATE UNIQUE INDEX FERBIT.PRO_DOKPRIMARY ON FERBIT.PRONET_DOCUMENTS (ID) ;
 
--------------------------------------------------------
--  CREATE for Procedure pronet_login
--------------------------------------------------------
create or replace procedure pronet_login (p_token out varchar2 , username in varchar2, pass in varchar2) is
    req clob := '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://analytics.program.pronet-kr.si/api/soap/">
   <soapenv:Header/>
   <soapenv:Body>
      <soap:Login soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <username xsi:type="xsd:string">'||username||'</username>
         <password xsi:type="xsd:string">'||pass||'</password>
      </soap:Login>
   </soapenv:Body>
</soapenv:Envelope>';

pout clob;
documentIds clob;
token varchar2(32767):='teest';
v_count number := 0;
begin 
 
    url_connect_soap('http://localhost/pronet/api/soap/knjiga-poste.php',req,'POST',pout,'','text/xml','http://analytics.program.pronet-kr.si/api/soap/#Login');
  INSERT INTO CLOB_TEST(A) VALUES (pout); 
  commit;
     SELECT extractValue( xmltype(pout)
                ,'/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:LoginResponse/token'
                           ,'xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://analytics.program.pronet-kr.si/api/soap/"') into token FROM dual;

    p_token := token;
end;
/

--------------------------------------------------------
--  CREATE for pronet_get_documentsIds 
--------------------------------------------------------
create or replace procedure pronet_get_documentsIds(ptoken in varchar2, p_taxNumber in varchar2) as 
req clob;
pout clob;
documentIds clob;
token varchar2(32767):='teest';
v_count number := 0;
l_xml xmltype;
begin  
    req := '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://analytics.program.pronet-kr.si/api/soap/">
           <soapenv:Header/>
           <soapenv:Body>
              <soap:GetDocumentIDs soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                 <token xsi:type="xsd:string">'||Ptoken||'</token>
                      <request>
                    <![CDATA[ 
                 <request> 
                 <requestID>1031</requestID>
                 <VATID>'||p_taxNumber||'</VATID>
                 <documentType>1</documentType>
                   <returnErrors>1</returnErrors>
                    <returnAttachments>0</returnAttachments>
                 <returnTexts>0</returnTexts>
                 <changeStatus>0</changeStatus>
              </request>]]>
                     </request>
              </soap:GetDocumentIDs>
           </soapenv:Body>
        </soapenv:Envelope>';

    url_connect_soap('http://127.0.0.1/pronet/api/soap/knjiga-poste.php',req,'POST',pout,'','text/xml','http://analytics.program.pronet-kr.si/api/soap/#GetDocumentIDs');

  
    l_xml:=xmltype(pout);
    l_xml := l_xml.extract('//*/*/*//ids');
    
    for i in (select vals FROM  XMLTable('/ids' 
                                    PASSING l_xml
                                    COLUMNS vals     CLOB    PATH   'text()') )  
                                loop
                                         FOR r IN (SELECT *
                                             FROM   XMLTABLE('//answer/documents/document'
                                                             PASSING XMLTYPE(i.vals)
                                                             COLUMNS partner varchar2(4000) PATH './partner', 
                                                                     "number" varchar2(4000) PATH './number',
                                                                     "date" varchar2(4000) PATH './date',
                                                                     "amount"  varchar2(4000) PATH './amount',
                                                                      id_r varchar2(4000) PATH './@ID'
                                                             ))
                                        LOOP
                                        dbms_output.put_line(r.id_r);
                                            insert into pronet_documents(id, status, date_added, partner, invoice_number, invoice_date, invoice_amount) values(r.id_r, -1, sysdate, r.partner,r."number", r."date", r."amount"); 
                                        END LOOP;
                                end loop;



/*
    SELECT extractValue( xmltype(pout)
            ,'/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:GetDocumentIDsResponse/ids'
                       ,'xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://analytics.program.pronet-kr.si/api/soap/"') into documentIds FROM dual;

DBMS_OUTPUT.PUT_LINE(documentIds);
   */
commit;
end;
/ 
--------------------------------------------------------
--  CREATE for pronet_get_document_new 
--------------------------------------------------------
create or replace procedure pronet_get_document(p_token in varchar2, p_number in number) is
    req clob := '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://analytics.program.pronet-kr.si/api/soap/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <soap:GetDocument soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                       <token>'||p_token||'</token>
                           <request>
                            <![CDATA[ 
                           <request>
                           <requestID>160023</requestID>
                           <documentID>'||p_number||'</documentID>
                        </request>]]>
                        </request>
                      </soap:GetDocument>
                   </soapenv:Body>
                </soapenv:Envelope>';

    pout clob;
    xmlDoc xmltype;
    l_document clob; 
    v_count number := 0;
    l_status number := -1;
    l_description varchar2(2000):='';
    eslogIdx number := 0;
    cnt number:=0;
begin 
 dbms_output.put_line('p_number='||p_number);
    url_connect_soap('http://127.0.0.1/pronet/api/soap/knjiga-poste.php',req,'POST',pout,'','text/xml','http://analytics.program.pronet-kr.si/api/soap/#GetDocument');

     FOR r IN (SELECT *
         FROM   XMLTABLE( 
                 Xmlnamespaces(
                 'http://schemas.xmlsoap.org/soap/envelope/' as "SOAP-ENV"
               , 'http://analytics.program.pronet-kr.si/api/soap/' as "ns1"
               ),

         '//SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:GetDocumentResponse/document'
                         PASSING xmltype(pout)
                         COLUMNS textt clob PATH '.'
                         ))

    LOOP

        xmlDoc  := xmltype(r.textt);

        FOR r IN (SELECT *
             FROM   XMLTABLE('//answer/documents/document/status'
                             PASSING xmlDoc
                             COLUMNS status number PATH './code',
                                     descriptio varchar2(2000) PATH './description'

                             ))
        LOOP
            if(r.status=0) then
                /*PDF DATOTEKA*/
                 FOR d IN (SELECT *
                     FROM   XMLTABLE('//answer/documents/document'
                                     PASSING xmlDoc
                                     COLUMNS eslog clob PATH './eslog',
                                             attach xmltype PATH './attachment',
                                             id number PATH './@ID'
                                           --  attach_name varchar2(1000) PATH './attachment/@name'
                                     ))
                    LOOP
                    write_to_file('ERACUNI', upper(d.id||'.XML'),d.eslog);

                        cnt := 0;
                    SELECT count(lID) into cnt FROM XMLTABLE('attachment' 
                                                PASSING d.ATTACH
                                                COLUMNS lname VARCHAR2(1000) PATH './@name' ,
                                                        ltype VARCHAR2(1000) PATH './@type' ,
                                                        lID number PATH './@ID',
                                                        ldata clob path 'text()');
                    dbms_output.put_line('vseh ' ||cnt);
                        FOR a IN (SELECT * FROM XMLTABLE('attachment' 
                                                PASSING d.ATTACH
                                                COLUMNS lname VARCHAR2(1000) PATH './@name' ,
                                                        ltype VARCHAR2(1000) PATH './@type' ,
                                                        lID number PATH './@ID',
                                                        ldata clob path 'text()'))   
                            loop
                                eslogIdx := 0;
                                INSERT INTO PRONET_RELATIONS(ID, FILE_NAME) VALUES(d.id, a.lname);
                              if(cnt=1) then --ali je samo en dokument
                                write_to_file('ERACUNI', upper(d.id||'.PDF'),a.ldata);
                              else
                                write_to_file('PRIPONKE', upper(a.lname),a.ldata);
                              end if;

                              --  eslogIdx:=dbms_lob.instr (DecodeBASE64(a.atr),'eSLOG_2_0', 1, 1 ) ;
                              /*  if(eslogIdx<=0) then--preveri ali je slog 2.0
                                   write_to_file('ERACUNI_NOV', upper(a.lname),a.atr);
                                end if; */
                            end loop;


                  /*   if(nvl(length(trim(d.attach_name)),0)=0) then
                        d.attach_name := TO_CHAR(SYSDATE,'ddmmyyyyhh24miss')||'.pdf';
                     end if; 

                     write_to_file('ERACUNI', upper(d.attach_name),d.attach);*/
                    END LOOP;
            end if ;
            update pronet_documents set status=r.status, STATUS_DESCRIPTION=r.descriptio,date_finished=sysdate where id=p_number;
        END LOOP;

     end loop;


end;
/

--------------------------------------------------------
--  CREATE for pronet_download_documents 
--------------------------------------------------------
create or replace procedure pronet_download_documents is
    p_token varchar2(4000);
    p_user varchar2(4000):='';
    p_password varchar2(4000):='';
    cursor not_download is select * from pronet_documents where status=-1;
BEGIN
    pronet_login (p_token,p_user,p_password);
        for r in not_download 
    loop
        pronet_get_document_new(p_token,r.id);
    end loop;
END;
/
--------------------------------------------------------
--  CREATE for pronet_get_doc_for_download 
--------------------------------------------------------
create or replace procedure pronet_get_doc_for_download is
    p_token varchar2(4000);
    p_user varchar2(10000) := '';
    p_pass varchar2(10000) := '';
    p_tax varchar2(10000) := '';
BEGIN
    pronet_login (p_token,p_user,p_pass);

    pronet_get_documentsIds(p_token,p_tax); 
END;
/

--------------------------------------------------------
--  CREATE for PRONET_JOB 
--------------------------------------------------------
BEGIN
    DBMS_SCHEDULER.CREATE_JOB (
            job_name => '"PRONET_JOB"',
            job_type => 'PLSQL_BLOCK',
            job_action => 'declare
begin
    pronet_get_doc_for_download;
    pronet_download_documents;
end;',
            number_of_arguments => 0,
            start_date => TO_TIMESTAMP_TZ('2023-06-19 06:52:24.000000000 EUROPE/PRAGUE','YYYY-MM-DD HH24:MI:SS.FF TZR'),
            repeat_interval => 'FREQ=HOURLY;BYDAY=MON,TUE,WED,THU,FRI,SAT,SUN',
            end_date => NULL,
            enabled => FALSE,
            auto_drop => FALSE,
            comments => 'RUN PRONET SYNC');

         
     
 
    DBMS_SCHEDULER.SET_ATTRIBUTE( 
             name => '"PRONET_JOB"', 
             attribute => 'logging_level', value => DBMS_SCHEDULER.LOGGING_OFF);
      
  
    
    DBMS_SCHEDULER.enable(
             name => '"PRONET_JOB"');
END;
