

/*
    to se mora dodati v trigger TBE_NAKLADI
    deklarirati se mora tudi polja za napake
    
    l_procedure          CHAR(50) := 'TBE_NAKLADI';                       --- ali obracun obstaja
    l_napak              NUMBER   := 0;
    Sql_error            varchar2 (250);                                 --- opis napake
    sql_stmt             VARCHAR2 (3900);
    p_transakcija        NUMBER   :=0;   
    l_error number:=0;

--telo triggerja
    begin 
    if(inserting) then
        INSERT_STATUS_MAIL(
        :new.vez,                     --VEZ
        :new.oseba,                        --holderId 
        :new.nalog,                      --orderNumber
        trim(:new.nnaslov1)||'<br/>'||trim(:new.nulica)||'<br/>'||trim(:new.nstposta)||' '||trim(:new.nposta),         --orderLoadPlace
        :new.DATUMNAK_S,                  --orderDateLoading
        trim(:new.pnaslov1)||'<br/>'||trim(:new.pulica)||'<br/>'||trim(:new.pstposta)||' '||trim(:new.pposta),       --orderUnloading
        :new.PDATUMRAZ_S,                    --orderDateUnloading
        trim(:new.kamion)||'/'||trim(:new.prikolicao),       --orderTruck
        :new.faza,                          --orderStatus
        :new.kupec,                       --clientId
        :new.vezo);                        --clientPersonId 
    
    end if;

  if(updating and (:new.faza<>:old.faza and :new.faza in (0,1,2))or ((:new.faza<>:old.faza and :new.faza in (0,1,2)) and :new.vozilo<>:old.vozilo or :new.prikolicao <> :old.prikolicao )) then
         INSERT_STATUS_MAIL(
        :new.vez,                     --VEZ
        :new.oseba,                        --holderId 
        :new.nalog,                      --orderNumber
        trim(:new.nnaslov1)||'<br/>'||trim(:new.nulica)||'<br/>'||trim(:new.nstposta)||' '||trim(:new.nposta),         --orderLoadPlace
        :new.DATUMNAK_S,                  --orderDateLoading
        trim(:new.pnaslov1)||'<br/>'||trim(:new.pulica)||'<br/>'||trim(:new.pstposta)||' '||trim(:new.pposta),       --orderUnloading
        :new.PDATUMRAZ_S,                    --orderDateUnloading
        trim(:new.kamion)||'/'||trim(:new.prikolicao),       --orderTruck
        :new.faza,                          --orderStatus
        :new.kupec,                       --clientId
        :new.vezo);                        --clientPersonId 

  end if ;
*/

CREATE SEQUENCE  ORDER_MAIL_SEQUENCE  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1  NO CACHE NOORDER  NOCYCLE ;

CREATE TABLE ORDER_MAIL_CONTENT(	
    ID NUMBER, 
    MAIL VARCHAR2(1000 BYTE), 
    SUBJECT VARCHAR2(1000 BYTE), 
    L_CONTENT CLOB, 
    L_STATUS NUMBER, 
    ADDED DATE, 
    SENT DATE
) ;



create or replace procedure url_connect(pUrl in varchar2,lmContent IN CLOB, pMethod IN VARCHAR2, pOut OUT CLOB, auth in varchar2 default '', contentType in varchar2 default 'application/json;charset=utf-8' )IS
/*
      mmessage_id   -  id sporoÄŤila
      muser_id      -  za koga je sprocilo; lahko je id uporabnika ali vozila(odvisno kak mMessageType poĹˇiljamo)
      mtype         -  tip sporocila; 0 sinhronizacija; 1 sporoÄŤilo
      mcontent      -  vsebina
      mfromuser     -  od koga je sporocilo 1-centrala//
      msendeddate   -  datum posiljanja
      mMessageType  -  ÄŤe gre za nalog 0; ÄŤe je sporÄŤilo 1
      */
  t_http_req     utl_http.req;
  t_http_resp    utl_http.resp;
  t_respond      VARCHAR2(30000);
  t_start_pos    INTEGER := 1;
  t_output       VARCHAR2(2000);
  l_mcontent     varchar2(10000);
  t_device       VARCHAR2(2000);
  t_data         VARCHAR(30000);
  sql_error      VARCHAR2(30000);
  l_error        number;
  l_napak        number;
  l_procedure    CHAR(15):='url_connect';
  buffer varchar2(4000);
  response_Text  varchar2(32000);
   l_buffer     varchar2(32767);
   l_length      number:=0;
   l_offset number:=1;
   amount number :=32767;
   pContent clob;
BEGIN
    pContent := ASCIISTR(lmContent);
    pContent := replace(pContent,'\','\u'); 
        l_length := DBMS_LOB.getlength(pContent);
     
    utl_http.Set_body_charset('UTF-8');
    utl_http.Set_transfer_timeout(300);
    t_http_req := utl_http.Begin_request(pUrl , pMethod);
    utl_http.Set_header(t_http_req, 'Content-Type',contentType);
    utl_http.Set_header(t_http_req, 'Connection', 'keep-alive');

    if(nvl(length(trim(auth)),0)<>0) then
        utl_http.Set_header(t_http_req, 'Authorization', auth);
    end if;


    if(trim(pMethod)='POST') then
        if(l_length>0) then 
            utl_http.Set_header(t_http_req, 'Content-Length',l_length);
            
            while(l_offset < l_length)
                          loop
                             dbms_lob.read(pContent, amount, l_offset, l_buffer); 
                             UTL_HTTP.WRITE_TEXT(r    => t_http_req, data => l_buffer);
                             l_offset := l_offset + amount; 
                          end loop;

        else
            utl_http.Set_header(t_http_req, 'Content-Length',0);
        end if;
    else
        utl_http.Set_header(t_http_req, 'Content-Length',0);
    end if ;

    t_http_resp := utl_http.Get_response(t_http_req);
    dbms_lob.createtemporary(pOut, false);

     begin
        loop
          utl_http.read_text(t_http_resp, l_buffer, 32000);
          dbms_lob.writeappend (pOut, length(l_buffer), l_buffer);
        end loop;
      exception
        when utl_http.end_of_body then
          utl_http.end_response(t_http_resp);
      end;
 
    EXCEPTION
    WHEN OTHERS THEN
         l_error   := SQLCODE;
        l_napak   := 1;
         DBMS_OUTPUT.PUT_LINE('SQLERRM urlConnect: '||SQLERRM);
        Sql_error := SUBSTR(SQLERRM, 1, 250);
        INSERT INTO SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,ID_RECORD,sql_stavek)
        VALUES (l_error, 'url_connect',Sql_error,0,'url_connect',0,purl);  


END;
/

create or replace PROCEDURE INSERT_STATUS_MAIL(
        orderId in NUMBER,
        holderId IN NUMBER,
        orderNumber IN number,
        orderLoading IN varchar2,
        orderDateLoading IN date,
        orderUnloading IN varchar2,
        orderDateUnloading IN date,
        orderTruck IN varchar2,
        orderStatus IN number,
        clientId IN number,
        clientPersonId IN NUMBER    ) IS
    l_id number; 
    p_subject varchar2(1000);
    p_content clob;
    l_country_id number;
    l_status_str varchar2(1000);
    orderHolder  varchar2(1000);
    orderHolderMail  varchar2(1000);
    orderClientMail varchar2(1000);
    posljiMail varchar2(1000);
    
    l_procedure          CHAR(50) := 'INSERT_STATUS_MAIL';                       --- ali obracun obstaja
    l_napak              NUMBER   := 0;
    Sql_error            varchar2 (250);                                 --- opis napake
    sql_stmt             VARCHAR2 (3900);
    p_transakcija        NUMBER   :=0;   
    l_error number:=0;
 begin 
 if(orderNumber>0)then-- and nvl(orderDateLoading,to_Date('01.01.1990','dd.mm.yyyy'))>to_date('01.01.2022','dd.mm.yyyy')) then
    begin
        select sdrzava into l_country_id from klienti where sifra=clientId;
    exception when others then
        l_error   := SQLCODE;
         l_napak   :=  l_napak+1;
         Sql_error := SUBSTR(SQLERRM, 1, 250);
         ERROR_LOG(l_error,'iskanje 1' ,Sql_error,0,l_procedure,0);
    end;
    begin
        select trim(ime)||' '||trim(priimek), eposta into orderHolder,orderHolderMail from delavci where sifra=holderId;
    exception when others then
        l_error   := SQLCODE;
         l_napak   :=  l_napak+1;
         Sql_error := SUBSTR(SQLERRM, 1, 250);
         ERROR_LOG(l_error,'iskanje 2' ,Sql_error,0,l_procedure,0);
    end;
    begin
        select  email,posljimail into orderClientMail,posljiMail from osebet where vezo=clientPersonId and klient=clientId;
     exception when others then
        l_error   := SQLCODE;
         l_napak   :=  l_napak+1;
         Sql_error := SUBSTR(SQLERRM, 1, 250);
         ERROR_LOG(l_error,'iskanje 3' ,Sql_error,0,l_procedure,0);
    end;
   -- if(trim(posljiMail)='D') then
       -- orderClientMail:='lakota.matej@gmail.com';
       orderClientMail:='ognjen.jankovic@mansped.hr';
       
    if(nvl(length(trim(orderClientMail)),0)>0) then   
        if(l_country_id=191) then 
            select  DECODE(orderStatus, 
                        0,  'PRIPREMLJEN', 
                        1,  'UTOVAREN', 
                        2,  'ISTOVAREN', 
                        3,  'KONČAN', 
                        5,  'POSLAN', 
                        6,  'SPREJET', 
                        9,  'USKLADIŠČEN', 
                        91, 'RAMPA', 
                        90, 'NA POTI', 
                        '')   
                         into l_status_str from dual;
                         
                p_subject := 'Narudžba '|| orderNumber;
                p_content :='<html><head><meta charset=''UTF-8''></head>'||
                        '<body style=''background-color:white;color:#01193b;''>'||
                            '<div style=''width:900px; margin:auto; font-size: 17px; color:#01193b;font-family: Arial , Helvetica, sans-serif;margin-top:50px;''>'||
                                '<div style=''width:300px;margin:auto; text-align: center; ''> <div style=''border-bottom: 5px solid #01193b;''>Broj narudžbe : '||orderNumber||'</div></div><br/><br/>'||
                                'Poštovani,<br/><br/>Vaša narudžbu je zaprimio/la '||orderHolder||'(<a style=''color:#4c9cd2'' href=''mailto:'||orderHolderMail||'''>'||orderHolderMail||'</a>), ta se vodi pod brojem <strong>'||orderNumber||'</strong>.<br/><br/>';
                                if(orderStatus=0 and nvl(length(trim(orderTruck)),0)=0) then
                                    p_content := p_content||'Uskoro će te biti obviješteni o nominiranom vozilu sa detaljnijim podatcima.<br/><br/><br/>';
                                end if;
                                p_content:=p_content||'<br/><br/>Stanje narudžbe:'||l_status_str||
                                '<br/><strong>Tegljač – Prikolica:</strong> '||orderTruck||
                                '<br/><br/><table style=''width: 100%;''><tr style=''vertical-align: top;''><td><strong>Mjesto utovara:</strong> </td><td>'||orderLoading||'<strong></td><td><strong>Datum</strong></td><td>'||to_char(orderDateLoading,'dd. mm. yyyy')||'</strong></td></tr><tr><td>&nbsp; </td></tr>'||
                                '<tr style=''vertical-align: top;''><td><strong>Mjesto istovara:</strong></td><td> '||orderUnloading||' </TD><td><strong>Datum</strong></td><TD> <strong>'||to_char(orderDateUnloading,'dd. mm. yyyy')||'</strong></TD></tr></table><br/><br/><br/>'||  
                                '<table>'||
                                    '<tr>'||
                                        '<td><strong>S poštovanjem,<br/>'||orderHolder||'</strong><br/><br/>  <br/><br/>'|| 
                                            '<span style=''font-size:13px;''>'||
                                            'Ukoliko niste dobili potvrdbu o zaprimljenoj narudžbi unutar 15 minuta,<br/> molimo Vas da pošaljete e-mail na <a style=''color:#4c9cd2'' href=''mailto:reklamacije@mansped.hr''>reklamacije@mansped.hr</a>'||
                                        '</span></td>'||
                                       '<td><a href="https://www.mansped.hr/"><img src=''https://scontent.flju1-1.fna.fbcdn.net/v/t1.6435-9/95806290_1215708505430777_6794172382874435584_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=174925&_nc_ohc=4qgxlTjq8bwAX-Se6bl&_nc_ht=scontent.flju1-1.fna&oh=00_AT99_f1-YleSFZVxLVugoifvpm6RrvVysPlUUKj0SFKmWg&oe=624D76BC''/></a></td>'||
                                     '</tr>'||
                                '</table>'|| 
                        '</div>'||
                        '</body>'||
                    '</html>';
        else
                select  DECODE(orderStatus, 
                        0,  'READY', 
                        1,  'LOADED', 
                        2,  'UNLOADED', 
                        3,  'ENDED', 
                        5,  'SENT', 
                        6,  'ACCPETED', 
                        9,  'WAREHOUSED', 
                        91, 'LOADING RAMP', 
                        90, 'ON THE WAY', 
                        '')   
                         into l_status_str from dual;
                p_subject := 'Loading order '|| orderNumber;
                    p_content :='<html><head><meta charset=''UTF-8''></head>'||
                            '<body style=''background-color:white;color:#01193b;''>'||
                                '<div style=''width:900px; margin:auto; font-size: 17px; color:#01193b;font-family: Arial , Helvetica, sans-serif;margin-top:50px;''>'||
                                    '<div style=''width:300px;margin:auto; text-align: center; ''> <div style=''border-bottom: 5px solid #01193b;''>Order number : '||orderNumber||'</div></div><br/><br/>'||
                                    'Dear Mr./Mrs.,<br/><br/>Your order was been received under number <strong>'||orderNumber||'</strong> by '||orderHolder||'(<a style=''color:#4c9cd2'' href=''mailto:'||orderHolderMail||'''>'||orderHolderMail||'</a>).<br/><br/>';
                                    if(orderStatus=0 and nvl(length(trim(orderTruck)),0)=0) then
                                        p_content := p_content||'You will soon be notified of the nominated vehicle with detailed information.<br/><br/><br/>';
                                    end if; 
                                    p_content := p_content||'<br/><br/>Order status:<strong>'||l_status_str||
                                    '<br/></strong>Truck / Trailer :'||orderTruck||
                                    '<br/><br/><table style=''width: 100%;''><tr style=''vertical-align: top;''><td><strong>Place of loading:</strong> </td><td>'||orderLoading||'<strong></td><td><strong>Date</strong></td><td>'||to_char(orderDateLoading,'dd. mm. yyyy')||'</strong><td></tr><tr><td>&nbsp;</td></tr>'||
                                    '<tr  style=''vertical-align: top;''><td><strong>Place of unloading:</strong></td><td>'||orderUnloading||'<strong></td><td><strong>Date</strong></td><td>'||to_char(orderDateUnloading,'dd. mm. yyyy')||'<td></tr></table><br/><br/><br/><br/>   '||    
                                    '<table>'||
                                        '<tr>'||
                                            '<td><strong>Sincerely yours, <br/>'||orderHolder||'</strong><br/><br/>  <br/><br/>'|| 
                                                '<span style=''font-size:13px;''>'||
                                                    'If you have not received a confirmation of receipt of the order within 15 minutes,<br/> please send an email to <a style=''color:#4c9cd2'' href=''mailto:reklamacije@mansped.hr''>reklamacije@mansped.hr</a>'||
                                            '</span></td>'||
                                                '<td><a href="https://www.mansped.hr/"><img src=''https://scontent.flju1-1.fna.fbcdn.net/v/t1.6435-9/95806290_1215708505430777_6794172382874435584_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=174925&_nc_ohc=4qgxlTjq8bwAX-Se6bl&_nc_ht=scontent.flju1-1.fna&oh=00_AT99_f1-YleSFZVxLVugoifvpm6RrvVysPlUUKj0SFKmWg&oe=624D76BC''/></a></td>'||
                                       
                                        '</tr>'||
                                    '</table> '||    
                            '</div>'||
                            '</body>'||

                        '</html>';

        end if;
    
        select ORDER_MAIL_SEQUENCE.nextval into l_id from dual;
    
        insert into order_mail_content (
            ID,
            MAIL,
            SUBJECT,
            L_CONTENT,
            L_STATUS,
            ADDED
        ) values(
            l_id,
            orderClientMail,
            p_subject,
            p_content,
            0,
            SYSDATE
         );
     end if;--length mail
   --  end if;
   end if;---orderNumber>0
 end;
 /

 create or replace procedure send_mail_order_status is
    a number;
    cursor mails is  select * from order_mail_content where l_status=0;
    l_content clob;
    pout clob;
    --CREATE SEQUENCE ORDER_MAIL_SEQUENCE INCREMENT BY 1 START WITH 1 MINVALUE 1;
    response json;
            --DBMS_OUTPUT.PUT_LINE('----------------');  
begin
    for m in mails 
    loop
        l_content:=trim('{"toMail":"'||m.mail||'","subject":"'||m.subject||'","content":"'||m.L_CONTENT||'", "mailType":1,"userName":"no-reply@mansped.hr", "password":"Gax81359"}');
       -- url_connect_odpad('http://www.ferbit.net/mobilno/Mobile/12/webresources/mail/sendMailNew/',l_content, 'POST', pOut, '',  'application/json;charset=utf-8');

        url_connect('http://www.ferbit.net/mobilno/Mobile/12/webresources/mail/sendMailNew/',l_content, 'POST', pOut, '',  'application/json;charset=utf-8');

       -- DBMS_OUTPUT.PUT_LINE(pout);  
        --DBMS_OUTPUT.PUT_LINE('----------------');  
        response := json(pout);
        if(json_ext.get_number(response,'status')=0) then 
            update order_mail_content set l_status=1, SENT=SYSDATE where id=m.id;
        end if;
    end loop;
end;
/


BEGIN
    DBMS_SCHEDULER.CREATE_JOB (
            job_name => '"SEND_MAIL_STATUS_JOB"',
            job_type => 'PLSQL_BLOCK',
            job_action => 'declare
            begin
                send_mail_order_status;
            end;',
            number_of_arguments => 0,
            start_date => sysdate,
            repeat_interval => 'FREQ=MINUTELY;INTERVAL=3',
            end_date => NULL,
            enabled => FALSE,
            auto_drop => FALSE,
            comments => '');

    DBMS_SCHEDULER.SET_ATTRIBUTE( 
             name => '"SEND_MAIL_STATUS_JOB"', 
             attribute => 'logging_level', value => DBMS_SCHEDULER.LOGGING_OFF);
    
    DBMS_SCHEDULER.enable(
             name => '"SEND_MAIL_STATUS_JOB"');
END;
