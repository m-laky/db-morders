grant execute on utl_http to MOBILNO;
grant execute on utl_file to mobilno

BEGIN
  DBMS_NETWORK_ACL_ADMIN.create_acl (
    acl          => 'ferbit_mobilno.xml',
    description  => 'A test of the ACL functionality',
    principal    => 'MOBILNO',
    is_grant     => TRUE,
    privilege    => 'connect',
    start_date   => null,
    end_date     => NULL);
 
  COMMIT;
END;
/
 
BEGIN
  DBMS_NETWORK_ACL_ADMIN.add_privilege (
    acl         => 'ferbit_mobilno.xml',
    principal   => 'MOBILNO',
    is_grant    => true,
    privilege   => 'connect',
    position    => NULL,
    start_date  => sysdate,
    end_date    => NULL);
 
  COMMIT;
END;
/
 
BEGIN
  DBMS_NETWORK_ACL_ADMIN.assign_acl (
    acl         => 'ferbit_mobilno.xml',
    host        => '*.ferbit.net');
    commit;
   
END;
/