
CREATE or replace PROCEDURE FIND_TABLE_WITH(v_owner  IN VARCHAR2,  v_data_type in VARCHAR2,v_search_string in VARCHAR2) AS 
 
  match_count number; 

BEGIN
  FOR t IN (SELECT table_name, column_name FROM all_tab_cols where owner=v_owner and data_type = v_data_type) LOOP
--dbms_output.put_line('SELECT COUNT(*) FROM '||v_owner||t.table_name||' WHERE '||t.column_name||' = :1' );
    EXECUTE IMMEDIATE 
    'SELECT COUNT(*) FROM '||v_owner||'.'||t.table_name||' WHERE '||t.column_name||' = :1'
    
    INTO match_count
    USING v_search_string;

    IF match_count > 0 THEN
      dbms_output.put_line( t.table_name ||' '||t.column_name||' '||match_count );
    END IF;

  END LOOP;
END;


 

