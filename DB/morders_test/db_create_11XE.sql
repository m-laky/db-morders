--------------------------------------------------------
--  DDL for Sequence FIREBASE_NOTIF_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  FIREBASE_NOTIF_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1926 NOCACHE  NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;

--------------------------------------------------------
--  DDL for Table FIREBASE_NOTIFICATIONS
--------------------------------------------------------

  CREATE TABLE FIREBASE_NOTIFICATIONS 
   (	ID NUMBER, 
	F_TYPE NUMBER, 
	USER_ID NUMBER, 
	F_STATUS NUMBER, 
	MESSAGE_ID VARCHAR2(150 BYTE), 
	DEVICE_ID VARCHAR2(2000 BYTE), 
	ORDER_ID NUMBER, 
	COMPANY_ID NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Trigger FIREBASE_NOTIF_TGR
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER FIREBASE_NOTIF_TGR 
   before insert on FIREBASE_NOTIFICATIONS 
   for each row 
begin  
   if inserting then 
      if :NEW.ID is null then 
         select FIREBASE_NOTIF_SEQ.nextval into :NEW.ID from dual; 
      end if; 
   end if; 
end;

/
ALTER TRIGGER FIREBASE_NOTIF_TGR ENABLE;

/*
    ORDERS CREATE STATMENTS
*/
  CREATE SEQUENCE  ORD_SEQ  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 13 NOCACHE  NOORDER  NOCYCLE ;

  CREATE TABLE ORDERS 
   (	ORDER_ID NUMBER DEFAULT 0 NOT NULL ENABLE, 
	WORKER_ID NUMBER DEFAULT 0, 
	ORDER_DATE DATE, 
	DELIVERY_DATE DATE, 
	COMPLETE_DATE DATE, 
	OTYPE NUMBER DEFAULT 0, 
	TYPE_D VARCHAR2(150 BYTE) DEFAULT 0, 
	CNT NUMBER DEFAULT 0, 
	VEHICLE_ID NUMBER DEFAULT 0, 
	VEHICLE VARCHAR2(20 BYTE) DEFAULT 0, 
	TERMINAL CHAR(1000 BYTE) DEFAULT 0, 
	SEQ NUMBER DEFAULT 0, 
	COMPANY NUMBER DEFAULT 0, 
	BUSSINES_UNIT NUMBER DEFAULT 0, 
	CUSTOMER NUMBER DEFAULT 0, 
	LOC VARCHAR2(40 BYTE) DEFAULT 0, 
	DESC_CODE VARCHAR2(30 BYTE) DEFAULT 0, 
	ACTIVE NUMBER DEFAULT 0, 
	SCANED_COUNT NUMBER DEFAULT 0, 
	ENDED NUMBER DEFAULT 0, 
	TRANSACTION_ID NUMBER DEFAULT 0, 
	SIGNATOR VARCHAR2(150 BYTE) DEFAULT 0, 
	SENDER VARCHAR2(100 BYTE) DEFAULT 0, 
	RECIVER VARCHAR2(100 BYTE) DEFAULT 0, 
	ROUTE NUMBER DEFAULT 0, 
	SCANED DATE, 
	OPROGRAM VARCHAR2(30 BYTE) DEFAULT 'MP', 
	NOTE1 VARCHAR2(200 BYTE) DEFAULT '-', 
	CART VARCHAR2(50 BYTE) DEFAULT 0, 
	WEIGHT NUMBER, 
	HEIGHT NUMBER, 
	WIDTH NUMBER, 
	ODEPTH NUMBER, 
	PRO_ID NUMBER, 
	STITLE VARCHAR2(200 BYTE) DEFAULT 0, 
	SADDRESS VARCHAR2(200 BYTE) DEFAULT 0, 
	SPOSTOFFICENUMBER VARCHAR2(150 BYTE) DEFAULT 0, 
	SCITY VARCHAR2(200 BYTE) DEFAULT 0, 
	RTITLE VARCHAR2(200 BYTE) DEFAULT 0, 
	RADDRESS VARCHAR2(200 BYTE) DEFAULT 0, 
	RPOSTOFFICENUMBER VARCHAR2(150 BYTE) DEFAULT 0, 
	RCITY VARCHAR2(200 BYTE) DEFAULT 0, 
	CMR VARCHAR2(150 BYTE) DEFAULT 0, 
	NOTE2 VARCHAR2(2000 BYTE) DEFAULT 0, 
	NOTE3 VARCHAR2(2000 BYTE) DEFAULT 0, 
	TEMPERATURE_1 NUMBER, 
	TEMPERATURE_2 NUMBER, 
	NR_PALETTES NUMBER, 
	NOTE4 VARCHAR2(1000 BYTE), 
	NOTE5 VARCHAR2(1000 BYTE), 
	NOTE6 VARCHAR2(1000 BYTE), 
	S_TELEPHONE VARCHAR2(50 BYTE), 
	R_TELEPHONE VARCHAR2(50 BYTE), 
	LOADING_LAT NUMBER, 
	LOADING_LNG NUMBER, 
	UNLOADING_LAT NUMBER, 
	UNLOADING_LNG NUMBER, 
	TRAILER VARCHAR2(1000 BYTE), 
	TRAILER_ID NUMBER, 
	RABAT1 NUMBER, 
	RABAT2 NUMBER, 
	DATE_MODIFIED DATE
   ) ;

  CREATE TRIGGER TBE_ORDERS 
BEFORE INSERT OR UPDATE OR DELETE
ON ORDERS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
L_VEZ                NUMBER   := 0; 
l_error              NUMBER   := 0;
l_avez               NUMBER   := 0;                                 
l_procedure          CHAR(50) := $$PLSQL_UNIT;                       --- ali obracun obstaja
l_napak              NUMBER   := 0;
Sql_error            varchar2 (250);                                 --- opis napake
sql_stmt             VARCHAR2 (3900);
p_transakcija        NUMBER   :=0;  
l_pro_id             NUMBER   := 0;
l_stevilka           NUMBER   := 0;
l_faza               NUMBER   := 0;
lfvo_Sifra           NUMBER   := 0;   
l_vozilo             varchar2(150) :='';
l_voznik             varchar2(150) :='';
BEGIN
    --INSERT INTO LOG (  OPIS) VALUES (Trim(l_procedure) );  
 If inserting or updating then
   if(nvl(:new.order_id,0)=0 or :new.order_id=0) then
    select ord_seq.nextval into :new.order_id from dual;
   end if ;
 End if;  

end;
/
ALTER TRIGGER TBE_ORDERS ENABLE;


/*
    SHIPMENTS CREATE STATMENTS
*/

  CREATE TABLE SHIPMENTS 
   (	ID NUMBER NOT NULL ENABLE, 
	BARCODE VARCHAR2(25 BYTE), 
	ORDER_ID NUMBER, 
	CNT NUMBER(22,5) DEFAULT 0, 
	SCANED_COUNT NUMBER DEFAULT 0, 
	SCANED NUMBER DEFAULT 0, 
	SCANED_DATE DATE, 
	WEREHOUSE CHAR(150 BYTE) DEFAULT 0, 
	LOC CHAR(60 BYTE) DEFAULT 0, 
	SOPTION NUMBER DEFAULT 0, 
	COMPLETED NUMBER DEFAULT 00, 
	SEQ NUMBER DEFAULT 0, 
	TITLE VARCHAR2(150 BYTE) DEFAULT 0, 
	CLIENT_ID NUMBER(*,0) DEFAULT 0, 
	PALETTE VARCHAR2(25 BYTE) DEFAULT 0, 
	STYPE CHAR(10 BYTE) DEFAULT 0, 
	PRODUCT NUMBER DEFAULT 0, 
	TERMINAL CHAR(10 BYTE) DEFAULT 1, 
	DAMAGE_ID NUMBER DEFAULT 0, 
	DAMAGE_DESC VARCHAR2(100 BYTE) DEFAULT 0, 
	LOADING_X NUMBER(22,10) DEFAULT 0, 
	LOADING_Y NUMBER(22,10) DEFAULT 0, 
	UNLOADING_X NUMBER(22,10) DEFAULT 0, 
	UNLOADING_Y NUMBER(22,10) DEFAULT 0, 
	ACTIVE NUMBER DEFAULT 1, 
	QUANTITY_O NUMBER DEFAULT 0, 
	LOT VARCHAR2(18 BYTE) DEFAULT 0, 
	NOTE VARCHAR2(500 BYTE) DEFAULT 0, 
	NOTE1 VARCHAR2(500 BYTE) DEFAULT 0, 
	NOTE2 VARCHAR2(500 BYTE) DEFAULT 0, 
	NOTE3 VARCHAR2(500 BYTE) DEFAULT 0, 
	NR_PACKAGE NUMBER DEFAULT 0, 
	NR_PALETS NUMBER DEFAULT 0, 
	TRANSACTION_ID NUMBER DEFAULT 0, 
	QUANTITY_R NUMBER DEFAULT 0, 
	CART NUMBER DEFAULT 0, 
	ROUTE NUMBER DEFAULT 0, 
	PRICE NUMBER DEFAULT 0, 
	WEIGHT NUMBER DEFAULT 0, 
	HEIGHT NUMBER DEFAULT 0, 
	WIDTH NUMBER DEFAULT 0, 
	SDEPTH NUMBER DEFAULT 0, 
	PRO_ID NUMBER, 
	DOC_ID NUMBER, 
	RABAT1 NUMBER, 
	RABAT2 NUMBER
   );

CREATE TRIGGER TBE_SHIPMENTS 
BEFORE INSERT ON SHIPMENTS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
v_ID        NUMBER:=0;

BEGIN
  --insert into prometna@star(voz) values(trim(:new.title));
  NULL;
END ;
 
/
ALTER TRIGGER TBE_SHIPMENTS ENABLE;

/*
    ARTICLES CREATE STATMENTS
*/

CREATE TABLE ARTICLES 
   (	ID NUMBER DEFAULT 1 NOT NULL ENABLE, 
	TITLE VARCHAR2(1000 BYTE) DEFAULT 'A', 
	TYPE VARCHAR2(200 BYTE) DEFAULT 1, 
	BARCODE VARCHAR2(50 BYTE) DEFAULT 'A', 
	TYPEART CHAR(100 BYTE) DEFAULT 'A', 
	LAST DATE, 
	COMPANY NUMBER, 
	NOTE1 VARCHAR2(2000 BYTE), 
	NOTE2 VARCHAR2(2000 BYTE), 
	NOTE3 VARCHAR2(2000 BYTE), 
	NOTE4 VARCHAR2(2000 BYTE), 
	NOTE5 VARCHAR2(2000 BYTE), 
	PRICE NUMBER
   )   ;

CREATE  TRIGGER ART_UPDATE 
before insert or update 
on articles
for each row
begin
  :new.last:=sysdate+1;
null;
end;
/
ALTER TRIGGER ART_UPDATE ENABLE;

Insert into ARTICLES (ID,TITLE,TYPE,BARCODE,TYPEART,LAST,COMPANY,NOTE1,NOTE2,NOTE3,NOTE4,NOTE5,PRICE) values ('55','Potni nalog','DT','DT0055','0',to_date('11.04.21 10:42:51','DD.MM.RR HH24:MI:SS'),'0','-','-','-','-','-','0');
Insert into ARTICLES (ID,TITLE,TYPE,BARCODE,TYPEART,LAST,COMPANY,NOTE1,NOTE2,NOTE3,NOTE4,NOTE5,PRICE) values ('70','Kakovost','DT','DT0070','0',to_date('11.04.21 10:42:51','DD.MM.RR HH24:MI:SS'),'0','-','-','-','-','-','0');
Insert into ARTICLES (ID,TITLE,TYPE,BARCODE,TYPEART,LAST,COMPANY,NOTE1,NOTE2,NOTE3,NOTE4,NOTE5,PRICE) values ('60','Vagoni','DT','DT0060','0',to_date('11.04.21 10:42:51','DD.MM.RR HH24:MI:SS'),'0','-','-','-','-','-','0');


/*
    ATTACHMENTS CREATE STATMENTS
*/

  CREATE TABLE ATTACHMENTS 
   (	
    ID NUMBER, 
	ORDER_ID NUMBER, 
	ATYPE NUMBER, 
	ATTACHMENT_DATE DATE, 
	DOC_ID NUMBER, 
	STATUS NUMBER DEFAULT 0, 
	ORDER_TYPE VARCHAR2(20 BYTE) DEFAULT 0, 
	LOT VARCHAR2(50 BYTE), 
	LINKID VARCHAR2(400 BYTE), 
	SENDED NUMBER DEFAULT 0, 
	WORKER_ID NUMBER, 
	IMAGE_COMMENT VARCHAR2(400 BYTE), 
	EMAIL VARCHAR2(150 BYTE) DEFAULT '-', 
	FILE_NAME VARCHAR2(4000 BYTE), 
	SHIPMENT_ID NUMBER
   ) ;
/*
    USERS CREATE STATEMENTS
*/

  CREATE TABLE USERS
   (	ID NUMBER NOT NULL ENABLE, 
	NAME VARCHAR2(50 BYTE), 
	LAST_NAME VARCHAR2(150 BYTE), 
	USERNAME VARCHAR2(10 BYTE), 
	PASSWORD VARCHAR2(300 BYTE), 
	PIN VARCHAR2(20 BYTE), 
	LONGITUDE NUMBER DEFAULT 0, 
	LATITUDE NUMBER DEFAULT 0, 
	DEVICE VARCHAR2(3000 BYTE), 
	LAST_TIME DATE, 
	ADMIN NUMBER DEFAULT 0, 
	STATUS NUMBER DEFAULT 0, 
	COMPANY NUMBER
   ) ;
 
Insert into USERS (ID,NAME,LAST_NAME,USERNAME,PASSWORD,PIN,LONGITUDE,LATITUDE,DEVICE,LAST_TIME,ADMIN,STATUS,COMPANY) values ('999','Matej','Lakota','matejl','matejl','1234','0','0','-',null,'1','1',null);
/*
    DEVICES CREATE STATEMENTS
*/
  CREATE TABLE DEVICES
   (	ID NUMBER, 
	DEVICE_NAME VARCHAR2(60 BYTE), 
	DEVICE_ID VARCHAR2(3000 BYTE) DEFAULT 0, 
	X NUMBER, 
	Y NUMBER, 
	Z NUMBER, 
	LAST_TIME DATE DEFAULT SYSDATE, 
	LICENCE_PLATE VARCHAR2(50 BYTE) DEFAULT 0, 
	LINK_STR VARCHAR2(500 BYTE), 
	IMEI VARCHAR2(1000 BYTE), 
	USER_ID NUMBER, 
	D_TYPE NUMBER, 
	COMPANY_ID NUMBER
   ) ;
/*
    METHODS FOR CONNECTION TO PRIMARY SERVER
*/
create function  base64DecodeClobAsBlob_plsql(i_data_cl CLOB) 
return blob is
  v_out_bl blob;
  clob_size number;
  pos number;
  charBuff varchar2(32767);
  dBuffer RAW(32767);
  v_readSize_nr number;
  v_line_nr number;
begin
  dbms_lob.createTemporary
    (v_out_bl, true, dbms_lob.call);
  v_line_nr:=greatest(65, 
                 instr(i_data_cl,chr(10)),
                 instr(i_data_cl,chr(13)));
  v_readSize_nr:=
      floor(32767/v_line_nr)*v_line_nr;
  clob_size := dbms_lob.getLength(i_data_cl);
  pos := 1;

  WHILE (pos < clob_size) LOOP
    dbms_lob.read
      (i_data_cl, v_readSize_nr, pos, charBuff);
    dBuffer := UTL_ENCODE.base64_decode
      (utl_raw.cast_to_raw(charBuff));
    dbms_lob.writeAppend
     (v_out_bl,utl_raw.length(dBuffer),dBuffer);
    pos := pos + v_readSize_nr;
  end loop;
  return v_out_bl;
end;
/
create procedure  write_to_file(
dir        in varchar2,
ime        in char,
documentIn        clob )
as
    t_data         BLOB;
    v_buffer       RAW(32767);
    v_buffer_size  BINARY_INTEGER;
    v_amount       BINARY_INTEGER;
    v_offset       NUMBER(38) := 1;
    v_chunksize    INTEGER;
    v_out_file     UTL_FILE.FILE_TYPE;  
    dest_offset INTEGER := 1;
    src_offset INTEGER := 1;
    warning INTEGER;
    ctx INTEGER := DBMS_LOB.DEFAULT_LANG_CTX;
begin

    if nvl((DBMS_LOB.getlength(documentIn ))/1024/1024,0) > 0 then
  --  dbms_output.put_line('zacetek convert '|| to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));
     t_Data := base64decodeclobasblob_plsql(documentIn);
 --dbms_output.put_line('konec convert '|| to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));


           v_chunksize := DBMS_LOB.GETCHUNKSIZE(t_Data);
            IF (v_chunksize < 32767) THEN
                v_buffer_size := v_chunksize;
            ELSE
                v_buffer_size := 32767;
            END IF;
            v_amount := v_buffer_size;
            DBMS_LOB.OPEN(t_data, DBMS_LOB.LOB_READONLY);
            v_out_file := UTL_FILE.FOPEN(
                location      => dir, 
                filename      => ime, 
                open_mode     => 'wb',
                max_linesize  => 32767);
            WHILE v_amount >= v_buffer_size
            LOOP
              DBMS_LOB.READ(
                  lob_loc    => t_Data,
                  amount     => v_amount,
                  offset     => v_offset,
                  buffer     => v_buffer);
              v_offset := v_offset + v_amount;
              UTL_FILE.PUT_RAW (
                  file      => v_out_file,
                  buffer    => v_buffer,
                  autoflush => true);
              UTL_FILE.FFLUSH(file => v_out_file);
            END LOOP;
            UTL_FILE.FFLUSH(file => v_out_file);
            UTL_FILE.FCLOSE(v_out_file);
            DBMS_LOB.CLOSE(t_data);
         end if; 

end;
/
create procedure url_connect(pUrl in varchar2,pContent IN CLOB, pMethod IN VARCHAR2, pOut OUT CLOB, auth in varchar2 default '', contentType in varchar2 default 'application/json;charset=utf-8' )IS 
/*
      mmessage_id   -  id sporočila
      muser_id      -  za koga je sprocilo; lahko je id uporabnika ali vozila(odvisno kak mMessageType pošiljamo)
      mtype         -  tip sporocila; 0 sinhronizacija; 1 sporočilo
      mcontent      -  vsebina
      mfromuser     -  od koga je sporocilo 1-centrala
      msendeddate   -  datum posiljanja
      mMessageType  -  če gre za nalog 0; če je sporčilo 1
      */
  t_http_req     utl_http.req; 
  t_http_resp    utl_http.resp;  
  t_respond      VARCHAR2(30000); 
  t_start_pos    INTEGER := 1; 
  t_output       VARCHAR2(2000); 
  l_mcontent     varchar2(10000);
  t_device       VARCHAR2(2000); 
  t_data         VARCHAR(30000); 
  sql_error      VARCHAR2(30000); 
  l_error        number;
  l_napak        number; 
  l_procedure    CHAR(15):='SEND_MESSAGE';
  buffer varchar2(4000); 
  response_Text  varchar2(32000);
   l_buffer     varchar2(32767);
   l_length      number:=0;
   l_offset number:=1; 
   amount number :=32767;
BEGIN     
    l_length := DBMS_LOB.GETLENGTH(pContent);

    --utl_http.set_wallet ('file:/home/oracle/wallet', 'password123');
    utl_http.Set_body_charset('UTF-8'); 
    utl_http.Set_transfer_timeout(300); 
    t_http_req := utl_http.Begin_request(pUrl , pMethod); 
    utl_http.Set_header(t_http_req, 'Content-Type',contentType); 
    utl_http.Set_header(t_http_req, 'Connection', 'keep-alive');  

    if(nvl(length(trim(auth)),0)<>0) then
        utl_http.Set_header(t_http_req, 'Authorization', auth);  
    end if;


    if(trim(pMethod)='POST') then
        if(l_length>0) then


            utl_http.Set_header(t_http_req, 'Content-Length',l_length); 
            --utl_http.Write_text(t_http_req, pContent);


            while(l_offset < l_length) 
                          loop
                             dbms_lob.read(pContent, amount, l_offset, l_buffer);
                             UTL_HTTP.WRITE_TEXT(r    => t_http_req, data => l_buffer);
                             l_offset := l_offset + amount;

                          end loop;

        else
            utl_http.Set_header(t_http_req, 'Content-Length',0); 
        end if;
    else
        utl_http.Set_header(t_http_req, 'Content-Length',0); 
    end if ;

    t_http_resp := utl_http.Get_response(t_http_req); 
    dbms_lob.createtemporary(pOut, false);

     begin
        loop
          utl_http.read_text(t_http_resp, l_buffer, 32000);
          dbms_lob.writeappend (pOut, length(l_buffer), l_buffer);
        end loop;
      exception
        when utl_http.end_of_body then
          utl_http.end_response(t_http_resp);
      end; 

                  --          DBMS_OUTPUT.PUT_LINE(pout);
  /*  EXCEPTION 
    WHEN OTHERS THEN 
         l_error   := SQLCODE;
        l_napak   := 1;
         DBMS_OUTPUT.PUT_LINE('SQLERRM urlConnect: '||SQLERRM); 
        Sql_error := SUBSTR(SQLERRM, 1, 250);
        ERROR_LOG(l_error,'getCVSVehicle',Sql_error,0,l_procedure,0);*/
END;
/
create procedure morders_auth(taxNumber in varchar2, title in varchar2)  IS
    pOut clob;
    l_response  json;   
     mContent clob;
    syncObject json;  
    cnt number:=0;
BEGIN 

    syncObject := json();
    syncObject.put('barcode',taxNumber);
    syncObject.put('deviceToken',title);

    DBMS_LOB.CREATETEMPORARY(mContent,true); 
    syncObject.to_clob(mContent,true); 

   url_connect('http://www.ferbit.net/mobilno/Mobile/webresources/mOrders/API/auth',mContent,'POST',pOut,'','application/json;charset=utf-8');

    l_response := json(pout); 


    select count( tekst) into cnt from nastavi where kljuc='morders_token'; 
    if(cnt=0) then
        insert into nastavi(kljuc,tekst) values('morders_token',json_ext.get_string(l_response,'deviceToken')); 
    else
        update nastavi set tekst = json_ext.get_string(l_response,'deviceToken') where kljuc='morders_token';
    end if;
end;
/
create procedure morders_insert_articles is 
    pOut clob;
    l_response  json; 
    token varchar2(1000):='';
    TYPE VRNI_rt IS RECORD(document_id varchar2(10000));
    l_VRNI   VRNI_rt;
    b_kurzor        sys_refcursor;
    mArticeles json_list;
    mArticle json;
    syncObject json;
    tmp json;
    CURSOR C_ARTICLES IS SELECT ID,TITLE,TYPE,BARCODE,TYPEART,LAST,COMPANY,NOTE1,NOTE2,NOTE3,NOTE4,NOTE5,PRICE FROM ARTICLES;
    mContent clob;
BEGIN  

        select trim(tekst) into token from nastavi where kljuc='morders_token'; 

        mArticeles:=json_list();
        FOR article IN C_ARTICLES LOOP
            mArticle := json();
            mArticle.put('id', article.id);
            mArticle.put('title', nvl(trim(article.title),''));
            mArticle.put('barcode', nvl(trim(article.barcode),''));
            mArticle.put('type', nvl(trim(article.type),''));
            mArticle.put('typeart', nvl(trim(article.typeart),''));
            mArticle.put('last', nvl(to_char(article.last,'yyyy-mm-dd hh24:mi:ss'),'1990-01-01 00:00:00'));
            mArticle.put('company', 0);
            mArticle.put('price', nvl(article.price,0));
            mArticle.put('note1', nvl(trim(article.note1),''));
            mArticle.put('note2', nvl(trim(article.note2),''));
            mArticle.put('note3', nvl(trim(article.note3),''));
            mArticle.put('note4', nvl(trim(article.note4),''));
            mArticle.put('note5', nvl(trim(article.note5),''));
            mArticeles.append(mArticle.to_json_value); 
        end loop;
         syncObject := json();
         syncObject.put('articles',mArticeles);

         DBMS_LOB.CREATETEMPORARY(mContent,true); 
         syncObject.to_clob(mContent,true);
      --   dbms_output.put_line('----'); 
         url_connect('http://www.ferbit.net/mobilno/Mobile/webresources/mOrders/API/insertArticles',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
       --  DBMS_OUTPUT.PUT(pOut);
end;
/
create procedure morders_insert_orders(orderId IN number)  IS
    pOut clob;
    l_response  json; 
    token varchar2(1000);
    mOrders json_list;
    mOrder json;

    mShipments json_list;
    mShipment json;
    ry_shipment  SHIPMENTS%ROWTYPE;
    shipmentsCur        sys_refcursor;    
    queryShipments varchar2(30000):='';

    syncObject json;
    CURSOR C_ORDERS IS SELECT * FROM orders WHERE active=1;-- and order_id=orderId;
    mContent clob;

    inserted json_list;
BEGIN 

    select trim(tekst) into token from nastavi where kljuc='morders_token'; 

    mOrders := json_list();
    for corder in c_orders
    loop
        mOrder := json();
        morder.put('orderId',corder.order_id); 
        mOrder.put('workerId',nvl(corder.worker_id,0));
        mOrder.put('orderDate',to_char(nvl(corder.order_date,sysdate),'yyyy-mm-dd hh24:mi:ss'));
        mOrder.put('deliveryDate',to_char(nvl(corder.delivery_date,sysdate),'yyyy-mm-dd hh24:mi:ss'));
        mOrder.put('completeDate',to_char(nvl(corder.complete_date,sysdate),'yyyy-mm-dd hh24:mi:ss'));
        mOrder.put('type',nvl(corder.otype,0));
        mOrder.put('typeD',corder.type_d);
        mOrder.put('count',nvl(corder.cnt,0));
        mOrder.put('vehicleId',nvl(corder.vehicle_id,0));
        mOrder.put('vehicle',corder.vehicle);
        mOrder.put('terminal',corder.terminal);
        mOrder.put('seq',nvl(corder.seq,0));
        mOrder.put('company',nvl(corder.company,0));
        mOrder.put('bussinesUnit',nvl(corder.bussines_unit,0));
        mOrder.put('customer',corder.customer);
        mOrder.put('location',corder.loc);
        mOrder.put('descCode',corder.desc_code);
        mOrder.put('active',nvl(corder.active,0));
        mOrder.put('scanedCount',nvl(corder.scaned_count,0));
        mOrder.put('ended',nvl(corder.ended,0));
        mOrder.put('transactionId',nvl(corder.transaction_id,0));
        mOrder.put('signator',corder.signator);
        mOrder.put('sender',corder.sender);
        mOrder.put('reciver',corder.reciver);
        mOrder.put('route',nvl(corder.route,0));
        mOrder.put('scaned',corder.scaned);--datum
        mOrder.put('program',corder.oprogram);
        mOrder.put('note1',corder.note1);
        mOrder.put('cart',corder.cart);
        mOrder.put('weight',nvl(corder.weight,0));
        mOrder.put('height',nvl(corder.height,0));
        mOrder.put('width',nvl(corder.width,0));
        mOrder.put('depth',nvl(corder.odepth,0));
        mOrder.put('proId',nvl(corder.pro_id,0));
        mOrder.put('stitle',corder.stitle);
        mOrder.put('saddress',corder.saddress);
        mOrder.put('spostofficenumber',corder.spostofficenumber);
        mOrder.put('scity',corder.scity);
        mOrder.put('rtitle',corder.rtitle);
        mOrder.put('raddress',corder.raddress);
        mOrder.put('rpostofficenumber',corder.rpostofficenumber);
        mOrder.put('rcity',corder.rcity);
        mOrder.put('cmr',corder.cmr);
        mOrder.put('note2',corder.note2);
        mOrder.put('note3',corder.note3);
        mOrder.put('temperature1',nvl(corder.temperature_1,0));
        mOrder.put('temperature2',nvl(corder.temperature_2,0));
        mOrder.put('nrPalettes',nvl(corder.nr_palettes,0));
        mOrder.put('note4',corder.note4);
        mOrder.put('note5',corder.note5);
        mOrder.put('note6',corder.note6);
        mOrder.put('note6',corder.note6);
        mOrder.put('s_telephone',corder.s_telephone);
        mOrder.put('r_telephone',corder.r_telephone);
        mOrder.put('loading_lat',nvl(corder.loading_lat,0));
        mOrder.put('loading_lng',nvl(corder.loading_lng,0));
        mOrder.put('unloading_lat',nvl(corder.unloading_lat,0));
        mOrder.put('unloading_lng',nvl(corder.unloading_lng,0));
        mOrder.put('trailer',corder.trailer);
        mOrder.put('trailer_id',nvl(corder.trailer_id,0));
        mOrder.put('rabat1',nvl(corder.rabat1,0));
        mOrder.put('rabat2',nvl(corder.rabat2,0)); 
        mOrder.put('orderLogs',json_list());
        mShipments:=json_list();

        queryShipments := 'select * from shipments where order_id='||json_ext.get_number(mOrder,'orderId');
        OPEN shipmentsCur FOR queryShipments;
                 LOOP
                    FETCH shipmentsCur INTO ry_shipment;
                    EXIT WHEN shipmentsCur%NOTFOUND;  
                    DBMS_OUTPUT.PUT_LINE('adding ' || ry_shipment.id);
                        mShipment := json();
                        mShipment.put('id',ry_shipment.id);
                        mShipment.put('barcode',ry_shipment.barcode);
                        mShipment.put('orderId',ry_shipment.order_id);
                        mShipment.put('cnt',nvl(ry_shipment.cnt,0));
                        mShipment.put('scanedCount',nvl(ry_shipment.scaned_count,0));
                        mShipment.put('scaned',nvl(ry_shipment.scaned,0));
                        mShipment.put('scanedDate',to_char(ry_shipment.scaned_date,'yyyy-mm-dd hh24:mi:ss'));
                        mShipment.put('werehouse',ry_shipment.werehouse);
                        mShipment.put('loc',ry_shipment.loc);
                        mShipment.put('soption',nvl(ry_shipment.soption,0));
                        mShipment.put('completed',nvl(ry_shipment.completed,0));
                        mShipment.put('seq',nvl(ry_shipment.seq,0));
                        mShipment.put('title',ry_shipment.title);
                        mShipment.put('clientId',nvl(ry_shipment.client_id,0));
                        mShipment.put('palette',ry_shipment.palette);
                        mShipment.put('stype',ry_shipment.stype);
                        mShipment.put('product',nvl(ry_shipment.product,0));
                        mShipment.put('terminal',ry_shipment.terminal);
                        mShipment.put('damageId',nvl(ry_shipment.damage_id,0));
                        mShipment.put('damageDesc',ry_shipment.damage_desc);
                        mShipment.put('loadingX',nvl(ry_shipment.loading_x,0));
                        mShipment.put('loadingY',nvl(ry_shipment.loading_y,0));
                        mShipment.put('unloadingX',nvl(ry_shipment.unloading_x,0));
                        mShipment.put('unloadingY',nvl(ry_shipment.unloading_y,0));
                        mShipment.put('active',nvl(ry_shipment.active,0));
                        mShipment.put('quantityO',nvl(ry_shipment.quantity_o,0));
                        mShipment.put('lot',ry_shipment.lot);
                        mShipment.put('note',ry_shipment.note);
                        mShipment.put('note1',ry_shipment.note1);
                        mShipment.put('note2',ry_shipment.note2);
                        mShipment.put('note3',ry_shipment.note3);
                        mShipment.put('nrPackage',nvl(ry_shipment.nr_package,0));
                        mShipment.put('nrPalets',nvl(ry_shipment.nr_palets,0));
                        mShipment.put('transactionId',nvl(ry_shipment.transaction_id,0));
                        mShipment.put('quantityR',nvl(ry_shipment.quantity_r,0));
                        mShipment.put('cart',nvl(ry_shipment.cart,0));
                        mShipment.put('route',nvl(ry_shipment.route,0));
                        mShipment.put('price',nvl(ry_shipment.price,0));
                        mShipment.put('weight',nvl(ry_shipment.weight,0));
                        mShipment.put('height',nvl(ry_shipment.height,0));
                        mShipment.put('width',nvl(ry_shipment.width,0));
                        mShipment.put('sdepth',nvl(ry_shipment.sdepth,0));
                        mShipment.put('proId',nvl(ry_shipment.pro_id,0));
                        mShipment.put('docId',nvl(ry_shipment.doc_id,0));
                        mShipment.put('rabat1',nvl(ry_shipment.rabat1,0));
                        mShipment.put('rabat2',nvl(ry_shipment.rabat2,0)); 
                        mShipments.append(mShipment.to_json_value);
                END LOOP;  
        close shipmentsCur; 

        mOrder.put('shipments', mShipments);
        mOrders.append(mOrder.to_json_value);
    end loop;

    syncObject := json();
    syncObject.put('orders',mOrders);

    dbms_output.put_line(syncObject.to_char); 
    DBMS_LOB.CREATETEMPORARY(mContent,true); 
    syncObject.to_clob(mContent,true); 

   url_connect('http://www.ferbit.net/mobilno/Mobile/webresources/mOrders/API/insertOrders',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');

    l_response := json(pout);
    inserted := json_ext.get_json_list(l_response,'inserted');

    FOR i IN 1..inserted.count
        LOOP
            dbms_output.put_line('-- '||json_ext.get_number(json(inserted.get(i)),'newOrderId')); 
           update orders set active=2 where order_id=json_ext.get_number(json(inserted.get(i)),'newOrderId');
        end loop;
        commit;
end;
/
create procedure morders_insert_users(mUserId in number) is 
    pOut clob;
    l_response  json; 
    token varchar2(1000):='';
    TYPE VRNI_rt IS RECORD(document_id varchar2(10000));
    l_VRNI   VRNI_rt;
    b_kurzor        sys_refcursor; 
    mUser json;
    syncObject json;
    tmp json;
    CURSOR C_USER IS SELECT * FROM USERS where id=mUserId;
    mContent clob;
BEGIN  

        select trim(tekst) into token from nastavi where kljuc='morders_token'; 
--dbms_crypto.hash(utl_raw.cast_to_raw(''), dbms_crypto.HASH_MD5);




        mUser:=json();
        FOR luser IN C_USER LOOP 
            muser.put('id',luser.id);
            muser.put('name',luser.name);
            muser.put('lastName',luser.last_name);
            muser.put('username',luser.username);
            muser.put('password',DBMS_OBFUSCATION_TOOLKIT.md5(input => UTL_I18N.STRING_TO_RAW (luser.password, 'AL32UTF8')));
            muser.put('pin',luser.pin);
            muser.put('longitude',nvl(luser.longitude,0));
            muser.put('latitude',nvl(luser.latitude,0));
            muser.put('device',luser.device);
            muser.put('lastTime',luser.last_time);
            muser.put('admin',nvl(luser.admin,0));
            muser.put('status',nvl(luser.status,0));
            muser.put('company',0);
        end loop;
         syncObject := json();
         syncObject.put('user',mUser);

         DBMS_LOB.CREATETEMPORARY(mContent,true); 
         syncObject.to_clob(mContent,true);
         dbms_output.put_line('----'); 
         dbms_output.put_line(mContent); 
         url_connect('http://www.ferbit.net/mobilno/Mobile/webresources/mOrders/API/createUser',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
         DBMS_OUTPUT.PUT(pOut);
end;
/
create procedure morders_get_data(token in VARCHAR2, dateFrom in date, dateTo in date ) is
  mContent varchar2(32000);
    pOut clob;

     l_response       json;
     mOrders json_list;
     mOrder json;
     mShipments json_list;
     mShipment json;
     mAttachments json_list;
     mAttachment json;
     mCount number:=0;
     mCountShip number := 0;
     
BEGIN 
    mContent := '{"fromDate":"'||to_char(dateFrom,'yyyy-mm-dd hh24:mi:ss')||'","toDate":"'||to_char(dateTo,'yyyy-mm-dd hh24:mi:ss')||'"}';
    url_connect('http://www.ferbit.net/mobilno/Mobile/webresources/mOrders/API/getOrders/',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
 --  DBMS_OUTPUT.PUT_LINE(pOut); 
    l_response := json(pOut);
    mOrders := json_ext.get_json_list(l_response,'orders');
     FOR i IN 1..mOrders.count
        LOOP
            null;
             mOrder := json(mOrders.get(i));
             select count(*) into mCount from orders where order_id=json_ext.get_number(mOrder,'orderId');
             if(mCount>0) then
                    update orders set  
                        worker_id=json_ext.get_number(mOrder,'workerId'),--worker_id,
                        order_date=to_date(json_ext.get_string(mOrder,'orderDate'),'yyyy-mm-dd hh24:mi:ss'),--order_date,
                        delivery_date=to_date(json_ext.get_string(mOrder,'deliveryDate'),'yyyy-mm-dd hh24:mi:ss'),--delivery_date,
                        complete_date=to_date(json_ext.get_string(mOrder,'completeDate'),'yyyy-mm-dd hh24:mi:ss'),--complete_date,
                        otype=json_ext.get_number(mOrder,'type'),--otype,
                        type_d=json_ext.get_string(mOrder,'typeD'),--type_d
                        cnt=json_ext.get_number(mOrder,'count'),--cnt,
                        vehicle_id=json_ext.get_number(mOrder,'vehicleId'),--vehicle_id,
                        vehicle=json_ext.get_string(mOrder,'vehicle'),--vehicle,
                        terminal=json_ext.get_string(mOrder,'terminal'),--terminal,
                        seq=json_ext.get_number(mOrder,'seq'),--seq,
                        company=json_ext.get_number(mOrder,'company'),--company,
                        bussines_unit=json_ext.get_number(mOrder,'bussinesUnit'),--bussines_unit,
                        customer=json_ext.get_number(mOrder,'customer'),--customer,
                        loc=json_ext.get_string(mOrder,'location'),--loc,
                        desc_code=json_ext.get_string(mOrder,'descCide'),--desc_code,
                        active=json_ext.get_number(mOrder,'active'),--active,
                        scaned_count=json_ext.get_number(mOrder,'scanedCount'),--scaned_count,
                        ended=json_ext.get_number(mOrder,'ended'),--ended,
                        transaction_id=json_ext.get_number(mOrder,'transactionId'),--transaction_id,
                        signator=json_ext.get_string(mOrder,'signator'),--signator
                        sender=json_ext.get_string(mOrder,'sender'),--sender,
                        reciver=json_ext.get_string(mOrder,'reciver'),--reciver,
                        route=json_ext.get_number(mOrder,'route'),--route,
                        scaned=to_date(json_ext.get_string(mOrder,'scanedDate'),'yyyy-mm-dd hh24:mi:ss'),--scaned,
                        oprogram=json_ext.get_string(mOrder,'program'),--oprogram,
                        note1=json_ext.get_string(mOrder,'note1'),--note1,
                        cart=json_ext.get_string(mOrder,'cart'),--cart,
                        weight=json_ext.get_number(mOrder,'weight'),--weight,
                        height=json_ext.get_number(mOrder,'height'),--height,
                        width=json_ext.get_number(mOrder,'width'),--width,
                        odepth=json_ext.get_number(mOrder,'odepth'),--odepth,
                        pro_id=json_ext.get_number(mOrder,'proId'),--pro_id,
                        stitle=json_ext.get_string(mOrder,'stitle'),--stitle,
                        saddress=json_ext.get_string(mOrder,'saddress'),--saddress,
                        spostofficenumber=json_ext.get_string(mOrder,'spostofficenumber'),--spostofficenumber
                        scity=json_ext.get_string(mOrder,'scity'),--scity,
                        rtitle=json_ext.get_string(mOrder,'rtitle'),--rtitle,
                        raddress=json_ext.get_string(mOrder,'raddress'),--raddress,
                        rpostofficenumber=json_ext.get_string(mOrder,'rpostofficenumber'),--rpostofficenumber
                        rcity=json_ext.get_string(mOrder,'rcity'),--rcity,
                        cmr=json_ext.get_string(mOrder,'cmr'),--cmr
                        note2=json_ext.get_string(mOrder,'note2'),--note2,
                        note3=json_ext.get_string(mOrder,'note3'),--note3,
                        temperature_1=json_ext.get_number(mOrder,'temeprature1'),--temperature_1,
                        temperature_2=json_ext.get_number(mOrder,'temperature2'),--temperature_2,
                        nr_palettes=json_ext.get_number(mOrder,'nrPalettes'),--nr_palettes,
                        note4=json_ext.get_string(mOrder,'note4'),--note4,
                        note5=json_ext.get_string(mOrder,'note5'),--note5,
                        note6=json_ext.get_string(mOrder,'note6'),--note6,
                        s_telephone='',--s_telephone,
                        r_telephone='',--r_telephone,
                        loading_lat=json_ext.get_number(mOrder,'loadingLat'),--loading_lat,
                        loading_lng=json_ext.get_number(mOrder,'loadingLng'),--loading_lng,
                        unloading_lat=json_ext.get_number(mOrder,'unloadingLat'),--unloading_lat,
                        unloading_lng=json_ext.get_number(mOrder,'unloadingLng'),--unloading_lng,
                        trailer=json_ext.get_string(mOrder,'trailer'),--trailer,
                        trailer_id=json_ext.get_number(mOrder,'trailerId'),--trailer_id,
                        rabat1=json_ext.get_number(mOrder,'rabat1'),--rabat1,
                        rabat2=json_ext.get_number(mOrder,'rabat2')--rabat2
                    where
                        order_id=json_ext.get_number(mOrder,'orderId') ;
             else
            insert into orders(
                order_id,--NUMBER
                worker_id,--NUMBER
                order_date,--DATE
                delivery_date,--DATE
                complete_date,--DATE
                otype,--NUMBER
                type_d,--VARCHAR2(150 BYTE)
                cnt,--NUMBER
                vehicle_id,--NUMBER
                vehicle,--VARCHAR2(20 BYTE)
                terminal,--CHAR(1000 BYTE)
                seq,--NUMBER
                company,--NUMBER
                bussines_unit,--NUMBER
                customer,--NUMBER
                loc,--VARCHAR2(40 BYTE)
                desc_code,--VARCHAR2(30 BYTE)
                active,--NUMBER
                scaned_count,--NUMBER
                ended,--NUMBER
                transaction_id,--NUMBER
                signator,--VARCHAR2(150 BYTE)
                sender,--VARCHAR2(100 BYTE)
                reciver,--VARCHAR2(100 BYTE)
                route,--NUMBER
                scaned,--DATE
                oprogram,--VARCHAR2(30 BYTE)
                note1,--VARCHAR2(200 BYTE)
                cart,--VARCHAR2(50 BYTE)
                weight,--NUMBER
                height,--NUMBER
                width,--NUMBER
                odepth,--NUMBER
                pro_id,--NUMBER
                stitle,--VARCHAR2(200 BYTE)
                saddress,--VARCHAR2(200 BYTE)
                spostofficenumber,--VARCHAR2(150 BYTE)
                scity,--VARCHAR2(200 BYTE)
                rtitle,--VARCHAR2(200 BYTE)
                raddress,--VARCHAR2(200 BYTE)
                rpostofficenumber,--VARCHAR2(150 BYTE)
                rcity,--VARCHAR2(200 BYTE)
                cmr,--VARCHAR2(150 BYTE)
                note2,--VARCHAR2(2000 BYTE)
                note3,--VARCHAR2(2000 BYTE)
                temperature_1,--NUMBER
                temperature_2,--NUMBER
                nr_palettes,--NUMBER
                note4,--VARCHAR2(1000 BYTE)
                note5,--VARCHAR2(1000 BYTE)
                note6,--VARCHAR2(1000 BYTE)
                s_telephone,--VARCHAR2(50 BYTE)
                r_telephone,--VARCHAR2(50 BYTE)
                loading_lat,--NUMBER
                loading_lng,--NUMBER
                unloading_lat,--NUMBER
                unloading_lng,--NUMBER
                trailer,--VARCHAR2(1000 BYTE)
                trailer_id,--NUMBER
                rabat1,--NUMBER
                rabat2--NUMBER
            )values(
                json_ext.get_number(mOrder,'orderId'),--order_id,--NUMBER
                json_ext.get_number(mOrder,'workerId'),--worker_id,--NUMBER
                to_date(json_ext.get_string(mOrder,'orderDate'),'yyyy-mm-dd hh24:mi:ss'),--order_date,--DATE
                to_date(json_ext.get_string(mOrder,'deliveryDate'),'yyyy-mm-dd hh24:mi:ss'),--delivery_date,--DATE
                to_date(json_ext.get_string(mOrder,'completeDate'),'yyyy-mm-dd hh24:mi:ss'),--complete_date,--DATE
                json_ext.get_number(mOrder,'type'),--otype,--NUMBER
                json_ext.get_string(mOrder,'typeD'),--type_d,--VARCHAR2(150 BYTE)
                json_ext.get_number(mOrder,'count'),--cnt,--NUMBER
                json_ext.get_number(mOrder,'vehicleId'),--vehicle_id,--NUMBER
                json_ext.get_string(mOrder,'vehicle'),--vehicle,--VARCHAR2(20 BYTE)
                json_ext.get_string(mOrder,'terminal'),--terminal,--CHAR(1000 BYTE)
                json_ext.get_number(mOrder,'seq'),--seq,--NUMBER
                json_ext.get_number(mOrder,'company'),--company,--NUMBER
                json_ext.get_number(mOrder,'bussinesUnit'),--bussines_unit,--NUMBER
                json_ext.get_number(mOrder,'customer'),--customer,--NUMBER
                json_ext.get_string(mOrder,'location'),--loc,--VARCHAR2(40 BYTE)
                json_ext.get_string(mOrder,'descCide'),--desc_code,--VARCHAR2(30 BYTE)
                json_ext.get_number(mOrder,'active'),--active,--NUMBER
                json_ext.get_number(mOrder,'scanedCount'),--scaned_count,--NUMBER
                json_ext.get_number(mOrder,'ended'),--ended,--NUMBER
                json_ext.get_number(mOrder,'transactionId'),--transaction_id,--NUMBER
                json_ext.get_string(mOrder,'signator'),--signator,--VARCHAR2(150 BYTE)
                json_ext.get_string(mOrder,'sender'),--sender,--VARCHAR2(100 BYTE)
                json_ext.get_string(mOrder,'reciver'),--reciver,--VARCHAR2(100 BYTE)
                json_ext.get_number(mOrder,'route'),--route,--NUMBER
                to_date(json_ext.get_string(mOrder,'scanedDate'),'yyyy-mm-dd hh24:mi:ss'),--scaned,--DATE
                json_ext.get_string(mOrder,'program'),--oprogram,--VARCHAR2(30 BYTE)
                json_ext.get_string(mOrder,'note1'),--note1,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'cart'),--cart,--VARCHAR2(50 BYTE)
                json_ext.get_number(mOrder,'weight'),--weight,--NUMBER
                json_ext.get_number(mOrder,'height'),--height,--NUMBER
                json_ext.get_number(mOrder,'width'),--width,--NUMBER
                json_ext.get_number(mOrder,'odepth'),--odepth,--NUMBER
                json_ext.get_number(mOrder,'proId'),--pro_id,--NUMBER
                json_ext.get_string(mOrder,'stitle'),--stitle,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'saddress'),--saddress,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'spostofficenumber'),--spostofficenumber,--VARCHAR2(150 BYTE)
                json_ext.get_string(mOrder,'scity'),--scity,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'rtitle'),--rtitle,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'raddress'),--raddress,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'rpostofficenumber'),--rpostofficenumber,--VARCHAR2(150 BYTE)
                json_ext.get_string(mOrder,'rcity'),--rcity,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'cmr'),--cmr,--VARCHAR2(150 BYTE)
                json_ext.get_string(mOrder,'note2'),--note2,--VARCHAR2(2000 BYTE)
                json_ext.get_string(mOrder,'note3'),--note3,--VARCHAR2(2000 BYTE)
                json_ext.get_number(mOrder,'temeprature1'),--temperature_1,--NUMBER
                json_ext.get_number(mOrder,'temperature2'),--temperature_2,--NUMBER
                json_ext.get_number(mOrder,'nrPalettes'),--nr_palettes,--NUMBER
                json_ext.get_string(mOrder,'note4'),--note4,--VARCHAR2(1000 BYTE)
                json_ext.get_string(mOrder,'note5'),--note5,--VARCHAR2(1000 BYTE)
                json_ext.get_string(mOrder,'note6'),--note6,--VARCHAR2(1000 BYTE)
                '',--s_telephone,--VARCHAR2(50 BYTE)
                '',--r_telephone,--VARCHAR2(50 BYTE)
                json_ext.get_number(mOrder,'loadingLat'),--loading_lat,--NUMBER
                json_ext.get_number(mOrder,'loadingLng'),--loading_lng,--NUMBER
                json_ext.get_number(mOrder,'unloadingLat'),--unloading_lat,--NUMBER
                json_ext.get_number(mOrder,'unloadingLng'),--unloading_lng,--NUMBER
                json_ext.get_string(mOrder,'trailer'),--trailer,--VARCHAR2(1000 BYTE)
                json_ext.get_number(mOrder,'trailerId'),--trailer_id,--NUMBER
                json_ext.get_number(mOrder,'rabat1'),--rabat1,--NUMBER
                json_ext.get_number(mOrder,'rabat2')--rabat2--NUMBER
            );
            end if;
            mShipments := json_ext.get_json_list(mOrder,'shipments'); 
           
            FOR j IN 1..mShipments.count
                LOOP
                    
                    mShipment := json(mShipments.get(j));
                     select count(*) into mCountShip from shipments where id= json_ext.get_number(mShipment,'id');--NUMBER--id,--NUMBER
                     if(mCountShip>0) then
                        update shipments set 
                            barcode=json_ext.get_string(mShipment,'barcode'),--barcode,
                            order_id=json_ext.get_number(mShipment,'orderId'),--order_id,
                            cnt=json_ext.get_number(mShipment,'count'),--cnt
                            scaned_count=json_ext.get_number(mShipment,'scanedCount'),--scaned_count,
                            scaned=json_ext.get_number(mShipment,'scaned'),--scaned,
                            scaned_date=to_date(json_ext.get_string(mShipment,'scanedDate'),'yyyy-mm-dd hh24:mi:ss'),--scaned_date,
                            werehouse=json_ext.get_string(mShipment,'werehouse'),--werehouse,
                            loc=json_ext.get_string(mShipment,'location'),--loc,
                            soption=json_ext.get_number(mShipment,'option'),--soption,
                            completed=json_ext.get_number(mShipment,'completed'),--completed,
                            seq=json_ext.get_number(mShipment,'sequence'),--seq,
                            title=json_ext.get_string(mShipment,'title'),--title,
                            client_id=json_ext.get_number(mShipment,'clientId'),--client_id,(38,0)
                            palette=json_ext.get_string(mShipment,'palette'),--palette,
                            stype=json_ext.get_string(mShipment,'type'),--stype,
                            product=json_ext.get_number(mShipment,'product'),--product,
                            terminal=json_ext.get_string(mShipment,'terminal'),--terminal,
                            damage_id=json_ext.get_number(mShipment,'damageId'),--damage_id,
                            damage_desc=json_ext.get_string(mShipment,'damageDesc'),--damage_desc,
                            loading_x=json_ext.get_number(mShipment,'loadingX'),--loading_x,
                            loading_y=json_ext.get_number(mShipment,'loadingY'),--loading_y,
                            unloading_x=json_ext.get_number(mShipment,'unloadingX'),--unloading_x,
                            unloading_y=json_ext.get_number(mShipment,'unloadingY'),--unloading_y,
                            active=json_ext.get_number(mShipment,'active'),--active,
                            quantity_o=json_ext.get_number(mShipment,'quantityO'),--quantity_o,
                            lot=json_ext.get_string(mShipment,'lot'),--lot,
                            note=json_ext.get_string(mShipment,'note'),--note,
                            note1=json_ext.get_string(mShipment,'note1'),--note1,
                            note2=json_ext.get_string(mShipment,'note2'),--note2,
                            note3=json_ext.get_string(mShipment,'note3'),--note3,
                            nr_package=json_ext.get_number(mShipment,'nrPackage'),--nr_package,
                            nr_palets=json_ext.get_number(mShipment,'nrPalets'),--nr_palets,
                            transaction_id=json_ext.get_number(mShipment,'transactionId'),--transaction_
                            quantity_r=json_ext.get_number(mShipment,'quantityR'),--quantity_r,
                            cart=json_ext.get_number(mShipment,'cart'),--cart,
                            route=json_ext.get_number(mShipment,'route'),--route,
                            price=json_ext.get_number(mShipment,'price'),--price,
                            weight=json_ext.get_number(mShipment,'weight'),--weight,
                            height=json_ext.get_number(mShipment,'height'),--height,
                            width=json_ext.get_number(mShipment,'width'),--width,
                            sdepth=json_ext.get_number(mShipment,'depth'),--sdepth,
                            pro_id=json_ext.get_number(mShipment,'proId'),--pro_id,
                            doc_id=json_ext.get_number(mShipment,'docId'),--doc_id,
                            rabat1=json_ext.get_number(mShipment,'rabat1'),--rabat1,
                            rabat2=json_ext.get_number(mShipment,'rabat2')--rabat2,
                        where
                            id=json_ext.get_number(mShipment,'id');



                     else
                        insert into shipments(
                            id,--NUMBER
                            barcode,--VARCHAR2(25 BYTE)
                            order_id,--NUMBER
                            cnt,--NUMBER(22,5)
                            scaned_count,--NUMBER
                            scaned,--NUMBER
                            scaned_date,--DATE
                            werehouse,--CHAR(150 BYTE)
                            loc,--CHAR(60 BYTE)
                            soption,--NUMBER
                            completed,--NUMBER
                            seq,--NUMBER
                            title,--VARCHAR2(150 BYTE)
                            client_id,--NUMBER(38,0)
                            palette,--VARCHAR2(25 BYTE)
                            stype,--CHAR(10 BYTE)
                            product,--NUMBER
                            terminal,--CHAR(10 BYTE)
                            damage_id,--NUMBER
                            damage_desc,--VARCHAR2(100 BYTE)
                            loading_x,--NUMBER(22,10)
                            loading_y,--NUMBER(22,10)
                            unloading_x,--NUMBER(22,10)
                            unloading_y,--NUMBER(22,10)
                            active,--NUMBER
                            quantity_o,--NUMBER
                            lot,--VARCHAR2(18 BYTE)
                            note,--VARCHAR2(500 BYTE)
                            note1,--VARCHAR2(500 BYTE)
                            note2,--VARCHAR2(500 BYTE)
                            note3,--VARCHAR2(500 BYTE)
                            nr_package,--NUMBER
                            nr_palets,--NUMBER
                            transaction_id,--NUMBER
                            quantity_r,--NUMBER
                            cart,--NUMBER
                            route,--NUMBER
                            price,--NUMBER
                            weight,--NUMBER
                            height,--NUMBER
                            width,--NUMBER
                            sdepth,--NUMBER
                            pro_id,--NUMBER
                            doc_id,--NUMBER
                            rabat1,--NUMBER
                            rabat2--NUMBER
                        )values(
                            json_ext.get_number(mShipment,'id'),--NUMBER--id,--NUMBER
                            json_ext.get_string(mShipment,'barcode'),--barcode,--VARCHAR2(25 BYTE)
                            json_ext.get_number(mShipment,'orderId'),--order_id,--NUMBER
                            json_ext.get_number(mShipment,'count'),--cnt,--NUMBER(22,5)
                            json_ext.get_number(mShipment,'scanedCount'),--scaned_count,--NUMBER
                            json_ext.get_number(mShipment,'scaned'),--scaned,--NUMBER
                            to_date(json_ext.get_string(mShipment,'scanedDate'),'yyyy-mm-dd hh24:mi:ss'),--scaned_date,--DATE
                            json_ext.get_string(mShipment,'werehouse'),--werehouse,--CHAR(150 BYTE)
                            json_ext.get_string(mShipment,'location'),--loc,--CHAR(60 BYTE)
                            json_ext.get_number(mShipment,'option'),--soption,--NUMBER
                            json_ext.get_number(mShipment,'completed'),--completed,--NUMBER
                            json_ext.get_number(mShipment,'sequence'),--seq,--NUMBER
                            json_ext.get_string(mShipment,'title'),--title,--VARCHAR2(150 BYTE)
                            json_ext.get_number(mShipment,'clientId'),--client_id,--NUMBER(38,0)
                            json_ext.get_string(mShipment,'palette'),--palette,--VARCHAR2(25 BYTE)
                            json_ext.get_string(mShipment,'type'),--stype,--CHAR(10 BYTE)
                            json_ext.get_number(mShipment,'product'),--product,--NUMBER
                            json_ext.get_string(mShipment,'terminal'),--terminal,--CHAR(10 BYTE)
                            json_ext.get_number(mShipment,'damageId'),--damage_id,--NUMBER
                            json_ext.get_string(mShipment,'damageDesc'),--damage_desc,--VARCHAR2(100 BYTE)
                            json_ext.get_number(mShipment,'loadingX'),--loading_x,--NUMBER(22,10)
                            json_ext.get_number(mShipment,'loadingY'),--loading_y,--NUMBER(22,10)
                            json_ext.get_number(mShipment,'unloadingX'),--unloading_x,--NUMBER(22,10)
                            json_ext.get_number(mShipment,'unloadingY'),--unloading_y,--NUMBER(22,10)
                            json_ext.get_number(mShipment,'active'),--active,--NUMBER
                            json_ext.get_number(mShipment,'quantityO'),--quantity_o,--NUMBER
                            json_ext.get_string(mShipment,'lot'),--lot,--VARCHAR2(18 BYTE)
                            json_ext.get_string(mShipment,'note'),--note,--VARCHAR2(500 BYTE)
                            json_ext.get_string(mShipment,'note1'),--note1,--VARCHAR2(500 BYTE)
                            json_ext.get_string(mShipment,'note2'),--note2,--VARCHAR2(500 BYTE)
                            json_ext.get_string(mShipment,'note3'),--note3,--VARCHAR2(500 BYTE)
                            json_ext.get_number(mShipment,'nrPackage'),--nr_package,--NUMBER
                            json_ext.get_number(mShipment,'nrPalets'),--nr_palets,--NUMBER
                            json_ext.get_number(mShipment,'transactionId'),--transaction_id,--NUMBER
                            json_ext.get_number(mShipment,'quantityR'),--quantity_r,--NUMBER
                            json_ext.get_number(mShipment,'cart'),--cart,--NUMBER
                            json_ext.get_number(mShipment,'route'),--route,--NUMBER
                            json_ext.get_number(mShipment,'price'),--price,--NUMBER
                            json_ext.get_number(mShipment,'weight'),--weight,--NUMBER
                            json_ext.get_number(mShipment,'height'),--height,--NUMBER
                            json_ext.get_number(mShipment,'width'),--width,--NUMBER
                            json_ext.get_number(mShipment,'depth'),--sdepth,--NUMBER
                            json_ext.get_number(mShipment,'proId'),--pro_id,--NUMBER
                            json_ext.get_number(mShipment,'docId'),--doc_id,--NUMBER
                            json_ext.get_number(mShipment,'rabat1'),--rabat1,--NUMBER
                            json_ext.get_number(mShipment,'rabat2')--rabat2,--NUMBER
                        );
                    end if;
                end loop;
            
            mAttachments := json_ext.get_json_list(mOrder,'attachments');  
            FOR j IN 1..mAttachments.count
                LOOP
                    null;
                     mAttachment := json(mAttachments.get(j));
                     insert into attachments(
                          order_id,
                          atype,
                          attachment_date,
                          doc_id,
                          status,
                          order_type,
                          lot,
                          linkid,
                          sended,
                          worker_id,
                          image_comment,
                          email,
                          file_name,
                          shipment_id
                    )values(
                          json_ext.get_number(mAttachment,'orderId'),
                          json_ext.get_number(mAttachment,'type'),
                          to_Date(json_ext.get_string(mAttachment,'date'),'yyyy-mm-dd hh24:mi:ss'),
                          0,
                          0,
                          0,
                          '',
                          json_ext.get_string(mAttachment,'linkId'),
                          0,
                          json_ext.get_number(mAttachment,'userId'),
                          json_ext.get_string(mAttachment,'comment'),
                          '',
                          json_ext.get_string(mAttachment,'path'),
                          json_ext.get_number(mAttachment,'shipmentId')
                    );
                end loop;
        end loop;
END;
/
create procedure morders_get_document(token in VARCHAR2, documentId in VARCHAR2, path in varchar2) is
    mContent varchar2(32000);
    pOut clob;
    l_response  json;
    mAttachment json;
    zacetek number;
    konec number;
    image clob; 
    len number := 32000;
    str_length2 number;
    str_length number;
BEGIN 
 --DBMS_OUTPUT.PUT_line('DOCUEMMTN ID -'  ||documentId || ' start zahteve' || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));
    url_connect('http://www.ferbit.net/mobilno/Mobile/webresources/mOrders/API/getPhoto?DocumentId='||documentId,mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
    --   DBMS_OUTPUT.PUT_line(' konec yahteve' || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));
    
  -- dbms_output.put_line('Original clob size: ' || dbms_lob.getlength(pout));
   dbms_lob.createtemporary(image, false);
    --  image:=dbms_lob.substr(pout,zacetek,konec);
     zacetek := dbms_lob.instr(pout,'content')+10;
   --   DBMS_OUTPUT.PUT_line('zacetek ' || zacetek);
     konec := dbms_lob.instr(pout,'"',zacetek);
   str_length:= konec-zacetek; 
   while str_length > 0 loop
     str_length2 := least(str_length, 32768);
     str_length := str_length - str_length2;
     dbms_lob.copy(image, pout, str_length2, dbms_lob.getlength(image) + 1, zacetek);
     zacetek := zacetek + str_length2;
   end loop;
 --  dbms_output.put_line('Copied clob size: ' || dbms_lob.getlength(image));
 --   DBMS_OUTPUT.PUT_line('konec document id '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));
   
    write_to_file('DOKUMENTI', path,image);
   -- DBMS_OUTPUT.PUT_line('szapisano document id '||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));
    update attachments set status=1 where linkid=documentid;
end;
/
create procedure morders_get_update is
    token varchar2(1000):='';
    TYPE VRNI_rt IS RECORD(document_id varchar2(10000), path varchar2(10000));
    l_VRNI   VRNI_rt;
    b_kurzor        sys_refcursor;
    DateFrom date;
    DateTo date;
begin


  --  DBMS_OUTPUT.PUT_LINE('start '||to_char(sysdate,'yyyy-MM-dd hh24:mi:ss')); 
    
select trim(tekst) into token from nastavi where kljuc='morders_token'; 

    select to_date(tekst,'yyyy-mm-dd hh24:mi:ss') into DateFrom from nastavi where  kljuc='morders_PRENOS_DATUM';
    DateTo := sysdate;
    get_morders_data(token,DateFrom,DateTo);
  
    
    OPEN b_kurzor FOR 'select linkid, file_name from attachments where nvl(status,0)=0';
                     LOOP
                        FETCH b_kurzor INTO l_VRNI;
                        EXIT WHEN b_kurzor%NOTFOUND;
                         --   DBMS_OUTPUT.PUT_LINE('start document '||to_char(sysdate, 'dd.mm.yyyy hh24:mi:ss')); 
                         get_morders_document(token, l_VRNI.document_id, l_vrni.path);
                         --   DBMS_OUTPUT.PUT_LINE('end document '||to_char(sysdate, 'dd.mm.yyyy hh24:mi:ss')); 
                            
                        --    DBMS_OUTPUT.PUT_LINE('----- '); 
                    END LOOP;
        close b_kurzor;

        update nastavi set tekst=to_char(DateTo,'YYYY-MM-DD HH24:MI:SS') where kljuc='morders_PRENOS_DATUM';
        commit;
         -- DBMS_OUTPUT.PUT_LINE('end '||to_char(sysdate,'yyyy-MM-dd hh24:mi:ss')); 
end;
/