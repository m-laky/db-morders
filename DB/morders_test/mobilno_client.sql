--run as ferbit
--grant select on klienti to mobilno;
--grant select on skladisca to mobilno;

-- USER SQL
ALTER USER "mobilno"
DEFAULT TABLESPACE "USERS"
TEMPORARY TABLESPACE "TEMP"
ACCOUNT UNLOCK ;

-- QUOTAS

-- ROLES
ALTER USER "mobilno" DEFAULT ROLE "FER_ADMIN";

-- SYSTEM PRIVILEGES
GRANT CREATE JOB TO "mobilno" ;
GRANT ALTER ANY INDEX TO "mobilno" ;
GRANT CREATE ROLE TO "mobilno" ;
GRANT CREATE LIBRARY TO "mobilno" ;
GRANT CREATE TRIGGER TO "mobilno" ;
GRANT ALTER ANY PROCEDURE TO "mobilno" ;
GRANT CREATE ANY PROCEDURE TO "mobilno" ;
GRANT CREATE ANY DIRECTORY TO "mobilno" ;
GRANT ALTER SESSION TO "mobilno" ;
GRANT CREATE MATERIALIZED VIEW TO "mobilno" ;
GRANT MERGE ANY VIEW TO "mobilno" ;
GRANT CREATE ANY INDEX TO "mobilno" ;
GRANT DEBUG ANY PROCEDURE TO "mobilno" ;
GRANT CREATE ANY MEASURE FOLDER TO "mobilno" ;
GRANT CREATE ANY SEQUENCE TO "mobilno" ;
GRANT CREATE VIEW TO "mobilno" ;
GRANT ALTER ANY TABLE TO "mobilno" ;
GRANT CREATE SESSION TO "mobilno" ;
GRANT ALTER ANY SEQUENCE TO "mobilno" ;
GRANT CREATE EXTERNAL JOB TO "mobilno" ;
GRANT CREATE TABLE TO "mobilno" ;
GRANT EXECUTE ANY LIBRARY TO "mobilno" ;
GRANT EXECUTE ASSEMBLY TO "mobilno" ;
GRANT CREATE ANY RULE SET TO "mobilno" ;
GRANT CREATE TYPE TO "mobilno" ;
GRANT CREATE PUBLIC DATABASE LINK TO "mobilno" ;
GRANT CREATE ROLLBACK SEGMENT TO "mobilno" ;
GRANT CREATE ANY JOB TO "mobilno" ;
GRANT ALTER USER TO "mobilno" ;
GRANT CREATE PUBLIC SYNONYM TO "mobilno" ;
GRANT CREATE ANY RULE TO "mobilno" ;
GRANT CREATE ANY SYNONYM TO "mobilno" ;
GRANT EXECUTE ANY PROCEDURE TO "mobilno" ;
GRANT CREATE SYNONYM TO "mobilno" ;
GRANT EXECUTE ANY PROGRAM TO "mobilno" ;
GRANT EXECUTE ANY TYPE TO "mobilno" ;
GRANT CREATE SEQUENCE TO "mobilno" ;
GRANT CREATE ANY INDEXTYPE TO "mobilno" ;
GRANT ALTER ANY INDEXTYPE TO "mobilno" ;
GRANT CREATE USER TO "mobilno" ;
GRANT CREATE PROFILE TO "mobilno" ;
GRANT ALTER ANY ROLE TO "mobilno" ;
GRANT ALTER TABLESPACE TO "mobilno" ;
GRANT ALTER ANY TRIGGER TO "mobilno" ;
GRANT CREATE ANY VIEW TO "mobilno" ;
GRANT EXPORT FULL DATABASE TO "mobilno" ;
GRANT COMMENT ANY TABLE TO "mobilno" ;
GRANT DROP PUBLIC SYNONYM TO "mobilno" ;
GRANT INSERT ANY TABLE TO "mobilno" ;
GRANT DROP PROFILE TO "mobilno" ;
GRANT CREATE ANY MATERIALIZED VIEW TO "mobilno" ;
GRANT MANAGE SCHEDULER TO "mobilno" ;
GRANT ALTER ANY TYPE TO "mobilno" ;
GRANT DROP ANY PROCEDURE TO "mobilno" ;
GRANT DROP PUBLIC DATABASE LINK TO "mobilno" ;
GRANT CREATE ANY TRIGGER TO "mobilno" ;
GRANT CREATE ANY TABLE TO "mobilno" ;
GRANT DROP ANY DIRECTORY TO "mobilno" ;
GRANT CREATE ANY TYPE TO "mobilno" ;
GRANT CREATE ANY LIBRARY TO "mobilno" ;
GRANT CREATE PROCEDURE TO "mobilno" ;
GRANT ALTER DATABASE TO "mobilno" ;
GRANT ALTER ANY RULE TO "mobilno" ;

-- SYSTEM PRIVILEGES
GRANT SELECT ANY MINING MODEL TO "MOBILNO" ;
GRANT SELECT ANY TABLE TO "MOBILNO" ;
GRANT SELECT ANY CUBE DIMENSION TO "MOBILNO" ;
GRANT SELECT ANY SEQUENCE TO "MOBILNO" ;
GRANT SELECT ANY TRANSACTION TO "MOBILNO" ;
GRANT SELECT ANY DICTIONARY TO "MOBILNO" ;
GRANT SELECT ANY CUBE TO "MOBILNO" ;




--------------------------------------------------------
--  File created - Petek-marec-18-2022   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence ATT_AUTO
--------------------------------------------------------

   CREATE SEQUENCE  "MOBILNO"."ATT_AUTO"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence ERRAUTO
--------------------------------------------------------

   CREATE SEQUENCE  "MOBILNO"."ERRAUTO"  MINVALUE 0 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 266016 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence ORD_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "MOBILNO"."ORD_SEQ"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 14438 NOCACHE  NOORDER  NOCYCLE ;
  GRANT SELECT ON "MOBILNO"."ORD_SEQ" TO "FERBIT";
--------------------------------------------------------
--  DDL for Sequence SHIP_AUTO
--------------------------------------------------------

   CREATE SEQUENCE  "MOBILNO"."SHIP_AUTO"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 17 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table ARTICLES
--------------------------------------------------------

  CREATE TABLE "MOBILNO"."ARTICLES" 
   (	"ID" NUMBER DEFAULT 1, 
	"TITLE" VARCHAR2(1000 BYTE) DEFAULT 'A', 
	"TYPE" VARCHAR2(200 BYTE) DEFAULT 1, 
	"BARCODE" VARCHAR2(50 BYTE) DEFAULT 'A', 
	"TYPEART" CHAR(100 BYTE) DEFAULT 'A', 
	"LAST_DATE" DATE, 
	"COMPANY" NUMBER, 
	"NOTE1" VARCHAR2(2000 BYTE), 
	"NOTE2" VARCHAR2(2000 BYTE), 
	"NOTE3" VARCHAR2(2000 BYTE), 
	"NOTE4" VARCHAR2(2000 BYTE), 
	"NOTE5" VARCHAR2(2000 BYTE), 
	"PRICE" NUMBER,
  "ddv" number
   );
   
   
   

  GRANT INSERT ON "MOBILNO"."ARTICLES" TO "FERBIT";
--------------------------------------------------------
--  DDL for Table ATTACHMENTS
--------------------------------------------------------

  CREATE TABLE "MOBILNO"."ATTACHMENTS" 
   (	"ID" NUMBER, 
	"ORDER_ID" NUMBER, 
	"ATYPE" NUMBER, 
	"ATTACHMENT_DATE" DATE, 
	"DOC_ID" NUMBER, 
	"STATUS" NUMBER DEFAULT 0, 
	"ORDER_TYPE" VARCHAR2(20 BYTE) DEFAULT 0, 
	"LOT" VARCHAR2(50 BYTE), 
	"LINKID" VARCHAR2(400 BYTE), 
	"SENDED" NUMBER DEFAULT 0, 
	"WORKER_ID" NUMBER, 
	"IMAGE_COMMENT" VARCHAR2(400 BYTE), 
	"EMAIL" VARCHAR2(150 BYTE) DEFAULT '-', 
	"FILE_NAME" VARCHAR2(4000 BYTE), 
	"SHIPMENT_ID" NUMBER
   )  ;
--------------------------------------------------------
--  DDL for Table CLOBTEST
--------------------------------------------------------

  CREATE TABLE "MOBILNO"."CLOBTEST" 
   (	"A" CLOB
   )  ;
--------------------------------------------------------
--  DDL for Table KLIENTI_BACK
--------------------------------------------------------

  CREATE TABLE "MOBILNO"."KLIENTI_BACK" 
   (	"SIFRA" NUMBER, 
	"PLACE_ID" VARCHAR2(1000 BYTE), 
	"LAT" NUMBER, 
	"LNG" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table KLIENTI_NAREJENI
--------------------------------------------------------

  CREATE TABLE "MOBILNO"."KLIENTI_NAREJENI" 
   (	"ID" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table ORDERLOG
--------------------------------------------------------

  CREATE TABLE "MOBILNO"."ORDERLOG" 
   (	"ID" NUMBER, 
	"ORDER_ID" NUMBER, 
	"TITLE" VARCHAR2(2000 BYTE), 
	"L_TYPE" NUMBER, 
	"LOG_DATE" DATE, 
	"USER_ID" NUMBER, 
	"L_COMMENT" VARCHAR2(2000 BYTE), 
	"STATUS" NUMBER, 
	"PRO_ID" NUMBER, 
	"COMPANY_ID" NUMBER, 
	"FVO" NUMBER, 
	"ORDER_STATUS" NUMBER
   )  ;
--------------------------------------------------------
--  DDL for Table ORDERS
--------------------------------------------------------

  CREATE TABLE "MOBILNO"."ORDERS" 
   (	"ORDER_ID" NUMBER DEFAULT 0, 
	"WORKER_ID" NUMBER DEFAULT 0, 
	"ORDER_DATE" DATE, 
	"DELIVERY_DATE" DATE, 
	"COMPLETE_DATE" DATE, 
	"OTYPE" NUMBER DEFAULT 0, 
	"TYPE_D" VARCHAR2(150 BYTE) DEFAULT 0, 
	"CNT" NUMBER DEFAULT 0, 
	"VEHICLE_ID" NUMBER DEFAULT 0, 
	"VEHICLE" VARCHAR2(20 BYTE) DEFAULT 0, 
	"TERMINAL" CHAR(1000 BYTE) DEFAULT 0, 
	"SEQ" NUMBER DEFAULT 0, 
	"COMPANY" NUMBER DEFAULT 0, 
	"BUSSINES_UNIT" NUMBER DEFAULT 0, 
	"CUSTOMER" NUMBER DEFAULT 0, 
	"LOC" VARCHAR2(40 BYTE) DEFAULT 0, 
	"DESC_CODE" VARCHAR2(30 BYTE) DEFAULT 0, 
	"ACTIVE" NUMBER DEFAULT 0, 
	"SCANED_COUNT" NUMBER DEFAULT 0, 
	"ENDED" NUMBER DEFAULT 0, 
	"TRANSACTION_ID" NUMBER DEFAULT 0, 
	"SIGNATOR" VARCHAR2(150 BYTE) DEFAULT 0, 
	"SENDER" VARCHAR2(100 BYTE) DEFAULT 0, 
	"RECIVER" VARCHAR2(100 BYTE) DEFAULT 0, 
	"ROUTE" NUMBER DEFAULT 0, 
	"SCANED" DATE, 
	"OPROGRAM" VARCHAR2(30 BYTE) DEFAULT 'MP', 
	"NOTE1" VARCHAR2(200 BYTE) DEFAULT '-', 
	"CART" VARCHAR2(50 BYTE) DEFAULT 0, 
	"WEIGHT" NUMBER, 
	"HEIGHT" NUMBER, 
	"WIDTH" NUMBER, 
	"ODEPTH" NUMBER, 
	"PRO_ID" NUMBER, 
	"STITLE" VARCHAR2(200 BYTE) DEFAULT 0, 
	"SADDRESS" VARCHAR2(200 BYTE) DEFAULT 0, 
	"SPOSTOFFICENUMBER" VARCHAR2(150 BYTE) DEFAULT 0, 
	"SCITY" VARCHAR2(200 BYTE) DEFAULT 0, 
	"RTITLE" VARCHAR2(200 BYTE) DEFAULT 0, 
	"RADDRESS" VARCHAR2(200 BYTE) DEFAULT 0, 
	"RPOSTOFFICENUMBER" VARCHAR2(150 BYTE) DEFAULT 0, 
	"RCITY" VARCHAR2(200 BYTE) DEFAULT 0, 
	"CMR" VARCHAR2(150 BYTE) DEFAULT 0, 
	"NOTE2" VARCHAR2(2000 BYTE) DEFAULT ' ', 
	"NOTE3" VARCHAR2(2000 BYTE) DEFAULT ' ', 
	"TEMPERATURE_1" NUMBER, 
	"TEMPERATURE_2" NUMBER, 
	"NR_PALETTES" NUMBER, 
	"NOTE4" VARCHAR2(1000 BYTE), 
	"NOTE5" VARCHAR2(1000 BYTE), 
	"NOTE6" VARCHAR2(1000 BYTE), 
	"S_TELEPHONE" VARCHAR2(50 BYTE), 
	"R_TELEPHONE" VARCHAR2(50 BYTE), 
	"LOADING_LAT" NUMBER, 
	"LOADING_LNG" NUMBER, 
	"UNLOADING_LAT" NUMBER, 
	"UNLOADING_LNG" NUMBER, 
	"TRAILER" VARCHAR2(1000 BYTE), 
	"TRAILER_ID" NUMBER, 
	"RABAT1" NUMBER, 
	"RABAT2" NUMBER, 
	"DATE_MODIFIED" DATE, 
	"CASH" NUMBER, 
	"ZOI" VARCHAR2(1000 BYTE), 
	"EOR" VARCHAR2(1000 BYTE), 
	"BUSINESSPREMISE" VARCHAR2(1000 BYTE), 
	"ELECTRONICDEVICE" VARCHAR2(1000 BYTE), 
	"VAT22" NUMBER, 
	"VAT95" NUMBER, 
	"OSNOVA1" NUMBER, 
	"OSNOVA2" NUMBER, 
	"PRICE" NUMBER, 
	"FURS_ORDER" VARCHAR2(1000 BYTE), 
	"INVOICE_NUMBER" NUMBER
   ) ;
  GRANT INSERT, UPDATE ON "MOBILNO"."ORDERS" TO "FERBIT";
--------------------------------------------------------
--  DDL for Table SHIPMENTS
--------------------------------------------------------

  CREATE TABLE "MOBILNO"."SHIPMENTS" 
   (	"ID" NUMBER, 
	"BARCODE" VARCHAR2(25 BYTE), 
	"ORDER_ID" NUMBER, 
	"CNT" NUMBER(22,5) DEFAULT 0, 
	"SCANED_COUNT" NUMBER DEFAULT 0, 
	"SCANED" NUMBER DEFAULT 0, 
	"SCANED_DATE" DATE, 
	"WEREHOUSE" CHAR(150 BYTE) DEFAULT 0, 
	"LOC" CHAR(60 BYTE) DEFAULT 0, 
	"SOPTION" NUMBER DEFAULT 0, 
	"COMPLETED" NUMBER DEFAULT 00, 
	"SEQ" NUMBER DEFAULT 0, 
	"TITLE" VARCHAR2(150 BYTE) DEFAULT 0, 
	"CLIENT_ID" NUMBER(*,0) DEFAULT 0, 
	"PALETTE" VARCHAR2(25 BYTE) DEFAULT 0, 
	"STYPE" CHAR(10 BYTE) DEFAULT 0, 
	"PRODUCT" NUMBER DEFAULT 0, 
	"TERMINAL" CHAR(10 BYTE) DEFAULT 1, 
	"DAMAGE_ID" NUMBER DEFAULT 0, 
	"DAMAGE_DESC" VARCHAR2(100 BYTE) DEFAULT 0, 
	"LOADING_X" NUMBER(22,10) DEFAULT 0, 
	"LOADING_Y" NUMBER(22,10) DEFAULT 0, 
	"UNLOADING_X" NUMBER(22,10) DEFAULT 0, 
	"UNLOADING_Y" NUMBER(22,10) DEFAULT 0, 
	"ACTIVE" NUMBER DEFAULT 1, 
	"QUANTITY_O" NUMBER DEFAULT 0, 
	"LOT" VARCHAR2(18 BYTE) DEFAULT 0, 
	"NOTE" VARCHAR2(500 BYTE) DEFAULT 0, 
	"NOTE1" VARCHAR2(500 BYTE) DEFAULT 0, 
	"NOTE2" VARCHAR2(500 BYTE) DEFAULT 0, 
	"NOTE3" VARCHAR2(500 BYTE) DEFAULT 0, 
	"NR_PACKAGE" NUMBER DEFAULT 0, 
	"NR_PALETS" NUMBER DEFAULT 0, 
	"TRANSACTION_ID" NUMBER DEFAULT 0, 
	"QUANTITY_R" NUMBER DEFAULT 0, 
	"CART" NUMBER DEFAULT 0, 
	"ROUTE" NUMBER DEFAULT 0, 
	"PRICE" NUMBER DEFAULT 0, 
	"WEIGHT" NUMBER DEFAULT 0, 
	"HEIGHT" NUMBER DEFAULT 0, 
	"WIDTH" NUMBER DEFAULT 0, 
	"SDEPTH" NUMBER DEFAULT 0, 
	"PRO_ID" NUMBER, 
	"DOC_ID" NUMBER, 
	"RABAT1" NUMBER, 
	"RABAT2" NUMBER
   )  ;
--------------------------------------------------------
--  DDL for Table SQL_ERROR
--------------------------------------------------------

  CREATE TABLE "MOBILNO"."SQL_ERROR" 
   (	"ZAP_ST" NUMBER, 
	"NAPAKA" NUMBER, 
	"OPIS_NAPAKE" VARCHAR2(255 BYTE), 
	"NAPAKA_SQL" NUMBER, 
	"OPIS_NAPAKE_SQL" VARCHAR2(255 BYTE), 
	"DODATNI_OPIS" VARCHAR2(50 BYTE), 
	"SQL_STAVEK" VARCHAR2(4000 BYTE), 
	"DATUM" DATE, 
	"ID_RECORD" NUMBER, 
	"ID_STAR" VARCHAR2(100 BYTE), 
	"PROCEDURA" VARCHAR2(50 BYTE), 
	"STRANKA" NUMBER, 
	"TRID" NUMBER
   )  ;
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "MOBILNO"."USERS" 
   (	"ID" NUMBER, 
	"NAME" VARCHAR2(50 BYTE), 
	"LAST_NAME" VARCHAR2(150 BYTE), 
	"USERNAME" VARCHAR2(100 BYTE), 
	"PASSWORD" VARCHAR2(300 BYTE), 
	"PIN" VARCHAR2(20 BYTE), 
	"LONGITUDE" NUMBER DEFAULT 0, 
	"LATITUDE" NUMBER DEFAULT 0, 
	"DEVICE" VARCHAR2(3000 BYTE), 
	"LAST_TIME" DATE, 
	"ADMIN" NUMBER DEFAULT 0, 
	"STATUS" NUMBER DEFAULT 0, 
	"COMPANY" NUMBER
   )  ;
--------------------------------------------------------
--  DDL for Trigger TBE_ATTACHMENTS
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MOBILNO"."TBE_ATTACHMENTS" 
BEFORE INSERT 
ON ATTACHMENTS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH Row
declare
l_tip_dokumenta   number:=0;
l_doc_id_glavni   number:=0; 
l_pro_id          number:=0;    
dir_path varchar2(1000);
BEGIN

 
  if(:new.id=null or :new.id=0) then
   select att_auto.nextval into :new.id from dual;
  end if;
  
  
  If :new.shipment_id = 0 then
   Begin
   Select ord.Otype,ORD.PRO_ID into :new.ORDER_TYPE,l_pro_id from orders Ord where ORD.ORDER_ID=:new.order_id;
    EXCEPTION
    WHEN OTHERS THEN
    NULL;
   End;
  End if;
  
  If :new.order_type = 70 then
   Begin
  -- select teh.docid into l_doc_id_glavni from ferbit.tehtalnilist teh where teh.vez=l_pro_id;
  null;
      EXCEPTION
    WHEN OTHERS THEN
    NULL;
   End;
  
  End if;
  
  
  If :new.Order_Type = 60 then
   l_tip_dokumenta :=9;
  elsif  :new.Order_Type = 70 then
   l_tip_dokumenta :=8;
  end if;
  
  If :new.Order_Type = 60 then
   begin
     SELECT SHI.DOC_ID INTO :NEW.doc_id   FROM SHIPMENTS SHI WHERE   SHI.ID=:NEW.SHIPMENT_ID;
   EXCEPTION
    WHEN OTHERS THEN
    NULL;
   End;
  End if;
  
   If :new.Order_Type = 70 then
     begin
       -- SELECT dodAUTO.NEXTVAL INTO :NEW.doc_id   FROM DUAL;
       null;
     EXCEPTION
       WHEN OTHERS THEN
        NULL;
     End;
   end if;
   
   If :new.Order_Type = 21 then
     begin
   select teh.docid into l_doc_id_glavni from ferbit.dobavskl teh where teh.vez=l_pro_id;
        SELECT ferbit.dodAUTO.NEXTVAL INTO :NEW.doc_id   FROM DUAL;
  dir_path := 'P:\WinPro\Dokumenti_poslovanje\OMAHEN\SKLADISCE\';
      l_tip_dokumenta :=9;
     EXCEPTION
       WHEN OTHERS THEN
        NULL;
     End;
   end if;
  dbms_output.put_line('dodajanje dokumenta '||Trim(:NEW.FILE_NAME) || ' '||:new.doc_id||' '||l_doc_id_glavni);
 
  begin
  --select directory_path into dir_path from dba_directories where upper(directory_name) = 'SKLADISCE';
   DODAJ_DOKUMENT ( l_tip_dokumenta, l_doc_id_glavni , dir_path, Trim(:NEW.FILE_NAME),:new.doc_id, l_tip_dokumenta, :new.ORDER_TYPE,0);
  EXCEPTION
    WHEN OTHERS THEN
    NULL;
   End;
  
  
  
  If :new.order_type in (10,11) then
    Begin
        l_tip_dokumenta := 4;
        SELECT ferbit.dodAUTO.NEXTVAL INTO :NEW.doc_id   FROM DUAL;
        select pro_id into l_pro_id from orders where order_id = :new.order_id;
        select ferbit.nkl.docid into l_doc_id_glavni from ferbit.nakladi nkl where nkl.vez=l_pro_id;
        DODAJ_DOKUMENT ( l_tip_dokumenta, l_doc_id_glavni ,'P:\WinPro\Dokumenti_poslovanje\OMAHEN\LETO2016\NAKLADI\', Trim(:NEW.FILE_NAME),:new.doc_id, l_tip_dokumenta, :new.ORDER_TYPE,0);
    EXCEPTION
        WHEN OTHERS THEN
        NULL;
    End;
    
    End if;
  
   INSERT INTO ferbit.LOG (  OPIS) VALUES ( 'Dodajne shiments '||:NEW.doc_id);       
end;
/
ALTER TRIGGER "MOBILNO"."TBE_ATTACHMENTS" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TBE_ORDER_LOG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MOBILNO"."TBE_ORDER_LOG" 
BEFORE INSERT-- OR UPDATE OR DELETE
ON ORDERlog REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

fvo ferbit.FVOZNIKI%rowtype;
l_tip varchar2(1);
BEGIN
null;
   begin
    SELECT * INTO fvo FROM ferbit.fvozniki WHERE sifra=:new.fvo;
    
    
 l_tip := 'L';
  If fvo.zaporedje = -1 then 
    l_tip := 'F';
  end if;
    
    INSERT INTO ferbit.NAKLADILOG (
   AVEZ,TIP, DATUM, FIRMA, 
   FAZA, NKL_VEZ, PON_VEZ, 
   FVO_SIFRA, ZBI_VEZ, DOGODEK, 
   BOOKINGOPIS, FAKTURA, POSTAVKA, 
   VOZNIK, VOZNIKOPIS, PROCENT, 
   ZNESEK, VOZILO, PREVOZNIK, 
   VOZILOOPIS, PRIKOLICA, PRIKOLICAOPIS, 
   STATUS, KILOMETRI, PRAZNIKM, 
   DRZAVA, LATITUDE, LONGITUDE, 
   ZAPOREDJE, NAKLADA, KOLICINA, 
   TEZA, CMR, DATUM_OD, 
   URA, TRAJANJE, SOVOZNIK, 
   FAZAP, BKGKLJUC, VEZPOG, 
   PGPVEZ, POGNACIN, PREJEMNIK, 
   PNASLOV1, SKLADISCE, ZASKLADISCE) 
   VALUES (
        null,
        l_tip,
        :new.log_date,
        fvo.FIRMA,
        :new.order_status,
        fvo.vezn,
        0,
        fvo.SIFRA,
        fvo.VEZZBI,
        Decode(fvo.zaporedje,1,1,2),
       -- Decode(:new.order_status,-1,'Nepokrit',0,'Pripravljen',8,'Fakturiran')||' - '||Decode(fvo.zaporedje,1,'Zapis novega naloga',-1,'Zapis fakture','Dodajanje nove delitev za nalog'),
        :new.title,
        DECODE(fvo.FAZA,8,fvo.FAKTURA,0),  --faktura
        DECODE(fvo.FAZA,8,fvo.POSTAVKA,0), -- POSTAVKA 
        fvo.voznik,      --VOZNIK 
        fvo.voznikopis,  -- VOZNIKOPIS 
        fvo.procent ,    -- PROCENT 
        fvo.znesek,      -- ZNESEK 
        fvo.vozilo,      -- VOZILO 
        fvo.prevoznik,   -- PREVOZNIK 
        fvo.voziloopis,  -- VOZILOOPIS 
        fvo.prikolica,   -- PRIKOLICA 
        fvo.PRIKOLICAOPIS,--PRIKOLICAOPIS    
        fvo.STATUS,      -- STATUS 
        fvo.KILOMETRI,   -- KILOMETRI 
        fvo.PRAZNIKM,    -- PRAZNIKM 
        fvo.drzava,      -- DRZAVA 
        fvo.LATITUDE,    -- LATITUDE 
        fvo.LONGITUDE,   -- LONGITUDE 
        fvo.ZAPOREDJE,   -- ZAPOREDJE 
        fvo.NAKLADA ,    -- NAKLADA 
        fvo.KOLICINA,    -- KOLICINA 
        fvo.TEZA,        -- TEZA 
        fvo.CMR,         -- CMR 
        fvo.DATUM ,      -- DATUM_OD 
        fvo.ura,         -- URA 
        fvo.trajanje,    -- TRAJANJE 
        fvo.sovoznik,    -- SOVOZNIK 
        fvo.fazap ,      -- FAZAP 
        fvo.bkgkljuc,    -- BKGKLJUC 
        fvo.vezpog,      -- VEZPOG 
        fvo.pgpvez,      -- PGPVEZ 
        fvo.pognacin,    -- POGNACIN 
        fvo.prejemnik,   -- PREJEMNIK 
        fvo.pnaslov1,    -- PNASLOV1 
        fvo.skladisce,   -- SKLADISCE 
        fvo.ZASKLADISCE  -- ZASKLADISCE  
    );
   
   exception when others then
    null;
   end;
END ;
/
ALTER TRIGGER "MOBILNO"."TBE_ORDER_LOG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TBE_ORDERS
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MOBILNO"."TBE_ORDERS" 
BEFORE INSERT OR UPDATE OR DELETE
ON ORDERS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
L_VEZ                NUMBER   := 0;
l_error              NUMBER   := 0;
l_avez               NUMBER   := 0;
l_procedure          CHAR(50) := $$PLSQL_UNIT;                       --- ali obracun obstaja
l_napak              NUMBER   := 0;
Sql_error            varchar2 (250);                                 --- opis napake
sql_stmt             VARCHAR2 (3900);
p_transakcija        NUMBER   :=0;
l_pro_id             NUMBER   := 0;
l_stevilka           NUMBER   := 0;
l_faza               NUMBER   := 0;
lfvo_Sifra           NUMBER   := 0;
l_vozilo             varchar2(150) :='';
l_voznik             varchar2(150) :='';
l_kilometri number;
TYPE VRNI_rt IS RECORD(filename varchar2(10000), shipment_id number);
    l_VRNI   VRNI_rt;

 RetVal number;
    l_docid_pnalog number;  
    l_docid_pnalogp number;
    cursor shipments is select * From MOBILNO.shipments where order_id=:new.order_id;
    cnt number;
    l_filename varchar2(1000);
    l_glavni_doc_id number;
l_zapst number;
    q_attachemnts        sys_refcursor;    
    q_stavek    varchar2(32000):='';
    xid_dokument number;
BEGIN
    --INSERT INTO LOG (  OPIS) VALUES (Trim(l_procedure) );
 If inserting or updating then
   if(nvl(:new.order_id,0)=0 or :new.order_id=0) then
    select ord_seq.nextval into :new.order_id from dual;
   end if ;
 End if;
   INSERT INTO ferbit.LOG (  OPIS) VALUES ( 'Dodajne orders '||:NEW.order_id);       

 

  If updating and nvl(Length(trim(:new.note3)),0) > 0 then
    Begin
   -- update tehtalnilist THT SET THT.OPOMBE1 = SUBSTR(:NEW.NOTE3,1,78) WHERE THT.VEZ = :NEW.PRO_ID;
   null;
    EXCEPTION 
       WHEN OTHERS THEN
        l_error   := SQLCODE;
        l_napak   := 1;
        Sql_error := SUBSTR(SQLERRM, 1, 250);
        ERROR_LOG(l_error,'UPDATE tehtalni list',Sql_error,0,l_procedure,0);
    end;
   end if;
   
   if(updating and :new.otype=10) then 
   if(:new.temperature_2<>0 and :new.temperature_1<>0) then
                l_kilometri:=:new.temperature_2-:new.temperature_1;
            else
                 l_kilometri:= 0;
            end if;
      update ferbit.nakladi set faza = decode(
                                            :new.route,
                                            1,6,    --SPREJET
                                            23,23,  --V PRIHODU NA NAKLAD
                                            26,26,  --ČAKA NA RAZKLAD
                                            2,1,    --NALOŽEN
                                            24,24,  --V PRIHODU NA RAZKLAD
                                            27,27,  --ČAKA NA RAZKLAD
                                            3,2,    --RAZLOŽEN
                                            5       --POSLAN
                                        ),
                                teza = :new.weight,
                                cmr = :new.cmr,
                                NAKMETER = to_number(:new.cart),
                                sirina = :new.width,
                                visina = :new.height,
                                dolzina = :new.odepth,
                                stpalet = :new.nr_palettes,
                                komadi = :new.cnt,
                                KILOMETRI=l_kilometri
            where vez=:new.pro_id;
            
            update ferbit.fvozniki set zacetnikm=:new.temperature_1, koncnikm=:new.temperature_2 where sifra=:new.bussines_unit;
   end if;
   
   
    if( updating and :new.otype=55 and trim(:old.terminal)='0' and trim(:new.terminal)='-1') then
    DBMS_OUTPUT.PUT_LINE(:new.order_id);
    select ferbit.ponauto.nextval INTO L_VEZ from dual;
        Begin
         
            SELECT ferbit.DODAUTO.NEXTVAL into l_docid_pnalog FROM DUAL;
	     EXCEPTION 
	   WHEN OTHERS THEN
	     l_error   := SQLCODE;
	     l_napak   := 1;
	     Sql_error := SUBSTR(SQLERRM, 1, 250);
	     ERROR_LOG(l_error,'SELECT SEQPMAAUTO',Sql_error,0,l_procedure,0);
	   end;
         DBMS_OUTPUT.PUT_LINE('inserting pnalog '||l_vez);
        insert into ferbit.pnalog(
            VEZ,
            datumn_d_s,
            datumo_d_s,
            datump_d_s,
            ZACETNIKILOMETRI,
            KONCNIKILOMETRI,
            KILOMETRI,
            KATEGORIJA,
            SOPOTNIK,
            ime,
            oseba,
            vozilo,
            oznaka,
            valutap,
            nalog,
            leto,
            TIPTECAJA,
            TECAJ,
            FIRMA,
            registracija,
            vrstanaloga,
            docid
        ) values(
            L_VEZ,
            :new.order_date,
            :new.order_date, 
            :new.delivery_date, 
            :new.temperature_1, 
            :new.temperature_2,
            :new.temperature_2 - :new.temperature_1,
            'Z',
            :new.bussines_unit,
            nvl((select trim(title) from articles where id=:new.worker_id and trim(type)='E'),0),
            :new.worker_id,
            :new.vehicle_id,
            substr(:new.type_d,1,20),                                         --oznaka
            '978',                                                            --valuta
            to_char(ferbit.stevci('PNALOG' || to_char(sysdate,'YY') || '001')),
            to_number( to_char(sysdate,'YYYY')),                              --leto
            'U',                                                              --tiptecaja
            'S'    ,                                                          --tecaj
            1,
            :new.vehicle,
            'Prevoz blaga',
            l_docid_pnalog                                                     --docid
        );
         ferbit.create_doc_document(55, l_vez, l_docid_pnalog);
        :new.pro_id := l_Vez;
        /*
 
       nalogad             = substr((select trim(title)||' - '||:new.terminal from articles where id=:new.scaned_count and type='TEP') ,0,250),
       PROJECTID         = :new.scaned_count, kje je pustil nalog
       NALOGA            =  TRIM(:NEW.NOTE3)*/
       
        /*DODAJANJE SLIK IZ NALOGA*/
        select count(file_name) into cnt from attachments where order_id=:new.order_id;
        if(cnt>0) then
                q_stavek:='select a.file_name, 0 from attachments a where nvl(a.shipment_id,0)<=0  and a.ordeR_id=' || :new.order_id ;
                 dbms_output.put_line(q_stavek);
                OPEN q_attachemnts FOR q_stavek ;
                LOOP
                FETCH q_attachemnts INTO l_VRNI;
                EXIT WHEN q_attachemnts%NOTFOUND;
                    begin
                      --  insert into sql_error(sql_stavek) values('file name '||l_filename||'     l_docid_pnalogp :'||l_docid_pnalogp); 
                       -- insert into sql_error(sql_stavek) values(' ferbit.DODAJ_DOKUMENT ( 55, '||:new.order_id||' ,''\\OTL1\Prevoznik\WinPro\Dokumenti_poslovanje\OMAHEN\LETO2016\NAKLADI\'','|| Trim(l_filename)||','||l_docid_pnalogp||', 6, 55,-1);');
                       SELECT ferbit.DODAUTO.NEXTVAL into xid_dokument FROM DUAL;
                        ferbit.DODAJ_DOKUMENT ( 55, :new.order_id ,'\\OTL1\Prevoznik\WinPro\Dokumenti_poslovanje\OMAHEN\LETO2016\NAKLADI\', Trim(l_VRNI.filename),xid_dokument, 6, 55,-1);
                        RetVal := FERBIT.DOCRELPISI (l_docid_pnalog, xid_dokument);

                    exception when others
                        then
                        sql_error := Substr(SQLERRM, 1, 250);  
                        INSERT INTO sql_error(napaka, opis_napake, opis_napake_sql, trid, procedura, sql_stavek)  
                        VALUES      ( 1, 'POŠILJANJE PLANOV PONOVNO za vse', sql_error, 1, 'DR_SEND_ALL_DATA', '') ; 
                    end;
                end loop;
                close q_attachemnts;
        end if;
        /*DODAJANJE SLIK IZ NALOGA*/
       
       for tmpShp in shipments 
       loop
       if(trim(tmpShp.terminal)<>'PLACE') THEN
              Begin
                SELECT ferbit.PNAAUTO.NEXTVAL INTO l_zapst FROM DUAL;
                SELECT ferbit.DODAUTO.NEXTVAL into l_docid_pnalogp FROM DUAL;
             EXCEPTION 
           WHEN OTHERS THEN
             l_error   := SQLCODE;
             l_napak   := 1;
             Sql_error := SUBSTR(SQLERRM, 1, 250);
             ERROR_LOG(l_error,'SELECT SEQPMAAUTO',Sql_error,0,l_procedure,0);
           end;
                Begin
                    Insert into ferbit.PnalogP
                    (
                        zapst,
                        stnalog,
                        strosek,
                        opisS,
                        krat,
                        znesekS,
                        valuta,
                        oznakavalute,
                        zneseksit,
                        vrstaplacila,
                        datum_s,
                        racun,
                        tipvnosa,
                        kolicina,
                        poln,
                        dobavitelj,
                        sopotnik,
                        KRAJ_TANKANJA,
                        stanje_stevca,
                        SDRZAVA,
                        docid)
                    values(
                        l_zapst,        --zapst
                        L_VEZ,           --stnalog
                        tmpShp.product, --storsek
                        tmpShp.title,   --opisS
                        1,              --krat
                        tmpShp.price,   --zneseks
                        '978',          --valuta
                        'EUR',          --oznakavalute
                        tmpShp.Cnt,     --zneseksit
                        0,              --vrstaplacila
                        tmpShp.SCANED_DATE, --datum_s
                        tmpShp.note1,       --tacun
                        'A',                --tipvnosa
                        tmpShp.cnt,         --kolicna
                        to_number(trim(tmpShp.note3)),   --poln
                        0,--ABS(nvl(tmpShp.werehouse,0)),   --dobavitelj
                        tmpShp.damage_id,               --sopotnik
                        trim(tmpShp.note2),             --kraj_tankanja
                        tmpShp.weight,                  --stanje_Stevca
                        trim(to_char(substr(trim(nvl(tmpShp.loc,'')),1,3),'099')),--sdrzava
                        l_docid_pnalogp     --docid
                    ); 
                    update shipments set pro_id=l_zapst, doc_id=l_docid_pnalogp where order_id=:new.order_id and id=tmpShp.id;
                  
                    
                     dbms_output.put_line('order_id= '||:new.order_id||' shipment'||tmpShp.id);
                    select count(file_name) into cnt from attachments where order_id=:new.order_id and shipment_id=tmpShp.id;
                    dbms_output.put_line('count'||cnt);
                    /*DODAJANJE DATOTEK IZ POSTAVK*/
                    if(cnt>0) then 
                        q_stavek:='select a.file_name, A.SHIPMeNT_ID from attachments a where a.ordeR_id=' || :new.order_id || ' and a.SHIPMeNT_ID=' || tmpShp.id;
                         dbms_output.put_line(q_stavek);
                        OPEN q_attachemnts FOR q_stavek ;
                            LOOP
                            FETCH q_attachemnts INTO l_VRNI;
                            EXIT WHEN q_attachemnts%NOTFOUND;
                                begin
                                  --  insert into sql_error(sql_stavek) values('file name '||l_filename||'     l_docid_pnalogp :'||l_docid_pnalogp); 
                                   -- insert into sql_error(sql_stavek) values(' ferbit.DODAJ_DOKUMENT ( 55, '||:new.order_id||' ,''\\OTL1\Prevoznik\WinPro\Dokumenti_poslovanje\OMAHEN\LETO2016\NAKLADI\'','|| Trim(l_filename)||','||l_docid_pnalogp||', 6, 55,-1);');
                                    IF(l_VRNI.shipment_id>0) then
                                        ferbit.DODAJ_DOKUMENT ( 55, :new.order_id ,'\\OTL1\Prevoznik\WinPro\Dokumenti_poslovanje\OMAHEN\LETO2016\NAKLADI\', Trim(l_VRNI.filename),l_docid_pnalogp, 6, 55,-1);
                                        RetVal := FERBIT.DOCRELPISI (l_docid_pnalog, l_docid_pnalogp);
                                    end if;
                                    SELECT ferbit.DODAUTO.NEXTVAL into l_docid_pnalogp FROM DUAL;
                                exception when others
                                    then
                                    sql_error := Substr(SQLERRM, 1, 250);  
                                    INSERT INTO sql_error(napaka, opis_napake, opis_napake_sql, trid, procedura, sql_stavek)  
                                    VALUES      ( 1, 'POŠILJANJE PLANOV PONOVNO za vse', sql_error, 1, 'DR_SEND_ALL_DATA', '') ; 
                                end;
                            end loop;
                        close q_attachemnts;
                    end if ;
                    /*DODAJANJE DATOTEK IZ POSTAVK*/
                   
                end;
            else
                null;
                dbms_output.put_line('rabat '||tmpShp.rabat1);
                   INSERT INTO FERBIT.PNALOGKRAJ (
                        ZAPST
                       ,STNALOGA
                       ,OD_SIFRA
                       ,DO_SIFRA 
                       ,ODKJE
                       ,DATUMOD_S
                       ,DATUMOD_TMP
                       ,URAOD
                       ,STANJESTEVCA
                       ,STPREVOZA
                       ,NAZAJ
                       ,POLNEDNEVNICE
                       ,MAJHNEDNEVNICE  
                       ,nazivstranke
                       ,drzava
                       ) 
                   VALUES
                       (
                       NULL
                       ,l_vez
                       ,0
                       ,0
                       ,tmpShp.title
                       ,tmpShp.scaned_date
                       ,(select trunc(tmpShp.scaned_date)- to_date('28.12.1800','dd.mm.yyyy') from dual)
                       , ((extract(hour from to_timestamp(to_char(tmpShp.scaned_date, 'dd.mm.yyyy hh24:mi:ss'))) *60)+extract(minute from to_timestamp(to_char(tmpShp.scaned_date, 'dd.mm.yyyy hh24:mi:ss'))))*1000
                       ,tmpShp.weight
                       ,l_vez
                       ,:new.route
                       ,tmpShp.transaction_id
                       ,:NEW.order_ID
                       ,tmpShp.loc
                       ,substr(trim(to_char(tmpShp.rabat1)),1,3)
                       );
            END IF;
       end loop;
    end if;
END ;
/
ALTER TRIGGER "MOBILNO"."TBE_ORDERS" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TBE_SHIPMENTS
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MOBILNO"."TBE_SHIPMENTS" 
BEFORE INSERT or update
ON SHIPMENTS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
    loc_vez              NUMBER;
    loc_faza             NUMBER;
    l_error              NUMBER   := 0;                                
    l_procedure          CHAR(50) := 'TBE_SHIPMENTS';                       --- ali obracun obstaja
    l_napak              NUMBER   := 0;
    l_program            char(4); 
    Sql_error            varchar2(250);                                         --- opis napake
    sql_stmt             VARCHAR2(3900);
    l_nalogvez           number   := 0;
    l_prejemnica         Number   := 0;
    l_dobavnica          Number   := 0;
    l_pro_id             Number   := 0;                                     --vez glave dokumenta      
    L_VEZLOKACIJAVSKL    NUMBER   := 0; 
    l_dogodek            NUMBER   := 0;
    l_znak               CHAR(1);
    l_avtovez            Number   := 0;
    l_zapst              Number   := 0;
    l_xid                Number  :=0;
    l_projekt            Number     := 0;
l_stm                char(6);
l_SERIJSKA           CHAR(30);
l_product            Number   := 0;
l_dobavProjekt       Number   := 0;
l_ODOKUMENT         CHAR(15 BYTE);
l_type               number:=0;
l_idp                number:=0;
BEGIN
    
      IF INSERTING THEN
        If nvl(:NEW.ID,0)=0 OR :NEW.ID=0 THEN
         SELECT SHIP_AUTO.NEXTVAL INTO :NEW.ID FROM DUAL;
        END IF;
       IF(:NEW.QUANTITY_R=-1) THEN
        :NEW.CLIENT_ID := :NEW.ID;
       END IF;
        If nvl(:NEW.PRODUCT,0)=0  THEN
         :new.PRODUCT:=0;
        END IF;
      END IF;  
       INSERT INTO ferbit.LOG (  OPIS) VALUES ( 'Dodajne shiments '||:NEW.ID);   
         
      If inserting or updating then
      
      
       Begin
        select ord.otype,ord.pro_id into l_type,l_pro_id from orders ord where ord.order_id=:new.order_id;
       EXCEPTION 
       WHEN OTHERS THEN
        l_error   := SQLCODE;
        l_napak   := 1;
        Sql_error := SUBSTR(SQLERRM, 1, 250);
        ERROR_LOG(l_error,'SELECT PROGRAM '||:new.order_id,Sql_error,0,l_procedure,0);
       end;
       If (:new.pro_id is null  OR :NEW.PRO_ID=0) then
        :new.pro_id := l_pro_id;
       end if;
        
      End if; 
      If nvl(l_Type,0)=60 then
       begin
        --SELECT dodAUTO.NEXTVAL INTO :NEW.doc_id   FROM DUAL;
        null;
       EXCEPTION
        WHEN OTHERS THEN
        NULL;
       End;
      
      
        Begin
        --SELECT PROOAUTO.NEXTVAL INTO :NEW.PRO_ID FROM DUAL;
        null;
         EXCEPTION 
       WHEN OTHERS THEN
         l_error   := SQLCODE;
         l_napak   := 1;
         Sql_error := SUBSTR(SQLERRM, 1, 250);
         ERROR_LOG(l_error,'SELECT SEQPMAAUTO',Sql_error,0,l_procedure,0);
       end;
      
      
      
        BEGIN
      /*  insert into prometna2 
        (IDP,voz,BLAGO,datum_s,leto,PLACNIK,STATUS,OPOMBA,OPOSTAJA,TIPVCP,DOCID)
         values
         (:NEW.PRO_id,trim(:new.title),SUBSTR(:NEW.NOTE1,1,20),:new.scaned_date,trim(to_char(:new.scaned_date,'YY')),1,'U',SUBSTR(:NEW.NOTE,1,20),'79' ||TRIM(TO_CHAR(:NEW.QUANTITY_O)),TRIM(TO_CHAR(:NEW.QUANTITY_R)),:NEW.doc_id );
       */
       null;
       EXCEPTION 
         WHEN OTHERS THEN
         l_error   := SQLCODE;
         l_napak   := 1;
         Sql_error := SUBSTR(SQLERRM, 1, 250);
         ERROR_LOG(l_error,'DODAJANE V PROMETNO',Sql_error,0,l_procedure,0);
        END ;
      end if;  
     
    END;
/
ALTER TRIGGER "MOBILNO"."TBE_SHIPMENTS" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TBE_SQL_ERROR
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MOBILNO"."TBE_SQL_ERROR" 
 BEFORE INSERT
 ON SQL_ERROR   REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE
 v_avtovez NUMBER;
 BEGIN
   :new.datum := sysdate();
   IF NVL(:NEW.ZAP_ST,0)=0    THEN
     SELECT ERRAUTO.NEXTVAL INTO :NEW.ZAP_ST   FROM DUAL;
     END IF;
 END;
/
ALTER TRIGGER "MOBILNO"."TBE_SQL_ERROR" ENABLE;
--------------------------------------------------------
--  DDL for Procedure DODAJ_DOKUMENT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."DODAJ_DOKUMENT" ( 
tip_dokumenta       in number, -- ??
xDocId              in number, -- glavni docId
mapa_dokumenta      in char,  -- pot na disku kjer je shranjena datoteka
ime_dokumenta       in char,
sekvenca            in number, -- sekvenca za DOC_DOKUMENT
vrsta               in int,
order_type in char,
lot number)   -- vrsta datoteke
as
t_xid int              :=0; 
t_ASSEMBLY_ID      int :=0; -- PART_ID glavnega dokumenta
t_ASSEMBLY_AVTO    int :=0; -- AVTO glavnega dokumenta 

stavek2            VARCHAR2(4000);
stavek3            VARCHAR2(4000);
stavek4            VARCHAR2(4000);
stavek5            VARCHAR2(4000);
stavek6            VARCHAR2(4000);
stavek7            VARCHAR2(4000);
napaka             int :=0;
l_QUANTITY_O    number :=0;
 -- iz PNALOG_MOBILE
 l_xid    number :=0;
l_tip    number :=0; -- mogoče ga je treba najt iz nastavitev !!
l_veja   number :=0;
l_pot    VARCHAR2(299 BYTE);
l_part   number :=0;
l_avto   number :=0;
l_error                  NUMBER:=0;                                
l_procedure              CHAR(15):='dodaj_dokument';                                    --- ali obracun obstaja
Sql_error                varchar2(250);
t_PRO_ID number :=0;
l_vezdokumenta       number  :=0;
l_docid              number  :=0;   
l_vrsta              number  :=0;
l_projectId          number  :=0;
 
begin
    INSERT INTO ferbit.LOG (  OPIS) VALUES ( 'Dodaj dokument'||sekvenca||' vrsta:' ||vrsta);   
  t_xid :=   sekvenca; 
     
  if  vrsta = 9  then
    l_tip     := 9 ; --mogoče ga je treba najt iz nastavitev !!
  elsif vrsta = 8 then
   l_tip      :=8; 
  end if;  
  
 
     
 if t_xid > 0 then --če obstaja sekvenca dodamo zapis v dokumente
   INSERT INTO ferbit.LOG (  OPIS) VALUES ( 'Prišli brez sequence' );
       begin
            stavek3 :=  'INSERT INTO ferbit.DOC_DOKUMENT (XID, XNAME, VRSTA, POT_DATOTEKE, IME_DATOTEKE, KRATKI_OPIS,TIP_DOKUMENTA ) values (:1,:2,:3,:4,:5,:6,:7)';
            execute immediate stavek3 using t_xid,ime_dokumenta,2, mapa_dokumenta, ime_dokumenta,ime_dokumenta,l_tip;
       EXCEPTION
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE(SUBSTR(SQLERRM,1,4000) );
            Sql_error:=SUBSTR(SQLERRM,1,250);
            INSERT INTO ferbit.LOG (  OPIS) VALUES ( Sql_error );
            napaka := 1;
    end;
/*
    if  vrsta in (9)  then -- če so te vrste najdemo korenski id od vrste dokumenta v doc_relacijah
   
        Begin 
            SELECT POT,VEJA into l_pot,l_veja  FROM  ferbit.DOC_TIP WHERE  vez=l_tip;
        EXCEPTION
         WHEN OTHERS THEN
           Sql_error := SUBSTR(SQLERRM, 1, 250);
           ERROR_LOG(l_error,'SELECT TIP',Sql_error,0,l_procedure,0); 
       End;
   
       Begin 
       SELECT PART_ID,AVTO into l_part,l_avto  FROM  ferbit.DOC_RELACIJE WHERE avto = l_veja;
       EXCEPTION
         WHEN OTHERS THEN
           Sql_error := SUBSTR(SQLERRM, 1, 250);
           ERROR_LOG(l_error,'SELECT RELACIJE',Sql_error,0,l_procedure,0); 
       End;
       --    INSERT INTO LOG (  OPIS) VALUES ( 'l_part,l_avto:' ||l_part||','||l_avto);
  
    end if;
*/
   Begin 
     INSERT INTO ferbit.LOG (  OPIS) VALUES ( 'Dodaj relacijo 1'||l_docid );  
      insert into ferbit.doc_relacije
      (assembly_id,part_id,assembly_avto)
      values( xDocId,sekvenca,l_veja); 
      EXCEPTION
      WHEN OTHERS THEN
      Sql_error := SUBSTR(SQLERRM, 1, 250);
      ERROR_LOG(l_error,'ZAPIS RELACIJE',Sql_error,0,l_procedure,0); 
    end; 
    


 end if; -- t_xid > 0


end;

/
--------------------------------------------------------
--  DDL for Procedure ERROR_LOG
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."ERROR_LOG" (p_NAPAKA in number, p_OPIS_NAPAKE in char,p_OPIS_NAPAKE_SQL in char,p_TRID in number,p_PROCEDURA in char,p_ID_RECORD in number)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
/******************************************************************************
   NAME:       ERROR_LOG
   PURPOSE:   Zapis loga
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0       18.04.2015  Leo Lakota
   PARAMETERS:
   pDat1             Prvi datum obdobja
   pDat2             Drugi datum obdobja
   p_Transakcija     Id Transakcije 
   CALLED BY:WinPro.exe
   CALLS:
   EXAMPLE USE:    execute OBRACUN_PRO('01.01.2015','31.01.2015',1555)
   ASSUMPTIONS:
   LIMITATIONS:
   ALGORITHM:
   NOTES:
******************************************************************************/

l_error              NUMBER:=0;                                
Sql_error            varchar2(250);                                         --- opis napake
sql_stmt             VARCHAR2(3900);
l_stevilor           NUMBER:=0;
l_kontrola           NUMBER:=0;
 

BEGIN
  
        INSERT INTO SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,ID_RECORD)
        VALUES (p_NAPAKA, p_OPIS_NAPAKE,p_OPIS_NAPAKE_SQL,p_TRID,p_PROCEDURA,p_ID_RECORD);  
        COMMIT;
 EXCEPTION
     WHEN OTHERS THEN
      ROLLBACK;
 
 
  
END ERROR_LOG;

/
--------------------------------------------------------
--  DDL for Procedure MORDERS_AUTH
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."MORDERS_AUTH" (taxNumber in varchar2, title in varchar2)  IS
    pOut clob;
    l_response  json;   
     mContent clob;
    syncObject json;  
    cnt number:=0;
BEGIN 

    syncObject := json();
    syncObject.put('barcode',taxNumber);
    syncObject.put('deviceToken',title);

    DBMS_LOB.CREATETEMPORARY(mContent,true); 
    syncObject.to_clob(mContent,true); 

 --  url_connect('http://127.0.0.1/Mobile/webresources/mOrders/API/auth',mContent,'POST',pOut,'','application/json;charset=utf-8');
   
   url_connect('http://192.168.0.10/MOBILNO/Mobile/webresources/mOrders/API/auth',mContent,'POST',pOut,'','application/json;charset=utf-8');
    dbms_output.put_line(pout);
    l_response := json(pout); 


    select count( tekst) into cnt from ferbit.nastavi where kljuc='morders_token'; 
    if(cnt=0) then
        insert into ferbit.nastavi(kljuc,tekst) values('morders_token',json_ext.get_string(l_response,'deviceToken')); 
    else
        update ferbit.nastavi set tekst=json_ext.get_string(l_response,'deviceToken') where kljuc='morders_token';
    end if;
end;

/
--------------------------------------------------------
--  DDL for Procedure MORDERS_GEOCODE_CLIENT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."MORDERS_GEOCODE_CLIENT" (pId in number) as
    l_vez number; 
    dataReturn json;
    dataCoord json_list; 
    mContent clob:='';
    pout clob;
    token varchar2(1000):='';
    vez number:=0;
    DateFrom date;
    syncObject json;
    dateTo date := sysdate+ (1/1440*3);--minus one minute
    pAddress varchar2(32000);
    addressCount number:=0;
    /*
        v exvez, ext_sifra1 se piše original vez ter davčna številka zato da bomo lahko naprej vedeli če nalog že obstaja
    */
begin
--insert into nastavi (tekst,kljuc) values(sysdate,'nkl_PRENOS_DATUM');
    
    select trim(tekst) into token from ferbit.nastavi where kljuc='morders_token';

    select trim(naslov5)||', '||trim(posta)||', '||trim(naslov4)||', '||trim(drzava) into pAddress from ferbit.klienti where sifra in (pId) ;
    if(length(pAddress)>10) then
    syncObject := json();
    syncObject.put('address',pAddress);
 
    DBMS_LOB.CREATETEMPORARY(mContent,true);
    syncObject.to_clob(mContent,true);
    
    url_connect('http://192.168.0.10/MOBILNO/Mobile/webresources/calculate/places/',mContent,'POST',pOut,''||token,'application/json;charset=utf-8');
   -- insert into clob_test(cdata) values(pout);
  
    begin
    dataReturn := json(pOut); 
    if(json_ext.get_number(dataReturn,'status')=0) then
        dataCoord := json_ext.get_json_list(dataReturn,'data');
        dataReturn := json(dataCoord.get(1));  
         dbms_output.put_line(''||pid);
          dbms_output.put_line(''|| json_ext.get_string(dataReturn,'place_id'));
          insert into klienti_back (  sifra ,  place_id ,  lat ,  lng ) values(pid,json_ext.get_string(dataReturn,'place_id'), json_ext.get_number(dataReturn,'lat'),json_ext.get_number(dataReturn,'lng'));
            update ferbit.klienti set placeid=substr(json_ext.get_string(dataReturn,'place_id'),1,50), ulica_longitude=json_ext.get_number(dataReturn,'lng'), ulica_latitude=json_ext.get_number(dataReturn,'lat'), skype=dataCoord.count where sifra=pId;
          
    else
    null;
       -- update ferbit.klienti set skype='-1' where sifra=pId;
    end if;
    commit;
    /*
        {
            "date": "2021-10-22 10:24:04",
            "data": [
                {
                    "country": "Slovenija",
                    "country_code": "SI",
                    "formatted_address": "Prešernova ulica, 2000 Maribor, Slovenija",
                    "address": "Prešernova ulica",
                    "post": "Maribor",
                    "lng": 15.6516498,
                    "postal_code": "2000",
                    "lat": 46.5630518,
                    "place_id": "ChIJD1MAUqp3b0cRBqdIgy29AgQ"
                },
                {
                    "country": "Slovenija",
                    "country_code": "SI",
                    "formatted_address": "Prešernova ulica, 3000 Celje, Slovenija",
                    "address": "Prešernova ulica",
                    "post": "Celje",
                    "lng": 15.2637299,
                    "postal_code": "3000",
                    "lat": 46.2288564,
                    "place_id": "ChIJW-CTVfxwZUcR_k2o8dj0D_k"
                },
                {
                    "country": "Slovenija",
                    "country_code": "SI",
                    "formatted_address": "Prešernova cesta, 1000 Ljubljana, Slovenija",
                    "address": "Prešernova cesta",
                    "post": "Ljubljana",
                    "lng": 14.4984073,
                    "postal_code": "1000",
                    "lat": 46.0512111,
                    "place_id": "ChIJkSYQvWAtZUcRNXMyG1UeSlc"
                }
            ],
            "provider": "Goole",
            "status_text": "OK",
            "status": 0
        }
    */
    exception when others then 
     DBMS_OUTPUT.PUT_LINE('SQLERRM  '||SQLERRM);
    --   dbms_output.put_line('NAPAKA');
       dbms_output.put_line(mContent);
      null;
    end ;
    commit;
        end if ;

end;

/
--------------------------------------------------------
--  DDL for Procedure MORDERS_GET_DATA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."MORDERS_GET_DATA" (token in VARCHAR2, dateFrom in date, dateTo in date ) is
  mContent varchar2(32000);
    pOut clob;

     l_response       json;
     mOrders json_list;
     mOrder json;
     mShipments json_list;
     mShipment json;
     mAttachments json_list;
     mAttachment json;
     mCount number:=0;
     mCountShip number := 0;
     mOrderLogs json_list;
     mOrderLog json;

BEGIN
    mContent := '{"fromDate":"'||to_char(dateFrom,'yyyy-mm-dd hh24:mi:ss')||'","toDate":"'||to_char(dateTo,'yyyy-mm-dd hh24:mi:ss')||'"}';
    url_connect('http://192.168.0.10/MOBILNO/Mobile/Nov/webresources/mOrders/API/getOrders/',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
 --   insert into clob_test(cdata) values(pOUT);
    l_response := json(pOut);
    mOrders := json_ext.get_json_list(l_response,'orders');
     FOR i IN 1..mOrders.count
        LOOP
            null;
             mOrder := json(mOrders.get(i));
             select count(*) into mCount from orders where order_id=json_ext.get_number(mOrder,'orderId');
             if(mCount>0) then
                    update orders set
                        worker_id=json_ext.get_number(mOrder,'workerId'),--worker_id,
                        order_date=to_date(json_ext.get_string(mOrder,'orderDate'),'yyyy-mm-dd hh24:mi:ss'),--order_date,
                        delivery_date=to_date(json_ext.get_string(mOrder,'deliveryDate'),'yyyy-mm-dd hh24:mi:ss'),--delivery_date,
                        complete_date=to_date(json_ext.get_string(mOrder,'completeDate'),'yyyy-mm-dd hh24:mi:ss'),--complete_date,
                        otype=json_ext.get_number(mOrder,'type'),--otype,
                        type_d=json_ext.get_string(mOrder,'typeD'),--type_d
                        cnt=json_ext.get_number(mOrder,'count'),--cnt,
                        vehicle_id=json_ext.get_number(mOrder,'vehicleId'),--vehicle_id,
                        vehicle=json_ext.get_string(mOrder,'vehicle'),--vehicle,
                        terminal=json_ext.get_string(mOrder,'terminal'),--terminal,
                        seq=json_ext.get_number(mOrder,'seq'),--seq,
                        company=json_ext.get_number(mOrder,'company'),--company,
                        bussines_unit=json_ext.get_number(mOrder,'bussinesUnit'),--bussines_unit,
                        customer=json_ext.get_number(mOrder,'customer'),--customer,
                        loc=json_ext.get_string(mOrder,'location'),--loc,
                        desc_code=json_ext.get_string(mOrder,'descCide'),--desc_code,
                        active=json_ext.get_number(mOrder,'active'),--active,
                        scaned_count=json_ext.get_number(mOrder,'scanedCount'),--scaned_count,
                        ended=json_ext.get_number(mOrder,'ended'),--ended,
                        transaction_id=json_ext.get_number(mOrder,'transactionId'),--transaction_id,
                        signator=json_ext.get_string(mOrder,'signator'),--signator
                        sender=json_ext.get_string(mOrder,'sender'),--sender,
                        reciver=json_ext.get_string(mOrder,'reciver'),--reciver,
                        route=json_ext.get_number(mOrder,'route'),--route,
                        scaned=to_date(json_ext.get_string(mOrder,'scanedDate'),'yyyy-mm-dd hh24:mi:ss'),--scaned,
                        oprogram=json_ext.get_string(mOrder,'program'),--oprogram,
                        note1=json_ext.get_string(mOrder,'note1'),--note1,
                        cart=json_ext.get_string(mOrder,'cart'),--cart,
                        weight=json_ext.get_number(mOrder,'weight'),--weight,
                        height=json_ext.get_number(mOrder,'height'),--height,
                        width=json_ext.get_number(mOrder,'width'),--width,
                        odepth=json_ext.get_number(mOrder,'odepth'),--odepth,
                        pro_id=json_ext.get_number(mOrder,'proId'),--pro_id,
                        stitle=json_ext.get_string(mOrder,'stitle'),--stitle,
                        saddress=json_ext.get_string(mOrder,'saddress'),--saddress,
                        spostofficenumber=json_ext.get_string(mOrder,'spostofficenumber'),--spostofficenumber
                        scity=json_ext.get_string(mOrder,'scity'),--scity,
                        rtitle=json_ext.get_string(mOrder,'rtitle'),--rtitle,
                        raddress=json_ext.get_string(mOrder,'raddress'),--raddress,
                        rpostofficenumber=json_ext.get_string(mOrder,'rpostofficenumber'),--rpostofficenumber
                        rcity=json_ext.get_string(mOrder,'rcity'),--rcity,
                        cmr=json_ext.get_string(mOrder,'cmr'),--cmr
                        note2=json_ext.get_string(mOrder,'note2'),--note2,
                        note3=json_ext.get_string(mOrder,'note3'),--note3,
                        temperature_1=json_ext.get_number(mOrder,'temperature1'),--temperature_1,
                        temperature_2=json_ext.get_number(mOrder,'temperature2'),--temperature_2,
                        nr_palettes=json_ext.get_number(mOrder,'nrPalettes'),--nr_palettes,
                        note4=json_ext.get_string(mOrder,'note4'),--note4,
                        note5=json_ext.get_string(mOrder,'note5'),--note5,
                        note6=json_ext.get_string(mOrder,'note6'),--note6,
                        s_telephone='',--s_telephone,
                        r_telephone='',--r_telephone,
                        loading_lat=json_ext.get_number(mOrder,'loadingLat'),--loading_lat,
                        loading_lng=json_ext.get_number(mOrder,'loadingLng'),--loading_lng,
                        unloading_lat=json_ext.get_number(mOrder,'unloadingLat'),--unloading_lat,
                        unloading_lng=json_ext.get_number(mOrder,'unloadingLng'),--unloading_lng,
                        trailer=json_ext.get_string(mOrder,'trailer'),--trailer,
                        trailer_id=json_ext.get_number(mOrder,'trailerId'),--trailer_id,
                        rabat1=json_ext.get_number(mOrder,'rabat1'),--rabat1,
                        rabat2=json_ext.get_number(mOrder,'rabat2')--rabat2
                    where
                        order_id=json_ext.get_number(mOrder,'orderId') ;
             else
            insert into orders(
                order_id,--NUMBER
                worker_id,--NUMBER
                order_date,--DATE
                delivery_date,--DATE
                complete_date,--DATE
                otype,--NUMBER
                type_d,--VARCHAR2(150 BYTE)
                cnt,--NUMBER
                vehicle_id,--NUMBER
                vehicle,--VARCHAR2(20 BYTE)
                terminal,--CHAR(1000 BYTE)
                seq,--NUMBER
                company,--NUMBER
                bussines_unit,--NUMBER
                customer,--NUMBER
                loc,--VARCHAR2(40 BYTE)
                desc_code,--VARCHAR2(30 BYTE)
                active,--NUMBER
                scaned_count,--NUMBER
                ended,--NUMBER
                transaction_id,--NUMBER
                signator,--VARCHAR2(150 BYTE)
                sender,--VARCHAR2(100 BYTE)
                reciver,--VARCHAR2(100 BYTE)
                route,--NUMBER
                scaned,--DATE
                oprogram,--VARCHAR2(30 BYTE)
                note1,--VARCHAR2(200 BYTE)
                cart,--VARCHAR2(50 BYTE)
                weight,--NUMBER
                height,--NUMBER
                width,--NUMBER
                odepth,--NUMBER
                pro_id,--NUMBER
                stitle,--VARCHAR2(200 BYTE)
                saddress,--VARCHAR2(200 BYTE)
                spostofficenumber,--VARCHAR2(150 BYTE)
                scity,--VARCHAR2(200 BYTE)
                rtitle,--VARCHAR2(200 BYTE)
                raddress,--VARCHAR2(200 BYTE)
                rpostofficenumber,--VARCHAR2(150 BYTE)
                rcity,--VARCHAR2(200 BYTE)
                cmr,--VARCHAR2(150 BYTE)
                note2,--VARCHAR2(2000 BYTE)
                note3,--VARCHAR2(2000 BYTE)
                temperature_1,--NUMBER
                temperature_2,--NUMBER
                nr_palettes,--NUMBER
                note4,--VARCHAR2(1000 BYTE)
                note5,--VARCHAR2(1000 BYTE)
                note6,--VARCHAR2(1000 BYTE)
                s_telephone,--VARCHAR2(50 BYTE)
                r_telephone,--VARCHAR2(50 BYTE)
                loading_lat,--NUMBER
                loading_lng,--NUMBER
                unloading_lat,--NUMBER
                unloading_lng,--NUMBER
                trailer,--VARCHAR2(1000 BYTE)
                trailer_id,--NUMBER
                rabat1,--NUMBER
                rabat2--NUMBER
            )values(
                json_ext.get_number(mOrder,'orderId'),--order_id,--NUMBER
                json_ext.get_number(mOrder,'workerId'),--worker_id,--NUMBER
                to_date(json_ext.get_string(mOrder,'orderDate'),'yyyy-mm-dd hh24:mi:ss'),--order_date,--DATE
                to_date(json_ext.get_string(mOrder,'deliveryDate'),'yyyy-mm-dd hh24:mi:ss'),--delivery_date,--DATE
                to_date(json_ext.get_string(mOrder,'completeDate'),'yyyy-mm-dd hh24:mi:ss'),--complete_date,--DATE
                json_ext.get_number(mOrder,'type'),--otype,--NUMBER
                json_ext.get_string(mOrder,'typeD'),--type_d,--VARCHAR2(150 BYTE)
                json_ext.get_number(mOrder,'count'),--cnt,--NUMBER
                json_ext.get_number(mOrder,'vehicleId'),--vehicle_id,--NUMBER
                json_ext.get_string(mOrder,'vehicle'),--vehicle,--VARCHAR2(20 BYTE)
                json_ext.get_string(mOrder,'terminal'),--terminal,--CHAR(1000 BYTE)
                json_ext.get_number(mOrder,'seq'),--seq,--NUMBER
                json_ext.get_number(mOrder,'company'),--company,--NUMBER
                json_ext.get_number(mOrder,'bussinesUnit'),--bussines_unit,--NUMBER
                json_ext.get_number(mOrder,'customer'),--customer,--NUMBER
                json_ext.get_string(mOrder,'location'),--loc,--VARCHAR2(40 BYTE)
                json_ext.get_string(mOrder,'descCide'),--desc_code,--VARCHAR2(30 BYTE)
                json_ext.get_number(mOrder,'active'),--active,--NUMBER
                json_ext.get_number(mOrder,'scanedCount'),--scaned_count,--NUMBER
                json_ext.get_number(mOrder,'ended'),--ended,--NUMBER
                json_ext.get_number(mOrder,'transactionId'),--transaction_id,--NUMBER
                json_ext.get_string(mOrder,'signator'),--signator,--VARCHAR2(150 BYTE)
                json_ext.get_string(mOrder,'sender'),--sender,--VARCHAR2(100 BYTE)
                json_ext.get_string(mOrder,'reciver'),--reciver,--VARCHAR2(100 BYTE)
                json_ext.get_number(mOrder,'route'),--route,--NUMBER
                to_date(json_ext.get_string(mOrder,'scanedDate'),'yyyy-mm-dd hh24:mi:ss'),--scaned,--DATE
                json_ext.get_string(mOrder,'program'),--oprogram,--VARCHAR2(30 BYTE)
                json_ext.get_string(mOrder,'note1'),--note1,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'cart'),--cart,--VARCHAR2(50 BYTE)
                json_ext.get_number(mOrder,'weight'),--weight,--NUMBER
                json_ext.get_number(mOrder,'height'),--height,--NUMBER
                json_ext.get_number(mOrder,'width'),--width,--NUMBER
                json_ext.get_number(mOrder,'odepth'),--odepth,--NUMBER
                json_ext.get_number(mOrder,'proId'),--pro_id,--NUMBER
                json_ext.get_string(mOrder,'stitle'),--stitle,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'saddress'),--saddress,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'spostofficenumber'),--spostofficenumber,--VARCHAR2(150 BYTE)
                json_ext.get_string(mOrder,'scity'),--scity,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'rtitle'),--rtitle,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'raddress'),--raddress,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'rpostofficenumber'),--rpostofficenumber,--VARCHAR2(150 BYTE)
                json_ext.get_string(mOrder,'rcity'),--rcity,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'cmr'),--cmr,--VARCHAR2(150 BYTE)
                json_ext.get_string(mOrder,'note2'),--note2,--VARCHAR2(2000 BYTE)
                json_ext.get_string(mOrder,'note3'),--note3,--VARCHAR2(2000 BYTE)
                json_ext.get_number(mOrder,'temperature1'),--temperature_1,--NUMBER
                json_ext.get_number(mOrder,'temperature2'),--temperature_2,--NUMBER
                json_ext.get_number(mOrder,'nrPalettes'),--nr_palettes,--NUMBER
                json_ext.get_string(mOrder,'note4'),--note4,--VARCHAR2(1000 BYTE)
                json_ext.get_string(mOrder,'note5'),--note5,--VARCHAR2(1000 BYTE)
                json_ext.get_string(mOrder,'note6'),--note6,--VARCHAR2(1000 BYTE)
                '',--s_telephone,--VARCHAR2(50 BYTE)
                '',--r_telephone,--VARCHAR2(50 BYTE)
                json_ext.get_number(mOrder,'loadingLat'),--loading_lat,--NUMBER
                json_ext.get_number(mOrder,'loadingLng'),--loading_lng,--NUMBER
                json_ext.get_number(mOrder,'unloadingLat'),--unloading_lat,--NUMBER
                json_ext.get_number(mOrder,'unloadingLng'),--unloading_lng,--NUMBER
                json_ext.get_string(mOrder,'trailer'),--trailer,--VARCHAR2(1000 BYTE)
                json_ext.get_number(mOrder,'trailerId'),--trailer_id,--NUMBER
                json_ext.get_number(mOrder,'rabat1'),--rabat1,--NUMBER
                json_ext.get_number(mOrder,'rabat2')--rabat2--NUMBER
            );
            end if;
            mShipments := json_ext.get_json_list(mOrder,'shipments');

            FOR j IN 1..mShipments.count
                LOOP

                    mShipment := json(mShipments.get(j));
                     select count(*) into mCountShip from shipments where id= json_ext.get_number(mShipment,'id');--NUMBER--id,--NUMBER
                     if(mCountShip>0) then
                        update shipments set
                            barcode=json_ext.get_string(mShipment,'barcode'),--barcode,
                            order_id=json_ext.get_number(mShipment,'orderId'),--order_id,
                            cnt=json_ext.get_number(mShipment,'count'),--cnt
                            scaned_count=json_ext.get_number(mShipment,'scanedCount'),--scaned_count,
                            scaned=json_ext.get_number(mShipment,'scaned'),--scaned,
                            scaned_date=to_date(json_ext.get_string(mShipment,'scanedDate'),'yyyy-mm-dd hh24:mi:ss'),--scaned_date,
                            werehouse=json_ext.get_string(mShipment,'werehouse'),--werehouse,
                            loc=json_ext.get_string(mShipment,'location'),--loc,
                            soption=json_ext.get_number(mShipment,'option'),--soption,
                            completed=json_ext.get_number(mShipment,'completed'),--completed,
                            seq=json_ext.get_number(mShipment,'sequence'),--seq,
                            title=json_ext.get_string(mShipment,'title'),--title,
                            client_id=json_ext.get_number(mShipment,'clientId'),--client_id,(38,0)
                            palette=json_ext.get_string(mShipment,'palette'),--palette,
                            stype=json_ext.get_string(mShipment,'type'),--stype,
                            product=json_ext.get_number(mShipment,'product'),--product,
                            terminal=json_ext.get_string(mShipment,'terminal'),--terminal,
                            damage_id=json_ext.get_number(mShipment,'damageId'),--damage_id,
                            damage_desc=json_ext.get_string(mShipment,'damageDesc'),--damage_desc,
                            loading_x=json_ext.get_number(mShipment,'loadingX'),--loading_x,
                            loading_y=json_ext.get_number(mShipment,'loadingY'),--loading_y,
                            unloading_x=json_ext.get_number(mShipment,'unloadingX'),--unloading_x,
                            unloading_y=json_ext.get_number(mShipment,'unloadingY'),--unloading_y,
                            active=json_ext.get_number(mShipment,'active'),--active,
                            quantity_o=json_ext.get_number(mShipment,'quantityO'),--quantity_o,
                            lot=json_ext.get_string(mShipment,'lot'),--lot,
                            note=json_ext.get_string(mShipment,'note'),--note,
                            note1=json_ext.get_string(mShipment,'note1'),--note1,
                            note2=json_ext.get_string(mShipment,'note2'),--note2,
                            note3=json_ext.get_string(mShipment,'note3'),--note3,
                            nr_package=json_ext.get_number(mShipment,'nrPackage'),--nr_package,
                            nr_palets=json_ext.get_number(mShipment,'nrPalets'),--nr_palets,
                            transaction_id=json_ext.get_number(mShipment,'transactionId'),--transaction_
                            quantity_r=json_ext.get_number(mShipment,'quantityR'),--quantity_r,
                            cart=json_ext.get_number(mShipment,'cart'),--cart,
                            route=json_ext.get_number(mShipment,'route'),--route,
                            price=json_ext.get_number(mShipment,'price'),--price,
                            weight=json_ext.get_number(mShipment,'weight'),--weight,
                            height=json_ext.get_number(mShipment,'height'),--height,
                            width=json_ext.get_number(mShipment,'width'),--width,
                            sdepth=json_ext.get_number(mShipment,'depth'),--sdepth,
                            pro_id=json_ext.get_number(mShipment,'proId'),--pro_id,
                            doc_id=json_ext.get_number(mShipment,'docId'),--doc_id,
                            rabat1=json_ext.get_number(mShipment,'rabat1'),--rabat1,
                            rabat2=json_ext.get_number(mShipment,'rabat2')--rabat2,
                        where
                            id=json_ext.get_number(mShipment,'id');



                     else
                        insert into shipments(
                            id,--NUMBER
                            barcode,--VARCHAR2(25 BYTE)
                            order_id,--NUMBER
                            cnt,--NUMBER(22,5)
                            scaned_count,--NUMBER
                            scaned,--NUMBER
                            scaned_date,--DATE
                            werehouse,--CHAR(150 BYTE)
                            loc,--CHAR(60 BYTE)
                            soption,--NUMBER
                            completed,--NUMBER
                            seq,--NUMBER
                            title,--VARCHAR2(150 BYTE)
                            client_id,--NUMBER(38,0)
                            palette,--VARCHAR2(25 BYTE)
                            stype,--CHAR(10 BYTE)
                            product,--NUMBER
                            terminal,--CHAR(10 BYTE)
                            damage_id,--NUMBER
                            damage_desc,--VARCHAR2(100 BYTE)
                            loading_x,--NUMBER(22,10)
                            loading_y,--NUMBER(22,10)
                            unloading_x,--NUMBER(22,10)
                            unloading_y,--NUMBER(22,10)
                            active,--NUMBER
                            quantity_o,--NUMBER
                            lot,--VARCHAR2(18 BYTE)
                            note,--VARCHAR2(500 BYTE)
                            note1,--VARCHAR2(500 BYTE)
                            note2,--VARCHAR2(500 BYTE)
                            note3,--VARCHAR2(500 BYTE)
                            nr_package,--NUMBER
                            nr_palets,--NUMBER
                            transaction_id,--NUMBER
                            quantity_r,--NUMBER
                            cart,--NUMBER
                            route,--NUMBER
                            price,--NUMBER
                            weight,--NUMBER
                            height,--NUMBER
                            width,--NUMBER
                            sdepth,--NUMBER
                            pro_id,--NUMBER
                            doc_id,--NUMBER
                            rabat1,--NUMBER
                            rabat2--NUMBER
                        )values(
                            json_ext.get_number(mShipment,'id'),--NUMBER--id,--NUMBER
                            json_ext.get_string(mShipment,'barcode'),--barcode,--VARCHAR2(25 BYTE)
                            json_ext.get_number(mShipment,'orderId'),--order_id,--NUMBER
                            json_ext.get_number(mShipment,'count'),--cnt,--NUMBER(22,5)
                            json_ext.get_number(mShipment,'scanedCount'),--scaned_count,--NUMBER
                            json_ext.get_number(mShipment,'scaned'),--scaned,--NUMBER
                            to_date(json_ext.get_string(mShipment,'scanedDate'),'yyyy-mm-dd hh24:mi:ss'),--scaned_date,--DATE
                            json_ext.get_string(mShipment,'werehouse'),--werehouse,--CHAR(150 BYTE)
                            json_ext.get_string(mShipment,'location'),--loc,--CHAR(60 BYTE)
                            json_ext.get_number(mShipment,'option'),--soption,--NUMBER
                            json_ext.get_number(mShipment,'completed'),--completed,--NUMBER
                            json_ext.get_number(mShipment,'sequence'),--seq,--NUMBER
                            json_ext.get_string(mShipment,'title'),--title,--VARCHAR2(150 BYTE)
                            json_ext.get_number(mShipment,'clientId'),--client_id,--NUMBER(38,0)
                            json_ext.get_string(mShipment,'palette'),--palette,--VARCHAR2(25 BYTE)
                            json_ext.get_string(mShipment,'type'),--stype,--CHAR(10 BYTE)
                            json_ext.get_number(mShipment,'product'),--product,--NUMBER
                            json_ext.get_string(mShipment,'terminal'),--terminal,--CHAR(10 BYTE)
                            json_ext.get_number(mShipment,'damageId'),--damage_id,--NUMBER
                            json_ext.get_string(mShipment,'damageDesc'),--damage_desc,--VARCHAR2(100 BYTE)
                            json_ext.get_number(mShipment,'loadingX'),--loading_x,--NUMBER(22,10)
                            json_ext.get_number(mShipment,'loadingY'),--loading_y,--NUMBER(22,10)
                            json_ext.get_number(mShipment,'unloadingX'),--unloading_x,--NUMBER(22,10)
                            json_ext.get_number(mShipment,'unloadingY'),--unloading_y,--NUMBER(22,10)
                            json_ext.get_number(mShipment,'active'),--active,--NUMBER
                            json_ext.get_number(mShipment,'quantityO'),--quantity_o,--NUMBER
                            json_ext.get_string(mShipment,'lot'),--lot,--VARCHAR2(18 BYTE)
                            json_ext.get_string(mShipment,'note'),--note,--VARCHAR2(500 BYTE)
                            json_ext.get_string(mShipment,'note1'),--note1,--VARCHAR2(500 BYTE)
                            json_ext.get_string(mShipment,'note2'),--note2,--VARCHAR2(500 BYTE)
                            json_ext.get_string(mShipment,'note3'),--note3,--VARCHAR2(500 BYTE)
                            json_ext.get_number(mShipment,'nrPackage'),--nr_package,--NUMBER
                            json_ext.get_number(mShipment,'nrPalets'),--nr_palets,--NUMBER
                            json_ext.get_number(mShipment,'transactionId'),--transaction_id,--NUMBER
                            json_ext.get_number(mShipment,'quantityR'),--quantity_r,--NUMBER
                            json_ext.get_number(mShipment,'cart'),--cart,--NUMBER
                            json_ext.get_number(mShipment,'route'),--route,--NUMBER
                            json_ext.get_number(mShipment,'price'),--price,--NUMBER
                            json_ext.get_number(mShipment,'weight'),--weight,--NUMBER
                            json_ext.get_number(mShipment,'height'),--height,--NUMBER
                            json_ext.get_number(mShipment,'width'),--width,--NUMBER
                            json_ext.get_number(mShipment,'depth'),--sdepth,--NUMBER
                            json_ext.get_number(mShipment,'proId'),--pro_id,--NUMBER
                            json_ext.get_number(mShipment,'docId'),--doc_id,--NUMBER
                            json_ext.get_number(mShipment,'rabat1'),--rabat1,--NUMBER
                            json_ext.get_number(mShipment,'rabat2')--rabat2,--NUMBER
                        );
                    end if;
                end loop;

            mAttachments := json_ext.get_json_list(mOrder,'attachments');
            FOR j IN 1..mAttachments.count
                LOOP
                    null;
                     mAttachment := json(mAttachments.get(j));
                     insert into attachments(
                          order_id,
                          atype,
                          attachment_date,
                          doc_id,
                          status,
                          order_type,
                          lot,
                          linkid,
                          sended,
                          worker_id,
                          image_comment,
                          email,
                          file_name,
                          shipment_id
                    )values(
                          json_ext.get_number(mAttachment,'orderId'),
                          json_ext.get_number(mAttachment,'type'),
                          to_Date(json_ext.get_string(mAttachment,'date'),'yyyy-mm-dd hh24:mi:ss'),
                          0,
                          0,
                           json_ext.get_number(mAttachment,'orderType'),
                          '',
                          json_ext.get_string(mAttachment,'linkId'),
                          0,
                          json_ext.get_number(mAttachment,'userId'),
                          json_ext.get_string(mAttachment,'comment'),
                          '',
                          json_ext.get_string(mAttachment,'path'),
                          json_ext.get_number(mAttachment,'shipmentId')
                    );
                end loop;
                begin
                    mOrderLogs := json_ext.get_json_list(mOrder,'orderLogs');
                    FOR j IN 1..mOrderLogs.count
                        LOOP 
                            mOrderLog := json(mOrderLogs.get(j));
                            
                            MERGE INTO orderlog o USING (SELECT 1 FROM DUAL) m 
                 ON ( o.id=  json_ext.get_number(mOrderLog,'id') and o.order_id=json_ext.get_number(mOrderLog,'orderId')) 
                WHEN NOT MATCHED THEN  
                            insert (
                                ID, 
                                ORDER_ID, 
                                TITLE, 
                                L_TYPE, 
                                LOG_DATE, 
                                USER_ID, 
                                L_COMMENT, 
                                STATUS, 
                                PRO_ID, 
                                COMPANY_ID,
                                FVO,
                                order_status
                            ) values(
                                json_ext.get_number(mOrderLog,'id'),
                                json_ext.get_number(mOrderLog,'orderId'),
                                json_ext.get_string(mOrderLog,'title'),
                                json_ext.get_number(mOrderLog,'type'),
                                to_date(json_ext.get_string(mOrderLog,'logDate'),'yyyy-mm-dd hh24:mi:ss'),
                                json_ext.get_number(mOrderLog,'userId'),
                                json_ext.get_string(mOrderLog,'comment'),
                                json_ext.get_number(mOrderLog,'status'),
                                json_ext.get_number(mOrderLog,'proId'),
                                json_ext.get_number(mOrderLog,'companyId') ,
                                json_ext.get_number(mOrderLog,'fvo') ,
                                json_ext.get_number(mOrderLog,'orderStatus') 
                            );
                        end loop;
                exception when others then
                    null;
                end;
            if(json_ext.get_number(mOrder,'type')=55) then
                update orders set terminal='-1' where order_id=json_ext.get_number(mOrder,'orderId');
            end if;
        end loop;
END;

/
--------------------------------------------------------
--  DDL for Procedure MORDERS_GET_DOCUMENT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."MORDERS_GET_DOCUMENT" (token in VARCHAR2, documentId in VARCHAR2, path in varchar2, docType in number) is
    mContent varchar2(32000);
    pOut clob;
    l_response  json;
    mAttachment json;
    zacetek number;
    konec number;
    image clob;
    len number := 32000;
    str_length2 number;
    str_length number;
    sql_error varchar2(32000);
BEGIN
begin
 --DBMS_OUTPUT.PUT_line('DOCUEMMTN ID -'  ||documentId || ' start zahteve' || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));
     url_connect('http://192.168.0.10/MOBILNO/Mobile/webresources/mOrders/API/getPhoto?DocumentId='||documentId,mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
    --   DBMS_OUTPUT.PUT_line(' konec yahteve' || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));

    --if(dbms_lob.instr(pout,'"status":0')>0) then
      --  INSERT INTO ferbit.SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,ID_RECORD,sql_stavek)
     --   VALUES (0, 'mor_get_doc',Sql_error,0,'url_connect',0,'slika prevzeta['||documentId||']');  
    
      -- dbms_output.put_line('Original clob size: ' || dbms_lob.getlength(pout));
      
        dbms_lob.createtemporary(image, false);
        zacetek := dbms_lob.instr(pout,'content');
        INSERT INTO SQL_ERROR(OPIS_NAPAKE)VALUES('slika zac '||zacetek); 
        if(zacetek>0) then
            zacetek:=zacetek+10;
            konec := dbms_lob.instr(pout,'"',zacetek);
            str_length:= konec-zacetek;
            
           -- INSERT INTO ferbit.SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,ID_RECORD,sql_stavek)
          --  VALUES (0, 'mor_get_doc',Sql_error,0,'url_connect',0,'velikost clob-a['||documentId||']'||str_length);  
           while str_length > 0 loop
             str_length2 := least(str_length, 32768);
             str_length := str_length - str_length2;
             dbms_lob.copy(image, pout, str_length2, dbms_lob.getlength(image) + 1, zacetek);
             zacetek := zacetek + str_length2;
           end loop;
         
          --  INSERT INTO ferbit.SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,ID_RECORD,sql_stavek)
          --  VALUES (0, 'mor_get_doc',Sql_error,0,'url_connect',0,'začetek vpisovanja['||documentId||']');  
            if(docType=10 or docType=55) then
                write_to_file('DOKUMENTI', path,image);
            else
                write_to_file('SKLADISCE', path,image);
            end if;
            update attachments set status=1 where linkid=documentid;
            INSERT INTO ferbit.SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,ID_RECORD,sql_stavek)
            VALUES (0, 'mor_get_doc',Sql_error,0,'url_connect',0,'koenc vpisovanja['||documentId||']');  
        end if;
  --  end if;
exception when others then
    null;
    sql_error := SUBSTR(SQLERRM, 1, 250);
    ERROR_LOG(1,'download doc',Sql_error,0,'mord_get_doc',0); 
end;
end;

/
--------------------------------------------------------
--  DDL for Procedure MORDERS_GET_UPDATE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."MORDERS_GET_UPDATE" is
    token varchar2(1000):='';
    TYPE VRNI_rt IS RECORD(document_id varchar2(10000), path varchar2(10000), docType number);
    l_VRNI   VRNI_rt;
    b_kurzor        sys_refcursor;
    DateFrom date;
    DateTo date;
begin


select trim(tekst) into token from ferbit.nastavi where kljuc='morders_token';
    begin
      select to_date(tekst,'yyyy-mm-dd hh24:mi:ss') into DateFrom from ferbit.nastavi where  kljuc='morders_PRENOS_DATUM';
    exception when others then
      DateFrom := sysdate-1;
    end;
    DateTo := sysdate;
   
  --  INSERT INTO SQL_ERROR(OPIS_NAPAKE)VALUES('DATEFROM '||to_char(datefrom,'dd.mm.yyyy hh24:mi:ss'));
-- INSERT INTO SQL_ERROR(OPIS_NAPAKE)VALUES('DateTo '||to_char(DateTo,'dd.mm.yyyy hh24:mi:ss')); 
    morders_get_data(token,DateFrom,DateTo);
    OPEN b_kurzor FOR 'select linkid, file_name, ORDER_TYPE from attachments where nvl(status,0)=0';
                     LOOP
                        FETCH b_kurzor INTO l_VRNI;
                        EXIT WHEN b_kurzor%NOTFOUND; 
                         morders_GET_document(token, l_VRNI.document_id, l_vrni.path,l_vrni.docType);  
                    END LOOP;
        close b_kurzor;
        
        
        MERGE INTO ferbit.nastavi n
            USING (select 1 from dual) m
            ON (n.kljuc='morders_PRENOS_DATUM')
          WHEN MATCHED THEN
            UPDATE SET n.tekst=to_char(DateTo,'YYYY-MM-DD HH24:MI:SS')
          WHEN NOT MATCHED THEN
            INSERT (kljuc, tekst)
            VALUES ('morders_PRENOS_DATUM', to_char(DateTo,'YYYY-MM-DD HH24:MI:SS'));
        

        --update ferbit.nastavi set tekst=to_char(DateTo,'YYYY-MM-DD HH24:MI:SS') where kljuc='morders_PRENOS_DATUM';
        commit;
         -- DBMS_OUTPUT.PUT_LINE('end '||to_char(sysdate,'yyyy-MM-dd hh24:mi:ss'));
end;

/
--------------------------------------------------------
--  DDL for Procedure MORDERS_INSERT_ARTICLES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."MORDERS_INSERT_ARTICLES" is
    pOut clob;
    l_response  json;
    token varchar2(1000):='';
    TYPE VRNI_rt IS RECORD(document_id varchar2(10000));
    l_VRNI   VRNI_rt;
    b_kurzor        sys_refcursor;
    mArticeles json_list;
    mArticle json;
    syncObject json;
    tmp json;
    CURSOR C_ARTICLES IS SELECT ID,TITLE,TYPE,BARCODE,TYPEART,LAST_date,COMPANY,NOTE1,NOTE2,NOTE3,NOTE4,NOTE5,PRICE FROM ARTICLES;
    mContent clob;
BEGIN

        select trim(tekst) into token from ferbit.nastavi where kljuc='morders_token';

        mArticeles:=json_list();
        FOR article IN C_ARTICLES LOOP
            mArticle := json();
            mArticle.put('id', article.id);
            mArticle.put('title', nvl(trim(article.title),''));
            mArticle.put('barcode', nvl(trim(article.barcode),''));
            mArticle.put('type', nvl(trim(article.type),''));
            mArticle.put('typeart', nvl(trim(article.typeart),''));
            mArticle.put('last', nvl(to_char(article.last_date,'yyyy-mm-dd hh24:mi:ss'),'1990-01-01 00:00:00'));
            mArticle.put('company', 0);
            mArticle.put('price', nvl(article.price,0));
            mArticle.put('note1', nvl(trim(article.note1),''));
            mArticle.put('note2', nvl(trim(article.note2),''));
            mArticle.put('note3', nvl(trim(article.note3),''));
            mArticle.put('note4', nvl(trim(article.note4),''));
            mArticle.put('note5', nvl(trim(article.note5),''));
            mArticeles.append(mArticle.to_json_value);
        end loop;
         syncObject := json();
         syncObject.put('articles',mArticeles);

         DBMS_LOB.CREATETEMPORARY(mContent,true);
         syncObject.to_clob(mContent,true);
     --    INSERT INTO TEST_LOG(C_DATA) VALUES(mcontent);
         dbms_output.put_line('----');
         url_connect('http://192.168.0.10/MOBILNO/Mobile/webresources/mOrders/API/insertArticles',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
--        url_connect('http://91.240.216.90:9080/MOBILNO/Mobile/webresources/mOrders/API/insertArticles',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
         DBMS_OUTPUT.PUT_line('konec- '||pOut);
end;

/
--------------------------------------------------------
--  DDL for Procedure MORDERS_INSERT_ORDERS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."MORDERS_INSERT_ORDERS" (orderId IN number)  IS
    pOut clob;
    l_response  json;
    token varchar2(1000);
    mOrders json_list;
    mOrder json;

    mShipments json_list;
    mShipment json;
    ry_shipment  SHIPMENTS%ROWTYPE;
    shipmentsCur        sys_refcursor;
    queryShipments varchar2(30000):='';

    syncObject json;
    CURSOR C_ORDERS IS SELECT * FROM orders WHERE active in (1);-- ;and order_id=orderId;
    mContent clob;

    inserted json_list;
BEGIN

    select trim(tekst) into token from ferbit.nastavi where kljuc='morders_token';

    mOrders := json_list();
    for corder in c_orders
    loop
        mOrder := json();
        morder.put('orderId',corder.order_id);
        mOrder.put('workerId',nvl(corder.worker_id,0));
        mOrder.put('orderDate',to_char(nvl(corder.order_date,sysdate),'yyyy-mm-dd hh24:mi:ss'));
        mOrder.put('deliveryDate',to_char(nvl(corder.delivery_date,sysdate),'yyyy-mm-dd hh24:mi:ss'));
        mOrder.put('completeDate',to_char(nvl(corder.complete_date,sysdate),'yyyy-mm-dd hh24:mi:ss'));
        mOrder.put('type',nvl(corder.otype,0));
        mOrder.put('typeD',corder.type_d);
        mOrder.put('count',nvl(corder.cnt,0));
        mOrder.put('vehicleId',nvl(corder.vehicle_id,0));
        mOrder.put('vehicle',corder.vehicle);
        mOrder.put('terminal',corder.terminal);
        mOrder.put('seq',nvl(corder.seq,0));
        mOrder.put('company',nvl(corder.company,0));
        mOrder.put('bussinesUnit',nvl(corder.bussines_unit,0));
        mOrder.put('customer',corder.customer);
        mOrder.put('location',corder.loc);
        mOrder.put('descCode',corder.desc_code);
        mOrder.put('active',nvl(corder.active,0));
        mOrder.put('scanedCount',nvl(corder.scaned_count,0));
        mOrder.put('ended',nvl(corder.ended,0));
        mOrder.put('transactionId',nvl(corder.transaction_id,0));
        mOrder.put('signator',corder.signator);
        mOrder.put('sender',corder.sender);
        mOrder.put('reciver',corder.reciver);
        mOrder.put('route',nvl(corder.route,0));
        mOrder.put('scaned',corder.scaned);--datum
        mOrder.put('program',corder.oprogram);
        mOrder.put('note1',corder.note1);
        mOrder.put('cart',corder.cart);
        mOrder.put('weight',nvl(corder.weight,0));
        mOrder.put('height',nvl(corder.height,0));
        mOrder.put('width',nvl(corder.width,0));
        mOrder.put('depth',nvl(corder.odepth,0));
        mOrder.put('proId',nvl(corder.pro_id,0));
        mOrder.put('stitle',corder.stitle);
        mOrder.put('saddress',corder.saddress);
        mOrder.put('spostofficenumber',corder.spostofficenumber);
        mOrder.put('scity',corder.scity);
        mOrder.put('rtitle',corder.rtitle);
        mOrder.put('raddress',corder.raddress);
        mOrder.put('rpostofficenumber',corder.rpostofficenumber);
        mOrder.put('rcity',corder.rcity);
        mOrder.put('cmr',corder.cmr);
        mOrder.put('note2',corder.note2);
        mOrder.put('note3',corder.note3);
        mOrder.put('temperature1',nvl(corder.temperature_1,0));
        mOrder.put('temperature2',nvl(corder.temperature_2,0));
        mOrder.put('nrPalettes',nvl(corder.nr_palettes,0));
        mOrder.put('note4',corder.note4);
        mOrder.put('note5',corder.note5);
        mOrder.put('note6',corder.note6);
        mOrder.put('note6',corder.note6);
        mOrder.put('stelephone',corder.s_telephone);
        mOrder.put('rtelephone',corder.r_telephone);
        mOrder.put('loadingLat',nvl(corder.loading_lat,0));
        mOrder.put('loadingLng',nvl(corder.loading_lng,0));
        mOrder.put('unloadingLat',nvl(corder.unloading_lat,0));
        mOrder.put('unloadingLng',nvl(corder.unloading_lng,0));
        mOrder.put('trailer',corder.trailer);
        mOrder.put('trailer_id',nvl(corder.trailer_id,0));
        mOrder.put('rabat1',nvl(corder.rabat1,0));
        mOrder.put('rabat2',nvl(corder.rabat2,0));
        mOrder.put('cash',nvl(corder.cash,0));
        mOrder.put('vat22',nvl(corder.vat22,0));
        mOrder.put('vat95',nvl(corder.vat95,0));
        mOrder.put('price',nvl(corder.price,0));
        mOrder.put('osnova1',nvl(corder.osnova1,0));
        mOrder.put('osnova2',nvl(corder.osnova2,0));
        mOrder.put('invoiceNumber',nvl(corder.invoice_Number,0));
        morder.put('orderLogs',json_list());
        morder.put('orderLogs',json_list());
        mShipments:=json_list();

        queryShipments := 'select * from shipments where order_id='||json_ext.get_number(mOrder,'orderId');
        OPEN shipmentsCur FOR queryShipments;
                 LOOP
                    FETCH shipmentsCur INTO ry_shipment;
                    EXIT WHEN shipmentsCur%NOTFOUND;
                    DBMS_OUTPUT.PUT_LINE('adding ' || ry_shipment.id);
                        mShipment := json();
                        mShipment.put('id',ry_shipment.id);
                        mShipment.put('barcode',ry_shipment.barcode);
                        mShipment.put('orderId',ry_shipment.order_id);
                        mShipment.put('cnt',nvl(ry_shipment.cnt,0));
                        mShipment.put('scanedCount',nvl(ry_shipment.scaned_count,0));
                        mShipment.put('scaned',nvl(ry_shipment.scaned,0));
                        mShipment.put('scanedDate',to_char(ry_shipment.scaned_date,'yyyy-mm-dd hh24:mi:ss'));
                        mShipment.put('werehouse',ry_shipment.werehouse);
                        mShipment.put('loc',ry_shipment.loc);
                        mShipment.put('soption',nvl(ry_shipment.soption,0));
                        mShipment.put('completed',nvl(ry_shipment.completed,0));
                        mShipment.put('seq',nvl(ry_shipment.seq,0));
                        mShipment.put('title',ry_shipment.title);
                        mShipment.put('clientId',nvl(ry_shipment.client_id,0));
                        mShipment.put('palette',ry_shipment.palette);
                        mShipment.put('stype',ry_shipment.stype);
                        mShipment.put('product',nvl(ry_shipment.product,0));
                        mShipment.put('terminal',ry_shipment.terminal);
                        mShipment.put('damageId',nvl(ry_shipment.damage_id,0));
                        mShipment.put('damageDesc',ry_shipment.damage_desc);
                        mShipment.put('loadingX',nvl(ry_shipment.loading_x,0));
                        mShipment.put('loadingY',nvl(ry_shipment.loading_y,0));
                        mShipment.put('unloadingX',nvl(ry_shipment.unloading_x,0));
                        mShipment.put('unloadingY',nvl(ry_shipment.unloading_y,0));
                        mShipment.put('active',nvl(ry_shipment.active,0));
                        mShipment.put('quantityO',nvl(ry_shipment.quantity_o,0));
                        mShipment.put('lot',ry_shipment.lot);
                        mShipment.put('note',ry_shipment.note);
                        mShipment.put('note1',ry_shipment.note1);
                        mShipment.put('note2',ry_shipment.note2);
                        mShipment.put('note3',ry_shipment.note3);
                        mShipment.put('nrPackage',nvl(ry_shipment.nr_package,0));
                        mShipment.put('nrPalets',nvl(ry_shipment.nr_palets,0));
                        mShipment.put('transactionId',nvl(ry_shipment.transaction_id,0));
                        mShipment.put('quantityR',nvl(ry_shipment.quantity_r,0));
                        mShipment.put('cart',nvl(ry_shipment.cart,0));
                        mShipment.put('route',nvl(ry_shipment.route,0));
                        mShipment.put('price',nvl(ry_shipment.price,0));
                        mShipment.put('weight',nvl(ry_shipment.weight,0));
                        mShipment.put('height',nvl(ry_shipment.height,0));
                        mShipment.put('width',nvl(ry_shipment.width,0));
                        mShipment.put('sdepth',nvl(ry_shipment.sdepth,0));
                        mShipment.put('proId',nvl(ry_shipment.pro_id,0));
                        mShipment.put('docId',nvl(ry_shipment.doc_id,0));
                        mShipment.put('rabat1',nvl(ry_shipment.rabat1,0));
                        mShipment.put('rabat2',nvl(ry_shipment.rabat2,0));
                        mShipments.append(mShipment.to_json_value);
                END LOOP;
        close shipmentsCur;

        mOrder.put('shipments', mShipments);
        mOrders.append(mOrder.to_json_value);
    end loop;

    syncObject := json();
    syncObject.put('orders',mOrders);
 
    DBMS_LOB.CREATETEMPORARY(mContent,true);
    syncObject.to_clob(mContent,true);
--
    -- url_connect('http://localhost/Mobile/webresources/mOrders/API/insertOrders',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
     url_connect('http://192.168.0.10/MOBILNO/Mobile/Nov/webresources/mOrders/API/insertOrders',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
  --  dbms_output.put_line(pout);
    l_response := json(pout);
    inserted := json_ext.get_json_list(l_response,'inserted');

    FOR i IN 1..inserted.count
        LOOP
            --dbms_output.put_line('-- '||json_ext.get_number(json(inserted.get(i)),'newOrderId'));
           update orders set active=2 where order_id=json_ext.get_number(json(inserted.get(i)),'newOrderId');
           commit;
        end loop;
        commit;
end;

/

  GRANT EXECUTE ON "MOBILNO"."MORDERS_INSERT_ORDERS" TO "FERBIT";
--------------------------------------------------------
--  DDL for Procedure MORDERS_INSERT_USERS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."MORDERS_INSERT_USERS" (mUserId in number) is
    pOut clob;
    l_response  json;
    token varchar2(1000):='';
    TYPE VRNI_rt IS RECORD(document_id varchar2(10000));
    l_VRNI   VRNI_rt;
    b_kurzor        sys_refcursor;
    mUser json;
    syncObject json;
    tmp json;
    CURSOR C_USER IS SELECT * FROM USERS where id=mUserId;
    mContent clob;
BEGIN

        select trim(tekst) into token from ferbit.nastavi where kljuc='morders_token';
--dbms_crypto.hash(utl_raw.cast_to_raw(''), dbms_crypto.HASH_MD5);




        mUser:=json();
        FOR luser IN C_USER LOOP
            muser.put('id',luser.id);
            muser.put('name',luser.name);
            muser.put('lastName',luser.last_name);
            muser.put('username',luser.username);
            muser.put('password',DBMS_OBFUSCATION_TOOLKIT.md5(input => UTL_I18N.STRING_TO_RAW (luser.password, 'AL32UTF8')));
            muser.put('pin',luser.pin);
            muser.put('longitude',nvl(luser.longitude,0));
            muser.put('latitude',nvl(luser.latitude,0));
            muser.put('device',luser.device);
            muser.put('lastTime',luser.last_time);
            muser.put('admin',nvl(luser.admin,0));
            muser.put('status',nvl(luser.status,0));
            muser.put('company',0);
        end loop;
         syncObject := json();
         syncObject.put('user',mUser);

         DBMS_LOB.CREATETEMPORARY(mContent,true);
         syncObject.to_clob(mContent,true);
         dbms_output.put_line('----');
         dbms_output.put_line(mContent);
         
         url_connect('http://192.168.0.10/MOBILNO/Mobile/webresources/mOrders/API/createUser',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
         DBMS_OUTPUT.PUT(pOut);
end;

/
--------------------------------------------------------
--  DDL for Procedure MORDERS_SEND_MESSAGES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."MORDERS_SEND_MESSAGES" is
     CURSOR cur IS select * from ferbit.izhodnisms where status IN (13,2,22) AND STANJE in (0,1) and trim(gsm) in ('999');--,10279,10282,10283,10284,10285,10286,10287);
     sms json;
     smsData json_list;
     mContent  clob;
     pout clob;
     token varchar2(1000);
     syncObject json;
     response json;
begin
    select trim(tekst) into token from ferbit.nastavi where kljuc='morders_token';
    smsData := json_list();
    for tmp in cur loop
--    DBMS_OUTPUT.PUT_LINE(nvl(length(trim(tmp.gsm)),0));
        if(TMP.STATUS in (13,22)) then
          insert into clobtest values('status 12');
  --      DBMS_OUTPUT.PUT_LINE('MANJŠE OD 9 FCM');
            sms := json();
            sms.put('id',tmp.stevec);
            sms.put('content',tmp.vsebina);
            sms.put('fromId',NVL(tmp.posiljatelj,0));
            sms.put('fromTitle','PISARNA');
            sms.put('toId',TRIM(NVL(tmp.gsm,' ')));
            sms.put('toTitle',TRIM(NVL(tmp.gsm,' ')));
            sms.put('dateSent',to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));
            sms.put('status',0);
            sms.put('proId',tmp.vezn);
            sms.put('messageType','fcmVehicle');
            smsData.append(sms.to_json_value);
        else
            if(nvl(tmp.posiljatelj,0)>0) then
                sms := json();
                sms.put('id',tmp.stevec);
                sms.put('content',tmp.vsebina);
                sms.put('fromId',tmp.posiljatelj);
                sms.put('fromTitle','sms');
                sms.put('toId',0);
                sms.put('toTitle',tmp.gsm);
                sms.put('dateSent',to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));
                sms.put('status',0);
                sms.put('proId',tmp.vezn);
                sms.put('messageType','sms');
                smsData.append(sms.to_json_value);
            end if;
        end if;
    end loop;
    syncObject := json();
    syncObject.put('messages',smsData);
    DBMS_LOB.CREATETEMPORARY(mContent,true);
    syncObject.to_clob(mContent,true);
    insert into clobtest values('test');
   insert into clobtest values(mContent);
   commit;
    --dbms_output.put_line(mContent);
    url_connect('http://192.168.0.10/MOBILNO/Mobile/Nov/webresources/mOrders/API/insertMessages/',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
   -- dbms_output.put_line('test '||pout);
   insert into clobtest values(pout);
    response:=json(pout);

    --dbms_output.put_line('status '||json_ext.get_number(response,'status'));
    if(json_ext.get_number(response,'status')=0) then
          FOR i IN 1..json_ext.get_json_list(syncObject,'messages').count loop
            sms := json(json_ext.get_json_list(syncObject,'messages').get(i));
            dbms_output.put_line('update izhodnisms set status=1 where stevec = '||json_ext.get_number(sms,'id'));
            update ferbit.izhodnisms set status=1 where stevec = json_ext.get_number(sms,'id');
          end loop;
    end if;
    commit;
end;

/
--------------------------------------------------------
--  DDL for Procedure MORDERS_SEND_WITH_JOB
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."MORDERS_SEND_WITH_JOB" (l_time in number) is
    Sql_error varchar2(1000);

 BEGIN
  INSERT INTO SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,SQL_STAVEK  ) 
      VALUES ( 3,'SEND ORDERS','OK',0,'SEND','' ); 
 BEGIN
    DBMS_SCHEDULER.RUN_JOB(job_name => '"FERBIT"."MORDERS_INSERT_ORDER_JOB"', USE_CURRENT_SESSION => FALSE);
END;
        /*
  BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'MORDERS_SEND_JOB'
      ,start_date      => sysdate+(2/(24*60*60))
      ,repeat_interval => NULL
      ,end_date        => NULL
      ,job_class       => 'DEFAULT_JOB_CLASS'
      ,job_type        => 'PLSQL_BLOCK'
      ,job_action      => 'DECLARE BEGIN MOBILNO.MORDERS_INSERT_ORDERS(0); END;'
      ,comments        => NULL
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MORDERS_SEND_JOB'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MORDERS_SEND_JOB'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_OFF);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MORDERS_SEND_JOB'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MORDERS_SEND_JOB'
     ,attribute => 'MAX_RUNS');
  BEGIN
    SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
      ( name      => 'MORDERS_SEND_JOB'
       ,attribute => 'STOP_ON_WINDOW_CLOSE'
       ,value     => FALSE);
  EXCEPTION
    -- could fail if program is of type EXECUTABLE...
    WHEN OTHERS THEN
      NULL;
  END;
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MORDERS_SEND_JOB'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 3);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MORDERS_SEND_JOB'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MORDERS_SEND_JOB'
     ,attribute => 'AUTO_DROP'
     ,value     => TRUE);
     
     sys.DBMS_SCHEDULER.enable(
             name => '"MORDERS_SEND_JOB"');
END;  
        */
        
end ;

/

  GRANT EXECUTE ON "MOBILNO"."MORDERS_SEND_WITH_JOB" TO "FERBIT";
--------------------------------------------------------
--  DDL for Procedure MORDESR_RUN_GEOCODING
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."MORDESR_RUN_GEOCODING" (p in number) is
   --cursor stranke  is select sifra from ferbit.klienti where nvl(ulica_latitude,0)=0 or nvl(ulica_longitude,0)=0 and skype<>'-1';
    cursor stranke is select sifra 
 from ferbit.klienti 
 where sifra in (select prejemnik 
                  from ferbit.nakladi 
                  where leto in ('21','20')
                  group by prejemnik 
                  union all 
                  select naklada
                  from ferbit.nakladi 
                  where leto in ('21','20') 
                  group by naklada) and sifra not in (select id from klienti_narejeni) ;
    
    
    i number ;
begin
    i:=0;
   for corder in stranke
    loop
        if(i>200) then
            EXIT;   
        end if; 
        --dbms_output.put_line(corder.sifra);
        morders_geocode_client(corder.sifra);
        insert into klienti_narejeni(id) values (corder.sifra);
        i:=i+1;
    end loop;
    
  --   select count(sifra) into i from ferbit.klienti where sifra in (select prejemnik from ferbit.nakladi where leto in ('21','20') group by prejemnik union all select naklada from ferbit.nakladi where leto in ('21','20') group by naklada) and (nvl(ulica_longitude,0)=0 or nvl(ulica_latitude,0)=0);
 -- select count(sifra) into i from ferbit.klienti where nvl(ulica_latitude,0)=0 or nvl(ulica_longitude,0)=0 and length(trim(naslov5)||', '||trim(posta)||', '||trim(naslov4)||', '||trim(drzava))>10;
 
 
  select count(sifra) into i
 from ferbit.klienti 
 where sifra in (select prejemnik 
                  from ferbit.nakladi 
                  where leto in ('21','20')
                  group by prejemnik 
                  union all 
                  select naklada
                  from ferbit.nakladi 
                  where leto in ('21','20') 
                  group by naklada) and sifra not in (select id from klienti_narejeni) ;  
 
  dbms_output.put_line('se : ' ||i);
end;

/
--------------------------------------------------------
--  DDL for Procedure URL_CONNECT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."URL_CONNECT" (pUrl in varchar2,pContent IN CLOB, pMethod IN VARCHAR2, pOut OUT CLOB, auth in varchar2 default '', contentType in varchar2 default 'application/json;charset=utf-8' )IS
/*
      mmessage_id   -  id sporoÄŤila
      muser_id      -  za koga je sprocilo; lahko je id uporabnika ali vozila(odvisno kak mMessageType poĹˇiljamo)
      mtype         -  tip sporocila; 0 sinhronizacija; 1 sporoÄŤilo
      mcontent      -  vsebina
      mfromuser     -  od koga je sporocilo 1-centrala
      msendeddate   -  datum posiljanja
      mMessageType  -  ÄŤe gre za nalog 0; ÄŤe je sporÄŤilo 1
      */
  t_http_req     utl_http.req;
  t_http_resp    utl_http.resp;
  t_respond      VARCHAR2(30000);
  t_start_pos    INTEGER := 1;
  t_output       VARCHAR2(2000);
  l_mcontent     varchar2(10000);
  t_device       VARCHAR2(2000);
  t_data         VARCHAR(30000);
  sql_error      VARCHAR2(30000);
  l_error        number;
  l_napak        number;
  l_procedure    CHAR(15):='url_connect';
  buffer varchar2(4000);
  response_Text  varchar2(32000);
   l_buffer     varchar2(32767);
   l_length      number:=0;
   l_offset number:=1;
   amount number :=32767;
BEGIN
    l_length := DBMS_LOB.GETLENGTH(pContent);

    --utl_http.set_wallet ('file:/home/oracle/wallet', 'password123');
    utl_http.Set_body_charset('UTF-8');
    utl_http.Set_transfer_timeout(300);
    t_http_req := utl_http.Begin_request(pUrl , pMethod);
    utl_http.Set_header(t_http_req, 'Content-Type',contentType);
    utl_http.Set_header(t_http_req, 'Connection', 'keep-alive');

    if(nvl(length(trim(auth)),0)<>0) then
        utl_http.Set_header(t_http_req, 'Authorization', auth);
    end if;


    if(trim(pMethod)='POST') then
        if(l_length>0) then 
            utl_http.Set_header(t_http_req, 'Content-Length',l_length);
            --utl_http.Write_text(t_http_req, pContent);
           -- l_length := DBMS_LOB.GETLENGTH(pContent);

            while(l_offset < l_length)
                          loop
                             dbms_lob.read(pContent, amount, l_offset, l_buffer);
                             UTL_HTTP.WRITE_TEXT(r    => t_http_req, data => l_buffer);
                             l_offset := l_offset + amount;

                          end loop;

        else
            utl_http.Set_header(t_http_req, 'Content-Length',0);
        end if;
    else
        utl_http.Set_header(t_http_req, 'Content-Length',0);
    end if ;

    t_http_resp := utl_http.Get_response(t_http_req);
    dbms_lob.createtemporary(pOut, false);

     begin
        loop
          utl_http.read_text(t_http_resp, l_buffer, 32000);
          dbms_lob.writeappend (pOut, length(l_buffer), l_buffer);
        end loop;
      exception
        when utl_http.end_of_body then
          utl_http.end_response(t_http_resp);
      end;

                        --    DBMS_OUTPUT.PUT_LINE(pout);
    EXCEPTION
    WHEN OTHERS THEN
         l_error   := SQLCODE;
        l_napak   := 1;
         DBMS_OUTPUT.PUT_LINE('SQLERRM urlConnect: '||SQLERRM);
        Sql_error := SUBSTR(SQLERRM, 1, 250);
      --  insert into clob_test(cdata) values(Sql_error);
  
        INSERT INTO ferbit.SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,ID_RECORD,sql_stavek)
        VALUES (l_error, 'mob_url_connect',Sql_error,0,'url_connect',0,purl);  
        
        
END;

/


--------------------------------------------------------
--  DDL for Function BASE64DECODECLOBASBLOB_PLSQL
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "MOBILNO"."BASE64DECODECLOBASBLOB_PLSQL" (i_data_cl CLOB) 
return blob is
  v_out_bl blob;
  clob_size number;
  pos number;
  charBuff varchar2(32767);
  dBuffer RAW(32767);
  v_readSize_nr number;
  v_line_nr number;
begin
  dbms_lob.createTemporary
    (v_out_bl, true, dbms_lob.call);
  v_line_nr:=greatest(65, 
                 instr(i_data_cl,chr(10)),
                 instr(i_data_cl,chr(13)));
  v_readSize_nr:=
      floor(32767/v_line_nr)*v_line_nr;
  clob_size := dbms_lob.getLength(i_data_cl);
  pos := 1;

  WHILE (pos < clob_size) LOOP
    dbms_lob.read
      (i_data_cl, v_readSize_nr, pos, charBuff);
    dBuffer := UTL_ENCODE.base64_decode
      (utl_raw.cast_to_raw(charBuff));
    dbms_lob.writeAppend
     (v_out_bl,utl_raw.length(dBuffer),dBuffer);
    pos := pos + v_readSize_nr;
  end loop;
  return v_out_bl;
end;

/

--------------------------------------------------------
--  DDL for Procedure WRITE_TO_FILE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MOBILNO"."WRITE_TO_FILE" (
dir        in varchar2,
ime        in char,
documentIn        clob )
as
    t_data         BLOB;
    v_buffer       RAW(32767);
    v_buffer_size  BINARY_INTEGER;
    v_amount       BINARY_INTEGER;
    v_offset       NUMBER(38) := 1;
    v_chunksize    INTEGER;
    v_out_file     UTL_FILE.FILE_TYPE;
    dest_offset INTEGER := 1;
    src_offset INTEGER := 1;
    warning INTEGER;
    ctx INTEGER := DBMS_LOB.DEFAULT_LANG_CTX;
    
    sql_error      VARCHAR2(30000);
  l_error        number;
  l_napak        number;
begin

    if nvl((DBMS_LOB.getlength(documentIn ))/1024/1024,0) > 0 then
  --  dbms_output.put_line('zacetek convert '|| to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));
     t_Data := base64decodeclobasblob_plsql(documentIn);
 --dbms_output.put_line('konec convert '|| to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));


           v_chunksize := DBMS_LOB.GETCHUNKSIZE(t_Data);
            IF (v_chunksize < 32767) THEN
                v_buffer_size := v_chunksize;
            ELSE
                v_buffer_size := 32767;
            END IF;
            v_amount := v_buffer_size;
            DBMS_LOB.OPEN(t_data, DBMS_LOB.LOB_READONLY);
            v_out_file := UTL_FILE.FOPEN(
                location      => dir,
                filename      => ime,
                open_mode     => 'wb',
                max_linesize  => 32767);
            WHILE v_amount >= v_buffer_size
            LOOP
              DBMS_LOB.READ(
                  lob_loc    => t_Data,
                  amount     => v_amount,
                  offset     => v_offset,
                  buffer     => v_buffer);
              v_offset := v_offset + v_amount;
              UTL_FILE.PUT_RAW (
                  file      => v_out_file,
                  buffer    => v_buffer,
                  autoflush => true);
              UTL_FILE.FFLUSH(file => v_out_file);
            END LOOP;
            UTL_FILE.FFLUSH(file => v_out_file);
            UTL_FILE.FCLOSE(v_out_file);
            DBMS_LOB.CLOSE(t_data);
         end if;
end;

/
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "MOBILNO"."USERS" MODIFY ("ID" NOT NULL ENABLE);


--------------------------------------------------------
--  BLOCK FOR CREATING DBMS SCHEDULER JOB
--------------------------------------------------------
  BEGIN
    DBMS_SCHEDULER.CREATE_JOB (
            job_name => '"MOBILNO"."MORDERS_GET_UPDATE_JOB"',
            job_type => 'PLSQL_BLOCK',
            job_action => 'declare
                            begin
                                morders_get_update;
                                morders_insert_orders(0);
                            end;',
            number_of_arguments => 0,
            start_date => TO_TIMESTAMP_TZ('2022-03-22 13:30:28.000000000 EUROPE/PRAGUE','YYYY-MM-DD HH24:MI:SS.FF TZR'),
            repeat_interval => 'FREQ=MINUTELY;INTERVAL=2',
            end_date => NULL,
            enabled => FALSE,
            auto_drop => FALSE,
            comments => '');

         
     
 
    DBMS_SCHEDULER.SET_ATTRIBUTE( 
             name => '"MOBILNO"."MORDERS_GET_UPDATE_JOB"', 
             attribute => 'logging_level', value => DBMS_SCHEDULER.LOGGING_OFF);
      
  
    
    DBMS_SCHEDULER.enable(
             name => '"MOBILNO"."MORDERS_GET_UPDATE_JOB"');
END;
 