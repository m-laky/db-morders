--
-- Create Schema Script 
--   Database Version   : 11.2.0.2.0 
--   TOAD Version       : 9.5.0.31 
--   DB Connect String  : 192.168.0.10:1521/xe 
--   Schema             : MOBILNO 
--   Script Created by  : MOBILNO 
--   Script Created at  : 7/19/2022 1:22:50 pop. 
--   Physical Location  :  
--   Notes              :  
--

-- Object Counts: 
--   Functions: 1       Lines of Code: 31 
--   Indexes: 1         Columns: 1          
--   Object Privileges: 6 
--   Packages: 1        Lines of Code: 527 
--   Package Bodies: 1  Lines of Code: 3442 
--   Procedures: 16     Lines of Code: 1578 
--   Sequences: 4 
--   Tables: 13         Columns: 346        
--   Triggers: 7 


CREATE SEQUENCE ATT_AUTO
  START WITH 2
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE ERRAUTO
  START WITH 1071460
  MAXVALUE 999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE ORD_SEQ
  START WITH 15107
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE SHIP_AUTO
  START WITH 17
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE TABLE SHIPMENTS
(
  ID              NUMBER,
  BARCODE         VARCHAR2(25 BYTE),
  ORDER_ID        NUMBER,
  CNT             NUMBER(22,5)                  DEFAULT 0,
  SCANED_COUNT    NUMBER                        DEFAULT 0,
  SCANED          NUMBER                        DEFAULT 0,
  SCANED_DATE     DATE,
  WEREHOUSE       CHAR(150 BYTE)                DEFAULT 0,
  LOC             CHAR(60 BYTE)                 DEFAULT 0,
  SOPTION         NUMBER                        DEFAULT 0,
  COMPLETED       NUMBER                        DEFAULT 00,
  SEQ             NUMBER                        DEFAULT 0,
  TITLE           VARCHAR2(150 BYTE)            DEFAULT 0,
  CLIENT_ID       INTEGER                       DEFAULT 0,
  PALETTE         VARCHAR2(25 BYTE)             DEFAULT 0,
  STYPE           CHAR(10 BYTE)                 DEFAULT 0,
  PRODUCT         NUMBER                        DEFAULT 0,
  TERMINAL        CHAR(10 BYTE)                 DEFAULT 1,
  DAMAGE_ID       NUMBER                        DEFAULT 0,
  DAMAGE_DESC     VARCHAR2(100 BYTE)            DEFAULT 0,
  LOADING_X       NUMBER(22,10)                 DEFAULT 0,
  LOADING_Y       NUMBER(22,10)                 DEFAULT 0,
  UNLOADING_X     NUMBER(22,10)                 DEFAULT 0,
  UNLOADING_Y     NUMBER(22,10)                 DEFAULT 0,
  ACTIVE          NUMBER                        DEFAULT 1,
  QUANTITY_O      NUMBER                        DEFAULT 0,
  LOT             VARCHAR2(18 BYTE)             DEFAULT 0,
  NOTE            VARCHAR2(500 BYTE)            DEFAULT 0,
  NOTE1           VARCHAR2(500 BYTE)            DEFAULT 0,
  NOTE2           VARCHAR2(500 BYTE)            DEFAULT 0,
  NOTE3           VARCHAR2(500 BYTE)            DEFAULT 0,
  NR_PACKAGE      NUMBER                        DEFAULT 0,
  NR_PALETS       NUMBER                        DEFAULT 0,
  TRANSACTION_ID  NUMBER                        DEFAULT 0,
  QUANTITY_R      NUMBER                        DEFAULT 0,
  CART            NUMBER                        DEFAULT 0,
  ROUTE           NUMBER                        DEFAULT 0,
  PRICE           NUMBER                        DEFAULT 0,
  WEIGHT          NUMBER                        DEFAULT 0,
  HEIGHT          NUMBER                        DEFAULT 0,
  WIDTH           NUMBER                        DEFAULT 0,
  SDEPTH          NUMBER                        DEFAULT 0,
  PRO_ID          NUMBER,
  DOC_ID          NUMBER,
  RABAT1          NUMBER,
  RABAT2          NUMBER
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE ARTICLES
(
  ID         NUMBER                             DEFAULT 1,
  TITLE      VARCHAR2(1000 BYTE)                DEFAULT 'A',
  TYPE       VARCHAR2(200 BYTE)                 DEFAULT 1,
  BARCODE    VARCHAR2(50 BYTE)                  DEFAULT 'A',
  TYPEART    CHAR(100 BYTE)                     DEFAULT 'A',
  LAST_DATE  DATE,
  COMPANY    NUMBER,
  NOTE1      VARCHAR2(2000 BYTE),
  NOTE2      VARCHAR2(2000 BYTE),
  NOTE3      VARCHAR2(2000 BYTE),
  NOTE4      VARCHAR2(2000 BYTE),
  NOTE5      VARCHAR2(2000 BYTE),
  PRICE      NUMBER
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE ATTACHMENTS
(
  ID               NUMBER,
  ORDER_ID         NUMBER,
  ATYPE            NUMBER,
  ATTACHMENT_DATE  DATE,
  DOC_ID           NUMBER,
  STATUS           NUMBER                       DEFAULT 0,
  ORDER_TYPE       VARCHAR2(20 BYTE)            DEFAULT 0,
  LOT              VARCHAR2(50 BYTE),
  LINKID           VARCHAR2(400 BYTE),
  SENDED           NUMBER                       DEFAULT 0,
  WORKER_ID        NUMBER,
  IMAGE_COMMENT    VARCHAR2(400 BYTE),
  EMAIL            VARCHAR2(150 BYTE)           DEFAULT '-',
  FILE_NAME        VARCHAR2(4000 BYTE),
  SHIPMENT_ID      NUMBER
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE SQL_ERROR
(
  ZAP_ST           NUMBER,
  NAPAKA           NUMBER,
  OPIS_NAPAKE      VARCHAR2(255 BYTE),
  NAPAKA_SQL       NUMBER,
  OPIS_NAPAKE_SQL  VARCHAR2(255 BYTE),
  DODATNI_OPIS     VARCHAR2(50 BYTE),
  SQL_STAVEK       VARCHAR2(4000 BYTE),
  DATUM            DATE,
  ID_RECORD        NUMBER,
  ID_STAR          VARCHAR2(100 BYTE),
  PROCEDURA        VARCHAR2(50 BYTE),
  STRANKA          NUMBER,
  TRID             NUMBER
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE MLOG$_ORDERS
(
  ORDER_ID           NUMBER,
  WORKER_ID          NUMBER,
  ORDER_DATE         DATE,
  DELIVERY_DATE      DATE,
  COMPLETE_DATE      DATE,
  OTYPE              NUMBER,
  TYPE_D             VARCHAR2(150 BYTE),
  CNT                NUMBER,
  VEHICLE_ID         NUMBER,
  VEHICLE            VARCHAR2(20 BYTE),
  TERMINAL           CHAR(1000 BYTE),
  SEQ                NUMBER,
  COMPANY            NUMBER,
  BUSSINES_UNIT      NUMBER,
  CUSTOMER           NUMBER,
  LOC                VARCHAR2(40 BYTE),
  DESC_CODE          VARCHAR2(30 BYTE),
  ACTIVE             NUMBER,
  SCANED_COUNT       NUMBER,
  ENDED              NUMBER,
  TRANSACTION_ID     NUMBER,
  SIGNATOR           VARCHAR2(150 BYTE),
  SENDER             VARCHAR2(100 BYTE),
  RECIVER            VARCHAR2(100 BYTE),
  ROUTE              NUMBER,
  SCANED             DATE,
  OPROGRAM           VARCHAR2(30 BYTE),
  NOTE1              VARCHAR2(200 BYTE),
  CART               VARCHAR2(50 BYTE),
  WEIGHT             NUMBER,
  HEIGHT             NUMBER,
  WIDTH              NUMBER,
  ODEPTH             NUMBER,
  PRO_ID             NUMBER,
  STITLE             VARCHAR2(200 BYTE),
  SADDRESS           VARCHAR2(200 BYTE),
  SPOSTOFFICENUMBER  VARCHAR2(150 BYTE),
  SCITY              VARCHAR2(200 BYTE),
  RTITLE             VARCHAR2(200 BYTE),
  RADDRESS           VARCHAR2(200 BYTE),
  RPOSTOFFICENUMBER  VARCHAR2(150 BYTE),
  RCITY              VARCHAR2(200 BYTE),
  CMR                VARCHAR2(150 BYTE),
  NOTE2              VARCHAR2(2000 BYTE),
  NOTE3              VARCHAR2(2000 BYTE),
  TEMPERATURE_1      NUMBER,
  TEMPERATURE_2      NUMBER,
  NR_PALETTES        NUMBER,
  NOTE4              VARCHAR2(1000 BYTE),
  NOTE5              VARCHAR2(1000 BYTE),
  NOTE6              VARCHAR2(1000 BYTE),
  S_TELEPHONE        VARCHAR2(50 BYTE),
  R_TELEPHONE        VARCHAR2(50 BYTE),
  LOADING_LAT        NUMBER,
  LOADING_LNG        NUMBER,
  UNLOADING_LAT      NUMBER,
  UNLOADING_LNG      NUMBER,
  TRAILER            VARCHAR2(1000 BYTE),
  TRAILER_ID         NUMBER,
  RABAT1             NUMBER,
  RABAT2             NUMBER,
  DATE_MODIFIED      DATE,
  CASH               NUMBER,
  ZOI                VARCHAR2(1000 BYTE),
  EOR                VARCHAR2(1000 BYTE),
  BUSINESSPREMISE    VARCHAR2(1000 BYTE),
  ELECTRONICDEVICE   VARCHAR2(1000 BYTE),
  VAT22              NUMBER,
  VAT95              NUMBER,
  OSNOVA1            NUMBER,
  OSNOVA2            NUMBER,
  PRICE              NUMBER,
  FURS_ORDER         VARCHAR2(1000 BYTE),
  INVOICE_NUMBER     NUMBER,
  MLOG_SYSDATE       DATE,
  MLOG_USER          VARCHAR2(30 BYTE),
  MLOG_AKCIJA        VARCHAR2(1 BYTE)
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE ORDERLOG
(
  ID            NUMBER,
  ORDER_ID      NUMBER,
  TITLE         VARCHAR2(2000 BYTE),
  L_TYPE        NUMBER,
  LOG_DATE      DATE,
  USER_ID       NUMBER,
  L_COMMENT     VARCHAR2(2000 BYTE),
  STATUS        NUMBER,
  PRO_ID        NUMBER,
  COMPANY_ID    NUMBER,
  FVO           NUMBER,
  ORDER_STATUS  NUMBER,
  TRAVEL_ID     NUMBER
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE USERS
(
  ID         NUMBER                             NOT NULL,
  NAME       VARCHAR2(50 BYTE),
  LAST_NAME  VARCHAR2(150 BYTE),
  USERNAME   VARCHAR2(100 BYTE),
  PASSWORD   VARCHAR2(300 BYTE),
  PIN        VARCHAR2(20 BYTE),
  LONGITUDE  NUMBER                             DEFAULT 0,
  LATITUDE   NUMBER                             DEFAULT 0,
  DEVICE     VARCHAR2(3000 BYTE),
  LAST_TIME  DATE,
  ADMIN      NUMBER                             DEFAULT 0,
  STATUS     NUMBER                             DEFAULT 0,
  COMPANY    NUMBER
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE ORDERS
(
  ORDER_ID           NUMBER                     DEFAULT 0,
  WORKER_ID          NUMBER                     DEFAULT 0,
  ORDER_DATE         DATE,
  DELIVERY_DATE      DATE,
  COMPLETE_DATE      DATE,
  OTYPE              NUMBER                     DEFAULT 0,
  TYPE_D             VARCHAR2(150 BYTE)         DEFAULT 0,
  CNT                NUMBER                     DEFAULT 0,
  VEHICLE_ID         NUMBER                     DEFAULT 0,
  VEHICLE            VARCHAR2(20 BYTE)          DEFAULT 0,
  TERMINAL           CHAR(1000 BYTE)            DEFAULT 0,
  SEQ                NUMBER                     DEFAULT 0,
  COMPANY            NUMBER                     DEFAULT 0,
  BUSSINES_UNIT      NUMBER                     DEFAULT 0,
  CUSTOMER           NUMBER                     DEFAULT 0,
  LOC                VARCHAR2(40 BYTE)          DEFAULT 0,
  DESC_CODE          VARCHAR2(30 BYTE)          DEFAULT 0,
  ACTIVE             NUMBER                     DEFAULT 0,
  SCANED_COUNT       NUMBER                     DEFAULT 0,
  ENDED              NUMBER                     DEFAULT 0,
  TRANSACTION_ID     NUMBER                     DEFAULT 0,
  SIGNATOR           VARCHAR2(150 BYTE)         DEFAULT 0,
  SENDER             VARCHAR2(100 BYTE)         DEFAULT 0,
  RECIVER            VARCHAR2(100 BYTE)         DEFAULT 0,
  ROUTE              NUMBER                     DEFAULT 0,
  SCANED             DATE,
  OPROGRAM           VARCHAR2(30 BYTE)          DEFAULT 'MP',
  NOTE1              VARCHAR2(200 BYTE)         DEFAULT '-',
  CART               VARCHAR2(50 BYTE)          DEFAULT 0,
  WEIGHT             NUMBER,
  HEIGHT             NUMBER,
  WIDTH              NUMBER,
  ODEPTH             NUMBER,
  PRO_ID             NUMBER,
  STITLE             VARCHAR2(200 BYTE)         DEFAULT 0,
  SADDRESS           VARCHAR2(200 BYTE)         DEFAULT 0,
  SPOSTOFFICENUMBER  VARCHAR2(150 BYTE)         DEFAULT 0,
  SCITY              VARCHAR2(200 BYTE)         DEFAULT 0,
  RTITLE             VARCHAR2(200 BYTE)         DEFAULT 0,
  RADDRESS           VARCHAR2(200 BYTE)         DEFAULT 0,
  RPOSTOFFICENUMBER  VARCHAR2(150 BYTE)         DEFAULT 0,
  RCITY              VARCHAR2(200 BYTE)         DEFAULT 0,
  CMR                VARCHAR2(150 BYTE)         DEFAULT 0,
  NOTE2              VARCHAR2(2000 BYTE)        DEFAULT ' ',
  NOTE3              VARCHAR2(2000 BYTE)        DEFAULT ' ',
  TEMPERATURE_1      NUMBER,
  TEMPERATURE_2      NUMBER,
  NR_PALETTES        NUMBER,
  NOTE4              VARCHAR2(1000 BYTE),
  NOTE5              VARCHAR2(1000 BYTE),
  NOTE6              VARCHAR2(1000 BYTE),
  S_TELEPHONE        VARCHAR2(50 BYTE),
  R_TELEPHONE        VARCHAR2(50 BYTE),
  LOADING_LAT        NUMBER,
  LOADING_LNG        NUMBER,
  UNLOADING_LAT      NUMBER,
  UNLOADING_LNG      NUMBER,
  TRAILER            VARCHAR2(1000 BYTE),
  TRAILER_ID         NUMBER,
  RABAT1             NUMBER,
  RABAT2             NUMBER,
  DATE_MODIFIED      DATE,
  CASH               NUMBER,
  ZOI                VARCHAR2(1000 BYTE),
  EOR                VARCHAR2(1000 BYTE),
  BUSINESSPREMISE    VARCHAR2(1000 BYTE),
  ELECTRONICDEVICE   VARCHAR2(1000 BYTE),
  VAT22              NUMBER,
  VAT95              NUMBER,
  OSNOVA1            NUMBER,
  OSNOVA2            NUMBER,
  PRICE              NUMBER,
  FURS_ORDER         VARCHAR2(1000 BYTE),
  INVOICE_NUMBER     NUMBER
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE CLOB_TEST
(
  CDATA  CLOB,
  D      DATE                                   DEFAULT sysdate
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE KLIENTI_BACK
(
  SIFRA     NUMBER,
  PLACE_ID  VARCHAR2(1000 BYTE),
  LAT       NUMBER,
  LNG       NUMBER
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE ORDERS_BACK
(
  ORDER_ID           NUMBER,
  WORKER_ID          NUMBER,
  ORDER_DATE         DATE,
  DELIVERY_DATE      DATE,
  COMPLETE_DATE      DATE,
  OTYPE              NUMBER,
  TYPE_D             VARCHAR2(150 BYTE),
  CNT                NUMBER,
  VEHICLE_ID         NUMBER,
  VEHICLE            VARCHAR2(20 BYTE),
  TERMINAL           CHAR(1000 BYTE),
  SEQ                NUMBER,
  COMPANY            NUMBER,
  BUSSINES_UNIT      NUMBER,
  CUSTOMER           NUMBER,
  LOC                VARCHAR2(40 BYTE),
  DESC_CODE          VARCHAR2(30 BYTE),
  ACTIVE             NUMBER,
  SCANED_COUNT       NUMBER,
  ENDED              NUMBER,
  TRANSACTION_ID     NUMBER,
  SIGNATOR           VARCHAR2(150 BYTE),
  SENDER             VARCHAR2(100 BYTE),
  RECIVER            VARCHAR2(100 BYTE),
  ROUTE              NUMBER,
  SCANED             DATE,
  OPROGRAM           VARCHAR2(30 BYTE),
  NOTE1              VARCHAR2(200 BYTE),
  CART               VARCHAR2(50 BYTE),
  WEIGHT             NUMBER,
  HEIGHT             NUMBER,
  WIDTH              NUMBER,
  ODEPTH             NUMBER,
  PRO_ID             NUMBER,
  STITLE             VARCHAR2(200 BYTE),
  SADDRESS           VARCHAR2(200 BYTE),
  SPOSTOFFICENUMBER  VARCHAR2(150 BYTE),
  SCITY              VARCHAR2(200 BYTE),
  RTITLE             VARCHAR2(200 BYTE),
  RADDRESS           VARCHAR2(200 BYTE),
  RPOSTOFFICENUMBER  VARCHAR2(150 BYTE),
  RCITY              VARCHAR2(200 BYTE),
  CMR                VARCHAR2(150 BYTE),
  NOTE2              VARCHAR2(2000 BYTE),
  NOTE3              VARCHAR2(2000 BYTE),
  TEMPERATURE_1      NUMBER,
  TEMPERATURE_2      NUMBER,
  NR_PALETTES        NUMBER,
  NOTE4              VARCHAR2(1000 BYTE),
  NOTE5              VARCHAR2(1000 BYTE),
  NOTE6              VARCHAR2(1000 BYTE),
  S_TELEPHONE        VARCHAR2(50 BYTE),
  R_TELEPHONE        VARCHAR2(50 BYTE),
  LOADING_LAT        NUMBER,
  LOADING_LNG        NUMBER,
  UNLOADING_LAT      NUMBER,
  UNLOADING_LNG      NUMBER,
  TRAILER            VARCHAR2(1000 BYTE),
  TRAILER_ID         NUMBER,
  RABAT1             NUMBER,
  RABAT2             NUMBER,
  DATE_MODIFIED      DATE,
  CASH               NUMBER,
  ZOI                VARCHAR2(1000 BYTE),
  EOR                VARCHAR2(1000 BYTE),
  BUSINESSPREMISE    VARCHAR2(1000 BYTE),
  ELECTRONICDEVICE   VARCHAR2(1000 BYTE),
  VAT22              NUMBER,
  VAT95              NUMBER,
  OSNOVA1            NUMBER,
  OSNOVA2            NUMBER,
  PRICE              NUMBER,
  FURS_ORDER         VARCHAR2(1000 BYTE),
  INVOICE_NUMBER     NUMBER
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE CLOBTEST
(
  A  CLOB
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE TABLE KLIENTI_NAREJENI
(
  ID  NUMBER
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX ORDERS_INDEX1 ON ORDERS
(ORDER_ID)
LOGGING
NOPARALLEL;


CREATE OR REPLACE package as_pdf3
is
/**********************************************
**
** Author: Anton Scheffer
** Date: 11-04-2012
** Website: http://technology.amis.nl
** See also: http://technology.amis.nl/?p=17718
**
** Changelog:
**   Date: 13-08-2012
**     added two procedure for Andreas Weiden
**     see https://sourceforge.net/projects/pljrxml2pdf/
**   Date: 16-04-2012
**     changed code for parse_png
**   Date: 15-04-2012
**     only dbms_lob.freetemporary for temporary blobs
**   Date: 11-04-2012
**     Initial release of as_pdf3
**
******************************************************************************
******************************************************************************
Copyright (C) 2012 by Anton Scheffer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

******************************************************************************
******************************************** */
--
  c_get_page_width    constant pls_integer := 0;
  c_get_page_height   constant pls_integer := 1;
  c_get_margin_top    constant pls_integer := 2;
  c_get_margin_right  constant pls_integer := 3;
  c_get_margin_bottom constant pls_integer := 4;
  c_get_margin_left   constant pls_integer := 5;
  c_get_x             constant pls_integer := 6;
  c_get_y             constant pls_integer := 7;
  c_get_fontsize      constant pls_integer := 8;
  c_get_current_font  constant pls_integer := 9;
--
  function file2blob( p_dir varchar2, p_file_name varchar2 )
  return blob;
--
  function conv2uu( p_value number, p_unit varchar2 )
  return number;
--
  procedure set_page_size
    ( p_width number
    , p_height number
    , p_unit varchar2 := 'cm'
    );
--
  procedure set_page_format( p_format varchar2 := 'A4' );
--
  procedure set_page_orientation( p_orientation varchar2 := 'PORTRAIT' );
--
  procedure set_margins
    ( p_top number := null
    , p_left number := null
    , p_bottom number := null
    , p_right number := null
    , p_unit varchar2 := 'cm'
    );
--
  procedure set_info
    ( p_title varchar2 := null
    , p_author varchar2 := null
    , p_subject varchar2 := null
    , p_keywords varchar2 := null
    );
--
  procedure init;
--
  function get_pdf
  return blob;
--
  procedure save_pdf
    ( p_dir varchar2 := 'MY_DIR'
    , p_filename varchar2 := 'my.pdf'
    , p_freeblob boolean := true
    );
--
  procedure txt2page( p_txt varchar2 );
--
  procedure put_txt( p_x number, p_y number, p_txt varchar2, p_degrees_rotation number := null );
--
  function str_len( p_txt varchar2 )
  return number;
--
  procedure write
    ( p_txt in varchar2
    , p_x in number := null
    , p_y in number := null
    , p_line_height in number := null
    , p_start in number := null -- left side of the available text box
    , p_width in number := null -- width of the available text box
    , p_alignment in varchar2 := null
    );
--
  procedure set_font
    ( p_index pls_integer
    , p_fontsize_pt number
    , p_output_to_doc boolean := true
    );
--
  function set_font
    ( p_fontname varchar2
    , p_fontsize_pt number
    , p_output_to_doc boolean := true
    )
  return pls_integer;
--
  procedure set_font
    ( p_fontname varchar2
    , p_fontsize_pt number
    , p_output_to_doc boolean := true
    );
--
  function set_font
    ( p_family varchar2
    , p_style varchar2 := 'N'
    , p_fontsize_pt number := null
    , p_output_to_doc boolean := true
    )
  return pls_integer;
--
  procedure set_font
    ( p_family varchar2
    , p_style varchar2 := 'N'
    , p_fontsize_pt number := null
    , p_output_to_doc boolean := true
    );
--
  procedure new_page;
--
  function load_ttf_font
    ( p_font blob
    , p_encoding varchar2 := 'WINDOWS-1252'
    , p_embed boolean := false
    , p_compress boolean := true
    , p_offset number := 1
    )
  return pls_integer;
--
  procedure load_ttf_font
    ( p_font blob
    , p_encoding varchar2 := 'WINDOWS-1252'
    , p_embed boolean := false
    , p_compress boolean := true
    , p_offset number := 1
    );
--
  function load_ttf_font
    ( p_dir varchar2 := 'MY_FONTS'
    , p_filename varchar2 := 'BAUHS93.TTF'
    , p_encoding varchar2 := 'WINDOWS-1252'
    , p_embed boolean := false
    , p_compress boolean := true
    )
  return pls_integer;
--
  procedure load_ttf_font
    ( p_dir varchar2 := 'MY_FONTS'
    , p_filename varchar2 := 'BAUHS93.TTF'
    , p_encoding varchar2 := 'WINDOWS-1252'
    , p_embed boolean := false
    , p_compress boolean := true
    );
--
  procedure load_ttc_fonts
    ( p_ttc blob
    , p_encoding varchar2 := 'WINDOWS-1252'
    , p_embed boolean := false
    , p_compress boolean := true
    );
--
  procedure load_ttc_fonts
    ( p_dir varchar2 := 'MY_FONTS'
    , p_filename varchar2 := 'CAMBRIA.TTC'
    , p_encoding varchar2 := 'WINDOWS-1252'
    , p_embed boolean := false
    , p_compress boolean := true
    );
--
  procedure set_color( p_rgb varchar2 := '000000' );
--
  procedure set_color
    ( p_red number := 0
    , p_green number := 0
    , p_blue number := 0
    );
--
  procedure set_bk_color( p_rgb varchar2 := 'ffffff' );
--
  procedure set_bk_color
    ( p_red number := 0
    , p_green number := 0
    , p_blue number := 0
    );
--
  procedure horizontal_line
    ( p_x in number
    , p_y in number
    , p_width in number
    , p_line_width in number := 0.5
    , p_line_color in varchar2 := '000000'
    );
--
  procedure vertical_line
    ( p_x in number
    , p_y in number
    , p_height in number
    , p_line_width in number := 0.5
    , p_line_color in varchar2 := '000000'
    );
--
  procedure rect
    ( p_x in number
    , p_y in number
    , p_width in number
    , p_height in number
    , p_line_color in varchar2 := null
    , p_fill_color in varchar2 := null
    , p_line_width in number := 0.5
    );
--
  function get( p_what in pls_integer )
  return number;
--
  procedure put_image
    ( p_img blob
    , p_x number
    , p_y number
    , p_width number := null
    , p_height number := null
    , p_align varchar2 := 'center'
    , p_valign varchar2 := 'top'
    );
--
  procedure put_image
    ( p_dir varchar2
    , p_file_name varchar2
    , p_x number
    , p_y number
    , p_width number := null
    , p_height number := null
    , p_align varchar2 := 'center'
    , p_valign varchar2 := 'top'
    );
--
  procedure put_image
    ( p_url varchar2
    , p_x number
    , p_y number
    , p_width number := null
    , p_height number := null
    , p_align varchar2 := 'center'
    , p_valign varchar2 := 'top'
    );
--
  procedure set_page_proc( p_src clob );
--
  type tp_col_widths is table of number;
  type tp_headers is table of varchar2(32767);
--
  procedure query2table
    ( p_query varchar2
    , p_widths tp_col_widths := null
    , p_headers tp_headers := null
    );
--
$IF not DBMS_DB_VERSION.VER_LE_10 $THEN
  procedure refcursor2table
    ( p_rc sys_refcursor
    , p_widths tp_col_widths := null
    , p_headers tp_headers := null
    );
--
$END
--
  procedure pr_goto_page( i_npage number );
--
  procedure pr_goto_current_page;
/*
begin
  as_pdf3.init;
  as_pdf3.write( 'Minimal usage' );
  as_pdf3.save_pdf;
end;
--
begin
  as_pdf3.init;
  as_pdf3.write( 'Some text with a newline-character included at this "
" place.' );
  as_pdf3.write( 'Normally text written with as_pdf3.write() is appended after the previous text. But the text wraps automaticly to a new line.' );
  as_pdf3.write( 'But you can place your text at any place', -1, 700 );
  as_pdf3.write( 'you want', 100, 650 );
  as_pdf3.write( 'You can even align it, left, right, or centered', p_y => 600, p_alignment => 'right' );
  as_pdf3.save_pdf;
end;
--
begin
  as_pdf3.init;
  as_pdf3.write( 'The 14 standard PDF-fonts and the WINDOWS-1252 encoding.' );
  as_pdf3.set_font( 'helvetica' );
  as_pdf3.write( 'helvetica, normal: ' || 'The quick brown fox jumps over the lazy dog. 1234567890', -1, 700 );
  as_pdf3.set_font( 'helvetica', 'I' );
  as_pdf3.write( 'helvetica, italic: ' || 'The quick brown fox jumps over the lazy dog. 1234567890', -1, -1 );
  as_pdf3.set_font( 'helvetica', 'b' );
  as_pdf3.write( 'helvetica, bold: ' || 'The quick brown fox jumps over the lazy dog. 1234567890', -1, -1 );
  as_pdf3.set_font( 'helvetica', 'BI' );
  as_pdf3.write( 'helvetica, bold italic: ' || 'The quick brown fox jumps over the lazy dog. 1234567890', -1, -1 );
  as_pdf3.set_font( 'times' );
  as_pdf3.write( 'times, normal: ' || 'The quick brown fox jumps over the lazy dog. 1234567890', -1, 625 );
  as_pdf3.set_font( 'times', 'I' );
  as_pdf3.write( 'times, italic: ' || 'The quick brown fox jumps over the lazy dog. 1234567890', -1, -1 );
  as_pdf3.set_font( 'times', 'b' );
  as_pdf3.write( 'times, bold: ' || 'The quick brown fox jumps over the lazy dog. 1234567890', -1, -1 );
  as_pdf3.set_font( 'times', 'BI' );
  as_pdf3.write( 'times, bold italic: ' || 'The quick brown fox jumps over the lazy dog. 1234567890', -1, -1 );
  as_pdf3.set_font( 'courier' );
  as_pdf3.write( 'courier, normal: ' || 'The quick brown fox jumps over the lazy dog. 1234567890', -1, 550 );
  as_pdf3.set_font( 'courier', 'I' );
  as_pdf3.write( 'courier, italic: ' || 'The quick brown fox jumps over the lazy dog. 1234567890', -1, -1 );
  as_pdf3.set_font( 'courier', 'b' );
  as_pdf3.write( 'courier, bold: ' || 'The quick brown fox jumps over the lazy dog. 1234567890', -1, -1 );
  as_pdf3.set_font( 'courier', 'BI' );
  as_pdf3.write( 'courier, bold italic: ' || 'The quick brown fox jumps over the lazy dog. 1234567890', -1, -1 );
--
  as_pdf3.set_font( 'courier' );
  as_pdf3.write( 'symbol:', -1, 475 );
  as_pdf3.set_font( 'symbol' );
  as_pdf3.write( 'The quick brown fox jumps over the lazy dog. 1234567890', -1, -1 );
  as_pdf3.set_font( 'courier' );
  as_pdf3.write( 'zapfdingbats:', -1, -1 );
  as_pdf3.set_font( 'zapfdingbats' );
  as_pdf3.write( 'The quick brown fox jumps over the lazy dog. 1234567890', -1, -1 );
--
  as_pdf3.set_font( 'times', 'N', 20 );
  as_pdf3.write( 'times, normal with fontsize 20pt', -1, 400 );
  as_pdf3.set_font( 'times', 'N', 6 );
  as_pdf3.write( 'times, normal with fontsize 5pt', -1, -1 );
  as_pdf3.save_pdf;
end;
--
declare
  x pls_integer;
begin
  as_pdf3.init;
  as_pdf3.write( 'But others fonts and encodings are possible using TrueType fontfiles.' );
  x := as_pdf3.load_ttf_font( 'MY_FONTS', 'refsan.ttf', 'CID', p_compress => false );
  as_pdf3.set_font( x, 12  );
  as_pdf3.write( 'The Windows MSReference SansSerif font contains a lot of encodings, for instance', -1, 700 );
  as_pdf3.set_font( x, 15  );
  as_pdf3.write( 'Albanian: Kush mund t� lexoni k�t� di�ka si kjo', -1, -1 );
  as_pdf3.write( 'Croatic: Tko mo�e citati to ne�to poput ovoga', -1, -1 );
  as_pdf3.write( 'Russian: ??? ????? ????????? ??? ???-?? ????? ?????', -1, -1);
  as_pdf3.write( 'Greek: ????? �p??e? ?a d?a�?se? a?t? t? ??t? sa? a?t?', -1, -1 ); 
--
  as_pdf3.set_font( 'helvetica', 12  );
  as_pdf3.write( 'Or by using a  TrueType collection file (ttc).', -1, 600 );
  as_pdf3.load_ttc_fonts( 'MY_FONTS',  'cambria.ttc', p_embed => true, p_compress => false );
  as_pdf3.set_font( 'cambria', 15 );   -- font family
  as_pdf3.write( 'Anton, testing 1,2,3 with Cambria', -1, -1 );
  as_pdf3.set_font( 'CambriaMT', 15 );  -- fontname
  as_pdf3.write( 'Anton, testing 1,2,3 with CambriaMath', -1, -1 );
  as_pdf3.save_pdf;
end;
--
begin
  as_pdf3.init;
  for i in 1 .. 10
  loop
    as_pdf3.horizontal_line( 30, 700 - i * 15, 100, i );
  end loop;
  for i in 1 .. 10
  loop
    as_pdf3.vertical_line( 150 + i * 15, 700, 100, i );
  end loop;
  for i in 0 .. 255
  loop
    as_pdf3.horizontal_line( 330, 700 - i, 100, 2, p_line_color =>  to_char( i, 'fm0x' ) || to_char( i, 'fm0x' ) || to_char( i, 'fm0x' ) );
  end loop;
  as_pdf3.save_pdf;
end;
--
declare
  t_logo varchar2(32767) :=
'/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkS' ||
'Ew8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJ' ||
'CQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIy' ||
'MjIyMjIyMjIyMjIyMjL/wAARCABqAJYDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEA' ||
'AAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIh' ||
'MUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6' ||
'Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZ' ||
'mqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx' ||
'8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREA' ||
'AgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAV' ||
'YnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hp' ||
'anN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPE' ||
'xcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3' ||
'+iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoorifiX4pk' ||
'8PaCILR9t9eExxsOqL/E315AHuaUmkrs1oUZVqipw3ZU8X/FCz0KeSw02Jb2+Thy' ||
'WxHGfQkdT7D8686ufih4suGJW/jgXssUC8fnk1ydvbz3lzHb28bzTyttRF5LMa7H' ||
'Uvh+3hvRI9T1+7kUPIsf2ezUMykgnlmIHbtXI5znqtj66ng8DhFGFRJyffVv5Fnw' ||
'r8QfEEvinTodR1N5rSaYRyIyIAd3A5A9SK7X4qeINV0Gz019LvGtmlkcOVVTkADH' ||
'UGvNdDsPDepa7ZWdtPrMU8syiN3EWFbqCcfSu3+NXGnaOM5/ev8A+giqi5ezepy1' ||
'6NF4+koxsne6scronxO1+01i2l1K/e6st2Joyij5T1IwByOv4V75BPHc28c8Lh45' ||
'FDKynIIPINfJleheGPiPJong+802Ul7uEYsCRkYbsfZev04pUqttJF5rlSqKM6Eb' ||
'PZpGv8RfiFf2etDTNDu/I+zf8fEqqG3Of4eQen8z7VB8O/GGv6x4vhs9Q1J57don' ||
'YoUUZIHHQV5fI7yyNJIxd3JZmY5JJ6k12nwo/wCR8t/+uEn8qUajlM6K+Ao0MFJc' ||
'qbS363O1+KviTWNBuNMXS71rYTLIZAqqd2NuOoPqayvht4u17WvFf2TUdRe4g+zu' ||
'+woo5BXB4HuaX42f8fOj/wC7L/Naw/hH/wAjv/26yfzWqcn7W1zjpUKTytzcVez1' ||
'sdt8QviJN4euhpelJG16VDSyuMiIHoMdz3rzZviN4tZif7YkHsIkx/6DTPiAkqeO' ||
'9WE2dxlBXP8Ad2jH6VJ4H8LWfizUp7S51FrV40DoiKC0nPOM+nH51MpTlOyOvDYX' ||
'C4fCKrUinpdu1zovAfjvXL7xfZ2ep6i89tOGTayKPmxkHgD0/WvbK83074RWWman' ||
'a30Wr3Zkt5VlUFVwSDnHSvQZ7u2tU3XE8cSju7gD9a6Kakl7x89mVTD1aqlh1pbt' ||
'YnorDfxj4eWTy11W3lfpthbzD+S5q7ZavBfy7IIrrGM75Ld41/NgKu6OB05pXaL9' ||
'FFFMgK8A+K+ote+NZYM5jtIliA9yNx/mPyr37tXzP42cv421gseftLD8sCsK7909' ||
'zIIKWJcn0Rf8Aa5o3h3WJtR1VZmdY9kAjj3YJ+8fbjj8TW/8QPHuj+J/D6WNgLjz' ||
'lnWQ+ZHtGAD3z71wNno2qahEZbLTrq5jB2l4oiwB9Mii80XVNPhE17p11bxE7d8s' ||
'RUZ9MmsFOSjZLQ9+phMNUxKqyl7y6XNHwR/yO+j/APXyP5GvQ/jX/wAg/SP+ur/+' ||
'givPPBH/ACO+j/8AXyP5GvQ/jX/yD9I/66v/AOgirh/CZyYv/kZ0vT/M8y8PaM/i' ||
'DV106J9kskcjRk9NyqSAfY4xWbLFJBM8UqFJI2KurDBUjgg11nww/wCR/sP92T/0' ||
'A16B4p+Gq614xtNQg2pZznN+AcH5e4/3uh/OojT5o3R0V8xjh8S6dT4bX+ev5nk1' ||
'7oU+n+HtP1W4yv26RxEhH8CgfN+JP5V0Hwo/5Hy3/wCuEn8q6b4zxJBY6JFEgSNG' ||
'kVVUYAAC4Fcn8MbqG08bQyzyBEEMnJ78dB6mq5VGokZ+3licunUe7TOn+Nn/AB86' ||
'P/uy/wA1rD+EZA8bEk4AtJMn8Vru/GHhW58c3lhKrmws7ZX3yzp875x91e3Tvj6V' ||
'zduPDPh6/GneGtOl8Qa2wKmRnzGvrk/dx9B+NXKL9pzHDQxEHgPq8dZWd/L1exf+' ||
'JHhuPxFdw6hozLPeIPLnCnCbBkhi5+UEfXofauEtLWy8OX0N7L4hQ3sDBli01POI' ||
'PoXOF9j1r1O18E6nrhSfxbqJkjHK6baHy4E9jjlq84+IXg4+GNWE1qh/sy5JMX/T' ||
'Nu6f1Ht9KVSL+OxeXYiMrYSU/wCu13/l8zudCn1jx3avcxaybO1Vijorbph9Qu1V' ||
'z/wKt+y+HHh63fzrq3k1CfqZbyQyc/Tp+leL+CvE0vhjxDDc7z9klIjuU7FSev1H' ||
'X8/WvpNWDqGUggjIIrSk1NXe5wZpTq4Spywdova2hFbWVrZxiO2t4oUH8MaBR+lT' ||
'0UVseM23uFFFFAgr5y+I9obPx5qQIwsrLKvuCo/qDX0bXkPxn0YiSw1mNflINvKf' ||
'Tuv/ALNWNdXiexklZU8Uk/tKxb+C16j6bqVgSN8cyygezDH81rR+MQ/4o6L/AK+0' ||
'/k1cV8JrXVv+Em+2WkJNgEaO5kY4XHUAerZxxXpHxB0b/hIdBSxjv7W1kWdZC1w2' ||
'BgA/40oXdOxti1CjmanfS6b8jxbwR/yO+j/9fI/ka9D+Nf8AyD9I/wCur/8AoIrG' ||
'8PeCJtJ8RWOoHVLa7S2lDslpFJIT7AgY/Ouu8a+HNT8bx2EVvB9hit3ZmkuiMkEY' ||
'4VST+eKiMGqbR1YnFUZY+nWT91L/ADPN/hh/yP8AYf7sn/oBr3y51O1tHEbybpj0' ||
'ijBZz/wEc1xXh34WafoVyl7PqNzNcoD8yN5SgEYPTn9auar438K+FI3hhkjluB1h' ||
'tQGYn/abp+ZzWlNckfeOHMakcbiL0E5aW2F8SeFJPG01kb7fYWlqWYKCDLJnHXsv' ||
'T3/Cqdzqngz4cwGC0hje+xjyofnmY/7THp+P5VjHUvHfjxWXToBoult/y1clWcfX' ||
'GT+AH1qx4Q+GN/oXiSLUtQurO5iRW+UKxbceh5HX3ovd3ivmChGnT5MRU0X2U/zZ' ||
'yfjXxR4p1K2ga/gfTNOu9xhtlOGdRjl+56j0HtS/CL/kd/8At1k/mteg/EHwRfeL' ||
'ZbB7O5t4RbhwwlB53Y6Y+lZ/gf4c6l4Y8Q/2jdXlrLH5LR7Yw2ckj1+lRyS9pc7F' ||
'jsM8BKmrRk09EQeNviHrnhnxLLp8FtZvBsWSNpFbcQRznB9Qa4bxF8Q9Y8S6abC8' ||
'htI4CwY+Wh3ZByOSTivS/H/gC78V6haXllcwQPHGY5PNB+YZyMY+prkP+FMa3/0E' ||
'rH8n/wAKKiqNtLYeBrZdCnCc7Ka9TzcKzkKoJZuAB3NfVWjwS22i2UE3MscCI/1C' ||
'gGuE8LfCe20e/i1DU7sXk8Lbo40TbGrdic8nFekVVGm46s485x9PEyjGlql1Ciii' ||
'tzxAooooAKo6vpFnrmmS6ffRl7eXG4A4PByCD26VeooHGTi7rcxL3w9btpEen2Nr' ||
'aRxRDEcciHaP++SDXG3fhzxxZzCTSpNICDpGqE5/77BP616bRUuKZ0UsVOn5+up5' ||
'd/wkfxI0vi98Nw3ajq0A5/8AHWP8qgfxz461aQwaX4Za2boWljY7T9W2ivWKTA9K' ||
'nkfc3WNpbujG/wA/yPKl8DeM/EZ3eI/EDW8DdYITn8MDC/zrqtC+HXh3QiskdmLi' ||
'4XkTXHzkH2HQfgK6yimoJamdTH1prlTsuy0QgAHAGKWsvWHvVNsLcS+QXIuGhAMg' ||
'G04wD74z3rHmfxAxkEJuFk3SL8yIUEe07GHq+duR67uMYqm7GEaXNrdHWUVx7z+K' ||
'y+/yiCixnylC4coX389t+Fx6ZHvTbj/hKHjufmmV1ineLywmN+UMa89cAsPfFLmL' ||
'+r/3l952VFcpqdvrcEt0bO4vJI1SAx/dOSZCJO2eFxSwPrZ1IBTc+WJ4wBIoEZh2' ||
'DeScZ3bt2O+cdqLi9j7t+ZHVUVzFzHrUN/dNFLdPaiaMADaSIyMuUGOSDgfTOKWV' ||
'/ES6XCbcF7j7S4XzAoJi2vs39hzt6e3vTuL2O2qOmormjHqU32F4ptRUGbFysgQE' ||
'LsY+n97aOK6KJzJEjlGTcoO1uo9j70XIlDl6j6KKKZAUUUUAFFFFABRRRQAUUUUA' ||
'Y3iDV59JjgNvCkrylwA5IAKxsw6e6gVnnxTchjmwZMSm2MbZ3LMUDKvoVJyN3Toa' ||
'6ggHqAaMD0FKzNYzglZxuci3i26jghmeCAiXG9Fc7rf94qEP/wB9H05HfrUl74ou' ||
'4PtKxW0TG3lQM+4lTG7KI2HrkMe/8JrqTGhzlF568daPLTbt2Lt6YxxSs+5ftKd/' ||
'hOah8SXL6iLcxwSL9ojgKITvIaMMXHJGBn8h1qO48V3Vs1y5sA8EJmVnQklSrbUJ' ||
'Hoe5HTjtXUrGinKooOMcCl2r6D8qLMXtKd/hOX1fxFqNjd3qW1ik0VpAszkkjgq5' ||
'zn2Kjjqc0j+JrmNeIoGZIkk25wZ9zEbY8E8jHqeSOldTtU5yBz1poiRcAIox0wOl' ||
'Fn3D2lOyXKcvZeJ72W5tPtVpFDaXErxiZmK4KiTjnr9wc+9aHh/W21W0WW4MMckh' ||
'OyNTzx178/pWyY0ZdrIpHoRQsaISVRQT6ChJinUhJO0bDqKKKoxCiiigAooooAKK' ||
'KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//Z';
begin
  as_pdf3.init;
  as_pdf3.put_image( to_blob( utl_encode.base64_decode( utl_raw.cast_to_raw( t_logo ) ) )
                   , 0
                   , as_pdf3.get( as_pdf3.C_GET_PAGE_HEIGHT ) - 260
                   , as_pdf3.get( as_pdf3.C_GET_PAGE_WIDTH )
                   );
  as_pdf3.write( 'jpg, gif and png images are supported.' );
  as_pdf3.write( 'And because PDF 1.3 (thats the format I use) doesn''t support alpha channels, neither does AS_PDF.', -1, -1 );
  as_pdf3.save_pdf;
end;
--
declare
  t_rc sys_refcursor;
  t_query varchar2(1000);
begin
  as_pdf3.init;
  as_pdf3.load_ttf_font( 'MY_FONTS', 'COLONNA.TTF', 'CID' );
  as_pdf3.set_page_proc( q'~
    begin    
      as_pdf3.set_font( 'helvetica', 8 );
      as_pdf3.put_txt( 10, 15, 'Page #PAGE_NR# of "PAGE_COUNT#' );
      as_pdf3.set_font( 'helvetica', 12 );
      as_pdf3.put_txt( 350, 15, 'This is a footer text' );
      as_pdf3.set_font( 'helvetica', 'B', 15 );
      as_pdf3.put_txt( 200, 780, 'This is a header text' );
      as_pdf3.put_image( 'MY_DIR', 'amis.jpg', 500, 15 );
   end;~' );
  as_pdf3.set_page_proc( q'~
    begin    
      as_pdf3.set_font( 'Colonna MT', 'N', 50 );
      as_pdf3.put_txt( 150, 200, 'Watermark Watermark Watermark', 60 );
   end;~' ); 
  t_query := 'select rownum, sysdate + level, ''example'' || level from dual connect by level <= 50'; 
  as_pdf3.query2table( t_query );
  open t_rc for t_query;
  as_pdf3.refcursor2table( t_rc );
  as_pdf3.save_pdf;
end;
*/
end;
/

SHOW ERRORS;


CREATE OR REPLACE package body as_pdf3
is
--
  type tp_pls_tab is table of pls_integer index by pls_integer;
  type tp_objects_tab is table of number(10) index by pls_integer;
  type tp_pages_tab is table of blob index by pls_integer;
  type tp_settings is record
    ( page_width number
    , page_height number
    , margin_left number
    , margin_right number
    , margin_top number
    , margin_bottom number
    );
  type tp_font is record
    ( standard boolean
    , family varchar2(100)
    , style varchar2(2)  -- N Normal
                         -- I Italic
                         -- B Bold
                         -- BI Bold Italic
    , subtype varchar2(15)
    , name varchar2(100)
    , fontname varchar2(100)
    , char_width_tab tp_pls_tab
    , encoding varchar2(100)    , charset varchar2(1000)
    , compress_font boolean := true
    , fontsize number
    , unit_norm number
    , bb_xmin pls_integer
    , bb_ymin pls_integer
    , bb_xmax pls_integer
    , bb_ymax pls_integer
    , flags pls_integer
    , first_char pls_integer
    , last_char pls_integer
    , italic_angle number
    , ascent pls_integer
    , descent pls_integer
    , capheight pls_integer
    , stemv pls_integer
    , diff varchar2(32767)
    , cid boolean := false
    , fontfile2 blob
    , ttf_offset pls_integer
    , used_chars tp_pls_tab
    , numGlyphs pls_integer
    , indexToLocFormat pls_integer
    , loca tp_pls_tab
    , code2glyph tp_pls_tab
    , hmetrics tp_pls_tab
    );
  type tp_font_tab is table of tp_font index by pls_integer;
  type tp_img is record
    ( adler32 varchar2(8)
    , width pls_integer
    , height pls_integer
    , color_res pls_integer
    , color_tab raw(768)
    , greyscale boolean
    , pixels blob
    , type varchar2(5)
    , nr_colors pls_integer
    , transparancy_index pls_integer
    );
  type tp_img_tab is table of tp_img index by pls_integer;
  type tp_info is record
    ( title varchar2(1024)
    , author varchar2(1024)
    , subject varchar2(1024)
    , keywords varchar2(32767)
    );
  type tp_page_prcs is table of clob index by pls_integer;
--
-- globals
  g_pdf_doc blob; -- the PDF-document being constructed
  g_objects tp_objects_tab;
  g_pages tp_pages_tab;
  g_settings tp_settings;
  g_fonts tp_font_tab;
  g_used_fonts tp_pls_tab;
  g_current_font pls_integer;
  g_images tp_img_tab;
  g_x number;  -- current x-location of the "cursor"
  g_y number;  -- current y-location of the "cursor"
  g_info tp_info;
  g_page_nr pls_integer;
  g_page_prcs tp_page_prcs;
--
-- constants
  c_nl constant varchar2(2) := chr(13) || chr(10);
--
  function num2raw( p_value number )
  return raw
  is
  begin
    return hextoraw( to_char( p_value, 'FM0XXXXXXX' ) );
  end;
--
  function raw2num( p_value raw )
  return number
  is
  begin
    return to_number( rawtohex( p_value ), 'XXXXXXXX' );
  end;
--
  function raw2num( p_value raw, p_pos pls_integer, p_len pls_integer )
  return pls_integer
  is
  begin
    return to_number( rawtohex( utl_raw.substr( p_value, p_pos, p_len ) ), 'XXXXXXXX' );
  end;
--
  function to_short( p_val raw, p_factor number := 1 )
  return number
  is
    t_rv number;
  begin
    t_rv := to_number( rawtohex( p_val ), 'XXXXXXXXXX' );
    if t_rv > 32767
    then
      t_rv := t_rv - 65536;
    end if;
    return t_rv * p_factor;
  end;
--
  function blob2num( p_blob blob, p_len integer, p_pos integer )
  return number
  is
  begin
    return to_number( rawtohex( dbms_lob.substr( p_blob, p_len, p_pos ) ), 'xxxxxxxx' );
  end;
--
  function file2blob( p_dir varchar2, p_file_name varchar2 )
  return blob
  is
    t_raw raw(32767);
    t_blob blob;
    fh utl_file.file_type;
  begin
    fh := utl_file.fopen( p_dir, p_file_name, 'rb' );
    dbms_lob.createtemporary( t_blob, true );
    loop
      begin
        utl_file.get_raw( fh, t_raw );
        dbms_lob.append( t_blob, t_raw );
      exception
        when no_data_found
        then
          exit;
      end;
    end loop;
    utl_file.fclose( fh );
    return t_blob;
  exception
    when others
    then
      if utl_file.is_open( fh )
      then
        utl_file.fclose( fh );
      end if;
      raise;
  end;
--
  procedure init_core_fonts
  is
    function uncompress_withs( p_compressed_tab varchar2 )
    return tp_pls_tab
    is
      t_rv tp_pls_tab;
      t_tmp raw(32767);
    begin
      if p_compressed_tab is not null
      then
        t_tmp := utl_compress.lz_uncompress

          ( utl_encode.base64_decode( utl_raw.cast_to_raw( p_compressed_tab ) ) );
        for i in 0 .. 255
        loop
          t_rv( i ) := to_number( utl_raw.substr( t_tmp, i * 4 + 1, 4 ), '0xxxxxxx' );
        end loop;
      end if;
      return t_rv;
    end;
--
    procedure init_core_font
      ( p_ind pls_integer
      , p_family varchar2
      , p_style varchar2
      , p_name varchar2
      , p_compressed_tab varchar2
      )
    is
    begin
      g_fonts( p_ind ).family := p_family;
      g_fonts( p_ind ).style := p_style;
      g_fonts( p_ind ).name := p_name;
      g_fonts( p_ind ).fontname := p_name;
      g_fonts( p_ind ).standard := true;
      g_fonts( p_ind ).encoding := 'WE8MSWIN1252';
      g_fonts( p_ind ).charset := sys_context( 'userenv', 'LANGUAGE' );
      g_fonts( p_ind ).charset := substr( g_fonts( p_ind ).charset
                                        , 1
                                        , instr( g_fonts( p_ind ).charset, '.' )
                                        ) || g_fonts( p_ind ).encoding;
      g_fonts( p_ind ).char_width_tab := uncompress_withs( p_compressed_tab );
    end;
  begin
    init_core_font( 1, 'helvetica', 'N', 'Helvetica'
      ,  'H4sIAAAAAAAAC81Tuw3CMBC94FQMgMQOLAGVGzNCGtc0dAxAT+8lsgE7RKJFomOA'
      || 'SLT4frHjBEFJ8XSX87372C8A1Qr+Ax5gsWGYU7QBAK4x7gTnGLOS6xJPOd8w5NsM'
      || '2OvFvQidAP04j1nyN3F7iSNny3E6DylPeeqbNqvti31vMpfLZuzH86oPdwaeo6X+'
      || '5X6Oz5VHtTqJKfYRNVu6y0ZyG66rdcxzXJe+Q/KJ59kql+bTt5K6lKucXvxWeHKf'
      || '+p6Tfersfh7RHuXMZjHsdUkxBeWtM60gDjLTLoHeKsyDdu6m8VK3qhnUQAmca9BG'
      || 'Dq3nP+sV/4FcD6WOf9K/ne+hdav+DTuNLeYABAAA' );
--
    init_core_font( 2, 'helvetica', 'I', 'Helvetica-Oblique'
      ,  'H4sIAAAAAAAAC81Tuw3CMBC94FQMgMQOLAGVGzNCGtc0dAxAT+8lsgE7RKJFomOA'
      || 'SLT4frHjBEFJ8XSX87372C8A1Qr+Ax5gsWGYU7QBAK4x7gTnGLOS6xJPOd8w5NsM'
      || '2OvFvQidAP04j1nyN3F7iSNny3E6DylPeeqbNqvti31vMpfLZuzH86oPdwaeo6X+'
      || '5X6Oz5VHtTqJKfYRNVu6y0ZyG66rdcxzXJe+Q/KJ59kql+bTt5K6lKucXvxWeHKf'
      || '+p6Tfersfh7RHuXMZjHsdUkxBeWtM60gDjLTLoHeKsyDdu6m8VK3qhnUQAmca9BG'
      || 'Dq3nP+sV/4FcD6WOf9K/ne+hdav+DTuNLeYABAAA' );
--
    init_core_font( 3, 'helvetica', 'B', 'Helvetica-Bold'
      ,  'H4sIAAAAAAAAC8VSsRHCMAx0SJcBcgyRJaBKkxXSqKahYwB6+iyRTbhLSUdHRZUB'
      || 'sOWXLF8SKCn+ZL/0kizZuaJ2/0fn8XBu10SUF28n59wbvoCr51oTD61ofkHyhBwK'
      || '8rXusVaGAb4q3rXOBP4Qz+wfUpzo5FyO4MBr39IH+uLclFvmCTrz1mB5PpSD52N1'
      || 'DfqS988xptibWfbw9Sa/jytf+dz4PqQz6wi63uxxBpCXY7uUj88jNDNy1mYGdl97'
      || '856nt2f4WsOFed4SpzumNCvlT+jpmKC7WgH3PJn9DaZfA42vlgh96d+wkHy0/V95'
      || 'xyv8oj59QbvBN2I/iAuqEAAEAAA=' );
--
    init_core_font( 4, 'helvetica', 'BI', 'Helvetica-BoldOblique'
      ,  'H4sIAAAAAAAAC8VSsRHCMAx0SJcBcgyRJaBKkxXSqKahYwB6+iyRTbhLSUdHRZUB'
      || 'sOWXLF8SKCn+ZL/0kizZuaJ2/0fn8XBu10SUF28n59wbvoCr51oTD61ofkHyhBwK'
      || '8rXusVaGAb4q3rXOBP4Qz+wfUpzo5FyO4MBr39IH+uLclFvmCTrz1mB5PpSD52N1'
      || 'DfqS988xptibWfbw9Sa/jytf+dz4PqQz6wi63uxxBpCXY7uUj88jNDNy1mYGdl97'
      || '856nt2f4WsOFed4SpzumNCvlT+jpmKC7WgH3PJn9DaZfA42vlgh96d+wkHy0/V95'
      || 'xyv8oj59QbvBN2I/iAuqEAAEAAA=' );
--
    init_core_font( 5, 'times', 'N', 'Times-Roman'
      ,  'H4sIAAAAAAAAC8WSKxLCQAyG+3Bopo4bVHbwHGCvUNNT9AB4JEwvgUBimUF3wCNR'
      || 'qAoGRZL9twlQikR8kzTvZBtF0SP6O7Ej1kTnSRfEhHw7+Jy3J4XGi8w05yeZh2sE'
      || '4j312ZDeEg1gvSJy6C36L9WX1urr4xrolfrSrYmrUCeDPGMu5+cQ3Ur3OXvQ+TYf'
      || '+2FGexOZvTM1L3S3o5fJjGQJX2n68U2ur3X5m3cTvfbxsk9pcsMee60rdTjnhNkc'
      || 'Zip9HOv9+7/tI3Oif3InOdV/oLdx3gq2HIRaB1Ob7XPk35QwwxDyxg3e09Dv6nSf'
      || 'rxQjvty8ywDce9CXvdF9R+4y4o+7J1P/I9sABAAA' );
--
    init_core_font( 6, 'times', 'I', 'Times-Italic'
      ,  'H4sIAAAAAAAAC8WSPQ6CQBCFF+i01NB5g63tPcBegYZTeAB6SxNLjLUH4BTEeAYr'
      || 'Kwpj5ezsW2YgoKXFl2Hnb9+wY4x5m7+TOOJMdIFsRywodkfMBX9aSz7bXGp+gj6+'
      || 'R4TvOtJ3CU5Eq85tgGsbxG3QN8iFZY1WzpxXwkckFTR7e1G6osZGWT1bDuBnTeP5'
      || 'KtW/E71c0yB2IFbBphuyBXIL9Y/9fPvhf8se6vsa8nmeQtU6NSf6ch9fc8P9DpqK'
      || 'cPa5/I7VxDwruTN9kV3LDvQ+h1m8z4I4x9LIbnn/Fv6nwOdyGq+d33jk7/cxztyq'
      || 'XRhTz/it7Mscg7fT5CO+9ahnYk20Hww5IrwABAAA' );
--
    init_core_font( 7, 'times', 'B', 'Times-Bold'
      , 'H4sIAAAAAAAAC8VSuw3CQAy9XBqUAVKxAZkgHQUNEiukySxpqOjTMQEDZIrUDICE'
      || 'RHUVVfy9c0IQJcWTfbafv+ece7u/Izs553cgAyN/APagl+wjgN3XKZ5kmTg/IXkw'
      || 'h4JqXUEfAb1I1VvwFYysk9iCffmN4+gtccSr5nlwDpuTepCZ/MH0FZibDUnO7MoR'
      || 'HXdDuvgjpzNxgevG+dF/hr3dWfoNyEZ8Taqn+7d7ozmqpGM8zdMYruFrXopVjvY2'
      || 'in9gXe+5vBf1KfX9E6TOVBsb8i5iqwQyv9+a3Gg/Cv+VoDtaQ7xdPwfNYRDji09g'
      || 'X/FvLNGmO62B9jSsoFwgfM+jf1z/SPwrkTMBOkCTBQAEAAA=' );
--
    init_core_font( 8, 'times', 'BI', 'Times-BoldItalic'
      ,  'H4sIAAAAAAAAC8WSuw2DMBCGHegYwEuECajIAGwQ0TBFBnCfPktkAKagzgCRIqWi'
      || 'oso9fr+Qo5RB+nT2ve+wMWYzf+fgjKmOJFelPhENnS0xANJXHfwHSBtjfoI8nMMj'
      || 'tXo63xKW/Cx9ONRn3US6C/wWvYeYNr+LH2IY6cHGPkJfvsc5kX7mFjF+Vqs9iT6d'
      || 'zwEL26y1Qz62nWlvD5VSf4R9zPuon/ne+C45+XxXf5lnTGLTOZCXPx8v9Qfdjdid'
      || '5vD/f/+/pE/Ur14kG+xjTHRc84pZWsC2Hjk2+Hgbx78j4Z8W4DlL+rBnEN5Bie6L'
      || 'fsL+1u/InuYCdsdaeAs+RxftKfGdfQDlDF/kAAQAAA==' );
--
    init_core_font( 9, 'courier', 'N', 'Courier', null );
    for i in 0 .. 255
    loop
      g_fonts( 9 ).char_width_tab( i ) := 600;
    end loop;
--
    init_core_font( 10, 'courier', 'I', 'Courier-Oblique', null );
    g_fonts( 10 ).char_width_tab := g_fonts( 9 ).char_width_tab;
--
    init_core_font( 11, 'courier', 'B', 'Courier-Bold', null );
    g_fonts( 11 ).char_width_tab := g_fonts( 9 ).char_width_tab;
--
    init_core_font( 12, 'courier', 'BI', 'Courier-BoldOblique', null );
    g_fonts( 12 ).char_width_tab := g_fonts( 9 ).char_width_tab;
--
    init_core_font( 13, 'symbol', 'N', 'Symbol'
      ,  'H4sIAAAAAAAAC82SIU8DQRCFZ28xIE+cqcbha4tENKk/gQCJJ6AweIK9H1CHqKnp'
      || 'D2gTFBaDIcFwCQkJSTG83fem7SU0qYNLvry5nZ25t7NnZkv7c8LQrFhAP6GHZvEY'
      || 'HOB9ylxGubTfNVRc34mKpFonzBQ/gUZ6Ds7AN6i5lv1dKv8Ab1eKQYSV4hUcgZFq'
      || 'J/Sec7fQHtdTn3iqfvdrb7m3e2pZW+xDG3oIJ/Li3gfMr949rlU74DyT1/AuTX1f'
      || 'YGhOzTP8B0/RggsEX/I03vgXPrrslZjfM8/pGu40t2ZjHgud97F7337mXP/GO4h9'
      || '3WmPPaOJ/jrOs9yC52MlrtUzfWupfTX51X/L+13Vl/J/s4W2S3pSfSh5DmeXerMf'
      || '+LXhWQAEAAA=' );
--
    init_core_font( 14, 'zapfdingbats', 'N', 'ZapfDingbats'
      ,  'H4sIAAAAAAAAC83ROy9EQRjG8TkzjdJl163SSHR0EpdsVkSi2UahFhUljUKUIgoq'
      || 'CrvJCtFQyG6EbSSERGxhC0ofQAQFxbIi8T/7PoUPIOEkvzxzzsycdy7O/fUTtToX'
      || 'bnCuvHPOV8gk4r423ovkGQ5od5OTWMeesmBz/RuZIWv4wCAY4z/xjipeqflC9qAD'
      || 'aRwxrxkJievSFzrRh36tZ1zttL6nkGX+A27xrLnttE/IBji9x7UvcIl9nPJ9AL36'
      || 'd1L9hyihoDW10L62cwhNyhntryZVExYl3kMj+zym+CrJv6M8VozPmfr5L8uwJORL'
      || 'tox7NFHG/Obj79FlwhqZ1X292xn6CbAXP/fjjv6rJYyBtUdl1vxEO6fcRB7bMmJ3'
      || 'GYZsTN0GdrDL/Ao5j1GZNr5kwqydX5z1syoiYEq5gCtlSrXi+mVbi3PfVAuhoQAE'
      || 'AAA=' );
--
  end;
--
  function to_char_round
    ( p_value number
    , p_precision pls_integer := 2
    )
  return varchar2
  is
  begin
    return to_char( round( p_value, p_precision ), 'TM9', 'NLS_NUMERIC_CHARACTERS=.,' );
  end;
--
  procedure raw2pdfdoc( p_raw blob )
  is
  begin
    dbms_lob.append( g_pdf_doc, p_raw );
  end;
--
  procedure txt2pdfdoc( p_txt varchar2 )
  is
  begin
    raw2pdfdoc( utl_raw.cast_to_raw( p_txt || c_nl ) );
  end;
--
  function add_object( p_txt varchar2 := null )
  return number
  is
    t_self number(10);
  begin
    t_self := g_objects.count( );
    g_objects( t_self ) := dbms_lob.getlength( g_pdf_doc );
--
    if p_txt is null
    then
      txt2pdfdoc( t_self || ' 0 obj' );
    else
      txt2pdfdoc( t_self || ' 0 obj' || c_nl || '<<' || p_txt || '>>' || c_nl || 'endobj' );
    end if;
--
    return t_self;
  end;
--
  procedure add_object( p_txt varchar2 := null )
  is
    t_dummy number(10) := add_object( p_txt );
  begin
    null;
  end;
--
  function adler32( p_src in blob )
  return varchar2
  is
    s1 pls_integer := 1;
    s2 pls_integer := 0;
    n  pls_integer;
    step_size number;
    tmp varchar2(32766);
    c65521 constant pls_integer := 65521;
  begin
    step_size := trunc( 16383 / dbms_lob.getchunksize( p_src ) ) * dbms_lob.getchunksize( p_src );
    for j in 0 .. trunc( ( dbms_lob.getlength( p_src ) - 1 ) / step_size )
    loop
      tmp := rawtohex( dbms_lob.substr( p_src, step_size, j * step_size + 1 ) );
      for i in 1 .. length( tmp ) / 2
      loop
        n := to_number( substr( tmp, i * 2 - 1, 2 ), 'xx' );
        s1 := s1 + n;
        if s1 >= c65521
        then
          s1 := s1 - c65521;
        end if;
        s2 := s2 + s1;
        if s2 >= c65521
        then
          s2 := s2 - c65521;
        end if;
      end loop;
    end loop;
    return to_char( s2, 'fm0XXX' ) || to_char( s1, 'fm0XXX' );
  end;
--
  function flate_encode( p_val blob )
  return blob
  is
    t_blob blob;
  begin
    t_blob := hextoraw( '789C' );
    dbms_lob.copy( t_blob
                 , utl_compress.lz_compress( p_val )
                 , dbms_lob.lobmaxsize
                 , 3
                 , 11
                 );
    dbms_lob.trim( t_blob, dbms_lob.getlength( t_blob ) - 8 );
    dbms_lob.append( t_blob, hextoraw( adler32( p_val ) ) );
    return t_blob;
  end;
--
  procedure put_stream
    ( p_stream blob
    , p_compress boolean := true
    , p_extra varchar2 := ''
    , p_tag boolean := true
    )
  is
    t_blob blob;
    t_compress boolean := false;
  begin
    if p_compress and nvl( dbms_lob.getlength( p_stream ), 0 ) > 0
    then
      t_compress := true;
      t_blob := flate_encode( p_stream );
    else
      t_blob := p_stream;
    end if;
    txt2pdfdoc( case when p_tag then '<<' end
                || case when t_compress then '/Filter /FlateDecode ' end
                || '/Length ' || nvl( length( t_blob ), 0 )
                || p_extra
                || '>>' );
    txt2pdfdoc( 'stream' );
    raw2pdfdoc( t_blob );
    txt2pdfdoc( 'endstream' );
    if dbms_lob.istemporary( t_blob ) = 1
    then
      dbms_lob.freetemporary( t_blob );
    end if;
  end;
--
  function add_stream
    ( p_stream blob
    , p_extra varchar2 := ''
    , p_compress boolean := true
    )
  return number
  is
    t_self number(10);
  begin
    t_self := add_object;
    put_stream( p_stream
              , p_compress
              , p_extra
              );
    txt2pdfdoc( 'endobj' );
    return t_self;
  end;
--
  function subset_font( p_index pls_integer )
  return blob
  is
    t_tmp blob;
    t_header blob;
    t_tables blob;
    t_len pls_integer;
    t_code pls_integer;
    t_glyph pls_integer;
    t_offset pls_integer;
    t_factor pls_integer;
    t_unicode pls_integer;
    t_used_glyphs tp_pls_tab;
    t_fmt varchar2(10);
    t_utf16_charset varchar2(1000);
    t_raw raw(32767);
    t_v varchar2(32767);
    t_table_records raw(32767);
  begin
    if g_fonts( p_index ).cid
    then
      t_used_glyphs := g_fonts( p_index ).used_chars;
      t_used_glyphs( 0 ) := 0;
    else
      t_utf16_charset := substr( g_fonts( p_index ).charset, 1, instr( g_fonts( p_index ).charset, '.' ) ) || 'AL16UTF16';
      t_used_glyphs( 0 ) := 0;
      t_code := g_fonts( p_index ).used_chars.first;
      while t_code is not null
      loop
        t_unicode := to_number( rawtohex( utl_raw.convert( hextoraw( to_char( t_code, 'fm0x' ) )
                                                                    , t_utf16_charset
                                                                    , g_fonts( p_index ).charset  -- ???? database characterset ?????
                                                                    )
                                        ), 'XXXXXXXX' );
        if g_fonts( p_index ).flags = 4 -- a symbolic font
        then
-- assume code 32, space maps to the first code from the font
          t_used_glyphs( g_fonts( p_index ).code2glyph( g_fonts( p_index ).code2glyph.first + t_unicode - 32 ) ) := 0;
        else
          t_used_glyphs( g_fonts( p_index ).code2glyph( t_unicode ) ) := 0;
        end if;
        t_code := g_fonts( p_index ).used_chars.next( t_code );
      end loop;
    end if;
--
    dbms_lob.createtemporary( t_tables, true );
    t_header := utl_raw.concat( hextoraw( '00010000' )
                              , dbms_lob.substr( g_fonts( p_index ).fontfile2, 8, g_fonts( p_index ).ttf_offset + 4 )
                              );
    t_offset := 12 + blob2num( g_fonts( p_index ).fontfile2, 2, g_fonts( p_index ).ttf_offset + 4 ) * 16;
    t_table_records := dbms_lob.substr( g_fonts( p_index ).fontfile2
                                      , blob2num( g_fonts( p_index ).fontfile2, 2, g_fonts( p_index ).ttf_offset + 4 ) * 16
                                      , g_fonts( p_index ).ttf_offset + 12
                                      );
    for i in 1 .. blob2num( g_fonts( p_index ).fontfile2, 2, g_fonts( p_index ).ttf_offset + 4 )
    loop
      case utl_raw.cast_to_varchar2( utl_raw.substr( t_table_records, i * 16 - 15, 4 ) )
        when 'post'
        then
          dbms_lob.append( t_header
                         , utl_raw.concat( utl_raw.substr( t_table_records, i * 16 - 15, 4 ) -- tag
                                         , hextoraw( '00000000' ) -- checksum
                                         , num2raw( t_offset + dbms_lob.getlength( t_tables ) ) -- offset
                                         , num2raw( 32 ) -- length
                                         )
                         );
          dbms_lob.append( t_tables
                         , utl_raw.concat( hextoraw( '00030000' )
                                         , dbms_lob.substr( g_fonts( p_index ).fontfile2
                                                          , 28
                                                          , raw2num( t_table_records, i * 16 - 7, 4 ) + 5
                                                          )
                                         )
                         );
        when 'loca'
        then
          if g_fonts( p_index ).indexToLocFormat = 0
          then
            t_fmt := 'fm0XXX';
          else
            t_fmt := 'fm0XXXXXXX';
          end if;
          t_raw := null;
          dbms_lob.createtemporary( t_tmp, true );
          t_len := 0;
          for g in 0 .. g_fonts( p_index ).numGlyphs - 1
          loop
            t_raw := utl_raw.concat( t_raw, hextoraw( to_char( t_len, t_fmt ) ) );
            if utl_raw.length( t_raw ) > 32770
            then
              dbms_lob.append( t_tmp, t_raw );
              t_raw := null;
            end if;
            if t_used_glyphs.exists( g )
            then
              t_len := t_len + g_fonts( p_index ).loca( g + 1 ) - g_fonts( p_index ).loca( g );
            end if;
          end loop;
          t_raw := utl_raw.concat( t_raw, hextoraw( to_char( t_len, t_fmt ) ) );
          dbms_lob.append( t_tmp, t_raw );
          dbms_lob.append( t_header
                         , utl_raw.concat( utl_raw.substr( t_table_records, i * 16 - 15, 4 ) -- tag
                                         , hextoraw( '00000000' ) -- checksum
                                         , num2raw( t_offset + dbms_lob.getlength( t_tables ) ) -- offset
                                         , num2raw( dbms_lob.getlength( t_tmp ) ) -- length
                                         )
                         );
          dbms_lob.append( t_tables, t_tmp );
          dbms_lob.freetemporary( t_tmp );
        when 'glyf'
        then
          if g_fonts( p_index ).indexToLocFormat = 0
          then
            t_factor := 2;
          else
            t_factor := 1;
          end if;
          t_raw := null;
          dbms_lob.createtemporary( t_tmp, true );
          for g in 0 .. g_fonts( p_index ).numGlyphs - 1
          loop
            if (   t_used_glyphs.exists( g )
               and g_fonts( p_index ).loca( g + 1 ) > g_fonts( p_index ).loca( g )
               )
            then
              t_raw := utl_raw.concat( t_raw
                                     , dbms_lob.substr( g_fonts( p_index ).fontfile2
                                                      , ( g_fonts( p_index ).loca( g + 1 ) - g_fonts( p_index ).loca( g ) ) * t_factor
                                                      , g_fonts( p_index ).loca( g ) * t_factor + raw2num( t_table_records, i * 16 - 7, 4 ) + 1
                                                      )
                                     );
              if utl_raw.length( t_raw ) > 32778
              then
                dbms_lob.append( t_tmp, t_raw );
                t_raw := null;
              end if;
            end if;
          end loop;
          if utl_raw.length( t_raw ) > 0
          then
            dbms_lob.append( t_tmp, t_raw );
          end if;
          dbms_lob.append( t_header
                         , utl_raw.concat( utl_raw.substr( t_table_records, i * 16 - 15, 4 ) -- tag
                                         , hextoraw( '00000000' ) -- checksum
                                         , num2raw( t_offset + dbms_lob.getlength( t_tables ) ) -- offset
                                         , num2raw( dbms_lob.getlength( t_tmp ) ) -- length
                                         )
                         );
          dbms_lob.append( t_tables, t_tmp );
          dbms_lob.freetemporary( t_tmp );
        else
          dbms_lob.append( t_header
                         , utl_raw.concat( utl_raw.substr( t_table_records, i * 16 - 15, 4 )    -- tag
                                         , utl_raw.substr( t_table_records, i * 16 - 11, 4 )    -- checksum
                                         , num2raw( t_offset + dbms_lob.getlength( t_tables ) ) -- offset
                                         , utl_raw.substr( t_table_records, i * 16 - 3, 4 )     -- length
                                         )
                         );
          dbms_lob.copy( t_tables
                       , g_fonts( p_index ).fontfile2
                       , raw2num( t_table_records, i * 16 - 3, 4 )
                       , dbms_lob.getlength( t_tables ) + 1
                       , raw2num( t_table_records, i * 16 - 7, 4 ) + 1
                       );
      end case;
    end loop;
    dbms_lob.append( t_header, t_tables );
    dbms_lob.freetemporary( t_tables );
    return t_header;
  end;
--
  function add_font( p_index pls_integer )
  return number
  is
    t_self number(10);
    t_fontfile number(10);
    t_font_subset blob;
    t_used pls_integer;
    t_used_glyphs tp_pls_tab;
    t_w varchar2(32767);
    t_unicode pls_integer;
    t_utf16_charset varchar2(1000);
    t_width number;
  begin
    if g_fonts( p_index ).standard
    then
      return add_object( '/Type/Font'
                       || '/Subtype/Type1'
                       || '/BaseFont/' || g_fonts( p_index ).name
                       || '/Encoding/WinAnsiEncoding' -- code page 1252
                       );
    end if;
--
    if g_fonts( p_index ).cid
    then
      t_self := add_object;
      txt2pdfdoc( '<</Type/Font/Subtype/Type0/Encoding/Identity-H'
                || '/BaseFont/' || g_fonts( p_index ).name
                || '/DescendantFonts ' || to_char( t_self + 1 ) || ' 0 R'
                || '/ToUnicode ' || to_char( t_self + 8 ) || ' 0 R'
                || '>>' );
      txt2pdfdoc( 'endobj' );
      add_object;
      txt2pdfdoc( '[' || to_char( t_self + 2 ) || ' 0 R]' );
      txt2pdfdoc( 'endobj' );
      add_object( '/Type/Font/Subtype/CIDFontType2/CIDToGIDMap/Identity/DW 1000'
                || '/BaseFont/' || g_fonts( p_index ).name
                || '/CIDSystemInfo ' || to_char( t_self + 3 ) || ' 0 R'
                || '/W ' || to_char( t_self + 4 ) || ' 0 R'
                || '/FontDescriptor ' || to_char( t_self + 5 ) || ' 0 R' );
      add_object( '/Ordering(Identity) /Registry(Adobe) /Supplement 0' );
--
      t_utf16_charset := substr( g_fonts( p_index ).charset, 1, instr( g_fonts( p_index ).charset, '.' ) ) || 'AL16UTF16';
      t_used_glyphs := g_fonts( p_index ).used_chars;
      t_used_glyphs( 0 ) := 0;
      t_used := t_used_glyphs.first();
      while t_used is not null
      loop
        if g_fonts( p_index ).hmetrics.exists( t_used )
        then
          t_width := g_fonts( p_index ).hmetrics( t_used );
        else
          t_width := g_fonts( p_index ).hmetrics( g_fonts( p_index ).hmetrics.last() );
        end if;
        t_width := trunc( t_width * g_fonts( p_index ).unit_norm );
        if t_used_glyphs.prior( t_used ) = t_used - 1
        then
          t_w := t_w || ' ' || t_width;
        else
          t_w := t_w || '] ' || t_used || ' [' || t_width;
        end if;
        t_used := t_used_glyphs.next( t_used );
      end loop;
      t_w := '[' || ltrim( t_w, '] ' ) || ']]';
      add_object;
      txt2pdfdoc( t_w );
      txt2pdfdoc( 'endobj' );
      add_object
        (    '/Type/FontDescriptor'
          || '/FontName/' || g_fonts( p_index ).name
          || '/Flags ' || g_fonts( p_index ).flags
          || '/FontBBox [' || g_fonts( p_index ).bb_xmin
          || ' ' || g_fonts( p_index ).bb_ymin
          || ' ' || g_fonts( p_index ).bb_xmax
          || ' ' || g_fonts( p_index ).bb_ymax
          || ']'
          || '/ItalicAngle ' || to_char_round( g_fonts( p_index ).italic_angle )
          || '/Ascent ' || g_fonts( p_index ).ascent
          || '/Descent ' || g_fonts( p_index ).descent
          || '/CapHeight ' || g_fonts( p_index ).capheight
          || '/StemV ' || g_fonts( p_index ).stemv
          || '/FontFile2 ' || to_char( t_self + 6 ) || ' 0 R' );
      t_fontfile := add_stream( g_fonts( p_index ).fontfile2
                              , '/Length1 ' || dbms_lob.getlength( g_fonts( p_index ).fontfile2 )
                              , g_fonts( p_index ).compress_font
                              );
      t_font_subset := subset_font( p_index );
      t_fontfile := add_stream( t_font_subset
                              , '/Length1 ' || dbms_lob.getlength( t_font_subset )
                              , g_fonts( p_index ).compress_font
                              );
      declare
        t_g2c tp_pls_tab;
        t_code     pls_integer;
        t_c_start  pls_integer;
        t_map varchar2(32767);
        t_cmap varchar2(32767);
        t_cor pls_integer;
        t_cnt pls_integer;
      begin
        t_code := g_fonts( p_index ).code2glyph.first;
        if g_fonts( p_index ).flags = 4 -- a symbolic font
        then
-- assume code 32, space maps to the first code from the font
          t_cor := t_code - 32;
        else
          t_cor := 0;
        end if;
        while t_code is not null
        loop
          t_g2c( g_fonts( p_index ).code2glyph( t_code ) ) := t_code - t_cor;
          t_code := g_fonts( p_index ).code2glyph.next( t_code );
        end loop;
        t_cnt := 0;
        t_used_glyphs := g_fonts( p_index ).used_chars;
        t_used := t_used_glyphs.first();
        while t_used is not null
        loop
          t_map := t_map || '<' || to_char( t_used, 'FM0XXX' )
                 || '> <' || to_char( t_g2c( t_used ), 'FM0XXX' )
                 || '>' || chr( 10 );
          if t_cnt = 99
          then
            t_cnt := 0;
            t_cmap := t_cmap || chr( 10 ) || '100 beginbfchar' || chr( 10 ) || t_map || 'endbfchar';
            t_map := '';
          else
            t_cnt := t_cnt + 1;
          end if;
          t_used := t_used_glyphs.next( t_used );
        end loop;
        if t_cnt > 0
        then
          t_cmap := t_cnt || ' beginbfchar' || chr( 10 ) || t_map || 'endbfchar';
        end if;
        t_fontfile := add_stream( utl_raw.cast_to_raw(
'/CIDInit /ProcSet findresource begin 12 dict begin
begincmap
/CIDSystemInfo
<< /Registry (Adobe) /Ordering (UCS) /Supplement 0 >> def
/CMapName /Adobe-Identity-UCS def /CMapType 2 def
1 begincodespacerange
<0000> <FFFF>
endcodespacerange
' || t_cmap || '
endcmap
CMapName currentdict /CMap defineresource pop
end
end' ) );
      end;
      return t_self;
    end if;
--
    g_fonts( p_index ).first_char := g_fonts( p_index ).used_chars.first();
    g_fonts( p_index ).last_char := g_fonts( p_index ).used_chars.last();
    t_self := add_object;
    txt2pdfdoc( '<</Type /Font '
              || '/Subtype /' || g_fonts( p_index ).subtype
              || ' /BaseFont /' || g_fonts( p_index ).name
              || ' /FirstChar ' || g_fonts( p_index ).first_char
              || ' /LastChar ' || g_fonts( p_index ).last_char
              || ' /Widths ' || to_char( t_self + 1 ) || ' 0 R'
              || ' /FontDescriptor ' || to_char( t_self + 2 ) || ' 0 R'
              || ' /Encoding ' || to_char( t_self + 3 ) || ' 0 R'
              || ' >>' );
    txt2pdfdoc( 'endobj' );
    add_object;
    txt2pdfdoc( '[' );
      begin
        for i in g_fonts( p_index ).first_char .. g_fonts( p_index ).last_char
        loop
          txt2pdfdoc( g_fonts( p_index ).char_width_tab( i ) );
        end loop;
      exception
        when others
        then
          dbms_output.put_line( '**** ' || g_fonts( p_index ).name );
      end;
      txt2pdfdoc( ']' );
      txt2pdfdoc( 'endobj' );
      add_object
        (    '/Type /FontDescriptor'
          || ' /FontName /' || g_fonts( p_index ).name
          || ' /Flags ' || g_fonts( p_index ).flags
          || ' /FontBBox [' || g_fonts( p_index ).bb_xmin
          || ' ' || g_fonts( p_index ).bb_ymin
          || ' ' || g_fonts( p_index ).bb_xmax
          || ' ' || g_fonts( p_index ).bb_ymax
          || ']'
          || ' /ItalicAngle ' || to_char_round( g_fonts( p_index ).italic_angle )
          || ' /Ascent ' || g_fonts( p_index ).ascent
          || ' /Descent ' || g_fonts( p_index ).descent
          || ' /CapHeight ' || g_fonts( p_index ).capheight
          || ' /StemV ' || g_fonts( p_index ).stemv
          || case
               when g_fonts( p_index ).fontfile2 is not null
                 then ' /FontFile2 ' || to_char( t_self + 4 ) || ' 0 R'
             end );
      add_object(    '/Type /Encoding /BaseEncoding /WinAnsiEncoding '
                         || g_fonts( p_index ).diff
                         || ' ' );
      if g_fonts( p_index ).fontfile2 is not null
      then
        t_font_subset := subset_font( p_index );
        t_fontfile :=
          add_stream( t_font_subset
                    , '/Length1 ' || dbms_lob.getlength( t_font_subset )
                    , g_fonts( p_index ).compress_font
                    );
    end if;
    return t_self;
  end;
--
  procedure add_image( p_img tp_img )
  is
    t_pallet number(10);
  begin
    if p_img.color_tab is not null
    then
      t_pallet := add_stream( p_img.color_tab );
    else
      t_pallet := add_object;  -- add an empty object
      txt2pdfdoc( 'endobj' );
    end if;
    add_object;
    txt2pdfdoc( '<</Type /XObject /Subtype /Image'
              ||  ' /Width ' || to_char( p_img.width )
              || ' /Height ' || to_char( p_img.height )
              || ' /BitsPerComponent ' || to_char( p_img.color_res )
              );
--
    if p_img.transparancy_index is not null
    then
      txt2pdfdoc( '/Mask [' || p_img.transparancy_index || ' ' || p_img.transparancy_index || ']' );
    end if;
    if p_img.color_tab is null
    then
      if p_img.greyscale
      then
        txt2pdfdoc( '/ColorSpace /DeviceGray' );
      else
        txt2pdfdoc( '/ColorSpace /DeviceRGB' );
      end if;
    else
      txt2pdfdoc(    '/ColorSpace [/Indexed /DeviceRGB '
                || to_char( utl_raw.length( p_img.color_tab ) / 3 - 1 )
                || ' ' || to_char( t_pallet ) || ' 0 R]'
                );
    end if;
--
    if p_img.type = 'jpg'
    then
      put_stream( p_img.pixels, false, '/Filter /DCTDecode', false );
    elsif p_img.type = 'png'
    then
      put_stream( p_img.pixels, false
                ,  ' /Filter /FlateDecode /DecodeParms <</Predictor 15 '
                || '/Colors ' || p_img.nr_colors
                || '/BitsPerComponent ' || p_img.color_res
                || ' /Columns ' || p_img.width
                || ' >> '
                , false );
    else
      put_stream( p_img.pixels, p_tag => false );
    end if;
    txt2pdfdoc( 'endobj' );
  end;
--
  function add_resources
  return number
  is
    t_ind pls_integer;
    t_self number(10);
    t_fonts tp_objects_tab;
  begin
--
    t_ind := g_used_fonts.first;
    while t_ind is not null
    loop
      t_fonts( t_ind ) := add_font( t_ind );
      t_ind := g_used_fonts.next( t_ind );
    end loop;
--
    t_self := add_object;
    txt2pdfdoc( '<</ProcSet [/PDF /Text]' );
--
    if g_used_fonts.count() > 0
    then
      txt2pdfdoc( '/Font <<' );
      t_ind := g_used_fonts.first;
      while t_ind is not null
      loop
        txt2pdfdoc( '/F'|| to_char( t_ind ) || ' '
                  || to_char( t_fonts( t_ind ) ) || ' 0 R'
                  );
        t_ind := g_used_fonts.next( t_ind );
      end loop;
      txt2pdfdoc( '>>' );
    end if;
--
    if g_images.count( ) > 0
    then
      txt2pdfdoc( '/XObject <<' );
      for i in g_images.first .. g_images.last
      loop
        txt2pdfdoc( '/I' || to_char( i ) || ' ' || to_char( t_self + 2 * i ) || ' 0 R' );
      end loop;
      txt2pdfdoc( '>>' );
    end if;
--
    txt2pdfdoc( '>>' );
    txt2pdfdoc( 'endobj' );
--
    if g_images.count( ) > 0
    then
      for i in g_images.first .. g_images.last
      loop
        add_image( g_images( i ) );
      end loop;
    end if;
    return t_self;
  end;
--
  procedure add_page
    ( p_page_ind pls_integer
    , p_parent number
    , p_resources number
    )
  is
    t_content number(10);
  begin
    t_content := add_stream( g_pages( p_page_ind ) );
    add_object;
    txt2pdfdoc( '<< /Type /Page' );
    txt2pdfdoc( '/Parent ' || to_char( p_parent ) || ' 0 R' );
    txt2pdfdoc( '/Contents ' || to_char( t_content ) || ' 0 R' );
    txt2pdfdoc( '/Resources ' || to_char( p_resources ) || ' 0 R' );
    txt2pdfdoc( '>>' );
    txt2pdfdoc( 'endobj' );
  end;
--
  function add_pages
  return number
  is
    t_self number(10);
    t_resources number(10);
  begin
    t_resources := add_resources;
    t_self := add_object;
    txt2pdfdoc( '<</Type/Pages/Kids [' );
--
    for i in g_pages.first .. g_pages.last
    loop
      txt2pdfdoc( to_char( t_self + i * 2 + 2 ) || ' 0 R' );
    end loop;
--
    txt2pdfdoc( ']' );
    txt2pdfdoc( '/Count ' || g_pages.count() );
    txt2pdfdoc(    '/MediaBox [0 0 '
                || to_char_round( g_settings.page_width
                                , 0
                                )
                || ' '
                || to_char_round( g_settings.page_height
                                , 0
                                )
                || ']' );
    txt2pdfdoc( '>>' );
    txt2pdfdoc( 'endobj' );
--
    if g_pages.count() > 0
    then
      for i in g_pages.first .. g_pages.last
      loop
        add_page( i, t_self, t_resources );
      end loop;
    end if;
--
    return t_self;
  end;
--
  function add_catalogue
  return number
  is
  begin
    return add_object( '/Type/Catalog'
                     || '/Pages ' || to_char( add_pages ) || ' 0 R'
                     || '/OpenAction [0 /XYZ null null 0.77]'
                     );
  end;
--
  function add_info
  return number
  is
    t_banner varchar2( 1000 );
  begin
    begin
      select    'running on '
             || replace( replace( replace( substr( banner
                                                 , 1
                                                 , 950
                                                 )
                                         , '\'
                                         , '\\'
                                         )
                                , '('
                                , '\('
                                )
                       , ')'
                       , '\)'
                       )
      into t_banner
      from v$version
      where instr( upper( banner )
                 , 'DATABASE'
                 ) > 0;
      t_banner := '/Producer (' || t_banner || ')';
    exception
      when others
      then
        null;
    end;
--
    return add_object( to_char( sysdate, '"/CreationDate (D:"YYYYMMDDhh24miss")"' )
                     || '/Creator (AS-PDF 0.3.0 by Anton Scheffer)'
                     || t_banner
                     || '/Title <FEFF' || utl_i18n.string_to_raw( g_info.title, 'AL16UTF16' ) || '>'
                     || '/Author <FEFF' || utl_i18n.string_to_raw( g_info.author, 'AL16UTF16' ) || '>'
                     || '/Subject <FEFF' || utl_i18n.string_to_raw( g_info.subject, 'AL16UTF16' ) || '>'
                     || '/Keywords <FEFF' || utl_i18n.string_to_raw( g_info.keywords, 'AL16UTF16' ) || '>'
                     );
  end;
--
  procedure finish_pdf
  is
    t_xref number;
    t_info number(10);
    t_catalogue number(10);
  begin
    if g_pages.count = 0
    then
      new_page;
    end if;
    if g_page_prcs.count > 0
    then
      for i in g_pages.first .. g_pages.last
      loop
        g_page_nr := i;
        for p in g_page_prcs.first .. g_page_prcs.last
        loop  
          begin
            execute immediate replace( replace( g_page_prcs( p ), '#PAGE_NR#', i + 1 ), '"PAGE_COUNT#', g_pages.count );
          exception
            when others then null;
          end;
        end loop;
      end loop;
    end if;
    dbms_lob.createtemporary( g_pdf_doc, true );
    txt2pdfdoc( '%PDF-1.3' );
    raw2pdfdoc( hextoraw( '25E2E3CFD30D0A' ) );          -- add a hex comment
    t_info := add_info;
    t_catalogue := add_catalogue;
    t_xref := dbms_lob.getlength( g_pdf_doc );
    txt2pdfdoc( 'xref' );
    txt2pdfdoc( '0 ' || to_char( g_objects.count() ) );
    txt2pdfdoc( '0000000000 65535 f ' );
    for i in 1 .. g_objects.count( ) - 1
    loop
      txt2pdfdoc( to_char( g_objects( i ), 'fm0000000000' ) || ' 00000 n' );
                        -- this line should be exactly 20 bytes, including EOL
    end loop;
    txt2pdfdoc( 'trailer' );
    txt2pdfdoc( '<< /Root ' || to_char( t_catalogue ) || ' 0 R' );
    txt2pdfdoc( '/Info ' || to_char( t_info ) || ' 0 R' );
    txt2pdfdoc( '/Size ' || to_char( g_objects.count() ) );
    txt2pdfdoc( '>>' );
    txt2pdfdoc( 'startxref' );
    txt2pdfdoc( to_char( t_xref ) );
    txt2pdfdoc( '%%EOF' );
--
    g_objects.delete;
    for i in g_pages.first .. g_pages.last
    loop
      dbms_lob.freetemporary( g_pages( i ) );
    end loop;
    g_objects.delete;
    g_pages.delete;
    g_fonts.delete;
    g_used_fonts.delete;
    g_page_prcs.delete;
    if g_images.count() > 0
    then
      for i in g_images.first .. g_images.last
      loop
        if dbms_lob.istemporary( g_images( i ).pixels ) = 1
        then
          dbms_lob.freetemporary( g_images( i ).pixels );
        end if;
      end loop;
      g_images.delete;
    end if;
  end;
--
  function conv2uu( p_value number, p_unit varchar2 )
  return number
  is
   c_inch constant number := 25.40025;
  begin
    return round( case lower( p_unit )
                    when 'mm' then p_value * 72 / c_inch
                    when 'cm' then p_value * 720 / c_inch
                    when 'pt' then p_value          -- also point
                    when 'point' then p_value
                    when 'inch'  then p_value * 72
                    when 'in'    then p_value * 72  -- also inch
                    when 'pica'  then p_value * 12
                    when 'p'     then p_value * 12  -- also pica
                    when 'pc'    then p_value * 12  -- also pica
                    when 'em'    then p_value * 12  -- also pica
                    when 'px'    then p_value       -- pixel voorlopig op point zetten
                    when 'px'    then p_value * 0.8 -- pixel
                    else null
                  end
                , 3
                );
  end;
--
  procedure set_page_size
    ( p_width number
    , p_height number
    , p_unit varchar2 := 'cm'
    )
  is
  begin
    g_settings.page_width := conv2uu( p_width, p_unit );
    g_settings.page_height := conv2uu( p_height, p_unit );
  end;
--
  procedure set_page_format( p_format varchar2 := 'A4' )
  is
  begin
    case upper( p_format )
      when 'A3'
      then
        set_page_size( 420, 297, 'mm' );
      when 'A4'
      then
        set_page_size( 297, 210, 'mm' );
      when 'A5'
      then
        set_page_size( 210, 148, 'mm' );
      when 'A6'
      then
        set_page_size( 148, 105, 'mm' );
      when 'LEGAL'
      then
        set_page_size( 14, 8.5, 'in' );
      when 'LETTER'
      then
        set_page_size( 11, 8.5, 'in' );
      when 'QUARTO'
      then
        set_page_size( 11, 9, 'in' );
      when 'EXECUTIVE'
      then
        set_page_size( 10.5, 7.25, 'in' );
      else
        null;
    end case;
  end;
--
  procedure set_page_orientation( p_orientation varchar2 := 'PORTRAIT' )
  is
    t_tmp number;
  begin
    if (  (   upper( p_orientation ) in ( 'L', 'LANDSCAPE' )
          and g_settings.page_height > g_settings.page_width
          )
       or ( upper( p_orientation ) in( 'P', 'PORTRAIT' )
          and g_settings.page_height < g_settings.page_width
          )
       )
    then
      t_tmp := g_settings.page_width;
      g_settings.page_width := g_settings.page_height;
      g_settings.page_height := t_tmp;
    end if;
  end;
--
  procedure set_margins
    ( p_top number := null
    , p_left number := null
    , p_bottom number := null
    , p_right number := null
    , p_unit varchar2 := 'cm'
    )
  is
    t_tmp number;
  begin
    t_tmp := nvl( conv2uu( p_top, p_unit ), -1 );
    if t_tmp < 0 or t_tmp > g_settings.page_height
    then
      t_tmp := conv2uu( 3, 'cm' );
    end if;
    g_settings.margin_top := t_tmp;
    t_tmp := nvl( conv2uu( p_bottom, p_unit ), -1 );
    if t_tmp < 0 or t_tmp > g_settings.page_height
    then
      t_tmp := conv2uu( 4, 'cm' );
    end if;
    g_settings.margin_bottom := t_tmp;
    t_tmp := nvl( conv2uu( p_left, p_unit ), -1 );
    if t_tmp < 0 or t_tmp > g_settings.page_width
    then
      t_tmp := conv2uu( 1, 'cm' );
    end if;
    g_settings.margin_left := t_tmp;
    t_tmp := nvl( conv2uu( p_right, p_unit ), -1 );
    if t_tmp < 0 or t_tmp > g_settings.page_width
    then
      t_tmp := conv2uu( 1, 'cm' );
    end if;
    g_settings.margin_right := t_tmp;
--
    if g_settings.margin_top + g_settings.margin_bottom + conv2uu( 1, 'cm' )> g_settings.page_height
    then
      g_settings.margin_top := 0;
      g_settings.margin_bottom := 0;
    end if;
    if g_settings.margin_left + g_settings.margin_right + conv2uu( 1, 'cm' )> g_settings.page_width
    then
      g_settings.margin_left := 0;
      g_settings.margin_right := 0;
    end if;
  end;
--
  procedure set_info
    ( p_title varchar2 := null
    , p_author varchar2 := null
    , p_subject varchar2 := null
    , p_keywords varchar2 := null
    )
  is
  begin
    g_info.title := substr( p_title, 1, 1024 );
    g_info.author := substr( p_author, 1, 1024 );
    g_info.subject := substr( p_subject, 1, 1024 );
    g_info.keywords := substr( p_keywords, 1, 16383 );
  end;
--
  procedure init
  is
  begin
    g_objects.delete;
    g_pages.delete;
    g_fonts.delete;
    g_used_fonts.delete;
    g_page_prcs.delete;
    g_images.delete;
    g_settings := null;
    g_current_font := null;
    g_x := null;
    g_y := null;
    g_info := null;
    g_page_nr := null;
    g_objects( 0 ) := 0;
    init_core_fonts;
    set_page_format;
    set_page_orientation;
    set_margins;
  end;
--
  function get_pdf
  return blob
  is
  begin
    finish_pdf;
    return g_pdf_doc;
  end;
--
  procedure save_pdf
    ( p_dir varchar2 := 'MY_DIR'
    , p_filename varchar2 := 'my.pdf'
    , p_freeblob boolean := true
    )
  is
    t_fh utl_file.file_type;
    t_len pls_integer := 32767;
  begin
    finish_pdf;
    t_fh := utl_file.fopen( p_dir, p_filename, 'wb' );
    for i in 0 .. trunc( ( dbms_lob.getlength( g_pdf_doc ) - 1 ) / t_len )
    loop
      utl_file.put_raw( t_fh
                      , dbms_lob.substr( g_pdf_doc
                                       , t_len
                                       , i * t_len + 1
                                       )
                      );
    end loop;
    utl_file.fclose( t_fh );
    if p_freeblob
    then
      dbms_lob.freetemporary( g_pdf_doc );
    end if;
  end;
--
  procedure raw2page( p_txt blob )
  is
  begin
    if g_pages.count() = 0
    then
      new_page;
    end if;
    dbms_lob.append( g_pages( coalesce( g_page_nr, g_pages.count( ) - 1 ) )
                   , utl_raw.concat( p_txt, hextoraw( '0D0A' ) )
                   );
  end;
--
  procedure txt2page( p_txt varchar2 )
  is
  begin
    raw2page( utl_raw.cast_to_raw( p_txt ) );
  end;
--
  procedure output_font_to_doc( p_output_to_doc boolean )
  is
  begin
    if p_output_to_doc
    then
      txt2page( 'BT /F' || g_current_font || ' '
              || to_char_round( g_fonts( g_current_font ).fontsize ) || ' Tf ET'
              );
    end if;
  end;
--
  procedure set_font
    ( p_index pls_integer
    , p_fontsize_pt number
    , p_output_to_doc boolean := true
    )
  is
  begin
    if p_index is not null
    then
      g_used_fonts( p_index ) := 0;
      g_current_font := p_index;
      g_fonts( p_index ).fontsize := p_fontsize_pt;
      output_font_to_doc( p_output_to_doc );
    end if;
  end;
--
  function set_font
    ( p_fontname varchar2
    , p_fontsize_pt number
    , p_output_to_doc boolean := true
    )
  return pls_integer
  is
    t_fontname varchar2(100);
  begin
    if p_fontname is null
    then
      if (  g_current_font is not null
         and p_fontsize_pt != g_fonts( g_current_font ).fontsize
         )
      then
        g_fonts( g_current_font ).fontsize := p_fontsize_pt;
        output_font_to_doc( p_output_to_doc );
      end if;
      return g_current_font;
    end if;
--
    t_fontname := lower( p_fontname );
    for i in g_fonts.first .. g_fonts.last
    loop
      if lower( g_fonts( i ).fontname ) = t_fontname
      then
        exit when g_current_font = i and g_fonts( i ).fontsize = p_fontsize_pt and g_page_nr is null;
        g_fonts( i ).fontsize := coalesce( p_fontsize_pt
                                         , g_fonts( nvl( g_current_font, i ) ).fontsize
                                         , 12
                                         );
        g_current_font := i;
        g_used_fonts( i ) := 0;
        output_font_to_doc( p_output_to_doc );
        return g_current_font;
      end if;
    end loop;
    return null;
  end;
--
  procedure set_font
    ( p_fontname varchar2
    , p_fontsize_pt number
    , p_output_to_doc boolean := true
    )
  is
    t_dummy pls_integer;
  begin
    t_dummy := set_font( p_fontname, p_fontsize_pt, p_output_to_doc );
  end;
--
  function set_font
    ( p_family varchar2
    , p_style varchar2 := 'N'
    , p_fontsize_pt number := null
    , p_output_to_doc boolean := true
    )
  return pls_integer
  is
    t_family varchar2(100);
    t_style varchar2(100);
  begin
    if p_family is null and g_current_font is null
    then
      return null;
    end if;
    if p_family is null and  p_style is null and p_fontsize_pt is null
    then
      return null;
    end if;
    t_family := coalesce( lower( p_family )
                        , g_fonts( g_current_font ).family
                        );
    t_style := upper( p_style );
    t_style := case t_style
                 when 'NORMAL' then 'N'
                 when 'REGULAR' then 'N'
                 when 'BOLD' then 'B'
                 when 'ITALIC' then 'I'
                 when 'OBLIQUE' then 'I'
                 else t_style
               end;
    t_style := coalesce( t_style
                       , case when g_current_font is null then 'N' else g_fonts( g_current_font ).style end
                       );
--
    for i in g_fonts.first .. g_fonts.last
    loop
      if (   g_fonts( i ).family = t_family
         and g_fonts( i ).style = t_style
         )
      then
        return set_font( g_fonts( i ).fontname, p_fontsize_pt, p_output_to_doc );
      end if;
    end loop;
    return null;
  end;
--
  procedure set_font
    ( p_family varchar2
    , p_style varchar2 := 'N'
    , p_fontsize_pt number := null
    , p_output_to_doc boolean := true
    )
  is
    t_dummy pls_integer;
  begin
    t_dummy := set_font( p_family, p_style, p_fontsize_pt, p_output_to_doc );
  end;
--
  procedure new_page
  is
  begin
    g_pages( g_pages.count() ) := null;
    dbms_lob.createtemporary( g_pages( g_pages.count() - 1 ), true );
    if g_current_font is not null and g_pages.count() > 0
    then
      txt2page( 'BT /F' || g_current_font || ' '
              || to_char_round( g_fonts( g_current_font ).fontsize )
              || ' Tf ET'
              );
    end if;
    g_x := null;
    g_y := null;
  end;
--
  function pdf_string( p_txt in blob )
  return blob
  is
    t_rv blob;
    t_ind integer;
    type tp_tab_raw is table of raw(1);
    tab_raw tp_tab_raw
      := tp_tab_raw( utl_raw.cast_to_raw( '\' )
                   , utl_raw.cast_to_raw( '(' )
                   , utl_raw.cast_to_raw( ')' )
                   );
  begin
    t_rv := p_txt;
    for i in tab_raw.first .. tab_raw.last
    loop
      t_ind := -1;
      loop
        t_ind := dbms_lob.instr( t_rv
                               , tab_raw( i )
                               , t_ind + 2
                               );
        exit when t_ind <= 0;
        dbms_lob.copy( t_rv
                     , t_rv
                     , dbms_lob.lobmaxsize
                     , t_ind + 1
                     , t_ind
                     );
        dbms_lob.copy( t_rv
                     , utl_raw.cast_to_raw( '\' )
                     , 1
                     , t_ind
                     , 1
                     );
      end loop;
    end loop;
    return t_rv;
  end;
--
  function txt2raw( p_txt varchar2 )
  return raw
  is
    t_rv raw(32767);
    t_unicode pls_integer;
  begin
    if g_current_font is null
    then
      set_font( 'helvetica' );
    end if;
    if g_fonts( g_current_font ).cid
    then
      for i in 1 .. length( p_txt )
      loop
        t_unicode := utl_raw.cast_to_binary_integer( utl_raw.convert( utl_raw.cast_to_raw( substr( p_txt, i, 1 ) )
                                                                    , 'AMERICAN_AMERICA.AL16UTF16'
                                                                    , sys_context( 'userenv', 'LANGUAGE' )  -- ???? font characterset ?????
                                                                    )
                                                 );
        if g_fonts( g_current_font ).flags = 4 -- a symbolic font
        then
-- assume code 32, space maps to the first code from the font
          t_unicode := g_fonts( g_current_font ).code2glyph.first + t_unicode - 32;
        end if;
        if g_fonts( g_current_font ).code2glyph.exists( t_unicode )
        then
          g_fonts( g_current_font ).used_chars( g_fonts( g_current_font ).code2glyph( t_unicode ) ) := 0;
          t_rv := utl_raw.concat( t_rv
                                , utl_raw.cast_to_raw( to_char( g_fonts( g_current_font ).code2glyph( t_unicode ), 'FM0XXX' ) )
                                );
        else
          t_rv := utl_raw.concat( t_rv, utl_raw.cast_to_raw( '0000' ) );
        end if;
      end loop;
      t_rv := utl_raw.concat( utl_raw.cast_to_raw( '<' )
                            , t_rv
                            , utl_raw.cast_to_raw( '>' )
                            );
    else
      t_rv := utl_raw.convert( utl_raw.cast_to_raw( p_txt )
                             , g_fonts( g_current_font ).charset
                             , sys_context( 'userenv', 'LANGUAGE' )
                             );
      for i in 1 .. utl_raw.length( t_rv )
      loop
        g_fonts( g_current_font ).used_chars( raw2num( t_rv, i, 1 ) ) := 0;
      end loop;
      t_rv := utl_raw.concat( utl_raw.cast_to_raw( '(' )
                            , pdf_string( t_rv )
                            , utl_raw.cast_to_raw( ')' )
                            );
    end if;
    return t_rv;
  end;
--
  procedure put_raw( p_x number, p_y number, p_txt raw, p_degrees_rotation number := null )
  is
    c_pi constant number := 3.14159265358979323846264338327950288419716939937510;
    t_tmp varchar2(32767);
    t_sin number;
    t_cos number;
  begin
    t_tmp := to_char_round( p_x ) || ' ' || to_char_round( p_y );
    if p_degrees_rotation is null
    then
      t_tmp := t_tmp || ' Td ';
    else
      t_sin := sin( p_degrees_rotation / 180 * c_pi );
      t_cos := cos( p_degrees_rotation / 180 * c_pi );
      t_tmp := to_char_round( t_cos, 5 ) || ' ' || t_tmp;
      t_tmp := to_char_round( - t_sin, 5 ) || ' ' || t_tmp;
      t_tmp := to_char_round( t_sin, 5 ) || ' ' || t_tmp;
      t_tmp := to_char_round( t_cos, 5 ) || ' ' || t_tmp;
      t_tmp := t_tmp || ' Tm ';
    end if;
    raw2page( utl_raw.concat( utl_raw.cast_to_raw( 'BT ' || t_tmp )
                            , p_txt
                            , utl_raw.cast_to_raw( ' Tj ET' )
                            )
              );
  end;
--
  procedure put_txt( p_x number, p_y number, p_txt varchar2, p_degrees_rotation number := null )
  is
  begin
    if p_txt is not null
    then
      put_raw( p_x, p_y, txt2raw( p_txt ), p_degrees_rotation );
    end if;
  end;
--
  function str_len( p_txt in varchar2 )
  return number
  is
    t_width number;
    t_char pls_integer;
    t_rtxt raw(32767);
    t_tmp number;
    t_font tp_font;
  begin
    if p_txt is null
    then
      return 0;
    end if;
--
    t_width := 0;
    t_font := g_fonts( g_current_font );
    if t_font.cid
    then
      t_rtxt := utl_raw.convert( utl_raw.cast_to_raw( p_txt )
                               , 'AMERICAN_AMERICA.AL16UTF16' -- 16 bit font => 2 bytes per char
                               , sys_context( 'userenv', 'LANGUAGE' )  -- ???? font characterset ?????
                               );
      for i in 1 .. utl_raw.length( t_rtxt ) / 2
      loop
        t_char := to_number( utl_raw.substr( t_rtxt, i * 2 - 1, 2 ), 'xxxx' );
        if t_font.flags = 4 -- a symbolic font
        then
-- assume code 32, space maps to the first code from the font
          t_char := t_font.code2glyph.first + t_char - 32;
        end if;
        if (   t_font.code2glyph.exists( t_char )
           and t_font.hmetrics.exists( t_font.code2glyph( t_char ) )
           )
        then
          t_tmp := t_font.hmetrics( t_font.code2glyph( t_char ) );
        else
          t_tmp := t_font.hmetrics( t_font.hmetrics.last() );
        end if;
        t_width := t_width + t_tmp;
      end loop;
      t_width := t_width * t_font.unit_norm;
      t_width := t_width * t_font.fontsize / 1000;
    else
      t_rtxt := utl_raw.convert( utl_raw.cast_to_raw( p_txt )
                               , t_font.charset  -- should be an 8 bit font
                               , sys_context( 'userenv', 'LANGUAGE' )
                               );
      for i in 1 .. utl_raw.length( t_rtxt )
      loop
        t_char := to_number( utl_raw.substr( t_rtxt, i, 1 ), 'xx' );
        t_width := t_width + t_font.char_width_tab( t_char );
      end loop;
      t_width := t_width * t_font.fontsize / 1000;
    end if;
    return t_width;
  end;
--
  procedure write
    ( p_txt in varchar2
    , p_x in number := null
    , p_y in number := null
    , p_line_height in number := null
    , p_start in number := null  -- left side of the available text box
    , p_width in number := null  -- width of the available text box
    , p_alignment in varchar2 := null
    )
  is
    t_line_height number;
    t_x number;
    t_y number;
    t_start number;
    t_width number;
    t_len number;
    t_cnt pls_integer;
    t_ind pls_integer;
    t_alignment varchar2(100);
  begin
    if p_txt is null
    then
      return;
    end if;
--
    if g_current_font is null
    then
      set_font( 'helvetica' );
    end if;
--
    t_line_height := nvl( p_line_height, g_fonts( g_current_font ).fontsize );
    if (  t_line_height < g_fonts( g_current_font ).fontsize
       or t_line_height > ( g_settings.page_height - g_settings.margin_top - t_line_height ) / 4
       )
    then
      t_line_height := g_fonts( g_current_font ).fontsize;
    end if;
    t_start := nvl( p_start, g_settings.margin_left );
    if (  t_start < g_settings.margin_left
       or t_start > g_settings.page_width - g_settings.margin_right - g_settings.margin_left
       )
    then
      t_start := g_settings.margin_left;
    end if;
    t_width := nvl( p_width
                  , g_settings.page_width - g_settings.margin_right - g_settings.margin_left
                  );
    if (  t_width < str_len( '   ' )
       or t_width > g_settings.page_width - g_settings.margin_right - g_settings.margin_left
       )
    then
      t_width := g_settings.page_width - g_settings.margin_right - g_settings.margin_left;
    end if;
    t_x := coalesce( p_x, g_x, g_settings.margin_left );
    t_y := coalesce( p_y
                   , g_y
                   , g_settings.page_height - g_settings.margin_top - t_line_height
                   );
    if t_y < 0
    then
      t_y := coalesce( g_y
                     , g_settings.page_height - g_settings.margin_top - t_line_height
                     ) - t_line_height;
    end if; 
    if t_x > t_start + t_width
    then
      t_x := t_start;
      t_y := t_y - t_line_height;
    elsif t_x < t_start
    then
      t_x := t_start;
    end if;
    if t_y < g_settings.margin_bottom
    then
      new_page;
      t_x := t_start;
      t_y := g_settings.page_height - g_settings.margin_top - t_line_height;
    end if;
--
    t_ind := instr( p_txt, chr(10) );
    if t_ind > 0
    then
      g_x := t_x;
      g_y := t_y;
      write( rtrim( substr( p_txt, 1, t_ind - 1 ), chr(13) ), t_x, t_y, t_line_height, t_start, t_width, p_alignment );
      t_y := g_y - t_line_height;
      if t_y < g_settings.margin_bottom
      then
        new_page;
        t_y := g_settings.page_height - g_settings.margin_top - t_line_height;
      end if;
      g_x := t_start;
      g_y := t_y;
      write( substr( p_txt, t_ind + 1 ), t_start, t_y, t_line_height, t_start, t_width, p_alignment );
      return;
    end if;
--
    t_len := str_len( p_txt );
    if t_len <= t_width - t_x + t_start
    then
      t_alignment := lower( substr( p_alignment, 1, 100 ) );
      if instr( t_alignment, 'right' ) > 0 or instr( t_alignment, 'end' ) > 0
      then
        t_x := t_start + t_width - t_len;
      elsif instr( t_alignment, 'center' ) > 0
      then
        t_x := ( t_width + t_x + t_start - t_len ) / 2;
      end if;
      put_txt( t_x, t_y, p_txt );
      g_x := t_x + t_len + str_len( ' ' );
      g_y := t_y;
      return;
    end if;
--
    t_cnt := 0;
    while (   instr( p_txt, ' ', 1, t_cnt + 1 ) > 0
          and str_len( substr( p_txt, 1, instr( p_txt, ' ', 1, t_cnt + 1 ) - 1 ) ) <= t_width - t_x + t_start
          )
    loop
      t_cnt := t_cnt + 1;
    end loop;
    if t_cnt > 0
    then
      t_ind := instr( p_txt, ' ', 1, t_cnt );
      write( substr( p_txt, 1, t_ind - 1 ), t_x, t_y, t_line_height, t_start, t_width, p_alignment );
      t_y := t_y - t_line_height;
      if t_y < g_settings.margin_bottom
      then
        new_page;
        t_y := g_settings.page_height - g_settings.margin_top - t_line_height;
      end if;
      write( substr( p_txt, t_ind + 1 ), t_start, t_y, t_line_height, t_start, t_width, p_alignment );
      return;
    end if;
--
    if t_x > t_start and t_len < t_width
    then
      t_y := t_y - t_line_height;
      if t_y < g_settings.margin_bottom
      then
        new_page;
        t_y := g_settings.page_height - g_settings.margin_top - t_line_height;
      end if;
      write( p_txt, t_start, t_y, t_line_height, t_start, t_width, p_alignment );
    else
      if length( p_txt ) = 1
      then
        if t_x > t_start
        then
          t_y := t_y - t_line_height;
          if t_y < g_settings.margin_bottom
          then
            new_page;
            t_y := g_settings.page_height - g_settings.margin_top - t_line_height;
          end if;
        end if;
        write( p_txt, t_x, t_y, t_line_height, t_start, t_len );
      else
        t_ind := 2; -- start with 2 to make sure we get amaller string!
        while str_len( substr( p_txt, 1, t_ind ) ) <= t_width - t_x + t_start
        loop
          t_ind := t_ind + 1;
        end loop;
        write( substr( p_txt, 1, t_ind - 1 ), t_x, t_y, t_line_height, t_start, t_width, p_alignment );
        t_y := t_y - t_line_height;
        if t_y < g_settings.margin_bottom
        then
          new_page;
          t_y := g_settings.page_height - g_settings.margin_top - t_line_height;
        end if;
        write( substr( p_txt, t_ind ), t_start, t_y, t_line_height, t_start, t_width, p_alignment );
      end if;
    end if;
  end;
--
  function load_ttf_font
    ( p_font blob
    , p_encoding varchar2 := 'WINDOWS-1252'
    , p_embed boolean := false
    , p_compress boolean := true
    , p_offset number := 1
    )
  return pls_integer
  is
    this_font tp_font;
    type tp_font_table is record
      ( offset pls_integer
      , length pls_integer
      );
    type tp_tables is table of tp_font_table index by varchar2(4);
    t_tables tp_tables;
    t_tag varchar2(4);
    t_blob blob;
    t_offset pls_integer;
    nr_hmetrics pls_integer;
    subtype tp_glyphname is varchar2(250);
    type tp_glyphnames is table of tp_glyphname index by pls_integer;
    t_glyphnames tp_glyphnames;
    t_glyph2name tp_pls_tab;
    t_font_ind pls_integer;
  begin
    if dbms_lob.substr( p_font, 4, p_offset ) != hextoraw( '00010000' ) --  OpenType Font
    then
      return null;
    end if;
    for i in 1 .. blob2num( p_font, 2, p_offset + 4 )
    loop
      t_tag :=
        utl_raw.cast_to_varchar2( dbms_lob.substr( p_font, 4, p_offset - 4 + i * 16 ) );
      t_tables( t_tag ).offset := blob2num( p_font, 4, p_offset + 4 + i * 16 ) + 1;
      t_tables( t_tag ).length := blob2num( p_font, 4, p_offset + 8 + i * 16 );
    end loop;
--
    if (  not t_tables.exists( 'cmap' )
       or not t_tables.exists( 'glyf' )
       or not t_tables.exists( 'head' )
       or not t_tables.exists( 'hhea' )
       or not t_tables.exists( 'hmtx' )
       or not t_tables.exists( 'loca' )
       or not t_tables.exists( 'maxp' )
       or not t_tables.exists( 'name' )
       or not t_tables.exists( 'post' )
       )
    then
      return null;
    end if;
--
    dbms_lob.createtemporary( t_blob, true );
    dbms_lob.copy( t_blob, p_font, t_tables( 'maxp' ).length, 1, t_tables( 'maxp' ).offset );
    this_font.numGlyphs := blob2num( t_blob, 2, 5 );
--
    dbms_lob.copy( t_blob, p_font, t_tables( 'cmap' ).length, 1, t_tables( 'cmap' ).offset );
    for i in 0 .. blob2num( t_blob, 2, 3 ) - 1
    loop
      if (   dbms_lob.substr( t_blob, 2, 5 + i * 8 ) = hextoraw( '0003' ) -- Windows
         and dbms_lob.substr( t_blob, 2, 5 + i * 8 + 2 )
               in ( hextoraw( '0000' ) -- Symbol
                  , hextoraw( '0001' ) -- Unicode BMP (UCS-2)
                  )
         )
      then
        if dbms_lob.substr( t_blob, 2, 5 + i * 8 + 2 ) = hextoraw( '0000' ) -- Symbol
        then
          this_font.flags := 4; -- symbolic
        else
          this_font.flags := 32; -- non-symbolic
        end if;
        t_offset := blob2num( t_blob, 4, 5 + i * 8 + 4 ) + 1;
        if dbms_lob.substr( t_blob, 2, t_offset ) != hextoraw( '0004' )
        then
          return null;
        end if;
        declare
          t_seg_cnt pls_integer;
          t_end_offs pls_integer;
          t_start_offs pls_integer;
          t_idDelta_offs pls_integer;
          t_idRangeOffset_offs pls_integer;
          t_tmp pls_integer;
          t_start pls_integer;
        begin
          t_seg_cnt := blob2num( t_blob, 2, t_offset + 6 ) / 2;
          t_end_offs := t_offset + 14;
          t_start_offs := t_end_offs + t_seg_cnt * 2 + 2;
          t_idDelta_offs := t_start_offs + t_seg_cnt * 2;
          t_idRangeOffset_offs := t_idDelta_offs + t_seg_cnt * 2;
          for seg in 0 .. t_seg_cnt - 1
          loop
            t_tmp := blob2num( t_blob, 2, t_idRangeOffset_offs + seg * 2 );
            if t_tmp = 0
            then
              t_tmp := blob2num( t_blob, 2, t_idDelta_offs + seg * 2 );
              for c in blob2num( t_blob, 2, t_start_offs + seg * 2 )
                    .. blob2num( t_blob, 2, t_end_offs + seg * 2 )
              loop
                this_font.code2glyph( c ) := mod( c + t_tmp, 65536 );
              end loop;
            else
              t_start := blob2num( t_blob, 2, t_start_offs + seg * 2 );
              for c in t_start .. blob2num( t_blob, 2, t_end_offs + seg * 2 )
              loop
                this_font.code2glyph( c ) := blob2num( t_blob, 2, t_idRangeOffset_offs + t_tmp + ( seg + c - t_start ) * 2 );
              end loop;
            end if;
          end loop;
        end;
        exit;
      end if;
    end loop;
--
    t_glyphnames( 0 ) := '.notdef';
    t_glyphnames( 1 ) := '.null';
    t_glyphnames( 2 ) := 'nonmarkingreturn';
    t_glyphnames( 3 ) := 'space';
    t_glyphnames( 4 ) := 'exclam';
    t_glyphnames( 5 ) := 'quotedbl';
    t_glyphnames( 6 ) := 'numbersign';
    t_glyphnames( 7 ) := 'dollar';
    t_glyphnames( 8 ) := 'percent';
    t_glyphnames( 9 ) := 'ampersand';
    t_glyphnames( 10 ) := 'quotesingle';
    t_glyphnames( 11 ) := 'parenleft';
    t_glyphnames( 12 ) := 'parenright';
    t_glyphnames( 13 ) := 'asterisk';
    t_glyphnames( 14 ) := 'plus';
    t_glyphnames( 15 ) := 'comma';
    t_glyphnames( 16 ) := 'hyphen';
    t_glyphnames( 17 ) := 'period';
    t_glyphnames( 18 ) := 'slash';
    t_glyphnames( 19 ) := 'zero';
    t_glyphnames( 20 ) := 'one';
    t_glyphnames( 21 ) := 'two';
    t_glyphnames( 22 ) := 'three';
    t_glyphnames( 23 ) := 'four';
    t_glyphnames( 24 ) := 'five';
    t_glyphnames( 25 ) := 'six';
    t_glyphnames( 26 ) := 'seven';
    t_glyphnames( 27 ) := 'eight';
    t_glyphnames( 28 ) := 'nine';
    t_glyphnames( 29 ) := 'colon';
    t_glyphnames( 30 ) := 'semicolon';
    t_glyphnames( 31 ) := 'less';
    t_glyphnames( 32 ) := 'equal';
    t_glyphnames( 33 ) := 'greater';
    t_glyphnames( 34 ) := 'question';
    t_glyphnames( 35 ) := 'at';
    t_glyphnames( 36 ) := 'A';
    t_glyphnames( 37 ) := 'B';
    t_glyphnames( 38 ) := 'C';
    t_glyphnames( 39 ) := 'D';
    t_glyphnames( 40 ) := 'E';
    t_glyphnames( 41 ) := 'F';
    t_glyphnames( 42 ) := 'G';
    t_glyphnames( 43 ) := 'H';
    t_glyphnames( 44 ) := 'I';
    t_glyphnames( 45 ) := 'J';
    t_glyphnames( 46 ) := 'K';
    t_glyphnames( 47 ) := 'L';
    t_glyphnames( 48 ) := 'M';
    t_glyphnames( 49 ) := 'N';
    t_glyphnames( 50 ) := 'O';
    t_glyphnames( 51 ) := 'P';
    t_glyphnames( 52 ) := 'Q';
    t_glyphnames( 53 ) := 'R';
    t_glyphnames( 54 ) := 'S';
    t_glyphnames( 55 ) := 'T';
    t_glyphnames( 56 ) := 'U';
    t_glyphnames( 57 ) := 'V';
    t_glyphnames( 58 ) := 'W';
    t_glyphnames( 59 ) := 'X';
    t_glyphnames( 60 ) := 'Y';
    t_glyphnames( 61 ) := 'Z';
    t_glyphnames( 62 ) := 'bracketleft';
    t_glyphnames( 63 ) := 'backslash';
    t_glyphnames( 64 ) := 'bracketright';
    t_glyphnames( 65 ) := 'asciicircum';
    t_glyphnames( 66 ) := 'underscore';
    t_glyphnames( 67 ) := 'grave';
    t_glyphnames( 68 ) := 'a';
    t_glyphnames( 69 ) := 'b';
    t_glyphnames( 70 ) := 'c';
    t_glyphnames( 71 ) := 'd';
    t_glyphnames( 72 ) := 'e';
    t_glyphnames( 73 ) := 'f';
    t_glyphnames( 74 ) := 'g';
    t_glyphnames( 75 ) := 'h';
    t_glyphnames( 76 ) := 'i';
    t_glyphnames( 77 ) := 'j';
    t_glyphnames( 78 ) := 'k';
    t_glyphnames( 79 ) := 'l';
    t_glyphnames( 80 ) := 'm';
    t_glyphnames( 81 ) := 'n';
    t_glyphnames( 82 ) := 'o';
    t_glyphnames( 83 ) := 'p';
    t_glyphnames( 84 ) := 'q';
    t_glyphnames( 85 ) := 'r';
    t_glyphnames( 86 ) := 's';
    t_glyphnames( 87 ) := 't';
    t_glyphnames( 88 ) := 'u';
    t_glyphnames( 89 ) := 'v';
    t_glyphnames( 90 ) := 'w';
    t_glyphnames( 91 ) := 'x';
    t_glyphnames( 92 ) := 'y';
    t_glyphnames( 93 ) := 'z';
    t_glyphnames( 94 ) := 'braceleft';
    t_glyphnames( 95 ) := 'bar';
    t_glyphnames( 96 ) := 'braceright';
    t_glyphnames( 97 ) := 'asciitilde';
    t_glyphnames( 98 ) := 'Adieresis';
    t_glyphnames( 99 ) := 'Aring';
    t_glyphnames( 100 ) := 'Ccedilla';
    t_glyphnames( 101 ) := 'Eacute';
    t_glyphnames( 102 ) := 'Ntilde';
    t_glyphnames( 103 ) := 'Odieresis';
    t_glyphnames( 104 ) := 'Udieresis';
    t_glyphnames( 105 ) := 'aacute';
    t_glyphnames( 106 ) := 'agrave';
    t_glyphnames( 107 ) := 'acircumflex';
    t_glyphnames( 108 ) := 'adieresis';
    t_glyphnames( 109 ) := 'atilde';
    t_glyphnames( 110 ) := 'aring';
    t_glyphnames( 111 ) := 'ccedilla';
    t_glyphnames( 112 ) := 'eacute';
    t_glyphnames( 113 ) := 'egrave';
    t_glyphnames( 114 ) := 'ecircumflex';
    t_glyphnames( 115 ) := 'edieresis';
    t_glyphnames( 116 ) := 'iacute';
    t_glyphnames( 117 ) := 'igrave';
    t_glyphnames( 118 ) := 'icircumflex';
    t_glyphnames( 119 ) := 'idieresis';
    t_glyphnames( 120 ) := 'ntilde';
    t_glyphnames( 121 ) := 'oacute';
    t_glyphnames( 122 ) := 'ograve';
    t_glyphnames( 123 ) := 'ocircumflex';
    t_glyphnames( 124 ) := 'odieresis';
    t_glyphnames( 125 ) := 'otilde';
    t_glyphnames( 126 ) := 'uacute';
    t_glyphnames( 127 ) := 'ugrave';
    t_glyphnames( 128 ) := 'ucircumflex';
    t_glyphnames( 129 ) := 'udieresis';
    t_glyphnames( 130 ) := 'dagger';
    t_glyphnames( 131 ) := 'degree';
    t_glyphnames( 132 ) := 'cent';
    t_glyphnames( 133 ) := 'sterling';
    t_glyphnames( 134 ) := 'section';
    t_glyphnames( 135 ) := 'bullet';
    t_glyphnames( 136 ) := 'paragraph';
    t_glyphnames( 137 ) := 'germandbls';
    t_glyphnames( 138 ) := 'registered';
    t_glyphnames( 139 ) := 'copyright';
    t_glyphnames( 140 ) := 'trademark';
    t_glyphnames( 141 ) := 'acute';
    t_glyphnames( 142 ) := 'dieresis';
    t_glyphnames( 143 ) := 'notequal';
    t_glyphnames( 144 ) := 'AE';
    t_glyphnames( 145 ) := 'Oslash';
    t_glyphnames( 146 ) := 'infinity';
    t_glyphnames( 147 ) := 'plusminus';
    t_glyphnames( 148 ) := 'lessequal';
    t_glyphnames( 149 ) := 'greaterequal';
    t_glyphnames( 150 ) := 'yen';
    t_glyphnames( 151 ) := 'mu';
    t_glyphnames( 152 ) := 'partialdiff';
    t_glyphnames( 153 ) := 'summation';
    t_glyphnames( 154 ) := 'product';
    t_glyphnames( 155 ) := 'pi';
    t_glyphnames( 156 ) := 'integral';
    t_glyphnames( 157 ) := 'ordfeminine';
    t_glyphnames( 158 ) := 'ordmasculine';
    t_glyphnames( 159 ) := 'Omega';
    t_glyphnames( 160 ) := 'ae';
    t_glyphnames( 161 ) := 'oslash';
    t_glyphnames( 162 ) := 'questiondown';
    t_glyphnames( 163 ) := 'exclamdown';
    t_glyphnames( 164 ) := 'logicalnot';
    t_glyphnames( 165 ) := 'radical';
    t_glyphnames( 166 ) := 'florin';
    t_glyphnames( 167 ) := 'approxequal';
    t_glyphnames( 168 ) := 'Delta';
    t_glyphnames( 169 ) := 'guillemotleft';
    t_glyphnames( 170 ) := 'guillemotright';
    t_glyphnames( 171 ) := 'ellipsis';
    t_glyphnames( 172 ) := 'nonbreakingspace';
    t_glyphnames( 173 ) := 'Agrave';
    t_glyphnames( 174 ) := 'Atilde';
    t_glyphnames( 175 ) := 'Otilde';
    t_glyphnames( 176 ) := 'OE';
    t_glyphnames( 177 ) := 'oe';
    t_glyphnames( 178 ) := 'endash';
    t_glyphnames( 179 ) := 'emdash';
    t_glyphnames( 180 ) := 'quotedblleft';
    t_glyphnames( 181 ) := 'quotedblright';
    t_glyphnames( 182 ) := 'quoteleft';
    t_glyphnames( 183 ) := 'quoteright';
    t_glyphnames( 184 ) := 'divide';
    t_glyphnames( 185 ) := 'lozenge';
    t_glyphnames( 186 ) := 'ydieresis';
    t_glyphnames( 187 ) := 'Ydieresis';
    t_glyphnames( 188 ) := 'fraction';
    t_glyphnames( 189 ) := 'currency';
    t_glyphnames( 190 ) := 'guilsinglleft';
    t_glyphnames( 191 ) := 'guilsinglright';
    t_glyphnames( 192 ) := 'fi';
    t_glyphnames( 193 ) := 'fl';
    t_glyphnames( 194 ) := 'daggerdbl';
    t_glyphnames( 195 ) := 'periodcentered';
    t_glyphnames( 196 ) := 'quotesinglbase';
    t_glyphnames( 197 ) := 'quotedblbase';
    t_glyphnames( 198 ) := 'perthousand';
    t_glyphnames( 199 ) := 'Acircumflex';
    t_glyphnames( 200 ) := 'Ecircumflex';
    t_glyphnames( 201 ) := 'Aacute';
    t_glyphnames( 202 ) := 'Edieresis';
    t_glyphnames( 203 ) := 'Egrave';
    t_glyphnames( 204 ) := 'Iacute';
    t_glyphnames( 205 ) := 'Icircumflex';
    t_glyphnames( 206 ) := 'Idieresis';
    t_glyphnames( 207 ) := 'Igrave';
    t_glyphnames( 208 ) := 'Oacute';
    t_glyphnames( 209 ) := 'Ocircumflex';
    t_glyphnames( 210 ) := 'apple';
    t_glyphnames( 211 ) := 'Ograve';
    t_glyphnames( 212 ) := 'Uacute';
    t_glyphnames( 213 ) := 'Ucircumflex';
    t_glyphnames( 214 ) := 'Ugrave';
    t_glyphnames( 215 ) := 'dotlessi';
    t_glyphnames( 216 ) := 'circumflex';
    t_glyphnames( 217 ) := 'tilde';
    t_glyphnames( 218 ) := 'macron';
    t_glyphnames( 219 ) := 'breve';
    t_glyphnames( 220 ) := 'dotaccent';
    t_glyphnames( 221 ) := 'ring';
    t_glyphnames( 222 ) := 'cedilla';
    t_glyphnames( 223 ) := 'hungarumlaut';
    t_glyphnames( 224 ) := 'ogonek';
    t_glyphnames( 225 ) := 'caron';
    t_glyphnames( 226 ) := 'Lslash';
    t_glyphnames( 227 ) := 'lslash';
    t_glyphnames( 228 ) := 'Scaron';
    t_glyphnames( 229 ) := 'scaron';
    t_glyphnames( 230 ) := 'Zcaron';
    t_glyphnames( 231 ) := 'zcaron';
    t_glyphnames( 232 ) := 'brokenbar';
    t_glyphnames( 233 ) := 'Eth';
    t_glyphnames( 234 ) := 'eth';
    t_glyphnames( 235 ) := 'Yacute';
    t_glyphnames( 236 ) := 'yacute';
    t_glyphnames( 237 ) := 'Thorn';
    t_glyphnames( 238 ) := 'thorn';
    t_glyphnames( 239 ) := 'minus';
    t_glyphnames( 240 ) := 'multiply';
    t_glyphnames( 241 ) := 'onesuperior';
    t_glyphnames( 242 ) := 'twosuperior';
    t_glyphnames( 243 ) := 'threesuperior';
    t_glyphnames( 244 ) := 'onehalf';
    t_glyphnames( 245 ) := 'onequarter';
    t_glyphnames( 246 ) := 'threequarters';
    t_glyphnames( 247 ) := 'franc';
    t_glyphnames( 248 ) := 'Gbreve';
    t_glyphnames( 249 ) := 'gbreve';
    t_glyphnames( 250 ) := 'Idotaccent';
    t_glyphnames( 251 ) := 'Scedilla';
    t_glyphnames( 252 ) := 'scedilla';
    t_glyphnames( 253 ) := 'Cacute';
    t_glyphnames( 254 ) := 'cacute';
    t_glyphnames( 255 ) := 'Ccaron';
    t_glyphnames( 256 ) := 'ccaron';
    t_glyphnames( 257 ) := 'dcroat';
--
    dbms_lob.copy( t_blob, p_font, t_tables( 'post' ).length, 1, t_tables( 'post' ).offset );
    this_font.italic_angle := to_short( dbms_lob.substr( t_blob, 2, 5 ) )
                            + to_short( dbms_lob.substr( t_blob, 2, 7 ) ) / 65536;
    case rawtohex( dbms_lob.substr( t_blob, 4, 1 ) )
      when '00010000'
      then
        for g in 0 .. 257
        loop
          t_glyph2name( g ) := g;
        end loop;
      when '00020000'
      then
        t_offset := blob2num( t_blob, 2, 33 ) * 2 + 35;
        while nvl( blob2num( t_blob, 1, t_offset ), 0 ) > 0
        loop
          t_glyphnames( t_glyphnames.count ) := utl_raw.cast_to_varchar2( dbms_lob.substr( t_blob, blob2num( t_blob, 1, t_offset ), t_offset + 1 ) );
          t_offset := t_offset + blob2num( t_blob, 1, t_offset ) + 1;
        end loop;
        for g in 0 .. blob2num( t_blob, 2, 33 ) - 1
        loop
          t_glyph2name( g ) := blob2num( t_blob, 2, 35 + 2 * g );
        end loop;
      when '00025000'
      then
        for g in 0 .. blob2num( t_blob, 2, 33 ) - 1
        loop
          t_offset := blob2num( t_blob, 1, 35 + g );
          if t_offset > 127
          then
            t_glyph2name( g ) := g - t_offset;
          else
            t_glyph2name( g ) := g + t_offset;
          end if;
        end loop;
      when '00030000'
      then
        t_glyphnames.delete;
      else
dbms_output.put_line( 'no post ' || dbms_lob.substr( t_blob, 4, 1 ) );
    end case;
--
    dbms_lob.copy( t_blob, p_font, t_tables( 'head' ).length, 1, t_tables( 'head' ).offset );
    if dbms_lob.substr( t_blob, 4, 13 ) = hextoraw( '5F0F3CF5' )  -- magic
    then
      declare
        t_tmp pls_integer := blob2num( t_blob, 2, 45 );
      begin
        if bitand( t_tmp, 1 ) = 1
        then
          this_font.style := 'B';
        end if;
        if bitand( t_tmp, 2 ) = 2
        then
          this_font.style := this_font.style || 'I';
          this_font.flags := this_font.flags + 64;
        end if;
        this_font.style := nvl( this_font.style, 'N' );
        this_font.unit_norm := 1000 / blob2num( t_blob, 2, 19 );
        this_font.bb_xmin := to_short( dbms_lob.substr( t_blob, 2, 37 ), this_font.unit_norm );
        this_font.bb_ymin := to_short( dbms_lob.substr( t_blob, 2, 39 ), this_font.unit_norm );
        this_font.bb_xmax := to_short( dbms_lob.substr( t_blob, 2, 41 ), this_font.unit_norm );
        this_font.bb_ymax := to_short( dbms_lob.substr( t_blob, 2, 43 ), this_font.unit_norm );
        this_font.indexToLocFormat := blob2num( t_blob, 2, 51 ); -- 0 for short offsets, 1 for long
      end;
    end if;
--
    dbms_lob.copy( t_blob, p_font, t_tables( 'hhea' ).length, 1, t_tables( 'hhea' ).offset );
    if dbms_lob.substr( t_blob, 4, 1 ) = hextoraw( '00010000' ) -- version 1.0
    then
      this_font.ascent := to_short( dbms_lob.substr( t_blob, 2, 5 ), this_font.unit_norm );
      this_font.descent := to_short( dbms_lob.substr( t_blob, 2, 7 ), this_font.unit_norm );
      this_font.capheight := this_font.ascent;
      nr_hmetrics := blob2num( t_blob, 2, 35 );
    end if;
--
    dbms_lob.copy( t_blob, p_font, t_tables( 'hmtx' ).length, 1, t_tables( 'hmtx' ).offset );
    for j in 0 .. nr_hmetrics - 1
    loop
      this_font.hmetrics( j ) := blob2num( t_blob, 2, 1 + 4 * j );
    end loop;
--
    dbms_lob.copy( t_blob, p_font, t_tables( 'name' ).length, 1, t_tables( 'name' ).offset );
    if dbms_lob.substr( t_blob, 2, 1 ) = hextoraw( '0000' ) -- format 0
    then
      t_offset := blob2num( t_blob, 2, 5 ) + 1;
      for j in 0 .. blob2num( t_blob, 2, 3 ) - 1
      loop
        if (   dbms_lob.substr( t_blob, 2, 7  + j * 12 ) = hextoraw( '0003' ) -- Windows
           and dbms_lob.substr( t_blob, 2, 11 + j * 12 ) = hextoraw( '0409' ) -- English United States
           )
        then
          case rawtohex( dbms_lob.substr( t_blob, 2, 13 + j * 12 ) )
            when '0001'
            then
              this_font.family := utl_i18n.raw_to_char( dbms_lob.substr( t_blob, blob2num( t_blob, 2, 15 + j * 12 ), t_offset + blob2num( t_blob, 2, 17 + j * 12 ) ), 'AL16UTF16' );
            when '0006'
            then
              this_font.name := utl_i18n.raw_to_char( dbms_lob.substr( t_blob, blob2num( t_blob, 2, 15 + j * 12 ), t_offset + blob2num( t_blob, 2, 17 + j * 12 ) ), 'AL16UTF16' );
            else
              null;
          end case;
        end if;
      end loop;
    end if;
--
    if this_font.italic_angle != 0
    then
      this_font.flags := this_font.flags + 64;
    end if;
    this_font.subtype := 'TrueType';
    this_font.stemv := 50;
    this_font.family := lower( this_font.family );
    this_font.encoding := utl_i18n.map_charset( p_encoding
                                              , utl_i18n.generic_context
                                              , utl_i18n.iana_to_oracle
                                              );
    this_font.encoding := nvl( this_font.encoding, upper( p_encoding ) );
    this_font.charset := sys_context( 'userenv', 'LANGUAGE' );
    this_font.charset := substr( this_font.charset
                               , 1
                               , instr( this_font.charset, '.' )
                               ) || this_font.encoding;
    this_font.cid := upper( p_encoding ) in ( 'CID', 'AL16UTF16', 'UTF', 'UNICODE' );
    this_font.fontname := this_font.name;
    this_font.compress_font := p_compress;
--
    if ( p_embed or this_font.cid ) and t_tables.exists( 'OS/2' )
    then
      dbms_lob.copy( t_blob, p_font, t_tables( 'OS/2' ).length, 1, t_tables( 'OS/2' ).offset );
      if blob2num( t_blob, 2, 9 ) != 2
      then
        this_font.fontfile2 := p_font;
        this_font.ttf_offset := p_offset;
        this_font.name := dbms_random.string( 'u', 6 ) || '+' || this_font.name;
--
        t_blob := dbms_lob.substr( p_font, t_tables( 'loca' ).length, t_tables( 'loca' ).offset );
        declare
          t_size pls_integer := 2 + this_font.indexToLocFormat * 2; -- 0 for short offsets, 1 for long
        begin
          for i in 0 .. this_font.numGlyphs
          loop
            this_font.loca( i ) := blob2num( t_blob, t_size, 1 + i * t_size );
          end loop;
        end;
      end if;
    end if;
--
    if not this_font.cid
    then
      if this_font.flags = 4 -- a symbolic font
      then
        declare
          t_real pls_integer;
        begin
          for t_code in 32 .. 255
          loop
            t_real := this_font.code2glyph.first + t_code - 32; -- assume code 32, space maps to the first code from the font
            if this_font.code2glyph.exists( t_real )
            then
              this_font.first_char := least( nvl( this_font.first_char, 255 ), t_code );
              this_font.last_char := t_code;
              if this_font.hmetrics.exists( this_font.code2glyph( t_real ) )
              then
                this_font.char_width_tab( t_code ) := trunc( this_font.hmetrics( this_font.code2glyph( t_real ) ) * this_font.unit_norm );
              else
                this_font.char_width_tab( t_code ) := trunc( this_font.hmetrics( this_font.hmetrics.last() ) * this_font.unit_norm );
              end if;
            else
              this_font.char_width_tab( t_code ) := trunc( this_font.hmetrics( 0 ) * this_font.unit_norm );
            end if;
          end loop;
        end;
      else
        declare
          t_unicode pls_integer;
          t_prv_diff pls_integer;
          t_utf16_charset varchar2(1000);
          t_winansi_charset varchar2(1000);
          t_glyphname tp_glyphname;
        begin
          t_prv_diff := -1;
          t_utf16_charset := substr( this_font.charset, 1, instr( this_font.charset, '.' ) ) || 'AL16UTF16';
          t_winansi_charset := substr( this_font.charset, 1, instr( this_font.charset, '.' ) ) || 'WE8MSWIN1252';
          for t_code in 32 .. 255
          loop
            t_unicode := utl_raw.cast_to_binary_integer( utl_raw.convert( hextoraw( to_char( t_code, 'fm0x' ) )
                                                                        , t_utf16_charset
                                                                        , this_font.charset
                                                                        )
                                                       );
            t_glyphname := '';
            this_font.char_width_tab( t_code ) := trunc( this_font.hmetrics( this_font.hmetrics.last() ) * this_font.unit_norm );
            if this_font.code2glyph.exists( t_unicode )
            then
              this_font.first_char := least( nvl( this_font.first_char, 255 ), t_code );
              this_font.last_char := t_code;
              if this_font.hmetrics.exists( this_font.code2glyph( t_unicode ) )
              then
                this_font.char_width_tab( t_code ) := trunc( this_font.hmetrics( this_font.code2glyph( t_unicode ) ) * this_font.unit_norm );
              end if;
              if t_glyph2name.exists( this_font.code2glyph( t_unicode ) )
              then
                if t_glyphnames.exists( t_glyph2name( this_font.code2glyph( t_unicode ) ) )
                then
                  t_glyphname := t_glyphnames( t_glyph2name( this_font.code2glyph( t_unicode ) ) );
                end if;
              end if;
            end if;
--
            if (   t_glyphname is not null
               and t_unicode != utl_raw.cast_to_binary_integer( utl_raw.convert( hextoraw( to_char( t_code, 'fm0x' ) )
                                                                               , t_winansi_charset
                                                                               , this_font.charset
                                                                               )
                                                              )
               )
            then
              this_font.diff := this_font.diff || case when t_prv_diff != t_code - 1 then ' ' || t_code end || ' /' || t_glyphname;
              t_prv_diff := t_code;
            end if;
          end loop;
        end;
        if this_font.diff is not null
        then
          this_font.diff := '/Differences [' || this_font.diff || ']';
        end if;
      end if;
    end if;
--
    t_font_ind := g_fonts.count( ) + 1; 
    g_fonts( t_font_ind ) := this_font;
/*
--
dbms_output.put_line( this_font.fontname || ' ' || this_font.family || ' ' || this_font.style
|| ' ' || this_font.flags
|| ' ' || this_font.code2glyph.first
|| ' ' || this_font.code2glyph.prior( this_font.code2glyph.last )
|| ' ' || this_font.code2glyph.last
|| ' nr glyphs: ' || this_font.numGlyphs
 ); */
--
    return t_font_ind;
  end;
--
  procedure load_ttf_font
    ( p_font blob
    , p_encoding varchar2 := 'WINDOWS-1252'
    , p_embed boolean := false
    , p_compress boolean := true
    , p_offset number := 1
    )
  is
    t_tmp pls_integer;
  begin
    t_tmp := load_ttf_font( p_font, p_encoding, p_embed, p_compress );
  end;
--
  function load_ttf_font
    ( p_dir varchar2 := 'MY_FONTS'
    , p_filename varchar2 := 'BAUHS93.TTF'
    , p_encoding varchar2 := 'WINDOWS-1252'
    , p_embed boolean := false
    , p_compress boolean := true
    )
  return pls_integer
  is
  begin
    return load_ttf_font( file2blob( p_dir, p_filename ), p_encoding, p_embed, p_compress );
  end;
--
  procedure load_ttf_font
    ( p_dir varchar2 := 'MY_FONTS'
    , p_filename varchar2 := 'BAUHS93.TTF'
    , p_encoding varchar2 := 'WINDOWS-1252'
    , p_embed boolean := false
    , p_compress boolean := true
    )
  is
  begin
    load_ttf_font( file2blob( p_dir, p_filename ), p_encoding, p_embed, p_compress );
  end;
--
  procedure load_ttc_fonts
    ( p_ttc blob
    , p_encoding varchar2 := 'WINDOWS-1252'
    , p_embed boolean := false
    , p_compress boolean := true
    )
  is
    type tp_font_table is record
      ( offset pls_integer
      , length pls_integer
      );
    type tp_tables is table of tp_font_table index by varchar2(4);
    t_tables tp_tables;
    t_tag varchar2(4);
    t_blob blob;
    t_offset pls_integer;
    t_font_ind pls_integer;
  begin
    if utl_raw.cast_to_varchar2( dbms_lob.substr( p_ttc, 4, 1 ) ) != 'ttcf'
    then
      return;
    end if;
    for f in 0 .. blob2num( p_ttc, 4, 9 ) - 1
    loop
      t_font_ind := load_ttf_font( p_ttc, p_encoding, p_embed, p_compress, blob2num( p_ttc, 4, 13 + f * 4 ) + 1 );
dbms_output.put_line( t_font_ind || ' ' || g_fonts( t_font_ind ).fontname || ' ' || g_fonts( t_font_ind ).family || ' ' || g_fonts( t_font_ind ).style );
    end loop;
  end;
--
  procedure load_ttc_fonts
    ( p_dir varchar2 := 'MY_FONTS'
    , p_filename varchar2 := 'CAMBRIA.TTC'
    , p_encoding varchar2 := 'WINDOWS-1252'
    , p_embed boolean := false
    , p_compress boolean := true
    )
  is
  begin
    load_ttc_fonts( file2blob( p_dir, p_filename ), p_encoding, p_embed, p_compress );
  end;
--
  function rgb( p_hex_rgb varchar2 )
  return varchar2
  is
  begin
    return to_char_round( nvl( to_number( substr( ltrim( p_hex_rgb, '#' )
                                                , 1, 2 )
                                        , 'xx' ) / 255
                              , 0 )
                         , 5 ) || ' '
        || to_char_round( nvl(   to_number( substr( ltrim( p_hex_rgb, '#' )
                                                , 3, 2 )
                                        , 'xx' ) / 255
                              , 0 )
                         , 5 ) || ' '
        || to_char_round( nvl(   to_number( substr( ltrim( p_hex_rgb, '#' )
                                                , 5, 2 )
                                        , 'xx' ) / 255
                              , 0 )
                         , 5 ) || ' ';
  end;
--
  procedure set_color( p_rgb varchar2 := '000000', p_backgr boolean )
  is
  begin
    txt2page( rgb( p_rgb ) || case when p_backgr then 'RG' else 'rg' end );
  end;
--
  procedure set_color( p_rgb varchar2 := '000000' )
  is
  begin
    set_color( p_rgb, false );
  end;
--
  procedure set_color
    ( p_red number := 0
    , p_green number := 0
    , p_blue number := 0
    )
  is
  begin
    if (     p_red between 0 and 255
       and p_blue  between 0 and 255
       and p_green between 0 and 255
       )
    then
      set_color(  to_char( p_red, 'fm0x' )
               || to_char( p_green, 'fm0x' )
               || to_char( p_blue, 'fm0x' )
               , false
               );
    end if;
  end;
--
  procedure set_bk_color( p_rgb varchar2 := 'ffffff' )
  is
  begin
    set_color( p_rgb, true );
  end;
--
  procedure set_bk_color
    ( p_red number := 0
    , p_green number := 0
    , p_blue number := 0
    )
  is
  begin
    if (     p_red between 0 and 255
       and p_blue  between 0 and 255
       and p_green between 0 and 255
       )
    then
      set_color(  to_char( p_red, 'fm0x' )
               || to_char( p_green, 'fm0x' )
               || to_char( p_blue, 'fm0x' )
               , true
               );
    end if;
  end;
--
  procedure horizontal_line
    ( p_x number
    , p_y number
    , p_width number
    , p_line_width number := 0.5
    , p_line_color varchar2 := '000000'
    )
  is
    t_use_color boolean;
  begin
    txt2page( 'q ' || to_char_round( p_line_width, 5 ) || ' w' );
    t_use_color := substr( p_line_color
                         , -6
                         ) != '000000';
    if t_use_color
    then
      set_color( p_line_color );
      set_bk_color( p_line_color );
    else
      txt2page( '0 g' );
    end if;
    txt2page(  to_char_round( p_x, 5 ) || ' '
            || to_char_round( p_y, 5 ) || ' m '
            || to_char_round( p_x + p_width, 5 ) || ' '
            || to_char_round( p_y, 5 ) || ' l b'
            );
    txt2page( 'Q' );
  end;
--
  procedure vertical_line
    ( p_x number
    , p_y number
    , p_height number
    , p_line_width number := 0.5
    , p_line_color varchar2 := '000000'
    )
  is
    t_use_color boolean;
  begin
    txt2page( 'q ' || to_char_round( p_line_width, 5 ) || ' w' );
    t_use_color := substr( p_line_color
                         , -6
                         ) != '000000';
    if t_use_color
    then
      set_color( p_line_color );
      set_bk_color( p_line_color );
    else
      txt2page( '0 g' );
    end if;
    txt2page(  to_char_round( p_x, 5 ) || ' '
            || to_char_round( p_y, 5 ) || ' m '
            || to_char_round( p_x, 5 ) || ' '
            || to_char_round( p_y + p_height, 5 ) || ' l b'
            );
    txt2page( 'Q' );
  end;
--
  procedure rect
    ( p_x number
    , p_y number
    , p_width number
    , p_height number
    , p_line_color varchar2 := null
    , p_fill_color varchar2 := null
    , p_line_width number := 0.5
    )
  is
  begin
    txt2page( 'q' );
    if substr( p_line_color, -6 ) != substr( p_fill_color, -6 )
    then
      txt2page( to_char_round( p_line_width, 5 ) || ' w' );
    end if;
    if substr( p_line_color, -6 ) != '000000'
    then
      set_bk_color( p_line_color );
    else
      txt2page( '0 g' );
    end if;
    if p_fill_color is not null
    then
      set_color( p_fill_color );
    end if;
    txt2page(  to_char_round( p_x, 5 ) || ' ' || to_char_round( p_y, 5 ) || ' '
            || to_char_round( p_width, 5 ) || ' ' || to_char_round( p_height, 5 ) || ' re '
            || case
                 when p_fill_color is null
                 then 'S'
                 else case when p_line_color is null then 'f' else 'b' end
               end
            );
    txt2page( 'Q' );
  end;
--
  function get( p_what pls_integer )
  return number
  is
  begin
    return case p_what
             when c_get_page_width    then g_settings.page_width
             when c_get_page_height   then g_settings.page_height
             when c_get_margin_top    then g_settings.margin_top
             when c_get_margin_right  then g_settings.margin_right
             when c_get_margin_bottom then g_settings.margin_bottom
             when c_get_margin_left   then g_settings.margin_left
             when c_get_x             then g_x
             when c_get_y             then g_y
             when c_get_fontsize      then g_fonts( g_current_font ).fontsize
             when c_get_current_font  then g_current_font
           end;
  end;
--
  function parse_jpg( p_img_blob blob )
  return tp_img
  is
    buf raw(4);
    t_img tp_img;
    t_ind integer;
  begin
    if (  dbms_lob.substr( p_img_blob, 2, 1 ) != hextoraw( 'FFD8' )                                      -- SOI Start of Image
       or dbms_lob.substr( p_img_blob, 2, dbms_lob.getlength( p_img_blob ) - 1 ) != hextoraw( 'FFD9' )   -- EOI End of Image
       )
    then  -- this is not a jpg I can handle
      return null;
    end if;
--
    t_img.pixels := p_img_blob;
    t_img.type := 'jpg';
    if dbms_lob.substr( t_img.pixels, 2, 3 ) in ( hextoraw( 'FFE0' )  -- a APP0 jpg
                                                , hextoraw( 'FFE1' )  -- a APP1 jpg
                                                )
    then
      t_img.color_res := 8;
      t_img.height := 1;
      t_img.width := 1;
--
      t_ind := 3;
      t_ind := t_ind + 2 + blob2num( t_img.pixels, 2, t_ind + 2 );
      loop
        buf := dbms_lob.substr( t_img.pixels, 2, t_ind );
        exit when buf = hextoraw( 'FFDA' );  -- SOS Start of Scan
        exit when buf = hextoraw( 'FFD9' );  -- EOI End Of Image
        exit when substr( rawtohex( buf ), 1, 2 ) != 'FF';
        if rawtohex( buf ) in ( 'FFD0'                                                          -- RSTn
                              , 'FFD1', 'FFD2', 'FFD3', 'FFD4', 'FFD5', 'FFD6', 'FFD7', 'FF01'  -- TEM
                              )
        then
          t_ind := t_ind + 2;
        else
          if buf = hextoraw( 'FFC0' )       -- SOF0 (Start Of Frame 0) marker
          then
            t_img.color_res := blob2num( t_img.pixels, 1, t_ind + 4 );
            t_img.height    := blob2num( t_img.pixels, 2, t_ind + 5 );
            t_img.width     := blob2num( t_img.pixels, 2, t_ind + 7 );
          end if;
          t_ind := t_ind + 2 + blob2num( t_img.pixels, 2, t_ind + 2 );
        end if;
      end loop;
    end if;
--
    return t_img;
  end;
--
  function parse_png( p_img_blob blob )
  return tp_img
  is
    t_img tp_img;
    buf raw(32767);
    len integer;
    ind integer;
    color_type pls_integer;
  begin
    if rawtohex( dbms_lob.substr( p_img_blob, 8, 1 ) ) != '89504E470D0A1A0A'  -- not the right signature
    then
      return null;
    end if;
    dbms_lob.createtemporary( t_img.pixels, true );
    ind := 9;
    loop
      len := blob2num( p_img_blob, 4, ind );  -- length
      exit when len is null or ind > dbms_lob.getlength( p_img_blob );
      case utl_raw.cast_to_varchar2( dbms_lob.substr( p_img_blob, 4, ind + 4 ) )  -- Chunk type
        when 'IHDR'
        then
          t_img.width     := blob2num( p_img_blob, 4, ind + 8 );
          t_img.height    := blob2num( p_img_blob, 4, ind + 12 );
          t_img.color_res := blob2num( p_img_blob, 1, ind + 16 );
          color_type      := blob2num( p_img_blob, 1, ind + 17 );
          t_img.greyscale := color_type in ( 0, 4 );
        when 'PLTE'
        then
          t_img.color_tab := dbms_lob.substr( p_img_blob, len, ind + 8 );
        when 'IDAT'
        then
          dbms_lob.copy( t_img.pixels, p_img_blob, len, dbms_lob.getlength( t_img.pixels ) + 1, ind + 8 );
        when 'IEND'
        then
          exit;
        else
          null;
      end case;
      ind := ind + 4 + 4 + len + 4;  -- Length + Chunk type + Chunk data + CRC
    end loop;
--
    t_img.type := 'png';
    t_img.nr_colors := case color_type
                         when 0 then 1
                         when 2 then 3
                         when 3 then 1
                         when 4 then 2
                         else 4
                       end;
--
    return t_img;
  end;
--
  function lzw_decompress
    ( p_blob blob
    , p_bits pls_integer
    )
  return blob
  is
    powers tp_pls_tab;
--
    g_lzw_ind pls_integer;
    g_lzw_bits pls_integer;
    g_lzw_buffer pls_integer;
    g_lzw_bits_used pls_integer;
--
    type tp_lzw_dict is table of raw(1000) index by pls_integer;
    t_lzw_dict tp_lzw_dict;
    t_clr_code pls_integer;
    t_nxt_code pls_integer;
    t_new_code pls_integer;
    t_old_code pls_integer;
    t_blob blob;
--
    function get_lzw_code
    return pls_integer
    is
      t_rv pls_integer;
    begin
      while g_lzw_bits_used < g_lzw_bits
      loop
        g_lzw_ind := g_lzw_ind + 1;
        g_lzw_buffer := blob2num( p_blob, 1, g_lzw_ind ) * powers( g_lzw_bits_used ) + g_lzw_buffer;
        g_lzw_bits_used := g_lzw_bits_used + 8;
      end loop;
      t_rv := bitand( g_lzw_buffer, powers( g_lzw_bits ) - 1 );
      g_lzw_bits_used := g_lzw_bits_used - g_lzw_bits;
      g_lzw_buffer := trunc( g_lzw_buffer / powers( g_lzw_bits ) );
      return t_rv;
    end;
--
  begin
    for i in 0 .. 30
    loop
      powers( i ) := power( 2, i );
    end loop;
--
    t_clr_code := powers( p_bits - 1 );
    t_nxt_code := t_clr_code + 2;
    for i in 0 .. least( t_clr_code - 1, 255 )
    loop
      t_lzw_dict( i ) := hextoraw( to_char( i, 'fm0X' ) );
    end loop;
    dbms_lob.createtemporary( t_blob, true );
    g_lzw_ind := 0;
    g_lzw_bits := p_bits;
    g_lzw_buffer := 0;
    g_lzw_bits_used := 0;
--
    t_old_code := null;
    t_new_code := get_lzw_code( );
    loop
      case nvl( t_new_code, t_clr_code + 1 )
        when t_clr_code + 1
        then
          exit;
        when t_clr_code
        then
          t_new_code := null;
          g_lzw_bits := p_bits;
          t_nxt_code := t_clr_code + 2;
        else
          if t_new_code = t_nxt_code
          then
            t_lzw_dict( t_nxt_code ) :=
              utl_raw.concat( t_lzw_dict( t_old_code )
                            , utl_raw.substr( t_lzw_dict( t_old_code ), 1, 1 )
                            );
            dbms_lob.append( t_blob, t_lzw_dict( t_nxt_code ) );
            t_nxt_code := t_nxt_code + 1;
          elsif t_new_code > t_nxt_code
          then
            exit;
          else
            dbms_lob.append( t_blob, t_lzw_dict( t_new_code ) );
            if t_old_code is not null
            then
              t_lzw_dict( t_nxt_code ) := utl_raw.concat( t_lzw_dict( t_old_code )
                                                        , utl_raw.substr( t_lzw_dict( t_new_code ), 1, 1 )
                                                        );
              t_nxt_code := t_nxt_code + 1;
            end if;
          end if;
          if     bitand( t_nxt_code, powers( g_lzw_bits ) - 1 ) = 0
             and g_lzw_bits < 12
          then
            g_lzw_bits := g_lzw_bits + 1;
          end if;
      end case;
      t_old_code := t_new_code;
      t_new_code := get_lzw_code( );
    end loop;
    t_lzw_dict.delete;
--
    return t_blob;
  end;
--
  function parse_gif( p_img_blob blob )
  return tp_img
  is
    img tp_img;
    buf raw(4000);
    ind integer;
    t_len pls_integer;
  begin
    if dbms_lob.substr( p_img_blob, 3, 1 ) != utl_raw.cast_to_raw( 'GIF' )
    then
      return null;
    end if;
    ind := 7;
    buf := dbms_lob.substr( p_img_blob, 7, 7 );  --  Logical Screen Descriptor
    ind := ind + 7;
    img.color_res := raw2num( utl_raw.bit_and( utl_raw.substr( buf, 5, 1 ), hextoraw( '70' ) ) ) / 16 + 1;
    img.color_res := 8;
    if raw2num( buf, 5, 1 ) > 127
    then
      t_len := 3 * power( 2, raw2num( utl_raw.bit_and( utl_raw.substr( buf, 5, 1 ), hextoraw( '07' ) ) ) + 1 );
      img.color_tab := dbms_lob.substr( p_img_blob, t_len, ind  ); -- Global Color Table
      ind := ind + t_len;
    end if;
--
    loop
      case dbms_lob.substr( p_img_blob, 1, ind )
        when hextoraw( '3B' ) -- trailer
        then
          exit;
        when hextoraw( '21' ) -- extension
        then
          if dbms_lob.substr( p_img_blob, 1, ind + 1 ) = hextoraw( 'F9' )
          then -- Graphic Control Extension
            if utl_raw.bit_and( dbms_lob.substr( p_img_blob, 1, ind + 3 ), hextoraw( '01' ) ) = hextoraw( '01' )
            then -- Transparent Color Flag set
              img.transparancy_index := blob2num( p_img_blob, 1, ind + 6 );
            end if;
          end if;
          ind := ind + 2; -- skip sentinel + label
          loop
            t_len := blob2num( p_img_blob, 1, ind ); -- Block Size
            exit when t_len = 0;
            ind := ind + 1 + t_len; -- skip Block Size + Data Sub-block
          end loop;
          ind := ind + 1;           -- skip last Block Size
        when hextoraw( '2C' )       -- image
        then
          declare
            img_blob blob;
            min_code_size pls_integer;
            code_size pls_integer;
            flags raw(1);
          begin
            img.width := utl_raw.cast_to_binary_integer( dbms_lob.substr( p_img_blob, 2, ind + 5 )
                                                       , utl_raw.little_endian
                                                       );
            img.height := utl_raw.cast_to_binary_integer( dbms_lob.substr( p_img_blob, 2, ind + 7 )
                                                        , utl_raw.little_endian
                                                        );
            img.greyscale := false;
            ind := ind + 1 + 8;                   -- skip sentinel + img sizes
            flags := dbms_lob.substr( p_img_blob, 1, ind );
            if utl_raw.bit_and( flags, hextoraw( '80' ) ) = hextoraw( '80' )
            then
              t_len := 3 * power( 2, raw2num( utl_raw.bit_and( flags, hextoraw( '07' ) ) ) + 1 );
              img.color_tab := dbms_lob.substr( p_img_blob, t_len, ind + 1 );          -- Local Color Table
            end if;
            ind := ind + 1;                                -- skip image Flags
            min_code_size := blob2num( p_img_blob, 1, ind );
            ind := ind + 1;                      -- skip LZW Minimum Code Size
            dbms_lob.createtemporary( img_blob, true );
            loop
              t_len := blob2num( p_img_blob, 1, ind ); -- Block Size
              exit when t_len = 0;
              dbms_lob.append( img_blob, dbms_lob.substr( p_img_blob, t_len, ind + 1 ) ); -- Data Sub-block
              ind := ind + 1 + t_len;      -- skip Block Size + Data Sub-block
            end loop;
            ind := ind + 1;                            -- skip last Block Size
            img.pixels := lzw_decompress( img_blob, min_code_size + 1 );
--
            if utl_raw.bit_and( flags, hextoraw( '40' ) ) = hextoraw( '40' )
            then                                        --  interlaced
              declare
                pass pls_integer;
                pass_ind tp_pls_tab;
              begin
                dbms_lob.createtemporary( img_blob, true );
                pass_ind( 1 ) := 1;
                pass_ind( 2 ) := trunc( ( img.height - 1 ) / 8 ) + 1;
                pass_ind( 3 ) := pass_ind( 2 ) + trunc( ( img.height + 3 ) / 8 );
                pass_ind( 4 ) := pass_ind( 3 ) + trunc( ( img.height + 1 ) / 4 );
                pass_ind( 2 ) := pass_ind( 2 ) * img.width + 1;
                pass_ind( 3 ) := pass_ind( 3 ) * img.width + 1;
                pass_ind( 4 ) := pass_ind( 4 ) * img.width + 1;
                for i in 0 .. img.height - 1
                loop
                  pass := case mod( i, 8 )
                            when 0 then 1
                            when 4 then 2
                            when 2 then 3
                            when 6 then 3
                            else 4
                          end;
                  dbms_lob.append( img_blob, dbms_lob.substr( img.pixels, img.width, pass_ind( pass ) ) );
                  pass_ind( pass ) := pass_ind( pass ) + img.width;
                end loop;
                img.pixels := img_blob;
              end;
            end if;
--
            dbms_lob.freetemporary( img_blob );
          end;
        else
          exit;
      end case;
    end loop;
--
    img.type := 'gif';
    return img;
  end;
--
  function parse_img
    ( p_blob in blob
    , p_adler32 in varchar2 := null
    , p_type in varchar2 := null
    )
  return tp_img
  is
    t_img tp_img;
  begin
    t_img.type := p_type;
    if t_img.type is null
    then
      if rawtohex( dbms_lob.substr( p_blob, 8, 1 ) ) = '89504E470D0A1A0A'
      then
        t_img.type := 'png';
      elsif dbms_lob.substr( p_blob , 3, 1 ) = utl_raw.cast_to_raw( 'GIF' )
      then
        t_img.type := 'gif';
      else
        t_img.type := 'jpg';
      end if;
    end if;
--
    t_img := case lower( t_img.type )
               when 'gif' then parse_gif( p_blob )
               when 'png' then parse_png( p_blob )
               when 'jpg' then parse_jpg( p_blob )
               else null
             end;
--
    if t_img.type is not null
    then
      t_img.adler32 := coalesce( p_adler32, adler32( p_blob ) );
    end if;
    return t_img;
  end;
--
  procedure put_image
    ( p_img blob
    , p_x number
    , p_y number
    , p_width number := null
    , p_height number := null
    , p_align varchar2 := 'center'
    , p_valign varchar2 := 'top'
  )
  is
    t_x number;
    t_y number;
    t_img tp_img;
    t_ind pls_integer;
    t_adler32 varchar2(8);
  begin
    if p_img is null
    then
      return;
    end if;
    t_adler32 := adler32( p_img );
    t_ind := g_images.first;
    while t_ind is not null
    loop
      exit when g_images( t_ind ).adler32 = t_adler32;
      t_ind := g_images.next( t_ind );
    end loop;
--
    if t_ind is null
    then
      t_img := parse_img( p_img, t_adler32 );
      if t_img.adler32 is null
      then
        return;
      end if;
      t_ind := g_images.count( ) + 1;
      g_images( t_ind ) := t_img;
    end if;
--
    t_x := case substr( upper( p_align ), 1, 1 )
             when 'L' then p_x -- left
             when 'S' then p_x -- start
             when 'R' then p_x + nvl( p_width, 0 ) - g_images( t_ind ).width -- right
             when 'E' then p_x + nvl( p_width, 0 ) - g_images( t_ind ).width -- end
             else ( p_x + nvl( p_width, 0 ) - g_images( t_ind ).width ) / 2       -- center
           end;
    t_y := case substr( upper( p_valign ), 1, 1 )
             when 'C' then ( p_y - nvl( p_height, 0 ) + g_images( t_ind ).height ) / 2  -- center
             when 'B' then p_y - nvl( p_height, 0 ) + g_images( t_ind ).height -- bottom
             else p_y                                          -- top
           end;
--
    txt2page( 'q ' || to_char_round( least( nvl( p_width, g_images( t_ind ).width ), g_images( t_ind ).width ) )
            || ' 0 0 ' || to_char_round( least( nvl( p_height, g_images( t_ind ).height ), g_images( t_ind ).height ) )
            || ' ' || to_char_round( t_x ) || ' ' || to_char_round( t_y )
            || ' cm /I' || to_char( t_ind ) || ' Do Q'
            );
  end;
--
  procedure put_image
    ( p_dir varchar2
    , p_file_name varchar2
    , p_x number
    , p_y number
    , p_width number := null
    , p_height number := null
    , p_align varchar2 := 'center'
    , p_valign varchar2 := 'top'
  )
  is
    t_blob blob;
  begin
    t_blob := file2blob( p_dir
                       , p_file_name
                       );
    put_image( t_blob
             , p_x
             , p_y
             , p_width
             , p_height
             , p_align
             , p_valign
             );
    dbms_lob.freetemporary( t_blob );
  end;
--
  procedure put_image
    ( p_url varchar2
    , p_x number
    , p_y number
    , p_width number := null
    , p_height number := null
    , p_align varchar2 := 'center'
    , p_valign varchar2 := 'top'
    )
  is
    t_blob blob;
  begin
    t_blob := httpuritype( p_url ).getblob( );
    put_image( t_blob
             , p_x
             , p_y
             , p_width
             , p_height
             , p_align
             , p_valign
             );
    dbms_lob.freetemporary( t_blob );
  end;
--
  procedure set_page_proc( p_src clob )
  is
  begin
    g_page_prcs( g_page_prcs.count ) := p_src;
  end;
--
  procedure cursor2table
    ( p_c integer
    , p_widths tp_col_widths := null
    , p_headers tp_headers := null
    )
  is
    t_col_cnt integer;
$IF DBMS_DB_VERSION.VER_LE_10 $THEN
    t_desc_tab dbms_sql.desc_tab2;
$ELSE
    t_desc_tab dbms_sql.desc_tab3;
$END
    d_tab dbms_sql.date_table;
    n_tab dbms_sql.number_table;
    v_tab dbms_sql.varchar2_table;
    t_bulk_size pls_integer := 200;
    t_r integer;
    t_cur_row pls_integer;
    type tp_integer_tab is table of integer;
    t_chars tp_integer_tab := tp_integer_tab( 1, 8, 9, 96, 112 );
    t_dates tp_integer_tab := tp_integer_tab( 12, 178, 179, 180, 181 , 231 );
    t_numerics tp_integer_tab := tp_integer_tab( 2, 100, 101 );
    t_widths tp_col_widths;
    t_tmp number;
    t_x number;
    t_y number;
    t_start_x number;
    t_lineheight number;
    t_padding number := 2;
    t_num_format varchar2(100) := 'tm9';
    t_date_format varchar2(100) := 'dd.mm.yyyy';
    t_txt varchar2(32767);
    c_rf number := 0.2; -- raise factor of text above cell bottom 
--
    procedure show_header
    is
    begin
      if p_headers is not null and p_headers.count > 0
      then
        t_x := t_start_x;
        for c in 1 .. t_col_cnt
        loop
          rect( t_x, t_y, t_widths( c ), t_lineheight );
          if c <= p_headers.count
          then
            put_txt( t_x + t_padding, t_y + c_rf * t_lineheight, p_headers( c ) );
          end if; 
          t_x := t_x + t_widths( c ); 
        end loop;
        t_y := t_y - t_lineheight;
      end if;
    end;
--
  begin
$IF DBMS_DB_VERSION.VER_LE_10 $THEN
    dbms_sql.describe_columns2( p_c, t_col_cnt, t_desc_tab );
$ELSE
    dbms_sql.describe_columns3( p_c, t_col_cnt, t_desc_tab );
$END
    if p_widths is null or p_widths.count < t_col_cnt
    then
      t_tmp := get( c_get_page_width ) - get( c_get_margin_left ) - get( c_get_margin_right );
      t_widths := tp_col_widths();
      t_widths.extend( t_col_cnt );  
      for c in 1 .. t_col_cnt
      loop
        t_widths( c ) := round( t_tmp / t_col_cnt, 1 ); 
      end loop;
    else
      t_widths := p_widths;
    end if;
--
    if get( c_get_current_font ) is null
    then 
      set_font( 'helvetica', 12 );
    end if;
--
    for c in 1 .. t_col_cnt
    loop
      case
        when t_desc_tab( c ).col_type member of t_numerics
        then
          dbms_sql.define_array( p_c, c, n_tab, t_bulk_size, 1 );
        when t_desc_tab( c ).col_type member of t_dates
        then
          dbms_sql.define_array( p_c, c, d_tab, t_bulk_size, 1 );
        when t_desc_tab( c ).col_type member of t_chars
        then
          dbms_sql.define_array( p_c, c, v_tab, t_bulk_size, 1 );
        else
          null;
      end case;
    end loop;
--
    t_start_x := get( c_get_margin_left );
    t_lineheight := get( c_get_fontsize ) * 1.2;
    t_y := coalesce( get( c_get_y ) - t_lineheight, get( c_get_page_height ) - get( c_get_margin_top ) ) - t_lineheight; 
--
    show_header;
--
    loop
      t_r := dbms_sql.fetch_rows( p_c );
      for i in 0 .. t_r - 1
      loop
        if t_y < get( c_get_margin_bottom )
        then
          new_page;
          t_y := get( c_get_page_height ) - get( c_get_margin_top ) - t_lineheight; 
          show_header;
        end if;
        t_x := t_start_x;
        for c in 1 .. t_col_cnt
        loop
          case
            when t_desc_tab( c ).col_type member of t_numerics
            then
              n_tab.delete;
              dbms_sql.column_value( p_c, c, n_tab );
              rect( t_x, t_y, t_widths( c ), t_lineheight );
              t_txt := to_char( n_tab( i + n_tab.first() ), t_num_format );
              if t_txt is not null
              then
                put_txt( t_x + t_widths( c ) - t_padding - str_len( t_txt ), t_y + c_rf * t_lineheight, t_txt );
              end if; 
              t_x := t_x + t_widths( c ); 
            when t_desc_tab( c ).col_type member of t_dates
            then
              d_tab.delete;
              dbms_sql.column_value( p_c, c, d_tab );
              rect( t_x, t_y, t_widths( c ), t_lineheight );
              t_txt := to_char( d_tab( i + d_tab.first() ), t_date_format );
              if t_txt is not null
              then
                put_txt( t_x + t_padding, t_y + c_rf * t_lineheight, t_txt );
              end if; 
              t_x := t_x + t_widths( c ); 
            when t_desc_tab( c ).col_type member of t_chars
            then
              v_tab.delete;
              dbms_sql.column_value( p_c, c, v_tab );
              rect( t_x, t_y, t_widths( c ), t_lineheight );
              t_txt := v_tab( i + v_tab.first() );
              if t_txt is not null
              then
                put_txt( t_x + t_padding, t_y + c_rf * t_lineheight, t_txt );
              end if; 
              t_x := t_x + t_widths( c ); 
            else
              null;
          end case;
        end loop;
        t_y := t_y - t_lineheight;
      end loop;
      exit when t_r != t_bulk_size;
    end loop;      
    g_y := t_y;
  end;
--
  procedure query2table
    ( p_query varchar2
    , p_widths tp_col_widths := null
    , p_headers tp_headers := null
    )
  is
    t_cx integer;
    t_dummy integer;
  begin
    t_cx := dbms_sql.open_cursor;
    dbms_sql.parse( t_cx, p_query, dbms_sql.native );
    t_dummy := dbms_sql.execute( t_cx );
    cursor2table( t_cx, p_widths, p_headers ); 
    dbms_sql.close_cursor( t_cx );
  end;
$IF not DBMS_DB_VERSION.VER_LE_10 $THEN
--
  procedure refcursor2table
    ( p_rc sys_refcursor
    , p_widths tp_col_widths := null
    , p_headers tp_headers := null
    )
  is
    t_cx integer;
    t_rc sys_refcursor;
  begin
    t_rc := p_rc;
    t_cx := dbms_sql.to_cursor_number( t_rc );
    cursor2table( t_cx, p_widths, p_headers ); 
    dbms_sql.close_cursor( t_cx );
  end;
$END
--
  procedure pr_goto_page( i_npage number )
  is
  begin
    if i_npage <= g_pages.count
    then
      g_page_nr := i_npage - 1;
    end if;
  end;
--
  procedure pr_goto_current_page
  is
  begin
    g_page_nr := null;
  end;
end;
/

SHOW ERRORS;


CREATE OR REPLACE procedure morders_insert_articles is
    pOut clob;
    l_response  json;
    token varchar2(1000):='';
    TYPE VRNI_rt IS RECORD(document_id varchar2(10000));
    l_VRNI   VRNI_rt;
    b_kurzor        sys_refcursor;
    mArticeles json_list;
    mArticle json;
    syncObject json;
    tmp json;
    dateLast varchar2(1000) := '';
    CURSOR C_ARTICLES IS SELECT ID,TITLE,TYPE,BARCODE,TYPEART,LAST_Date,COMPANY,NOTE1,NOTE2,NOTE3,NOTE4,NOTE5,PRICE FROM ARTICLES where last_DAte>(select to_date(nvl(trim(tekst),'2022-07-13 13:31:10'),'yyyy-mm-dd hh24:mi:ss') from ferbit.nastavi where trim(kljuc)='morders_art_date');
    mContent clob;
BEGIN

        select trim(tekst) into token from ferbit.nastavi where kljuc='morders_token';
        begin
            select nvl(trim(tekst),'2022-07-13 13:31:10') into dateLast from ferbit.nastavi where kljuc='morders_articles_date';
            exception when others then
            dateLast:='2022-07-13 13:31:10';
        end;

        mArticeles:=json_list();
        FOR article IN C_ARTICLES LOOP
        dbms_output.put_line('id '||article.id);
            mArticle := json();
            mArticle.put('id', article.id);
            mArticle.put('title', nvl(trim(article.title),''));
            mArticle.put('barcode', nvl(trim(article.barcode),''));
            mArticle.put('type', nvl(trim(article.type),''));
            mArticle.put('typeart', nvl(trim(article.typeart),''));
            mArticle.put('last', nvl(to_char(article.last_Date,'yyyy-mm-dd hh24:mi:ss'),'1990-01-01 00:00:00'));
            mArticle.put('company', 0);
            mArticle.put('price', nvl(article.price,0));
            mArticle.put('note1', nvl(trim(article.note1),''));
            mArticle.put('note2', nvl(trim(article.note2),''));
            mArticle.put('note3', nvl(trim(article.note3),''));
            mArticle.put('note4', nvl(trim(article.note4),''));
            mArticle.put('note5', nvl(trim(article.note5),''));
            mArticeles.append(mArticle.to_json_value);
        end loop;
        syncObject := json();
        syncObject.put('articles',mArticeles);

        DBMS_LOB.CREATETEMPORARY(mContent,true);
        syncObject.to_clob(mContent,true);
        --dbms_output.put_line('----');
        url_connect('http://192.168.0.10/Mobile/webresources/mOrders/API/insertArticles',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');

        merge into ferbit.nastavi n using (select 1 from dual) m
            on (trim(n.kljuc)='morders_art_date')
        when matched then
            update set tekst=to_char(sysdate,'YYYY-MM-DD HH24:MI:SS')
        when not matched then
            insert (kljuc, tekst) values('2022-07-13 13:31:10','morders_art_date');
        commit;
end;
/

SHOW ERRORS;


CREATE OR REPLACE PROCEDURE MORDERS_GET_UPDATE is
    token varchar2(1000):='';
    TYPE VRNI_rt IS RECORD(document_id varchar2(10000), path varchar2(10000), docType number);
    l_VRNI   VRNI_rt;
    b_kurzor        sys_refcursor;
    DateFrom date;
    DateTo date;
begin


select trim(tekst) into token from ferbit.nastavi where kljuc='morders_token';
    begin
      select to_date(tekst,'yyyy-mm-dd hh24:mi:ss') into DateFrom from ferbit.nastavi where  kljuc='morders_PRENOS_DATUM';
    exception when others then
      DateFrom := sysdate-1;
    end;
    DateTo := sysdate;
   
  --  INSERT INTO SQL_ERROR(OPIS_NAPAKE)VALUES('DATEFROM '||to_char(datefrom,'dd.mm.yyyy hh24:mi:ss'));
-- INSERT INTO SQL_ERROR(OPIS_NAPAKE)VALUES('DateTo '||to_char(DateTo,'dd.mm.yyyy hh24:mi:ss')); 
    morders_get_data(token,DateFrom,DateTo);
    OPEN b_kurzor FOR 'select linkid, file_name, ORDER_TYPE from attachments where nvl(status,0)=0';
                     LOOP
                        FETCH b_kurzor INTO l_VRNI;
                        EXIT WHEN b_kurzor%NOTFOUND; 
                         morders_GET_document(token, l_VRNI.document_id, l_vrni.path,l_vrni.docType);  
                    END LOOP;
        close b_kurzor;
        
        
        MERGE INTO ferbit.nastavi n
            USING (select 1 from dual) m
            ON (n.kljuc='morders_PRENOS_DATUM')
          WHEN MATCHED THEN
            UPDATE SET n.tekst=to_char(DateTo,'YYYY-MM-DD HH24:MI:SS')
          WHEN NOT MATCHED THEN
            INSERT (kljuc, tekst)
            VALUES ('morders_PRENOS_DATUM', to_char(DateTo,'YYYY-MM-DD HH24:MI:SS'));
        

        --update ferbit.nastavi set tekst=to_char(DateTo,'YYYY-MM-DD HH24:MI:SS') where kljuc='morders_PRENOS_DATUM';
        commit;
         -- DBMS_OUTPUT.PUT_LINE('end '||to_char(sysdate,'yyyy-MM-dd hh24:mi:ss'));
end;
/

SHOW ERRORS;


CREATE OR REPLACE PROCEDURE WRITE_TO_FILE (
dir        in varchar2,
ime        in char,
documentIn        clob )
as
    t_data         BLOB;
    v_buffer       RAW(32767);
    v_buffer_size  BINARY_INTEGER;
    v_amount       BINARY_INTEGER;
    v_offset       NUMBER(38) := 1;
    v_chunksize    INTEGER;
    v_out_file     UTL_FILE.FILE_TYPE;
    dest_offset INTEGER := 1;
    src_offset INTEGER := 1;
    warning INTEGER;
    ctx INTEGER := DBMS_LOB.DEFAULT_LANG_CTX;
    
    sql_error      VARCHAR2(30000);
  l_error        number;
  l_napak        number;
begin

    if nvl((DBMS_LOB.getlength(documentIn ))/1024/1024,0) > 0 then
  --  dbms_output.put_line('zacetek convert '|| to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));
     t_Data := base64decodeclobasblob_plsql(documentIn);
 --dbms_output.put_line('konec convert '|| to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));


           v_chunksize := DBMS_LOB.GETCHUNKSIZE(t_Data);
            IF (v_chunksize < 32767) THEN
                v_buffer_size := v_chunksize;
            ELSE
                v_buffer_size := 32767;
            END IF;
            v_amount := v_buffer_size;
            DBMS_LOB.OPEN(t_data, DBMS_LOB.LOB_READONLY);
            v_out_file := UTL_FILE.FOPEN(
                location      => dir,
                filename      => ime,
                open_mode     => 'wb',
                max_linesize  => 32767);
            WHILE v_amount >= v_buffer_size
            LOOP
              DBMS_LOB.READ(
                  lob_loc    => t_Data,
                  amount     => v_amount,
                  offset     => v_offset,
                  buffer     => v_buffer);
              v_offset := v_offset + v_amount;
              UTL_FILE.PUT_RAW (
                  file      => v_out_file,
                  buffer    => v_buffer,
                  autoflush => true);
              UTL_FILE.FFLUSH(file => v_out_file);
            END LOOP;
            UTL_FILE.FFLUSH(file => v_out_file);
            UTL_FILE.FCLOSE(v_out_file);
            DBMS_LOB.CLOSE(t_data);
         end if;
end;
/

SHOW ERRORS;


CREATE OR REPLACE PROCEDURE ERROR_LOG (p_NAPAKA in number, p_OPIS_NAPAKE in char,p_OPIS_NAPAKE_SQL in char,p_TRID in number,p_PROCEDURA in char,p_ID_RECORD in number)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
/******************************************************************************
   NAME:       ERROR_LOG
   PURPOSE:   Zapis loga
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0       18.04.2015  Leo Lakota
   PARAMETERS:
   pDat1             Prvi datum obdobja
   pDat2             Drugi datum obdobja
   p_Transakcija     Id Transakcije 
   CALLED BY:WinPro.exe
   CALLS:
   EXAMPLE USE:    execute OBRACUN_PRO('01.01.2015','31.01.2015',1555)
   ASSUMPTIONS:
   LIMITATIONS:
   ALGORITHM:
   NOTES:
******************************************************************************/

l_error              NUMBER:=0;                                
Sql_error            varchar2(250);                                         --- opis napake
sql_stmt             VARCHAR2(3900);
l_stevilor           NUMBER:=0;
l_kontrola           NUMBER:=0;
 

BEGIN
  
        INSERT INTO SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,ID_RECORD)
        VALUES (p_NAPAKA, p_OPIS_NAPAKE,p_OPIS_NAPAKE_SQL,p_TRID,p_PROCEDURA,p_ID_RECORD);  
        COMMIT;
 EXCEPTION
     WHEN OTHERS THEN
      ROLLBACK;
 
 
  
END ERROR_LOG;
/

SHOW ERRORS;


CREATE OR REPLACE PROCEDURE DODAJ_DOKUMENT ( 
tip_dokumenta       in number, -- ??
xDocId              in number, -- glavni docId
mapa_dokumenta      in char,  -- pot na disku kjer je shranjena datoteka
ime_dokumenta       in char,
sekvenca            in number, -- sekvenca za DOC_DOKUMENT
vrsta               in int,
order_type in char,
lot number)   -- vrsta datoteke
as
t_xid int              :=0; 
t_ASSEMBLY_ID      int :=0; -- PART_ID glavnega dokumenta
t_ASSEMBLY_AVTO    int :=0; -- AVTO glavnega dokumenta 

stavek2            VARCHAR2(4000);
stavek3            VARCHAR2(4000);
stavek4            VARCHAR2(4000);
stavek5            VARCHAR2(4000);
stavek6            VARCHAR2(4000);
stavek7            VARCHAR2(4000);
napaka             int :=0;
l_QUANTITY_O    number :=0;
 -- iz PNALOG_MOBILE
 l_xid    number :=0;
l_tip    number :=0; -- mogo�e ga je treba najt iz nastavitev !!
l_veja   number :=0;
l_pot    VARCHAR2(299 BYTE);
l_part   number :=0;
l_avto   number :=0;
l_error                  NUMBER:=0;                                
l_procedure              CHAR(15):='dodaj_dokument';                                    --- ali obracun obstaja
Sql_error                varchar2(250);
t_PRO_ID number :=0;
l_vezdokumenta       number  :=0;
l_docid              number  :=0;   
l_vrsta              number  :=0;
l_projectId          number  :=0;
 
begin
    INSERT INTO ferbit.LOG (  OPIS) VALUES ( 'Dodaj dokument'||sekvenca||' vrsta:' ||vrsta);   
  t_xid :=   sekvenca; 
     
     /*
  if  vrsta = 9  then
    l_tip     := 9 ; --mogo�e ga je treba najt iz nastavitev !!
  elsif vrsta = 8 then
   l_tip      :=8; 
  end if;  */
   if  vrsta = 55  then
        l_tip     := 6 ; --mogo�e ga je treba najt iz nastavitev !!
  elsif vrsta = 50 then
        l_tip     :=16 ;
  elsif vrsta = 51 then
        l_tip     :=105 ;
  elsif vrsta = 52 then
        l_tip     :=17 ;
  elsif vrsta = 53 then
        l_tip     :=4 ;
   elsif vrsta = 54 then
        l_tip     :=18 ;
   elsif vrsta = 56 then
        l_tip     :=19 ;
   elsif vrsta = 10 then
        l_tip:=14;
   elsif vrsta = 11 then
        l_tip:=14;
    else 
        l_tip     :=14 ; --mogo�e ga je treba najt iz nastavitev !!
  end if;   
  
 
     
 if t_xid > 0 then --�e obstaja sekvenca dodamo zapis v dokumente
   INSERT INTO ferbit.LOG (  OPIS) VALUES ( 'Pri�li brez sequence' );
       begin
            stavek3 :=  'INSERT INTO ferbit.DOC_DOKUMENT (XID, XNAME, VRSTA, POT_DATOTEKE, IME_DATOTEKE, KRATKI_OPIS,TIP_DOKUMENTA ) values (:1,:2,:3,:4,:5,:6,:7)';
            execute immediate stavek3 using t_xid,ime_dokumenta,2, mapa_dokumenta, ime_dokumenta,ime_dokumenta,l_tip;
       EXCEPTION
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE(SUBSTR(SQLERRM,1,4000) );
            Sql_error:=SUBSTR(SQLERRM,1,250);
            INSERT INTO ferbit.LOG (  OPIS) VALUES ( Sql_error );
            napaka := 1;
    end;
/*
    if  vrsta in (9)  then -- �e so te vrste najdemo korenski id od vrste dokumenta v doc_relacijah
   
        Begin 
            SELECT POT,VEJA into l_pot,l_veja  FROM  ferbit.DOC_TIP WHERE  vez=l_tip;
        EXCEPTION
         WHEN OTHERS THEN
           Sql_error := SUBSTR(SQLERRM, 1, 250);
           ERROR_LOG(l_error,'SELECT TIP',Sql_error,0,l_procedure,0); 
       End;
   
       Begin 
       SELECT PART_ID,AVTO into l_part,l_avto  FROM  ferbit.DOC_RELACIJE WHERE avto = l_veja;
       EXCEPTION
         WHEN OTHERS THEN
           Sql_error := SUBSTR(SQLERRM, 1, 250);
           ERROR_LOG(l_error,'SELECT RELACIJE',Sql_error,0,l_procedure,0); 
       End;
       --    INSERT INTO LOG (  OPIS) VALUES ( 'l_part,l_avto:' ||l_part||','||l_avto);
  
    end if;
*/
   Begin 
     INSERT INTO ferbit.LOG (  OPIS) VALUES ( 'Dodaj relacijo 1'||l_docid );  
      insert into ferbit.doc_relacije
      (assembly_id,part_id,assembly_avto)
      values( xDocId,sekvenca,l_veja); 
      EXCEPTION
      WHEN OTHERS THEN
      Sql_error := SUBSTR(SQLERRM, 1, 250);
      ERROR_LOG(l_error,'ZAPIS RELACIJE',Sql_error,0,l_procedure,0); 
    end; 
    


 end if; -- t_xid > 0


end;
/

SHOW ERRORS;


CREATE OR REPLACE procedure morders_auth(taxNumber in varchar2, title in varchar2)  IS
    pOut clob;
    l_response  json;   
     mContent clob;
    syncObject json;  
    cnt number:=0;
BEGIN 

    syncObject := json();
    syncObject.put('barcode',taxNumber);
    syncObject.put('deviceToken',title);

    DBMS_LOB.CREATETEMPORARY(mContent,true); 
    syncObject.to_clob(mContent,true); 

 --  url_connect('http://127.0.0.1/Mobile/webresources/mOrders/API/auth',mContent,'POST',pOut,'','application/json;charset=utf-8');
   
   url_connect('http://192.168.0.10/mobilno/Mobile/webresources/mOrders/API/auth',mContent,'POST',pOut,'','application/json;charset=utf-8');
    dbms_output.put_line(pout);
    l_response := json(pout); 


    select count( tekst) into cnt from ferbit.nastavi where kljuc='morders_token'; 
    if(cnt=0) then
        insert into ferbit.nastavi(kljuc,tekst) values('morders_token',json_ext.get_string(l_response,'deviceToken')); 
    else
        update ferbit.nastavi set tekst=json_ext.get_string(l_response,'deviceToken') where kljuc='morders_token';
    end if;
end;
/

SHOW ERRORS;


CREATE OR REPLACE procedure                                 morders_send_with_job (l_time in number) is
    Sql_error varchar2(1000);
   
 
BEGIN
    DBMS_SCHEDULER.CREATE_JOB (
            job_name => '"MOBILNO"."MORD_SEND_JOB_TMP"',
            job_type => 'PLSQL_BLOCK',
            job_action => '"declare begin  morders_get_update;  morders_insert_orders(0);  morders_send_messages; end;"',
            number_of_arguments => 0,
            start_date => TO_TIMESTAMP_TZ(sysdate+(l_time/(24*60*60))),
            repeat_interval => NULL,
            end_date => NULL,
            enabled => FALSE,
            auto_drop => TRUE,
            comments => '');
 
    DBMS_SCHEDULER.SET_ATTRIBUTE( 
             name => '"MOBILNO"."MORD_SEND_JOB_TMP"', 
             attribute => 'job_priority', value => '1');
    DBMS_SCHEDULER.SET_ATTRIBUTE( 
             name => '"MOBILNO"."MORD_SEND_JOB_TMP"', 
             attribute => 'logging_level', value => DBMS_SCHEDULER.LOGGING_FULL);
       
    DBMS_SCHEDULER.enable(
             name => '"MOBILNO"."MORD_SEND_JOB_TMP"');
END;
/

SHOW ERRORS;


CREATE OR REPLACE procedure        dodaj_logiranje11
( tableName in VARCHAR2 )
AS
v_cursor             NUMBER;
v_dummy              NUMBER;
sel_columnList       VARCHAR2(4000);
view_create          VARCHAR2(4000);
trig_create          VARCHAR2(32767);
columnName           VARCHAR2(100) := '';
columnList           VARCHAR2(32767) := '';
columnListNew        VARCHAR2(32767) := '';
columnListOld        VARCHAR2(32767) := '';
tableRName           VARCHAR2(26) := '';
BEGIN
       v_cursor := DBMS_SQL.OPEN_CURSOR;


       -- drop stolpcev
  /*     begin
        DBMS_SQL.PARSE(v_cursor, 'alter table '||tableName||' drop column log_sysdate', DBMS_SQL.NATIVE);
        DBMS_SQL.PARSE(v_cursor, 'alter table '||tableName||' drop column log_user', DBMS_SQL.NATIVE);
        exception
        when others then
        null;
       end;
*/
        -- dobi stolpce       
       sel_columnList := 'select column_name from user_tab_columns where table_name = upper(:tableN) order by column_id';
       DBMS_SQL.PARSE(v_cursor, sel_columnList, DBMS_SQL.NATIVE);
       DBMS_SQL.BIND_VARIABLE(v_cursor, ':tableN', tableName);
       DBMS_SQL.DEFINE_COLUMN(v_cursor, 1, columnName,30);

       v_dummy := DBMS_SQL.EXECUTE(v_cursor);

       -- spiši stolpce v string (columnList)

       LOOP
        IF DBMS_SQL.FETCH_ROWS(v_cursor) = 0 THEN
          EXIT;
        END IF;

        DBMS_SQL.COLUMN_VALUE(v_cursor,1,columnName);
        columnList    := TRIM(columnList) || ', ' || TRIM(columnName);      
        columnListNew := TRIM(columnListNew) || ', ' || ':New.'||TRIM(columnName); 
        columnListOld := TRIM(columnListOld) || ', ' ||':Old.'|| TRIM(columnName); 
       END LOOP;






        DBMS_SQL.CLOSE_CURSOR(v_cursor);
        v_cursor := DBMS_SQL.OPEN_CURSOR;

       -- kreiraj view
       view_create := 'create TABLE MLOG$_'||TRIM(tableName)||' as select * from '||TRIM(tableName)||' where 1=0';
      DBMS_SQL.PARSE(v_cursor, view_create, DBMS_SQL.NATIVE);

       tableRName := substr(tableName,1,20);

       -- dodamo dva stolpca
       DBMS_SQL.PARSE(v_cursor, 'alter table MLOG$_'||tableName||' add (mlog_sysdate date)', DBMS_SQL.NATIVE);
       DBMS_SQL.PARSE(v_cursor, 'alter table MLOG$_'||tableName||' add (mlog_user varchar2(30) )', DBMS_SQL.NATIVE);
       DBMS_SQL.PARSE(v_cursor, 'alter table MLOG$_'||tableName||' add (mlog_AKCIJA varchar2(1) )', DBMS_SQL.NATIVE);


       -- kreiramo trigger
       trig_create := 'create or replace trigger MLOG$T'||tableName||' before DELETE OR INSERT OR UPDATE on '||tableName;
       trig_create := trig_create || ' referencing new as new for each row ';
       trig_create := trig_create || ' Declare ';
       trig_create := trig_create || ' v_ChangeType string(1):=''''; ';
       trig_create := trig_create || ' Begin ';
       trig_create := trig_create || ' IF INSERTING THEN ';
       trig_create := trig_create || '      v_ChangeType := ''I'';';
       trig_create := trig_create || '   ELSIF UPDATING THEN ';
       trig_create := trig_create || '       v_ChangeType := ''U'';';
       trig_create := trig_create || '    ELSE';
       trig_create := trig_create || '      v_ChangeType := ''D'';';
       trig_create := trig_create || '   END IF;';
       trig_create := trig_create || '  Begin'; 
       trig_create := trig_create || ' If inserting then ';
       trig_create := trig_create || ' insert into MLOG$_'||tableName;
       trig_create := trig_create || ' ('||Trim(substr(columnList,2))||',mlog_sysdate,mlog_user,mlog_AKCIJA) ';
       trig_create := trig_create || '  values ('||trim(substr(columnListNew,2))||',sysdate,user,v_ChangeType);'; 
       trig_create := trig_create || 'end If;';
       trig_create := trig_create || ' If deleting or updating then ';
       trig_create := trig_create || ' insert into MLOG$_'||tableName;
       trig_create := trig_create || ' ('||Trim(substr(columnList,2))||',mlog_sysdate,mlog_user,mlog_AKCIJA) ';
       trig_create := trig_create || '  values ('||Trim(substr(columnListOld,2))||',sysdate,user,v_ChangeType);'; 
       trig_create := trig_create || 'end If;';
       trig_create := trig_create || ' EXCEPTION ';
       trig_create := trig_create || ' WHEN OTHERS THEN ';
       trig_create := trig_create || '   NULL;';
       trig_create := trig_create || ' END;';
       trig_create := trig_create || ' end;';
       DBMS_SQL.PARSE(v_cursor, trig_create, DBMS_SQL.NATIVE);


END;
/

SHOW ERRORS;


CREATE OR REPLACE PROCEDURE URL_CONNECT(pUrl in varchar2,pContent IN CLOB, pMethod IN VARCHAR2, pOut OUT CLOB, auth in varchar2 default '', contentType in varchar2 default 'application/json;charset=utf-8' )IS
/*
      mmessage_id   -  id sporočila
      muser_id      -  za koga je sprocilo; lahko je id uporabnika ali vozila(odvisno kak mMessageType pošiljamo)
      mtype         -  tip sporocila; 0 sinhronizacija; 1 sporočilo
      mcontent      -  vsebina
      mfromuser     -  od koga je sporocilo 1-centrala
      msendeddate   -  datum posiljanja
      mMessageType  -  če gre za nalog 0; če je sporčilo 1
      */
  t_http_req     utl_http.req;
  t_http_resp    utl_http.resp;
  t_respond      VARCHAR2(30000);
  t_start_pos    INTEGER := 1;
  t_output       VARCHAR2(2000);
  l_mcontent     varchar2(10000);
  t_device       VARCHAR2(2000);
  t_data         VARCHAR(30000);
  sql_error      VARCHAR2(30000);
  l_error        number;
  l_napak        number;
  l_procedure    CHAR(15):='url_connect';
  buffer varchar2(4000);
  response_Text  varchar2(32000);
   l_buffer     varchar2(32767);
   l_length      number:=0;
   l_offset number:=1;
   amount number :=32767;
BEGIN
    l_length := DBMS_LOB.GETLENGTH(pContent);

    --utl_http.set_wallet ('file:/home/oracle/wallet', 'password123');
    utl_http.Set_body_charset('UTF-8');
    utl_http.Set_transfer_timeout(300);
    t_http_req := utl_http.Begin_request(pUrl , pMethod);
    utl_http.Set_header(t_http_req, 'Content-Type',contentType);
    utl_http.Set_header(t_http_req, 'Connection', 'keep-alive');

    if(nvl(length(trim(auth)),0)<>0) then
        utl_http.Set_header(t_http_req, 'Authorization', auth);
    end if;


    if(trim(pMethod)='POST') then
        if(l_length>0) then 
            utl_http.Set_header(t_http_req, 'Content-Length',l_length);
            --utl_http.Write_text(t_http_req, pContent);
           -- l_length := DBMS_LOB.GETLENGTH(pContent);

            while(l_offset < l_length)
                          loop
                             dbms_lob.read(pContent, amount, l_offset, l_buffer);
                             UTL_HTTP.WRITE_TEXT(r    => t_http_req, data => l_buffer);
                             l_offset := l_offset + amount;

                          end loop;

        else
            utl_http.Set_header(t_http_req, 'Content-Length',0);
        end if;
    else
        utl_http.Set_header(t_http_req, 'Content-Length',0);
    end if ;

    t_http_resp := utl_http.Get_response(t_http_req);
    dbms_lob.createtemporary(pOut, false);

     begin
        loop
          utl_http.read_text(t_http_resp, l_buffer, 32000);
          dbms_lob.writeappend (pOut, length(l_buffer), l_buffer);
        end loop;
      exception
        when utl_http.end_of_body then
          utl_http.end_response(t_http_resp);
      end;

                        --    DBMS_OUTPUT.PUT_LINE(pout);
    EXCEPTION
    WHEN OTHERS THEN
         l_error   := SQLCODE;
        l_napak   := 1;
         DBMS_OUTPUT.PUT_LINE('SQLERRM urlConnect: '||SQLERRM);
        Sql_error := SUBSTR(SQLERRM, 1, 250);
      --  insert into clob_test(cdata) values(Sql_error);
  
        INSERT INTO ferbit.SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,ID_RECORD,sql_stavek)
        VALUES (l_error, 'mob_url_connect',Sql_error,0,'url_connect',0,purl);  
        
        
END;
/

SHOW ERRORS;


CREATE OR REPLACE PROCEDURE MORDERS_GET_DATA(token in VARCHAR2, dateFrom in date, dateTo in date ) is
  mContent varchar2(32000);
    pOut clob;

     l_response       json;
     mOrders json_list;
     mOrder json;
     mShipments json_list;
     mShipment json;
     mAttachments json_list;
     mAttachment json;
     mCount number:=0;
     mCountShip number := 0;
     mOrderLogs json_list;
     mOrderLog json;
     att_cnt number:=0;

BEGIN
    mContent := '{"fromDate":"'||to_char(dateFrom,'yyyy-mm-dd hh24:mi:ss')||'","toDate":"'||to_char(dateTo,'yyyy-mm-dd hh24:mi:ss')||'"}';
    url_connect('http://192.168.0.10/mobilno/Mobile/webresources/mOrders/API/getOrders/',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
  --  insert into clob_test(cdata) values(pOUT);
    commit;
    l_response := json(pOut);
    mOrders := json_ext.get_json_list(l_response,'orders');
     FOR i IN 1..mOrders.count
        LOOP
            null;
             mOrder := json(mOrders.get(i));
             
 --DBMS_OUTPUT.PUT_LINE('order '||json_ext.get_number(mOrder,'orderId'));
 --insert into clob_test(cdata) values('order '||json_ext.get_number(mOrder,'orderId'));
 commit;
             select count(*) into mCount from orders where order_id=json_ext.get_number(mOrder,'orderId');
             if(mCount>0) then
                    update orders set
                        worker_id=json_ext.get_number(mOrder,'workerId'),--worker_id,
                        order_date=to_date(json_ext.get_string(mOrder,'orderDate'),'yyyy-mm-dd hh24:mi:ss'),--order_date,
                        delivery_date=to_date(json_ext.get_string(mOrder,'deliveryDate'),'yyyy-mm-dd hh24:mi:ss'),--delivery_date,
                        complete_date=to_date(json_ext.get_string(mOrder,'completeDate'),'yyyy-mm-dd hh24:mi:ss'),--complete_date,
                        otype=json_ext.get_number(mOrder,'type'),--otype,
                        type_d=json_ext.get_string(mOrder,'typeD'),--type_d
                        cnt=json_ext.get_number(mOrder,'count'),--cnt,
                        vehicle_id=json_ext.get_number(mOrder,'vehicleId'),--vehicle_id,
                        vehicle=json_ext.get_string(mOrder,'vehicle'),--vehicle,
                        terminal=json_ext.get_string(mOrder,'terminal'),--terminal,
                        seq=json_ext.get_number(mOrder,'seq'),--seq,
                        company=json_ext.get_number(mOrder,'company'),--company,
                        bussines_unit=json_ext.get_number(mOrder,'bussinesUnit'),--bussines_unit,
                        customer=json_ext.get_number(mOrder,'customer'),--customer,
                        loc=json_ext.get_string(mOrder,'location'),--loc,
                        desc_code=json_ext.get_string(mOrder,'descCide'),--desc_code,
                        active=2,--json_ext.get_number(mOrder,'active'),--active,
                        scaned_count=json_ext.get_number(mOrder,'scanedCount'),--scaned_count,
                        ended=json_ext.get_number(mOrder,'ended'),--ended,
                        transaction_id=json_ext.get_number(mOrder,'transactionId'),--transaction_id,
                        signator=json_ext.get_string(mOrder,'signator'),--signator
                        sender=json_ext.get_string(mOrder,'sender'),--sender,
                        reciver=json_ext.get_string(mOrder,'reciver'),--reciver,
                        route=json_ext.get_number(mOrder,'route'),--route,
                        scaned=to_date(json_ext.get_string(mOrder,'scanedDate'),'yyyy-mm-dd hh24:mi:ss'),--scaned,
                        oprogram=json_ext.get_string(mOrder,'program'),--oprogram,
                        note1=json_ext.get_string(mOrder,'note1'),--note1,
                        cart=json_ext.get_string(mOrder,'cart'),--cart,
                        weight=json_ext.get_number(mOrder,'weight'),--weight,
                        height=json_ext.get_number(mOrder,'height'),--height,
                        width=json_ext.get_number(mOrder,'width'),--width,
                        odepth=json_ext.get_number(mOrder,'odepth'),--odepth,
                        pro_id=json_ext.get_number(mOrder,'proId'),--pro_id,
                        stitle=json_ext.get_string(mOrder,'stitle'),--stitle,
                        saddress=json_ext.get_string(mOrder,'saddress'),--saddress,
                        spostofficenumber=json_ext.get_string(mOrder,'spostofficenumber'),--spostofficenumber
                        scity=json_ext.get_string(mOrder,'scity'),--scity,
                        rtitle=json_ext.get_string(mOrder,'rtitle'),--rtitle,
                        raddress=json_ext.get_string(mOrder,'raddress'),--raddress,
                        rpostofficenumber=json_ext.get_string(mOrder,'rpostofficenumber'),--rpostofficenumber
                        rcity=json_ext.get_string(mOrder,'rcity'),--rcity,
                        cmr=json_ext.get_string(mOrder,'cmr'),--cmr
                        note2=json_ext.get_string(mOrder,'note2'),--note2,
                        note3=json_ext.get_string(mOrder,'note3'),--note3,
                        temperature_1=json_ext.get_number(mOrder,'temperature1'),--temperature_1,
                        temperature_2=json_ext.get_number(mOrder,'temperature2'),--temperature_2,
                        nr_palettes=json_ext.get_number(mOrder,'nrPalettes'),--nr_palettes,
                        note4=json_ext.get_string(mOrder,'note4'),--note4,
                        note5=json_ext.get_string(mOrder,'note5'),--note5,
                        note6=json_ext.get_string(mOrder,'note6'),--note6,
                        s_telephone='',--s_telephone,
                        r_telephone='',--r_telephone,
                        loading_lat=json_ext.get_number(mOrder,'loadingLat'),--loading_lat,
                        loading_lng=json_ext.get_number(mOrder,'loadingLng'),--loading_lng,
                        unloading_lat=json_ext.get_number(mOrder,'unloadingLat'),--unloading_lat,
                        unloading_lng=json_ext.get_number(mOrder,'unloadingLng'),--unloading_lng,
                        trailer=json_ext.get_string(mOrder,'trailer'),--trailer,
                        trailer_id=json_ext.get_number(mOrder,'trailerId'),--trailer_id,
                        rabat1=json_ext.get_number(mOrder,'rabat1'),--rabat1,
                        rabat2=json_ext.get_number(mOrder,'rabat2')--rabat2
                    where
                        order_id=json_ext.get_number(mOrder,'orderId') ;
             else
            insert into orders(
                order_id,--NUMBER
                worker_id,--NUMBER
                order_date,--DATE
                delivery_date,--DATE
                complete_date,--DATE
                otype,--NUMBER
                type_d,--VARCHAR2(150 BYTE)
                cnt,--NUMBER
                vehicle_id,--NUMBER
                vehicle,--VARCHAR2(20 BYTE)
                terminal,--CHAR(1000 BYTE)
                seq,--NUMBER
                company,--NUMBER
                bussines_unit,--NUMBER
                customer,--NUMBER
                loc,--VARCHAR2(40 BYTE)
                desc_code,--VARCHAR2(30 BYTE)
                active,--NUMBER
                scaned_count,--NUMBER
                ended,--NUMBER
                transaction_id,--NUMBER
                signator,--VARCHAR2(150 BYTE)
                sender,--VARCHAR2(100 BYTE)
                reciver,--VARCHAR2(100 BYTE)
                route,--NUMBER
                scaned,--DATE
                oprogram,--VARCHAR2(30 BYTE)
                note1,--VARCHAR2(200 BYTE)
                cart,--VARCHAR2(50 BYTE)
                weight,--NUMBER
                height,--NUMBER
                width,--NUMBER
                odepth,--NUMBER
                pro_id,--NUMBER
                stitle,--VARCHAR2(200 BYTE)
                saddress,--VARCHAR2(200 BYTE)
                spostofficenumber,--VARCHAR2(150 BYTE)
                scity,--VARCHAR2(200 BYTE)
                rtitle,--VARCHAR2(200 BYTE)
                raddress,--VARCHAR2(200 BYTE)
                rpostofficenumber,--VARCHAR2(150 BYTE)
                rcity,--VARCHAR2(200 BYTE)
                cmr,--VARCHAR2(150 BYTE)
                note2,--VARCHAR2(2000 BYTE)
                note3,--VARCHAR2(2000 BYTE)
                temperature_1,--NUMBER
                temperature_2,--NUMBER
                nr_palettes,--NUMBER
                note4,--VARCHAR2(1000 BYTE)
                note5,--VARCHAR2(1000 BYTE)
                note6,--VARCHAR2(1000 BYTE)
                s_telephone,--VARCHAR2(50 BYTE)
                r_telephone,--VARCHAR2(50 BYTE)
                loading_lat,--NUMBER
                loading_lng,--NUMBER
                unloading_lat,--NUMBER
                unloading_lng,--NUMBER
                trailer,--VARCHAR2(1000 BYTE)
                trailer_id,--NUMBER
                rabat1,--NUMBER
                rabat2--NUMBER
            )values(
                json_ext.get_number(mOrder,'orderId'),--order_id,--NUMBER
                json_ext.get_number(mOrder,'workerId'),--worker_id,--NUMBER
                to_date(json_ext.get_string(mOrder,'orderDate'),'yyyy-mm-dd hh24:mi:ss'),--order_date,--DATE
                to_date(json_ext.get_string(mOrder,'deliveryDate'),'yyyy-mm-dd hh24:mi:ss'),--delivery_date,--DATE
                to_date(json_ext.get_string(mOrder,'completeDate'),'yyyy-mm-dd hh24:mi:ss'),--complete_date,--DATE
                json_ext.get_number(mOrder,'type'),--otype,--NUMBER
                json_ext.get_string(mOrder,'typeD'),--type_d,--VARCHAR2(150 BYTE)
                json_ext.get_number(mOrder,'count'),--cnt,--NUMBER
                json_ext.get_number(mOrder,'vehicleId'),--vehicle_id,--NUMBER
                json_ext.get_string(mOrder,'vehicle'),--vehicle,--VARCHAR2(20 BYTE)
                json_ext.get_string(mOrder,'terminal'),--terminal,--CHAR(1000 BYTE)
                json_ext.get_number(mOrder,'seq'),--seq,--NUMBER
                json_ext.get_number(mOrder,'company'),--company,--NUMBER
                json_ext.get_number(mOrder,'bussinesUnit'),--bussines_unit,--NUMBER
                json_ext.get_number(mOrder,'customer'),--customer,--NUMBER
                json_ext.get_string(mOrder,'location'),--loc,--VARCHAR2(40 BYTE)
                json_ext.get_string(mOrder,'descCide'),--desc_code,--VARCHAR2(30 BYTE)
                json_ext.get_number(mOrder,'active'),--active,--NUMBER
                json_ext.get_number(mOrder,'scanedCount'),--scaned_count,--NUMBER
                json_ext.get_number(mOrder,'ended'),--ended,--NUMBER
                json_ext.get_number(mOrder,'transactionId'),--transaction_id,--NUMBER
                json_ext.get_string(mOrder,'signator'),--signator,--VARCHAR2(150 BYTE)
                json_ext.get_string(mOrder,'sender'),--sender,--VARCHAR2(100 BYTE)
                json_ext.get_string(mOrder,'reciver'),--reciver,--VARCHAR2(100 BYTE)
                json_ext.get_number(mOrder,'route'),--route,--NUMBER
                to_date(json_ext.get_string(mOrder,'scanedDate'),'yyyy-mm-dd hh24:mi:ss'),--scaned,--DATE
                json_ext.get_string(mOrder,'program'),--oprogram,--VARCHAR2(30 BYTE)
                json_ext.get_string(mOrder,'note1'),--note1,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'cart'),--cart,--VARCHAR2(50 BYTE)
                json_ext.get_number(mOrder,'weight'),--weight,--NUMBER
                json_ext.get_number(mOrder,'height'),--height,--NUMBER
                json_ext.get_number(mOrder,'width'),--width,--NUMBER
                json_ext.get_number(mOrder,'odepth'),--odepth,--NUMBER
                json_ext.get_number(mOrder,'proId'),--pro_id,--NUMBER
                json_ext.get_string(mOrder,'stitle'),--stitle,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'saddress'),--saddress,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'spostofficenumber'),--spostofficenumber,--VARCHAR2(150 BYTE)
                json_ext.get_string(mOrder,'scity'),--scity,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'rtitle'),--rtitle,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'raddress'),--raddress,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'rpostofficenumber'),--rpostofficenumber,--VARCHAR2(150 BYTE)
                json_ext.get_string(mOrder,'rcity'),--rcity,--VARCHAR2(200 BYTE)
                json_ext.get_string(mOrder,'cmr'),--cmr,--VARCHAR2(150 BYTE)
                json_ext.get_string(mOrder,'note2'),--note2,--VARCHAR2(2000 BYTE)
                json_ext.get_string(mOrder,'note3'),--note3,--VARCHAR2(2000 BYTE)
                json_ext.get_number(mOrder,'temperature1'),--temperature_1,--NUMBER
                json_ext.get_number(mOrder,'temperature2'),--temperature_2,--NUMBER
                json_ext.get_number(mOrder,'nrPalettes'),--nr_palettes,--NUMBER
                json_ext.get_string(mOrder,'note4'),--note4,--VARCHAR2(1000 BYTE)
                json_ext.get_string(mOrder,'note5'),--note5,--VARCHAR2(1000 BYTE)
                json_ext.get_string(mOrder,'note6'),--note6,--VARCHAR2(1000 BYTE)
                '',--s_telephone,--VARCHAR2(50 BYTE)
                '',--r_telephone,--VARCHAR2(50 BYTE)
                json_ext.get_number(mOrder,'loadingLat'),--loading_lat,--NUMBER
                json_ext.get_number(mOrder,'loadingLng'),--loading_lng,--NUMBER
                json_ext.get_number(mOrder,'unloadingLat'),--unloading_lat,--NUMBER
                json_ext.get_number(mOrder,'unloadingLng'),--unloading_lng,--NUMBER
                json_ext.get_string(mOrder,'trailer'),--trailer,--VARCHAR2(1000 BYTE)
                json_ext.get_number(mOrder,'trailerId'),--trailer_id,--NUMBER
                json_ext.get_number(mOrder,'rabat1'),--rabat1,--NUMBER
                json_ext.get_number(mOrder,'rabat2')--rabat2--NUMBER
            );
            end if;
            mShipments := json_ext.get_json_list(mOrder,'shipments');

            FOR j IN 1..mShipments.count
                LOOP

                    mShipment := json(mShipments.get(j));
                     select count(*) into mCountShip from shipments where id= json_ext.get_number(mShipment,'id');--NUMBER--id,--NUMBER
                     if(mCountShip>0) then
                        update shipments set
                            barcode=json_ext.get_string(mShipment,'barcode'),--barcode,
                            order_id=json_ext.get_number(mShipment,'orderId'),--order_id,
                            cnt=json_ext.get_number(mShipment,'count'),--cnt
                            scaned_count=json_ext.get_number(mShipment,'scanedCount'),--scaned_count,
                            scaned=json_ext.get_number(mShipment,'scaned'),--scaned,
                            scaned_date=to_date(json_ext.get_string(mShipment,'scanedDate'),'yyyy-mm-dd hh24:mi:ss'),--scaned_date,
                            werehouse=json_ext.get_string(mShipment,'werehouse'),--werehouse,
                            loc=json_ext.get_string(mShipment,'location'),--loc,
                            soption=json_ext.get_number(mShipment,'option'),--soption,
                            completed=json_ext.get_number(mShipment,'completed'),--completed,
                            seq=json_ext.get_number(mShipment,'sequence'),--seq,
                            title=json_ext.get_string(mShipment,'title'),--title,
                            client_id=json_ext.get_number(mShipment,'clientId'),--client_id,(38,0)
                            palette=json_ext.get_string(mShipment,'palette'),--palette,
                            stype=json_ext.get_string(mShipment,'type'),--stype,
                            product=json_ext.get_number(mShipment,'product'),--product,
                            terminal=json_ext.get_string(mShipment,'terminal'),--terminal,
                            damage_id=json_ext.get_number(mShipment,'damageId'),--damage_id,
                            damage_desc=json_ext.get_string(mShipment,'damageDesc'),--damage_desc,
                            loading_x=json_ext.get_number(mShipment,'loadingX'),--loading_x,
                            loading_y=json_ext.get_number(mShipment,'loadingY'),--loading_y,
                            unloading_x=json_ext.get_number(mShipment,'unloadingX'),--unloading_x,
                            unloading_y=json_ext.get_number(mShipment,'unloadingY'),--unloading_y,
                            active=json_ext.get_number(mShipment,'active'),--active,
                            quantity_o=json_ext.get_number(mShipment,'quantityO'),--quantity_o,
                            lot=json_ext.get_string(mShipment,'lot'),--lot,
                            note=json_ext.get_string(mShipment,'note'),--note,
                            note1=json_ext.get_string(mShipment,'note1'),--note1,
                            note2=json_ext.get_string(mShipment,'note2'),--note2,
                            note3=json_ext.get_string(mShipment,'note3'),--note3,
                            nr_package=json_ext.get_number(mShipment,'nrPackage'),--nr_package,
                            nr_palets=json_ext.get_number(mShipment,'nrPalets'),--nr_palets,
                            transaction_id=json_ext.get_number(mShipment,'transactionId'),--transaction_
                            quantity_r=json_ext.get_number(mShipment,'quantityR'),--quantity_r,
                            cart=json_ext.get_number(mShipment,'cart'),--cart,
                            route=json_ext.get_number(mShipment,'route'),--route,
                            price=json_ext.get_number(mShipment,'price'),--price,
                            weight=json_ext.get_number(mShipment,'weight'),--weight,
                            height=json_ext.get_number(mShipment,'height'),--height,
                            width=json_ext.get_number(mShipment,'width'),--width,
                            sdepth=json_ext.get_number(mShipment,'depth'),--sdepth,
                            pro_id=json_ext.get_number(mShipment,'proId'),--pro_id,
                            doc_id=json_ext.get_number(mShipment,'docId'),--doc_id,
                            rabat1=json_ext.get_number(mShipment,'rabat1'),--rabat1,
                            rabat2=json_ext.get_number(mShipment,'rabat2')--rabat2,
                        where
                            id=json_ext.get_number(mShipment,'id');



                     else
                        insert into shipments(
                            id,--NUMBER
                            barcode,--VARCHAR2(25 BYTE)
                            order_id,--NUMBER
                            cnt,--NUMBER(22,5)
                            scaned_count,--NUMBER
                            scaned,--NUMBER
                            scaned_date,--DATE
                            werehouse,--CHAR(150 BYTE)
                            loc,--CHAR(60 BYTE)
                            soption,--NUMBER
                            completed,--NUMBER
                            seq,--NUMBER
                            title,--VARCHAR2(150 BYTE)
                            client_id,--NUMBER(38,0)
                            palette,--VARCHAR2(25 BYTE)
                            stype,--CHAR(10 BYTE)
                            product,--NUMBER
                            terminal,--CHAR(10 BYTE)
                            damage_id,--NUMBER
                            damage_desc,--VARCHAR2(100 BYTE)
                            loading_x,--NUMBER(22,10)
                            loading_y,--NUMBER(22,10)
                            unloading_x,--NUMBER(22,10)
                            unloading_y,--NUMBER(22,10)
                            active,--NUMBER
                            quantity_o,--NUMBER
                            lot,--VARCHAR2(18 BYTE)
                            note,--VARCHAR2(500 BYTE)
                            note1,--VARCHAR2(500 BYTE)
                            note2,--VARCHAR2(500 BYTE)
                            note3,--VARCHAR2(500 BYTE)
                            nr_package,--NUMBER
                            nr_palets,--NUMBER
                            transaction_id,--NUMBER
                            quantity_r,--NUMBER
                            cart,--NUMBER
                            route,--NUMBER
                            price,--NUMBER
                            weight,--NUMBER
                            height,--NUMBER
                            width,--NUMBER
                            sdepth,--NUMBER
                            pro_id,--NUMBER
                            doc_id,--NUMBER
                            rabat1,--NUMBER
                            rabat2--NUMBER
                        )values(
                            json_ext.get_number(mShipment,'id'),--NUMBER--id,--NUMBER
                            json_ext.get_string(mShipment,'barcode'),--barcode,--VARCHAR2(25 BYTE)
                            json_ext.get_number(mShipment,'orderId'),--order_id,--NUMBER
                            json_ext.get_number(mShipment,'count'),--cnt,--NUMBER(22,5)
                            json_ext.get_number(mShipment,'scanedCount'),--scaned_count,--NUMBER
                            json_ext.get_number(mShipment,'scaned'),--scaned,--NUMBER
                            to_date(json_ext.get_string(mShipment,'scanedDate'),'yyyy-mm-dd hh24:mi:ss'),--scaned_date,--DATE
                            json_ext.get_string(mShipment,'werehouse'),--werehouse,--CHAR(150 BYTE)
                            json_ext.get_string(mShipment,'location'),--loc,--CHAR(60 BYTE)
                            json_ext.get_number(mShipment,'option'),--soption,--NUMBER
                            json_ext.get_number(mShipment,'completed'),--completed,--NUMBER
                            json_ext.get_number(mShipment,'sequence'),--seq,--NUMBER
                            json_ext.get_string(mShipment,'title'),--title,--VARCHAR2(150 BYTE)
                            json_ext.get_number(mShipment,'clientId'),--client_id,--NUMBER(38,0)
                            json_ext.get_string(mShipment,'palette'),--palette,--VARCHAR2(25 BYTE)
                            json_ext.get_string(mShipment,'type'),--stype,--CHAR(10 BYTE)
                            json_ext.get_number(mShipment,'product'),--product,--NUMBER
                            json_ext.get_string(mShipment,'terminal'),--terminal,--CHAR(10 BYTE)
                            json_ext.get_number(mShipment,'damageId'),--damage_id,--NUMBER
                            json_ext.get_string(mShipment,'damageDesc'),--damage_desc,--VARCHAR2(100 BYTE)
                            json_ext.get_number(mShipment,'loadingX'),--loading_x,--NUMBER(22,10)
                            json_ext.get_number(mShipment,'loadingY'),--loading_y,--NUMBER(22,10)
                            json_ext.get_number(mShipment,'unloadingX'),--unloading_x,--NUMBER(22,10)
                            json_ext.get_number(mShipment,'unloadingY'),--unloading_y,--NUMBER(22,10)
                            json_ext.get_number(mShipment,'active'),--active,--NUMBER
                            json_ext.get_number(mShipment,'quantityO'),--quantity_o,--NUMBER
                            json_ext.get_string(mShipment,'lot'),--lot,--VARCHAR2(18 BYTE)
                            json_ext.get_string(mShipment,'note'),--note,--VARCHAR2(500 BYTE)
                            json_ext.get_string(mShipment,'note1'),--note1,--VARCHAR2(500 BYTE)
                            json_ext.get_string(mShipment,'note2'),--note2,--VARCHAR2(500 BYTE)
                            json_ext.get_string(mShipment,'note3'),--note3,--VARCHAR2(500 BYTE)
                            json_ext.get_number(mShipment,'nrPackage'),--nr_package,--NUMBER
                            json_ext.get_number(mShipment,'nrPalets'),--nr_palets,--NUMBER
                            json_ext.get_number(mShipment,'transactionId'),--transaction_id,--NUMBER
                            json_ext.get_number(mShipment,'quantityR'),--quantity_r,--NUMBER
                            json_ext.get_number(mShipment,'cart'),--cart,--NUMBER
                            json_ext.get_number(mShipment,'route'),--route,--NUMBER
                            json_ext.get_number(mShipment,'price'),--price,--NUMBER
                            json_ext.get_number(mShipment,'weight'),--weight,--NUMBER
                            json_ext.get_number(mShipment,'height'),--height,--NUMBER
                            json_ext.get_number(mShipment,'width'),--width,--NUMBER
                            json_ext.get_number(mShipment,'depth'),--sdepth,--NUMBER
                            json_ext.get_number(mShipment,'proId'),--pro_id,--NUMBER
                            json_ext.get_number(mShipment,'docId'),--doc_id,--NUMBER
                            json_ext.get_number(mShipment,'rabat1'),--rabat1,--NUMBER
                            json_ext.get_number(mShipment,'rabat2')--rabat2,--NUMBER
                        );
                    end if;
                end loop;

            mAttachments := json_ext.get_json_list(mOrder,'attachments');
            FOR j IN 1..mAttachments.count
                LOOP
                    null;
                     mAttachment := json(mAttachments.get(j));
                     begin
                        att_cnt :=0;
                        select count(file_name) into att_cnt from attachments where file_name=json_ext.get_string(mAttachment,'path');
                     exception when others then
                        null; 
                        att_cnt :=0;
                     end;
                     
                     if(att_cnt<=0) then
                         insert into attachments(
                              order_id,
                              atype,
                              attachment_date,
                              doc_id,
                              status,
                              order_type,
                              lot,
                              linkid,
                              sended,
                              worker_id,
                              image_comment,
                              email,
                              file_name,
                              shipment_id
                        )values(
                              json_ext.get_number(mAttachment,'orderId'),
                              json_ext.get_number(mAttachment,'type'),
                              to_Date(json_ext.get_string(mAttachment,'date'),'yyyy-mm-dd hh24:mi:ss'),
                              0,
                              0,
                               json_ext.get_number(mAttachment,'orderType'),
                              '',
                              json_ext.get_string(mAttachment,'linkId'),
                              0,
                              json_ext.get_number(mAttachment,'userId'),
                              json_ext.get_string(mAttachment,'comment'),
                              '',
                              json_ext.get_string(mAttachment,'path'),
                              json_ext.get_number(mAttachment,'shipmentId')
                        );
                    end if;
                end loop;
                begin
                    mOrderLogs := json_ext.get_json_list(mOrder,'orderLogs');
                    FOR j IN 1..mOrderLogs.count
                        LOOP 
                            mOrderLog := json(mOrderLogs.get(j));
                            
                            MERGE INTO orderlog o USING (SELECT 1 FROM DUAL) m 
                 ON ( o.id=  json_ext.get_number(mOrderLog,'id') and o.order_id=json_ext.get_number(mOrderLog,'orderId')) 
                WHEN NOT MATCHED THEN  
                            insert (
                                ID, 
                                ORDER_ID, 
                                TITLE, 
                                L_TYPE, 
                                LOG_DATE, 
                                USER_ID, 
                                L_COMMENT, 
                                STATUS, 
                                PRO_ID, 
                                COMPANY_ID,
                                FVO,
                                order_status
                            ) values(
                                json_ext.get_number(mOrderLog,'id'),
                                json_ext.get_number(mOrderLog,'orderId'),
                                json_ext.get_string(mOrderLog,'title'),
                                json_ext.get_number(mOrderLog,'type'),
                                to_date(json_ext.get_string(mOrderLog,'logDate'),'yyyy-mm-dd hh24:mi:ss'),
                                json_ext.get_number(mOrderLog,'userId'),
                                json_ext.get_string(mOrderLog,'comment'),
                                json_ext.get_number(mOrderLog,'status'),
                                json_ext.get_number(mOrderLog,'proId'),
                                json_ext.get_number(mOrderLog,'companyId') ,
                                json_ext.get_number(mOrderLog,'fvo') ,
                                json_ext.get_number(mOrderLog,'orderStatus') 
                            );
                        end loop;
                exception when others then
                    null;
                end;
            if(json_ext.get_number(mOrder,'type')=55) then
                update orders set terminal='-1' where order_id=json_ext.get_number(mOrder,'orderId');
            end if;
        end loop;
END;
/

SHOW ERRORS;


CREATE OR REPLACE PROCEDURE MORDERS_GET_DOCUMENT(token in VARCHAR2, documentId in VARCHAR2, path in varchar2, docType in number) is
    mContent varchar2(32000);
    pOut clob;
    l_response  json;
    mAttachment json;
    zacetek number;
    konec number;
    image clob;
    len number := 32000;
    str_length2 number;
    str_length number;
    sql_error varchar2(32000);
    l_vpisano number :=0;
BEGIN
begin
-- DBMS_OUTPUT.PUT_line('DOCUEMMTN ID -'  ||documentId || ' start zahteve' || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));
     url_connect('http://192.168.0.10/mobilno/Mobile/webresources/mOrders/API/getPhoto?DocumentId='||documentId,mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
--   DBMS_OUTPUT.PUT_line(' konec yahteve' || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));

    --if(dbms_lob.instr(pout,'"status":0')>0) then
      --  INSERT INTO ferbit.SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,ID_RECORD,sql_stavek)
     --   VALUES (0, 'mor_get_doc',Sql_error,0,'url_connect',0,'slika prevzeta['||documentId||']');  
    
     --  dbms_output.put_line('Original clob size: ' || dbms_lob.getlength(pout));
      
        dbms_lob.createtemporary(image, false);
        zacetek := dbms_lob.instr(pout,'content');
      --  INSERT INTO SQL_ERROR(OPIS_NAPAKE)VALUES('slika zac '||zacetek); 
        if(zacetek>0) then
            zacetek:=zacetek+10;
            konec := dbms_lob.instr(pout,'"',zacetek);
            str_length:= konec-zacetek;
            
           -- INSERT INTO ferbit.SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,ID_RECORD,sql_stavek)
          --  VALUES (0, 'mor_get_doc',Sql_error,0,'url_connect',0,'velikost clob-a['||documentId||']'||str_length);  
           while str_length > 0 loop
             str_length2 := least(str_length, 32768);
             str_length := str_length - str_length2;
             dbms_lob.copy(image, pout, str_length2, dbms_lob.getlength(image) + 1, zacetek);
             zacetek := zacetek + str_length2;
           end loop;
         
          --  INSERT INTO ferbit.SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,ID_RECORD,sql_stavek)
          --  VALUES (0, 'mor_get_doc',Sql_error,0,'url_connect',0,'za�etek vpisovanja['||documentId||']');  
            if(docType=10 or docType=55) then 
                begin  
                  as_pdf3.init;
                  as_pdf3.set_page_orientation('portrait');
                  as_pdf3.put_image(  base64DecodeClobAsBlob_plsql(image ),5,5,585,832,'start','t' );
                  as_pdf3.save_pdf('DOKUMENTI',path,true); 
                  l_vpisano:=1;
                exception when others then
                    sql_error := SUBSTR(SQLERRM, 1, 250);
                    ERROR_LOG(1,'download doc',Sql_error,0,'mord_get_doc',0); 
                    l_vpisano:=0;
                end ;
                if(l_vpisano=0) then
                    write_to_file('DOKUMENTI', path,image);
                end if;
            else 
            begin
                as_pdf3.init;
                as_pdf3.set_page_orientation('portrait');
                as_pdf3.put_image(  base64DecodeClobAsBlob_plsql(image ),5,5,585,832,'start','t' );
                as_pdf3.save_pdf('SKLADISCE',path,true); 
                
                 exception when others then
                    sql_error := SUBSTR(SQLERRM, 1, 250);
                    ERROR_LOG(1,'download doc',Sql_error,0,'mord_get_doc',0); 
                    l_vpisano:=0;
                end ;
                if(l_vpisano=0) then
                    write_to_file('SKLADISCE', path,image);
                end if;
            end if;
            update attachments set status=1 where linkid=documentid;
            INSERT INTO ferbit.SQL_ERROR (NAPAKA, OPIS_NAPAKE,OPIS_NAPAKE_SQL,TRID,PROCEDURA,ID_RECORD,sql_stavek)
            VALUES (0, 'mor_get_doc',Sql_error,0,'url_connect',0,'koenc vpisovanja['||documentId||']');  
        end if;
  --  end if;
exception when others then
    null;
    sql_error := SUBSTR(SQLERRM, 1, 250);
    ERROR_LOG(1,'download doc',Sql_error,0,'mord_get_doc',0); 
end;
end;
/

SHOW ERRORS;


CREATE OR REPLACE PROCEDURE MORDERS_INSERT_ORDERS(orderId IN number)  IS
    pOut clob;
    l_response  json;
    token varchar2(1000);
    mOrders json_list;
    mOrder json;

    mShipments json_list;
    mShipment json;
    ry_shipment  SHIPMENTS%ROWTYPE;
    shipmentsCur        sys_refcursor;
    queryShipments varchar2(30000):='';

    syncObject json;
    CURSOR C_ORDERS IS SELECT * FROM orders WHERE active in (1);-- ;and order_id=orderId;
    mContent clob;

    inserted json_list;
BEGIN

    select trim(tekst) into token from ferbit.nastavi where kljuc='morders_token';

    mOrders := json_list();
    for corder in c_orders
    loop
        mOrder := json();
        morder.put('orderId',corder.order_id);
        mOrder.put('workerId',nvl(corder.worker_id,0));
        mOrder.put('orderDate',to_char(nvl(corder.order_date,sysdate),'yyyy-mm-dd hh24:mi:ss'));
        mOrder.put('deliveryDate',to_char(nvl(corder.delivery_date,sysdate),'yyyy-mm-dd hh24:mi:ss'));
        mOrder.put('completeDate',to_char(nvl(corder.complete_date,sysdate),'yyyy-mm-dd hh24:mi:ss'));
        mOrder.put('type',nvl(corder.otype,0));
        mOrder.put('typeD',corder.type_d);
        mOrder.put('count',nvl(corder.cnt,0));
        mOrder.put('vehicleId',nvl(corder.vehicle_id,0));
        mOrder.put('vehicle',corder.vehicle);
        mOrder.put('terminal',corder.terminal);
        mOrder.put('seq',nvl(corder.seq,0));
        mOrder.put('company',nvl(corder.company,0));
        mOrder.put('bussinesUnit',nvl(corder.bussines_unit,0));
        mOrder.put('customer',corder.customer);
        mOrder.put('location',corder.loc);
        mOrder.put('descCode',corder.desc_code);
        mOrder.put('active',nvl(corder.active,0));
        mOrder.put('scanedCount',nvl(corder.scaned_count,0));
        mOrder.put('ended',nvl(corder.ended,0));
        mOrder.put('transactionId',nvl(corder.transaction_id,0));
        mOrder.put('signator',corder.signator);
        mOrder.put('sender',corder.sender);
        mOrder.put('reciver',corder.reciver);
        mOrder.put('route',nvl(corder.route,0));
        mOrder.put('scaned',corder.scaned);--datum
        mOrder.put('program',corder.oprogram);
        mOrder.put('note1',corder.note1);
        mOrder.put('cart',corder.cart);
        mOrder.put('weight',nvl(corder.weight,0));
        mOrder.put('height',nvl(corder.height,0));
        mOrder.put('width',nvl(corder.width,0));
        mOrder.put('depth',nvl(corder.odepth,0));
        mOrder.put('proId',nvl(corder.pro_id,0));
        mOrder.put('stitle',corder.stitle);
        mOrder.put('saddress',corder.saddress);
        mOrder.put('spostofficenumber',corder.spostofficenumber);
        mOrder.put('scity',corder.scity);
        mOrder.put('rtitle',corder.rtitle);
        mOrder.put('raddress',corder.raddress);
        mOrder.put('rpostofficenumber',corder.rpostofficenumber);
        mOrder.put('rcity',corder.rcity);
        mOrder.put('cmr',corder.cmr);
        mOrder.put('note2',corder.note2);
        mOrder.put('note3',corder.note3);
        mOrder.put('temperature1',nvl(corder.temperature_1,0));
        mOrder.put('temperature2',nvl(corder.temperature_2,0));
        mOrder.put('nrPalettes',nvl(corder.nr_palettes,0));
        mOrder.put('note4',corder.note4);
        mOrder.put('note5',corder.note5);
        mOrder.put('note6',corder.note6);
        mOrder.put('note6',corder.note6);
        mOrder.put('stelephone',corder.s_telephone);
        mOrder.put('rtelephone',corder.r_telephone);
        mOrder.put('loadingLat',nvl(corder.loading_lat,0));
        mOrder.put('loadingLng',nvl(corder.loading_lng,0));
        mOrder.put('unloadingLat',nvl(corder.unloading_lat,0));
        mOrder.put('unloadingLng',nvl(corder.unloading_lng,0));
        mOrder.put('trailer',corder.trailer);
        mOrder.put('trailer_id',nvl(corder.trailer_id,0));
        mOrder.put('rabat1',nvl(corder.rabat1,0));
        mOrder.put('rabat2',nvl(corder.rabat2,0));
        mOrder.put('cash',nvl(corder.cash,0));
        mOrder.put('vat22',nvl(corder.vat22,0));
        mOrder.put('vat95',nvl(corder.vat95,0));
        mOrder.put('price',nvl(corder.price,0));
        mOrder.put('osnova1',nvl(corder.osnova1,0));
        mOrder.put('osnova2',nvl(corder.osnova2,0));
        mOrder.put('invoiceNumber',nvl(corder.invoice_Number,0));
        morder.put('orderLogs',json_list());
        morder.put('orderLogs',json_list());
        mShipments:=json_list();

        queryShipments := 'select * from shipments where order_id='||json_ext.get_number(mOrder,'orderId');
        OPEN shipmentsCur FOR queryShipments;
                 LOOP
                    FETCH shipmentsCur INTO ry_shipment;
                    EXIT WHEN shipmentsCur%NOTFOUND;
                    DBMS_OUTPUT.PUT_LINE('adding ' || ry_shipment.id);
                        mShipment := json();
                        mShipment.put('id',ry_shipment.id);
                        mShipment.put('barcode',ry_shipment.barcode);
                        mShipment.put('orderId',ry_shipment.order_id);
                        mShipment.put('cnt',nvl(ry_shipment.cnt,0));
                        mShipment.put('scanedCount',nvl(ry_shipment.scaned_count,0));
                        mShipment.put('scaned',nvl(ry_shipment.scaned,0));
                        mShipment.put('scanedDate',to_char(ry_shipment.scaned_date,'yyyy-mm-dd hh24:mi:ss'));
                        mShipment.put('werehouse',ry_shipment.werehouse);
                        mShipment.put('loc',ry_shipment.loc);
                        mShipment.put('soption',nvl(ry_shipment.soption,0));
                        mShipment.put('completed',nvl(ry_shipment.completed,0));
                        mShipment.put('seq',nvl(ry_shipment.seq,0));
                        mShipment.put('title',ry_shipment.title);
                        mShipment.put('clientId',nvl(ry_shipment.client_id,0));
                        mShipment.put('palette',ry_shipment.palette);
                        mShipment.put('stype',ry_shipment.stype);
                        mShipment.put('product',nvl(ry_shipment.product,0));
                        mShipment.put('terminal',ry_shipment.terminal);
                        mShipment.put('damageId',nvl(ry_shipment.damage_id,0));
                        mShipment.put('damageDesc',ry_shipment.damage_desc);
                        mShipment.put('loadingX',nvl(ry_shipment.loading_x,0));
                        mShipment.put('loadingY',nvl(ry_shipment.loading_y,0));
                        mShipment.put('unloadingX',nvl(ry_shipment.unloading_x,0));
                        mShipment.put('unloadingY',nvl(ry_shipment.unloading_y,0));
                        mShipment.put('active',nvl(ry_shipment.active,0));
                        mShipment.put('quantityO',nvl(ry_shipment.quantity_o,0));
                        mShipment.put('lot',ry_shipment.lot);
                        mShipment.put('note',ry_shipment.note);
                        mShipment.put('note1',ry_shipment.note1);
                        mShipment.put('note2',ry_shipment.note2);
                        mShipment.put('note3',ry_shipment.note3);
                        mShipment.put('nrPackage',nvl(ry_shipment.nr_package,0));
                        mShipment.put('nrPalets',nvl(ry_shipment.nr_palets,0));
                        mShipment.put('transactionId',nvl(ry_shipment.transaction_id,0));
                        mShipment.put('quantityR',nvl(ry_shipment.quantity_r,0));
                        mShipment.put('cart',nvl(ry_shipment.cart,0));
                        mShipment.put('route',nvl(ry_shipment.route,0));
                        mShipment.put('price',nvl(ry_shipment.price,0));
                        mShipment.put('weight',nvl(ry_shipment.weight,0));
                        mShipment.put('height',nvl(ry_shipment.height,0));
                        mShipment.put('width',nvl(ry_shipment.width,0));
                        mShipment.put('sdepth',nvl(ry_shipment.sdepth,0));
                        mShipment.put('proId',nvl(ry_shipment.pro_id,0));
                        mShipment.put('docId',nvl(ry_shipment.doc_id,0));
                        mShipment.put('rabat1',nvl(ry_shipment.rabat1,0));
                        mShipment.put('rabat2',nvl(ry_shipment.rabat2,0));
                        mShipments.append(mShipment.to_json_value);
                END LOOP;
        close shipmentsCur;

        mOrder.put('shipments', mShipments);
        mOrders.append(mOrder.to_json_value);
    end loop;

    syncObject := json();
    syncObject.put('orders',mOrders);
 
    DBMS_LOB.CREATETEMPORARY(mContent,true);
    syncObject.to_clob(mContent,true);
--
    -- url_connect('http://localhost/Mobile/webresources/mOrders/API/insertOrders',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
     url_connect('http://192.168.0.10/mobilno/Mobile/webresources/mOrders/API/insertOrders',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
    dbms_output.put_line(pout);
    l_response := json(pout);
    inserted := json_ext.get_json_list(l_response,'inserted');

    FOR i IN 1..inserted.count
        LOOP
            --dbms_output.put_line('-- '||json_ext.get_number(json(inserted.get(i)),'newOrderId'));
           update orders set active=2 where order_id=json_ext.get_number(json(inserted.get(i)),'newOrderId');
           commit;
        end loop;
        commit;
end;
/

SHOW ERRORS;


CREATE OR REPLACE PROCEDURE MORDERS_INSERT_USERS(mUserId in number) is
    pOut clob;
    l_response  json;
    token varchar2(1000):='';
    TYPE VRNI_rt IS RECORD(document_id varchar2(10000));
    l_VRNI   VRNI_rt;
    b_kurzor        sys_refcursor;
    mUser json;
    syncObject json;
    tmp json;
    CURSOR C_USER IS SELECT * FROM USERS where id=mUserId;
    mContent clob;
BEGIN

        select trim(tekst) into token from ferbit.nastavi where kljuc='morders_token';
--dbms_crypto.hash(utl_raw.cast_to_raw(''), dbms_crypto.HASH_MD5);




        mUser:=json();
        FOR luser IN C_USER LOOP
            muser.put('id',luser.id);
            muser.put('name',luser.name);
            muser.put('lastName',luser.last_name);
            muser.put('username',luser.username);
            muser.put('password',DBMS_OBFUSCATION_TOOLKIT.md5(input => UTL_I18N.STRING_TO_RAW (luser.password, 'AL32UTF8')));
            muser.put('pin',luser.pin);
            muser.put('longitude',nvl(luser.longitude,0));
            muser.put('latitude',nvl(luser.latitude,0));
            muser.put('device',luser.device);
            muser.put('lastTime',luser.last_time);
            muser.put('admin',nvl(luser.admin,0));
            muser.put('status',nvl(luser.status,0));
            muser.put('company',0);
        end loop;
         syncObject := json();
         syncObject.put('user',mUser);

         DBMS_LOB.CREATETEMPORARY(mContent,true);
         syncObject.to_clob(mContent,true);
         dbms_output.put_line('----');
         dbms_output.put_line(mContent);
         
         url_connect('http://192.168.0.10/mobilno/Mobile/webresources/mOrders/API/createUser',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
         DBMS_OUTPUT.PUT(pOut);
end;
/

SHOW ERRORS;


CREATE OR REPLACE procedure morders_geocode_client(pId in number) as
    l_vez number; 
    dataReturn json;
    dataCoord json_list; 
    mContent clob:='';
    pout clob;
    token varchar2(1000):='';
    vez number:=0;
    DateFrom date;
    syncObject json;
    dateTo date := sysdate+ (1/1440*3);--minus one minute
    pAddress varchar2(32000);
    addressCount number:=0;
    /*
        v exvez, ext_sifra1 se pi�e original vez ter dav�na �tevilka zato da bomo lahko naprej vedeli �e nalog �e obstaja
    */
begin
--insert into nastavi (tekst,kljuc) values(sysdate,'nkl_PRENOS_DATUM');
    
    select trim(tekst) into token from ferbit.nastavi where kljuc='morders_token';

    select trim(naslov5)||', '||trim(posta)||', '||trim(naslov4)||', '||trim(drzava) into pAddress from ferbit.klienti where sifra in (pId) ;
    if(length(pAddress)>10) then
    syncObject := json();
    syncObject.put('address',pAddress);
 
    DBMS_LOB.CREATETEMPORARY(mContent,true);
    syncObject.to_clob(mContent,true);
    
    url_connect('http://192.168.0.10/mobilno/Mobile/webresources/calculate/places/',mContent,'POST',pOut,''||token,'application/json;charset=utf-8');
   -- insert into clob_test(cdata) values(pout);
  
    begin
    dataReturn := json(pOut); 
    if(json_ext.get_number(dataReturn,'status')=0) then
        dataCoord := json_ext.get_json_list(dataReturn,'data');
        dataReturn := json(dataCoord.get(1));  
         dbms_output.put_line(''||pid);
          dbms_output.put_line(''|| json_ext.get_string(dataReturn,'place_id'));
          insert into klienti_back (  sifra ,  place_id ,  lat ,  lng ) values(pid,json_ext.get_string(dataReturn,'place_id'), json_ext.get_number(dataReturn,'lat'),json_ext.get_number(dataReturn,'lng'));
            update ferbit.klienti set placeid=substr(json_ext.get_string(dataReturn,'place_id'),1,50), ulica_longitude=json_ext.get_number(dataReturn,'lng'), ulica_latitude=json_ext.get_number(dataReturn,'lat'), skype=dataCoord.count where sifra=pId;
          
    else
    null;
       -- update ferbit.klienti set skype='-1' where sifra=pId;
    end if;
    commit;
    /*
        {
            "date": "2021-10-22 10:24:04",
            "data": [
                {
                    "country": "Slovenija",
                    "country_code": "SI",
                    "formatted_address": "Pre�ernova ulica, 2000 Maribor, Slovenija",
                    "address": "Pre�ernova ulica",
                    "post": "Maribor",
                    "lng": 15.6516498,
                    "postal_code": "2000",
                    "lat": 46.5630518,
                    "place_id": "ChIJD1MAUqp3b0cRBqdIgy29AgQ"
                },
                {
                    "country": "Slovenija",
                    "country_code": "SI",
                    "formatted_address": "Pre�ernova ulica, 3000 Celje, Slovenija",
                    "address": "Pre�ernova ulica",
                    "post": "Celje",
                    "lng": 15.2637299,
                    "postal_code": "3000",
                    "lat": 46.2288564,
                    "place_id": "ChIJW-CTVfxwZUcR_k2o8dj0D_k"
                },
                {
                    "country": "Slovenija",
                    "country_code": "SI",
                    "formatted_address": "Pre�ernova cesta, 1000 Ljubljana, Slovenija",
                    "address": "Pre�ernova cesta",
                    "post": "Ljubljana",
                    "lng": 14.4984073,
                    "postal_code": "1000",
                    "lat": 46.0512111,
                    "place_id": "ChIJkSYQvWAtZUcRNXMyG1UeSlc"
                }
            ],
            "provider": "Goole",
            "status_text": "OK",
            "status": 0
        }
    */
    exception when others then 
     DBMS_OUTPUT.PUT_LINE('SQLERRM  '||SQLERRM);
    --   dbms_output.put_line('NAPAKA');
       dbms_output.put_line(mContent);
      null;
    end ;
    commit;
        end if ;

end;
/

SHOW ERRORS;


CREATE OR REPLACE procedure mordesr_run_geocoding(p in number) is
   --cursor stranke  is select sifra from ferbit.klienti where nvl(ulica_latitude,0)=0 or nvl(ulica_longitude,0)=0 and skype<>'-1';
    cursor stranke is select sifra 
 from ferbit.klienti 
 where sifra in (select prejemnik 
                  from ferbit.nakladi 
                  where leto in ('21','20')
                  group by prejemnik 
                  union all 
                  select naklada
                  from ferbit.nakladi 
                  where leto in ('21','20') 
                  group by naklada) and sifra not in (select id from klienti_narejeni) ;
    
    
    i number ;
begin
    i:=0;
   for corder in stranke
    loop
        if(i>200) then
            EXIT;   
        end if; 
        --dbms_output.put_line(corder.sifra);
        morders_geocode_client(corder.sifra);
        insert into klienti_narejeni(id) values (corder.sifra);
        i:=i+1;
    end loop;
    
  --   select count(sifra) into i from ferbit.klienti where sifra in (select prejemnik from ferbit.nakladi where leto in ('21','20') group by prejemnik union all select naklada from ferbit.nakladi where leto in ('21','20') group by naklada) and (nvl(ulica_longitude,0)=0 or nvl(ulica_latitude,0)=0);
 -- select count(sifra) into i from ferbit.klienti where nvl(ulica_latitude,0)=0 or nvl(ulica_longitude,0)=0 and length(trim(naslov5)||', '||trim(posta)||', '||trim(naslov4)||', '||trim(drzava))>10;
 
 
  select count(sifra) into i
 from ferbit.klienti 
 where sifra in (select prejemnik 
                  from ferbit.nakladi 
                  where leto in ('21','20')
                  group by prejemnik 
                  union all 
                  select naklada
                  from ferbit.nakladi 
                  where leto in ('21','20') 
                  group by naklada) and sifra not in (select id from klienti_narejeni) ;  
 
  dbms_output.put_line('se : ' ||i);
end;
/

SHOW ERRORS;


CREATE OR REPLACE procedure morders_send_messages is
     
     sms json;
     smsData json_list;
     mContent  clob;
     pout clob;
     token varchar2(1000);
     syncObject json;
     response json;
     CURSOR cur IS select * from ferbit.izhodnisms where status IN (13,2,22) AND STANJE in (0,1) and trim(gsm) in (select to_char(id) from ferbit.vozila_nova_Verzija where status=1)  and datum_s>to_date('01.04.2022 04:40','dd.mm.yyyy hh24:mi');--,10279,10282,10283,10284,10285,10286,10287);
begin
    select trim(tekst) into token from ferbit.nastavi where kljuc='morders_token';
    smsData := json_list();
    for tmp in cur loop
--    DBMS_OUTPUT.PUT_LINE(nvl(length(trim(tmp.gsm)),0));
        if(TMP.STATUS in (13,22)) then
          insert into clobtest values('status 12');
  --      DBMS_OUTPUT.PUT_LINE('MANJ�E OD 9 FCM');
            sms := json();
            sms.put('id',tmp.stevec);
            sms.put('content',tmp.vsebina);
            sms.put('fromId',999);
            sms.put('fromTitle','PISARNA');
            sms.put('toId',TRIM(NVL(tmp.gsm,' ')));
            sms.put('toTitle',TRIM(NVL(tmp.gsm,' ')));
            sms.put('dateSent',to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));
            sms.put('status',0);
            sms.put('proId',tmp.vezn);
            sms.put('messageType','fcmVehicle');
            smsData.append(sms.to_json_value);
        else
            if(nvl(tmp.posiljatelj,0)>0) then
                sms := json();
                sms.put('id',tmp.stevec);
                sms.put('content',tmp.vsebina);
                sms.put('fromId',999);
                sms.put('fromTitle','sms');
                sms.put('toId',0);
                sms.put('toTitle',tmp.gsm);
                sms.put('dateSent',to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'));
                sms.put('status',0);
                sms.put('proId',tmp.vezn);
                sms.put('messageType','sms');
                smsData.append(sms.to_json_value);
            end if;
        end if;
    end loop;
    syncObject := json();
    syncObject.put('messages',smsData);
    DBMS_LOB.CREATETEMPORARY(mContent,true);
    syncObject.to_clob(mContent,true);
    insert into clobtest values('test');
   insert into clobtest values(mContent);
   commit;
    --dbms_output.put_line(mContent);
    url_connect('http://192.168.0.10/mobilno/Mobile/webresources/mOrders/API/insertMessages/',mContent,'POST',pOut,'Basic  '||token,'application/json;charset=utf-8');
   -- dbms_output.put_line('test '||pout);
   insert into clobtest values(pout);
    response:=json(pout);

    --dbms_output.put_line('status '||json_ext.get_number(response,'status'));
    if(json_ext.get_number(response,'status')=0) then
          FOR i IN 1..json_ext.get_json_list(syncObject,'messages').count loop
            sms := json(json_ext.get_json_list(syncObject,'messages').get(i));
            dbms_output.put_line('update izhodnisms set status=1 where stevec = '||json_ext.get_number(sms,'id'));
            update ferbit.izhodnisms set status=1 where stevec = json_ext.get_number(sms,'id');
          end loop;
    end if;
    commit;
end;
/

SHOW ERRORS;


CREATE OR REPLACE FUNCTION BASE64DECODECLOBASBLOB_PLSQL (i_data_cl CLOB) 
return blob is
  v_out_bl blob;
  clob_size number;
  pos number;
  charBuff varchar2(32767);
  dBuffer RAW(32767);
  v_readSize_nr number;
  v_line_nr number;
begin
  dbms_lob.createTemporary
    (v_out_bl, true, dbms_lob.call);
  v_line_nr:=greatest(65, 
                 instr(i_data_cl,chr(10)),
                 instr(i_data_cl,chr(13)));
  v_readSize_nr:=
      floor(32767/v_line_nr)*v_line_nr;
  clob_size := dbms_lob.getLength(i_data_cl);
  pos := 1;

  WHILE (pos < clob_size) LOOP
    dbms_lob.read
      (i_data_cl, v_readSize_nr, pos, charBuff);
    dBuffer := UTL_ENCODE.base64_decode
      (utl_raw.cast_to_raw(charBuff));
    dbms_lob.writeAppend
     (v_out_bl,utl_raw.length(dBuffer),dBuffer);
    pos := pos + v_readSize_nr;
  end loop;
  return v_out_bl;
end;
/

SHOW ERRORS;


CREATE OR REPLACE TRIGGER TBE_ORDERS
BEFORE INSERT OR UPDATE OR DELETE
ON ORDERS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
L_VEZ                NUMBER   := 0;
l_error              NUMBER   := 0;
l_avez               NUMBER   := 0;
l_procedure          CHAR(50) := $$PLSQL_UNIT;                       --- ali obracun obstaja
l_napak              NUMBER   := 0;
Sql_error            varchar2 (250);                                 --- opis napake
sql_stmt             VARCHAR2 (3900);
p_transakcija        NUMBER   :=0;
l_pro_id             NUMBER   := 0;
l_stevilka           NUMBER   := 0;
l_faza               NUMBER   := 0;
lfvo_Sifra           NUMBER   := 0;
l_vozilo             varchar2(150) :='';
l_voznik            number;
l_vozniko             varchar2(150) :='';
l_kilometri number;
l_sopotnik number := 0;

TYPE VRNI_rt IS RECORD(filename varchar2(10000), shipment_id number);
    l_VRNI   VRNI_rt;

 RetVal number;
    l_docid_pnalog number;  
    l_docid_pnalogp number;
    cursor shipments is select * From mobilno.shipments where order_id=:new.order_id;
    cnt number;
    l_filename varchar2(1000);
    l_glavni_doc_id number;
    l_zapst number;
    q_attachemnts        sys_refcursor;    
    q_stavek    varchar2(32000):='';
    xid_dokument number;
    
    datumVeljavnosti date;
    l_art_sifra number ; 
    l_label varchar2(1000):='';
BEGIN

/*
if((inserting or updating) and (:new.otype=55 or :new.otype=10) and :new.vehicle_id=882) then
    :new.vehicle_id:=50;
end if;

if(inserting  and  :new.otype=10 and :new.vehicle_id=50) then
    :new.vehicle_id:=882;
end if;
*/

    --INSERT INTO LOG (  OPIS) VALUES (Trim(l_procedure) );
 If inserting or updating then
   if(nvl(:new.order_id,0)=0 or :new.order_id=0) then
    select ORD_SEQ.nextval into :new.order_id from dual;
   end if ;
 End if;
 --  INSERT INTO ferbit.LOG (  OPIS) VALUES ( 'Dodajne orders '||:NEW.order_id);       

 

  If updating and nvl(Length(trim(:new.note3)),0) > 0 then
    Begin
   -- update tehtalnilist THT SET THT.OPOMBE1 = SUBSTR(:NEW.NOTE3,1,78) WHERE THT.VEZ = :NEW.PRO_ID;
   null;
    EXCEPTION 
       WHEN OTHERS THEN
        l_error   := SQLCODE;
        l_napak   := 1;
        Sql_error := SUBSTR(SQLERRM, 1, 250);
        ERROR_LOG(l_error,'UPDATE tehtalni list',Sql_error,0,l_procedure,0);
    end;
   end if;
   
   --nakladalni nalog
   if(updating and :new.otype=10 and :new.route<>-2) then  
   
       if(nvl(:new.temperature_2,0)<>0 and nvl(:new.temperature_1,0)<>0) then
                    l_kilometri:=nvl(nvl(:new.temperature_2,0)-nvl(:new.temperature_1,0),0);
                else
                     l_kilometri:= 0;
       end if;
       
          DBMS_OUTPUT.PUT_LINE('cart  = '||:new.cart);
          :new.cart := replace(:new.cart,'.',',');
      update ferbit.nakladi set faza = decode(
                                            :new.route,
                                            1,6,    --SPREJET
                                            23,23,  --V PRIHODU NA NAKLAD
                                            26,26,  --�AKA NA RAZKLAD
                                            2,1,    --NALO�EN
                                            24,24,  --V PRIHODU NA RAZKLAD
                                            27,27,  --�AKA NA RAZKLAD
                                            3,2,    --RAZLO�EN
                                            90,90,  --na poti v skladi��e
                                            91,91,  --na rampi
                                            5       --POSLAN
                                        ),
                                teza = :new.weight,
                                cmr = :new.cmr,
                                NAKMETER = to_number(:new.cart),
                                sirina = :new.width,
                                visina = :new.height,
                                dolzina = :new.odepth,
                                stpalet = :new.nr_palettes,
                                komadi = :new.cnt,
                                KILOMETRI=l_kilometri,
                                skladisce=decode(:new.route,90,:new.loc
                                                           ,91,:new.loc,
                                                0),
                                                datumnak_s =  :new.delivery_date,
                                                pdatumraz_s= :new.complete_Date
                                                
            where vez=:new.pro_id;
            
            update ferbit.fvozniki set zacetnikm=:new.temperature_1, koncnikm=:new.temperature_2 where sifra=:new.bussines_unit;
            
            begin
--            DBMS_OUTPUT.PUT_line('pred update delitve '||:NEW.loc||'  '||:new.bussines_unit||'   '||:new.route);
                if(:new.route=90) then
                         UPDATE ferbit.FVOZNIKI FVO SET  
                         (FVO.FAZA,FVO.ZASKLADISCE,FVO.PREJEMNIK,FVO.PNASLOV1,FVO.PNASLOV2,FVO.PULICA,FVO.PSTEVILKA,FVO.PSTPOSTA,FVO.PPOSTA,FVO.PDRZAVA)
                         = (SELECT 90, to_number(:NEW.loc), KLI.SIFRA,KLI.NASLOV1,KLI.NASLOV2,KLI.NASLOV5,KLI.STEVILKA,KLI.POSTA,KLI.NASLOV4,KLI.SDRZAVA FROM ferbit.KLIENTI KLI WHERE KLI.SIFRA=
                          (SELECT SKL.KLIENT FROM ferbit.SKLADISCA SKL WHERE SKL.SKLADISCE = :NEW.loc)) WHERE FVO.SIFRA=:new.bussines_unit ;
                end if;
            exception when others then
             l_error   := SQLCODE;
             l_napak   := 1;
             Sql_error := SUBSTR(SQLERRM, 1, 250);
             ERROR_LOG(l_error,'u fvo skl',Sql_error,0,l_procedure,0);
            end; 
   end if;
   
   --potni nalog
    if( updating and :new.otype=55 and trim(:old.terminal)='0' and trim(:new.terminal)='-1') then
    DBMS_OUTPUT.PUT_LINE(:new.order_id);
    select ferbit.ponauto.nextval INTO L_VEZ from dual;
        Begin
         
            SELECT ferbit.DODAUTO.NEXTVAL into l_docid_pnalog FROM DUAL;
	     EXCEPTION 
	   WHEN OTHERS THEN
	     l_error   := SQLCODE;
	     l_napak   := 1;
	     Sql_error := SUBSTR(SQLERRM, 1, 250);
	     ERROR_LOG(l_error,'SELECT SEQPMAAUTO',Sql_error,0,l_procedure,0);
	   end;
         DBMS_OUTPUT.PUT_LINE('inserting pnalog '||l_vez);
         
         l_voznik:=:new.worker_id;
         begin
            select voznik into l_voznik  from ferbit.vozilo where sifra=:new.vehicle_id;
         exception when others then
             l_error   := SQLCODE;
             l_napak   := 1;
             Sql_error := SUBSTR(SQLERRM, 1, 250);
             ERROR_LOG(l_error,'SELECT SEQPMAAUTO',Sql_error,0,l_procedure,0);
         end;
         
        insert into ferbit.pnalog(
            VEZ,
            datumn_d_s,
            datumo_d_s,
            datump_d_s,
            ZACETNIKILOMETRI,
            KONCNIKILOMETRI,
            KILOMETRI,
            KATEGORIJA,
            SOPOTNIK,
            ime,
            oseba,
            vozilo,
            oznaka,
            valutap,
            nalog,
            leto,
            TIPTECAJA,
            TECAJ,
            FIRMA,
            registracija,
            vrstanaloga,
            docid
        ) values(
            L_VEZ,
            :new.order_date,
            :new.order_date, 
            :new.delivery_date, 
            :new.temperature_1, 
            :new.temperature_2,
            :new.temperature_2 - :new.temperature_1,
            'Z',
            decode(:new.bussines_unit,-1,0,:new.bussines_unit),
            nvl((select trim(title) from articles where id=:new.worker_id and trim(type)='E'),0),
            l_voznik,
            :new.vehicle_id,
            substr(:new.type_d,1,20),                                         --oznaka
            '978',                                                            --valuta
            to_char(ferbit.stevci('PNALOG' || to_char(sysdate,'YY') || '001')),
            to_number( to_char(sysdate,'YYYY')),                              --leto
            'U',                                                              --tiptecaja
            'S'    ,                                                          --tecaj
            1,
            :new.vehicle,
            'Prevoz blaga',
            l_docid_pnalog                                                     --docid
        );
         ferbit.create_doc_document(55, l_vez, l_docid_pnalog);
        :new.pro_id := l_Vez;
        /*
 
       nalogad             = substr((select trim(title)||' - '||:new.terminal from articles where id=:new.scaned_count and type='TEP') ,0,250),
       PROJECTID         = :new.scaned_count, kje je pustil nalog
       NALOGA            =  TRIM(:NEW.NOTE3)*/
       
        /*DODAJANJE SLIK IZ NALOGA*/
        select count(file_name) into cnt from attachments where order_id=:new.order_id;
        if(cnt>0) then
                q_stavek:='select a.file_name, 0 from attachments a where nvl(a.shipment_id,0)<=0  and a.ordeR_id=' || :new.order_id ;
              --   dbms_output.put_line(q_stavek);
                OPEN q_attachemnts FOR q_stavek ;
                LOOP
                FETCH q_attachemnts INTO l_VRNI;
                EXIT WHEN q_attachemnts%NOTFOUND;
                    begin
                      --  insert into sql_error(sql_stavek) values('file name '||l_filename||'     l_docid_pnalogp :'||l_docid_pnalogp); 
                       -- insert into sql_error(sql_stavek) values(' ferbit.DODAJ_DOKUMENT ( 55, '||:new.order_id||' ,''\\OTL1\Prevoznik\WinPro\Dokumenti_poslovanje\OMAHEN\LETO2016\NAKLADI\'','|| Trim(l_filename)||','||l_docid_pnalogp||', 6, 55,-1);');
                       SELECT ferbit.DODAUTO.NEXTVAL into xid_dokument FROM DUAL;
                        ferbit.DODAJ_DOKUMENT ( 55, :new.order_id ,'\\OTL1\Prevoznik\WinPro\Dokumenti_poslovanje\OMAHEN\LETO2016\NAKLADI\', Trim(l_VRNI.filename),xid_dokument, 6, 55,-1);
                        RetVal := FERBIT.DOCRELPISI (l_docid_pnalog, xid_dokument);

                    exception when others
                        then
                        sql_error := Substr(SQLERRM, 1, 250);  
                        INSERT INTO sql_error(napaka, opis_napake, opis_napake_sql, trid, procedura, sql_stavek)  
                        VALUES      ( 1, 'PO�ILJANJE PLANOV PONOVNO za vse', sql_error, 1, 'DR_SEND_ALL_DATA', '') ; 
                    end;
                end loop;
                close q_attachemnts;
        end if;
        /*DODAJANJE SLIK IZ NALOGA*/
       
       for tmpShp in shipments 
       loop
       --dbms_output.put_line('pplace '||trim(tmpShp.terminal));
       if(trim(tmpShp.terminal)='PLACE' and tmpShp.route=3) then
         begin
            
            dbms_output.put_line('INSERT INTO ferbit.POTNI_NAKLADALNI (NAR_VEZ,PON_VEZ,FVO_VEZ,VEZN) VALUES('||tmpShp.PRO_ID||','||:new.pro_id||','||tmpShp.transaction_id||','||tmpShp.PRO_ID||');');
            
            
                 INSERT INTO ferbit.POTNI_NAKLADALNI (NAR_VEZ,PON_VEZ,FVO_VEZ,VEZN) VALUES(tmpShp.PRO_ID,:new.pro_id,tmpShp.transaction_id,tmpShp.PRO_ID);           
            exception when others then
                l_error   := SQLCODE;
                l_napak   := 1;
                Sql_error := SUBSTR(SQLERRM, 1, 250);
                ERROR_LOG(l_error,'insert pot_nakl',Sql_error,0,l_procedure,0);
            end;
       end if;
       if(trim(tmpShp.terminal)<>'PLACE') THEN
           
       
       
              Begin
                SELECT ferbit.PNAAUTO.NEXTVAL INTO l_zapst FROM DUAL;
                SELECT ferbit.DODAUTO.NEXTVAL into l_docid_pnalogp FROM DUAL;
             EXCEPTION 
           WHEN OTHERS THEN
             l_error   := SQLCODE;
             l_napak   := 1;
             Sql_error := SUBSTR(SQLERRM, 1, 250);
             ERROR_LOG(l_error,'SELECT SEQPMAAUTO',Sql_error,0,l_procedure,0);
           end;
                Begin
                if(tmpShp.damage_id>0) then
                    l_sopotnik := tmpShp.damage_id;
                else
                    l_sopotnik := 0;
                    end if;
               --  decode(:new.bussines_unit,-1,0,:new.bussines_unit),
                    Insert into ferbit.PnalogP
                    (
                        zapst,
                        stnalog,
                        strosek,
                        opisS,
                        krat,
                        znesekS,
                        valuta,
                        oznakavalute,
                        zneseksit,
                        vrstaplacila,
                        datum_s,
                        racun,
                        tipvnosa,
                        kolicina,
                        poln,
                        dobavitelj,
                        sopotnik,
                        KRAJ_TANKANJA,
                        stanje_stevca,
                        SDRZAVA,
                        docid)
                    values(
                        l_zapst,        --zapst
                        L_VEZ,           --stnalog
                        decode(trim(tmpShp.terminal),'FUEL',5,tmpShp.product), --storsek
                        tmpShp.title,   --opisS
                        1,              --krat
                        tmpShp.price,   --zneseks
                        '978',          --valuta
                        'EUR',          --oznakavalute
                        tmpShp.Cnt,     --zneseksit
                        0,              --vrstaplacila
                        tmpShp.SCANED_DATE, --datum_s
                        tmpShp.note1,       --tacun
                        'A',                --tipvnosa
                        tmpShp.cnt,         --kolicna
                        to_number(trim(tmpShp.note3)),   --poln
                        0,--ABS(nvl(tmpShp.werehouse,0)),   --dobavitelj
                        l_sopotnik,               --sopotnik
                        trim(tmpShp.note2),             --kraj_tankanja
                        tmpShp.weight,                  --stanje_Stevca
                        substr(trim(to_char(substr(trim(nvl(tmpShp.loc,'')),1,3),'099')),1,3),--sdrzava
                        l_docid_pnalogp     --docid
                    ); 
                    --update shipments set pro_id=l_zapst, doc_id=l_docid_pnalogp where order_id=:new.order_id and id=tmpShp.id;
                  
                    
                     dbms_output.put_line('order_id= '||:new.order_id||' shipment'||tmpShp.id);
                    select count(file_name) into cnt from attachments where order_id=:new.order_id and shipment_id=tmpShp.id;
                    dbms_output.put_line('count'||cnt);
                    /*DODAJANJE DATOTEK IZ POSTAVK*/
                    if(cnt>0) then 
                        q_stavek:='select a.file_name, A.SHIPMeNT_ID from attachments a where a.ordeR_id=' || :new.order_id || ' and a.SHIPMeNT_ID=' || tmpShp.id;
                         dbms_output.put_line(q_stavek);
                        OPEN q_attachemnts FOR q_stavek ;
                            LOOP
                            FETCH q_attachemnts INTO l_VRNI;
                            EXIT WHEN q_attachemnts%NOTFOUND;
                                begin
                                  --  insert into sql_error(sql_stavek) values('file name '||l_filename||'     l_docid_pnalogp :'||l_docid_pnalogp); 
                                   -- insert into sql_error(sql_stavek) values(' ferbit.DODAJ_DOKUMENT ( 55, '||:new.order_id||' ,''\\OTL1\Prevoznik\WinPro\Dokumenti_poslovanje\OMAHEN\LETO2016\NAKLADI\'','|| Trim(l_filename)||','||l_docid_pnalogp||', 6, 55,-1);');
                                    IF(l_VRNI.shipment_id>0) then
                                        ferbit.DODAJ_DOKUMENT ( 55, :new.order_id ,'\\OTL1\Prevoznik\WinPro\Dokumenti_poslovanje\OMAHEN\LETO2016\NAKLADI\', Trim(l_VRNI.filename),l_docid_pnalogp, 6, 55,-1);
                                        RetVal := FERBIT.DOCRELPISI (l_docid_pnalog, l_docid_pnalogp);
                                    end if;
                                    SELECT ferbit.DODAUTO.NEXTVAL into l_docid_pnalogp FROM DUAL;
                                exception when others
                                    then
                                    sql_error := Substr(SQLERRM, 1, 250);  
                                    INSERT INTO sql_error(napaka, opis_napake, opis_napake_sql, trid, procedura, sql_stavek)  
                                    VALUES      ( 1, 'PO�ILJANJE PLANOV PONOVNO za vse', sql_error, 1, 'DR_SEND_ALL_DATA', '') ; 
                                end;
                            end loop;
                        close q_attachemnts;
                    end if ;
                    /*DODAJANJE DATOTEK IZ POSTAVK*/
                   
                end;
            else
                null;
              --  dbms_output.put_line('rabat '||tmpShp.rabat1);
                   INSERT INTO FERBIT.PNALOGKRAJ (
                        ZAPST
                       ,STNALOGA
                       ,OD_SIFRA
                       ,DO_SIFRA 
                       ,ODKJE
                       ,DATUMOD_S
                       ,DATUMOD_TMP
                       ,URAOD
                       ,STANJESTEVCA
                       ,STPREVOZA
                       ,NAZAJ
                       ,POLNEDNEVNICE
                       ,MAJHNEDNEVNICE  
                       ,nazivstranke
                       ,drzava
                       ) 
                   VALUES
                       (
                       NULL
                       ,l_vez
                       ,0
                       ,0
                       ,tmpShp.title
                       ,tmpShp.scaned_date
                       ,(select trunc(tmpShp.scaned_date)- to_date('28.12.1800','dd.mm.yyyy') from dual)
                       , ((extract(hour from to_timestamp(to_char(tmpShp.scaned_date, 'dd.mm.yyyy hh24:mi:ss'))) *60)+extract(minute from to_timestamp(to_char(tmpShp.scaned_date, 'dd.mm.yyyy hh24:mi:ss'))))*1000
                       ,tmpShp.weight
                       ,l_vez
                       ,:new.route
                       ,tmpShp.transaction_id
                       ,:NEW.order_ID
                       ,tmpShp.loc
                       ,substr(trim(to_char(tmpShp.rabat1)),1,3)
                       );
            END IF;
       end loop;
    end if;
    
    --prejem/odprema
    if (updating and (:new.otype=20 or :new.otype=21) and trim(:old.terminal)='0' and trim(:new.terminal)='-1') then
        begin
              dbms_output.put_line(' updating');
              for tmpShp in shipments 
                loop
                
                --dodaj artikel �e ga je dodal uporabnik
                if(tmpShp.id<0) then 
                    SELECT ferbit.ARTSAUTO.NEXTVAL INTO l_art_sifra FROM DUAL;
                    insert into ferbit.artikliskl(sifra, artikel, vodenje, tezap, homolog) values(l_art_sifra, tmpShp.title, 3,-1,tmpShp.barcode);
                    tmpShp.product := l_Art_sifra;
                    update shipments set product = l_Art_sifra where id=tmpShp.id and order_id=tmpShp.order_id;
                    
                    if (nvl(length(trim(tmpShp.note2)),0)=19 ) then 
                        datumVeljavnosti := to_date(tmpShp.note2,'yyyy-MM-dd hh24:mi:ss');
                    else
                        datumVeljavnosti := null;
                    end if;
                    --insert into promets()
                    select ferbit.prmsauto.nextval into l_art_sifra from dual;
                    insert into ferbit.promets(avtovez, vezd, znak,lokacijavskl,sifra, artikel, sklad   , kolicina,stpak,opisposkodbe,veljado, temp_wh, sarza, barcode, teza, tovorki, datum) values(l_art_sifra, :new.pro_id, '-', tmpShp.loc ,tmpShp.product,tmpShp.title, to_number(nvl(tmpShp.werehouse,0))    ,  tmpShp.cnt,tmpShp.cnt,tmpShp.damage_Desc,datumVeljavnosti, l_label,l_stevilka, tmpShp.barcode, tmpShp.weight,1, tmpShp.scaned_date );
                else
                      --update prometas
                    if (nvl(length(trim(tmpShp.note2)),0)=19 ) then 
                        datumVeljavnosti := to_date(tmpShp.note2,'yyyy-MM-dd hh24:mi:ss');
                    else
                        datumVeljavnosti := null;
                    end if;
                    update ferbit.promets set kolicina=tmpShp.cnt, stpak=tmpShp.cnt, lokacijavskl=tmpShp.loc, veljado=datumVeljavnosti, opisposkodbe=tmpShp.damage_Desc where avtovez=tmpShp.pro_id;
                    update ferbit.artikliskl set atc=substr(trim(tmpShp.note),1,20), homolog=substr(trim(tmpShp.note1),1,13) where sifra=tmpShp.product;
                end if;
                
              
                end loop;
        exception when others then
           sql_error := Substr(SQLERRM, 1, 250);  
           dbms_output.put_line('napaka '||sql_error);
            INSERT INTO sql_error(napaka, opis_napake, opis_napake_sql, trid, procedura, sql_stavek)  
            VALUES      ( 1, 'update promets', sql_error, 1, 'tgr_orders_nov', '') ; 
        end;
    end if;
    
    --premik
    if (updating and ( :new.otype=22) and trim(:old.terminal)='0' and trim(:new.terminal)='-1') then
        --premik palet
        if(:new.order_id<0) then
            select ferbit.dobauto.nextval into l_art_sifra from dual;
            l_stevilka := ferbit.stevci('PRENOS'||to_char(sysdate,'yy')||:new.customer);
            :new.pro_id := l_art_sifra;
            l_label:='Prn:'||to_char(l_stevilka,'fm00000')||'/'||to_char(sysdate,'YY');
            insert into ferbit.dobavskl(vez,podd,DATUMPRED, sklad, stevilka, leto, pozicija, faktura ) values(l_art_sifra,3, :new.order_date, :new.loc, l_stevilka,to_char(sysdate,'YY'),1, l_label );
            
             for tmpShp in shipments 
                loop 
                --dodaj artikel �e ga je dodal uporabnik
                
                 if (nvl(length(trim(tmpShp.note2)),0)=19 ) then 
                        datumVeljavnosti := to_date(tmpShp.note2,'yyyy-MM-dd hh24:mi:ss');
                    else
                        datumVeljavnosti := null;
                    end if;
                
                    select ferbit.prmsauto.nextval into l_art_sifra from dual;
                    insert into ferbit.promets(avtovez, vezd, znak,lokacijavskl,sifra, artikel, sklad   , kolicina,stpak,opisposkodbe,veljado, temp_wh, sarza, barcode, teza, tovorki, datum) values(l_art_sifra, :new.pro_id, '-', tmpShp.loc ,tmpShp.product,tmpShp.title, to_number(nvl(tmpShp.werehouse,0))    ,  tmpShp.cnt,tmpShp.cnt,tmpShp.damage_Desc,datumVeljavnosti, l_label,l_stevilka, tmpShp.barcode, tmpShp.weight,1, tmpShp.scaned_date );
                    select ferbit.prmsauto.nextval into l_art_sifra from dual;
                    insert into ferbit.promets(avtovez, vezd, znak,lokacijavskl,sifra, artikel, sklad,    kolicina,stpak,opisposkodbe,veljado, temp_wh, sarza,barcode, teza, tovorki, datum) values(l_art_sifra, :new.pro_id, '+', to_number(nvl(tmpShp.palette,0)), tmpShp.product, tmpShp.title, to_number(nvl(tmpShp.werehouse,0))       , tmpShp.cnt,tmpShp.cnt, tmpShp.damage_Desc,datumVeljavnosti,l_label,l_stevilka, tmpShp.barcode, tmpShp.weight,1, tmpShp.scaned_date  );
                null;
                end loop;
        else
            null;-- update za enkrat �e ni podprt
            /*
            for tmpShp in shipments 
                loop 
                --dodaj artikel �e ga je dodal uporabnik
                
                 if (nvl(length(trim(tmpShp.note2)),0)=19 ) then 
                        datumVeljavnosti := to_date(tmpShp.note2,'yyyy-MM-dd hh24:mi:ss');
                    else
                        datumVeljavnosti := null;
                    end if;
            end loop;   
            */
        end if; 
    end if;
END ;
/
SHOW ERRORS;


CREATE OR REPLACE TRIGGER TBE_ORDER_LOG
BEFORE INSERT-- OR UPDATE OR DELETE
ON ORDERlog REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
 l_error              NUMBER   := 0; 
l_procedure          CHAR(50) := $$PLSQL_UNIT;                       --- ali obracun obstaja
l_napak              NUMBER   := 0;
Sql_error            varchar2 (250);                                 --- opis napake
sql_stmt             VARCHAR2 (3900);

fvo ferbit.FVOZNIKI%rowtype;
l_tip varchar2(1);
BEGIN
    if :new.l_type=10 then
        begin
            
            SELECT * INTO fvo FROM ferbit.fvozniki WHERE sifra=:new.fvo;
            l_tip := 'L';
            If fvo.zaporedje = -1 then 
                l_tip := 'F';
            end if;
            
            INSERT INTO ferbit.NAKLADILOG (
            AVEZ,TIP, DATUM, FIRMA, 
            FAZA, NKL_VEZ, PON_VEZ, 
            FVO_SIFRA, ZBI_VEZ, DOGODEK, 
            BOOKINGOPIS, FAKTURA, POSTAVKA, 
            VOZNIK, VOZNIKOPIS, PROCENT, 
            ZNESEK, VOZILO, PREVOZNIK, 
            VOZILOOPIS, PRIKOLICA, PRIKOLICAOPIS, 
            STATUS, KILOMETRI, PRAZNIKM, 
            DRZAVA, LATITUDE, LONGITUDE, 
            ZAPOREDJE, NAKLADA, KOLICINA, 
            TEZA, CMR, DATUM_OD, 
            URA, TRAJANJE, SOVOZNIK, 
            FAZAP, BKGKLJUC, VEZPOG, 
            PGPVEZ, POGNACIN, PREJEMNIK, 
            PNASLOV1, SKLADISCE, ZASKLADISCE) 
            VALUES (
                null,
                l_tip,
                :new.log_date,
                fvo.FIRMA,
                :new.order_status,
                fvo.vezn,
                0,
                fvo.SIFRA,
                fvo.VEZZBI,
                Decode(fvo.zaporedje,1,1,2),
               -- Decode(:new.order_status,-1,'Nepokrit',0,'Pripravljen',8,'Fakturiran')||' - '||Decode(fvo.zaporedje,1,'Zapis novega naloga',-1,'Zapis fakture','Dodajanje nove delitev za nalog'),
                :new.title,
                DECODE(fvo.FAZA,8,fvo.FAKTURA,0),  --faktura
                DECODE(fvo.FAZA,8,fvo.POSTAVKA,0), -- POSTAVKA 
                fvo.voznik,      --VOZNIK 
                fvo.voznikopis,  -- VOZNIKOPIS 
                fvo.procent ,    -- PROCENT 
                fvo.znesek,      -- ZNESEK 
                fvo.vozilo,      -- VOZILO 
                fvo.prevoznik,   -- PREVOZNIK 
                fvo.voziloopis,  -- VOZILOOPIS 
                fvo.prikolica,   -- PRIKOLICA 
                fvo.PRIKOLICAOPIS,--PRIKOLICAOPIS    
                fvo.STATUS,      -- STATUS 
                fvo.KILOMETRI,   -- KILOMETRI 
                fvo.PRAZNIKM,    -- PRAZNIKM 
                fvo.drzava,      -- DRZAVA 
                fvo.LATITUDE,    -- LATITUDE 
                fvo.LONGITUDE,   -- LONGITUDE 
                fvo.ZAPOREDJE,   -- ZAPOREDJE 
                fvo.NAKLADA ,    -- NAKLADA 
                fvo.KOLICINA,    -- KOLICINA 
                fvo.TEZA,        -- TEZA 
                fvo.CMR,         -- CMR 
                fvo.DATUM ,      -- DATUM_OD 
                fvo.ura,         -- URA 
                fvo.trajanje,    -- TRAJANJE 
                fvo.sovoznik,    -- SOVOZNIK 
                fvo.fazap ,      -- FAZAP 
                fvo.bkgkljuc,    -- BKGKLJUC 
                fvo.vezpog,      -- VEZPOG 
                fvo.pgpvez,      -- PGPVEZ 
                fvo.pognacin,    -- POGNACIN 
                fvo.prejemnik,   -- PREJEMNIK 
                fvo.pnaslov1,    -- PNASLOV1 
                fvo.skladisce,   -- SKLADISCE 
                fvo.ZASKLADISCE  -- ZASKLADISCE  
            );
            
        exception when others then
            l_error   := SQLCODE;
            l_napak   :=  l_napak+1;
            Sql_error := SUBSTR(SQLERRM, 1, 250);
            ERROR_LOG(l_error,'nak log'||TRIM(:NEW.ID),Sql_error,0,l_procedure,0);  
        end;
    end if;
END ;
/
SHOW ERRORS;


CREATE OR REPLACE TRIGGER TBE_SQL_ERROR  
 BEFORE INSERT
 ON SQL_ERROR   REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE
 v_avtovez NUMBER;
 BEGIN
   :new.datum := sysdate();
   IF NVL(:NEW.ZAP_ST,0)=0    THEN
     SELECT ERRAUTO.NEXTVAL INTO :NEW.ZAP_ST   FROM DUAL;
     END IF;
 END;
/
SHOW ERRORS;


CREATE OR REPLACE TRIGGER MLOG$TORDERS before DELETE OR INSERT OR UPDATE on ORDERS referencing new as new for each row
Declare  v_ChangeType string(1):='';  Begin  IF INSERTING THEN       v_ChangeType := 'I';   ELSIF UPDATING THEN        v_ChangeType := 'U';    ELSE      v_ChangeType := 'D';   END IF;  Begin If inserting then  insert into MLOG$_ORDERS (ORDER_ID, WORKER_ID, ORDER_DATE, DELIVERY_DATE, COMPLETE_DATE, OTYPE, TYPE_D, CNT, VEHICLE_ID, VEHICLE, TERMINAL, SEQ, COMPANY, BUSSINES_UNIT, CUSTOMER, LOC, DESC_CODE, ACTIVE, SCANED_COUNT, ENDED, TRANSACTION_ID, SIGNATOR, SENDER, RECIVER, ROUTE, SCANED, OPROGRAM, NOTE1, CART, WEIGHT, HEIGHT, WIDTH, ODEPTH, PRO_ID, STITLE, SADDRESS, SPOSTOFFICENUMBER, SCITY, RTITLE, RADDRESS, RPOSTOFFICENUMBER, RCITY, CMR, NOTE2, NOTE3, TEMPERATURE_1, TEMPERATURE_2, NR_PALETTES, NOTE4, NOTE5, NOTE6, S_TELEPHONE, R_TELEPHONE, LOADING_LAT, LOADING_LNG, UNLOADING_LAT, UNLOADING_LNG, TRAILER, TRAILER_ID, RABAT1, RABAT2, DATE_MODIFIED, CASH, ZOI, EOR, BUSINESSPREMISE, ELECTRONICDEVICE, VAT22, VAT95, OSNOVA1, OSNOVA2, PRICE, FURS_ORDER, INVOICE_NUMBER,mlog_sysdate,mlog_user,mlog_AKCIJA)   values (:New.ORDER_ID, :New.WORKER_ID, :New.ORDER_DATE, :New.DELIVERY_DATE, :New.COMPLETE_DATE, :New.OTYPE, :New.TYPE_D, :New.CNT, :New.VEHICLE_ID, :New.VEHICLE, :New.TERMINAL, :New.SEQ, :New.COMPANY, :New.BUSSINES_UNIT, :New.CUSTOMER, :New.LOC, :New.DESC_CODE, :New.ACTIVE, :New.SCANED_COUNT, :New.ENDED, :New.TRANSACTION_ID, :New.SIGNATOR, :New.SENDER, :New.RECIVER, :New.ROUTE, :New.SCANED, :New.OPROGRAM, :New.NOTE1, :New.CART, :New.WEIGHT, :New.HEIGHT, :New.WIDTH, :New.ODEPTH, :New.PRO_ID, :New.STITLE, :New.SADDRESS, :New.SPOSTOFFICENUMBER, :New.SCITY, :New.RTITLE, :New.RADDRESS, :New.RPOSTOFFICENUMBER, :New.RCITY, :New.CMR, :New.NOTE2, :New.NOTE3, :New.TEMPERATURE_1, :New.TEMPERATURE_2, :New.NR_PALETTES, :New.NOTE4, :New.NOTE5, :New.NOTE6, :New.S_TELEPHONE, :New.R_TELEPHONE, :New.LOADING_LAT, :New.LOADING_LNG, :New.UNLOADING_LAT, :New.UNLOADING_LNG, :New.TRAILER, :New.TRAILER_ID, :New.RABAT1, :New.RABAT2, :New.DATE_MODIFIED, :New.CASH, :New.ZOI, :New.EOR, :New.BUSINESSPREMISE, :New.ELECTRONICDEVICE, :New.VAT22, :New.VAT95, :New.OSNOVA1, :New.OSNOVA2, :New.PRICE, :New.FURS_ORDER, :New.INVOICE_NUMBER,sysdate,user,v_ChangeType);end If; If deleting or updating then  insert into MLOG$_ORDERS (ORDER_ID, WORKER_ID, ORDER_DATE, DELIVERY_DATE, COMPLETE_DATE, OTYPE, TYPE_D, CNT, VEHICLE_ID, VEHICLE, TERMINAL, SEQ, COMPANY, BUSSINES_UNIT, CUSTOMER, LOC, DESC_CODE, ACTIVE, SCANED_COUNT, ENDED, TRANSACTION_ID, SIGNATOR, SENDER, RECIVER, ROUTE, SCANED, OPROGRAM, NOTE1, CART, WEIGHT, HEIGHT, WIDTH, ODEPTH, PRO_ID, STITLE, SADDRESS, SPOSTOFFICENUMBER, SCITY, RTITLE, RADDRESS, RPOSTOFFICENUMBER, RCITY, CMR, NOTE2, NOTE3, TEMPERATURE_1, TEMPERATURE_2, NR_PALETTES, NOTE4, NOTE5, NOTE6, S_TELEPHONE, R_TELEPHONE, LOADING_LAT, LOADING_LNG, UNLOADING_LAT, UNLOADING_LNG, TRAILER, TRAILER_ID, RABAT1, RABAT2, DATE_MODIFIED, CASH, ZOI, EOR, BUSINESSPREMISE, ELECTRONICDEVICE, VAT22, VAT95, OSNOVA1, OSNOVA2, PRICE, FURS_ORDER, INVOICE_NUMBER,mlog_sysdate,mlog_user,mlog_AKCIJA)   values (:Old.ORDER_ID, :Old.WORKER_ID, :Old.ORDER_DATE, :Old.DELIVERY_DATE, :Old.COMPLETE_DATE, :Old.OTYPE, :Old.TYPE_D, :Old.CNT, :Old.VEHICLE_ID, :Old.VEHICLE, :Old.TERMINAL, :Old.SEQ, :Old.COMPANY, :Old.BUSSINES_UNIT, :Old.CUSTOMER, :Old.LOC, :Old.DESC_CODE, :Old.ACTIVE, :Old.SCANED_COUNT, :Old.ENDED, :Old.TRANSACTION_ID, :Old.SIGNATOR, :Old.SENDER, :Old.RECIVER, :Old.ROUTE, :Old.SCANED, :Old.OPROGRAM, :Old.NOTE1, :Old.CART, :Old.WEIGHT, :Old.HEIGHT, :Old.WIDTH, :Old.ODEPTH, :Old.PRO_ID, :Old.STITLE, :Old.SADDRESS, :Old.SPOSTOFFICENUMBER, :Old.SCITY, :Old.RTITLE, :Old.RADDRESS, :Old.RPOSTOFFICENUMBER, :Old.RCITY, :Old.CMR, :Old.NOTE2, :Old.NOTE3, :Old.TEMPERATURE_1, :Old.TEMPERATURE_2, :Old.NR_PALETTES, :Old.NOTE4, :Old.NOTE5, :Old.NOTE6, :Old.S_TELEPHONE, :Old.R_TELEPHONE, :Old.LOADING_LAT, :Old.LOADING_LNG, :Old.UNLOADING_LAT, :Old.UNLOADING_LNG, :Old.TRAILER, :Old.TRAILER_ID, :Old.RABAT1, :Old.RABAT2, :Old.DATE_MODIFIED, :Old.CASH, :Old.ZOI, :Old.EOR, :Old.BUSINESSPREMISE, :Old.ELECTRONICDEVICE, :Old.VAT22, :Old.VAT95, :Old.OSNOVA1, :Old.OSNOVA2, :Old.PRICE, :Old.FURS_ORDER, :Old.INVOICE_NUMBER,sysdate,user,v_ChangeType);end If; EXCEPTION  WHEN OTHERS THEN    NULL; END; end;
/
SHOW ERRORS;


CREATE OR REPLACE TRIGGER TBE_ATTACHMENTS 
BEFORE INSERT 
ON ATTACHMENTS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH Row
declare
l_tip_dokumenta   number:=0;
l_doc_id_glavni   number:=0; 
l_pro_id          number:=0;    
dir_path varchar2(1000);
BEGIN
    if(inserting) then
        :new.file_name:=replace(trim(:new.file_name),'.jpg','.pdf');
    end if;
 
  if(:new.id=null or :new.id=0) then
   select att_auto.nextval into :new.id from dual;
  end if;
  
  
  If :new.shipment_id = 0 then
   Begin
   Select ord.Otype,ORD.PRO_ID into :new.ORDER_TYPE,l_pro_id from orders Ord where ORD.ORDER_ID=:new.order_id;
    EXCEPTION
    WHEN OTHERS THEN
    NULL;
   End;
  End if;
  
  If :new.order_type = 70 then
   Begin
  -- select teh.docid into l_doc_id_glavni from ferbit.tehtalnilist teh where teh.vez=l_pro_id;
  null;
      EXCEPTION
    WHEN OTHERS THEN
    NULL;
   End;
  
  End if;
  
  
  If :new.Order_Type = 60 then
   l_tip_dokumenta :=9;
  elsif  :new.Order_Type = 70 then
   l_tip_dokumenta :=8;
  end if;
  
  If :new.Order_Type = 60 then
   begin
     SELECT SHI.DOC_ID INTO :NEW.doc_id   FROM SHIPMENTS SHI WHERE   SHI.ID=:NEW.SHIPMENT_ID;
   EXCEPTION
    WHEN OTHERS THEN
    NULL;
   End;
  End if;
  
   If :new.Order_Type = 70 then
     begin
       -- SELECT dodAUTO.NEXTVAL INTO :NEW.doc_id   FROM DUAL;
       null;
     EXCEPTION
       WHEN OTHERS THEN
        NULL;
     End;
   end if;
   
   If :new.Order_Type = 21 then
     begin
   select teh.docid into l_doc_id_glavni from ferbit.dobavskl teh where teh.vez=l_pro_id;
        SELECT ferbit.dodAUTO.NEXTVAL INTO :NEW.doc_id   FROM DUAL;
  dir_path := 'P:\WinPro\Dokumenti_poslovanje\OMAHEN\SKLADISCE\';
      l_tip_dokumenta :=9;
     EXCEPTION
       WHEN OTHERS THEN
        NULL;
     End;
   end if;
  dbms_output.put_line('dodajanje dokumenta '||Trim(:NEW.FILE_NAME) || ' '||:new.doc_id||' '||l_doc_id_glavni);
 
  begin
  --select directory_path into dir_path from dba_directories where upper(directory_name) = 'SKLADISCE';
   DODAJ_DOKUMENT ( l_tip_dokumenta, l_doc_id_glavni , dir_path, Trim(:NEW.FILE_NAME),:new.doc_id, l_tip_dokumenta, :new.ORDER_TYPE,0);
  EXCEPTION
    WHEN OTHERS THEN
    NULL;
   End;
  
  
  
  If :new.order_type in (10,11) then
    Begin
        l_tip_dokumenta := 3;
        SELECT ferbit.dodAUTO.NEXTVAL INTO :NEW.doc_id   FROM DUAL;
        select pro_id into l_pro_id from orders where order_id = :new.order_id;
        select ferbit.nkl.docid into l_doc_id_glavni from ferbit.nakladi nkl where nkl.vez=l_pro_id;
        DODAJ_DOKUMENT ( l_tip_dokumenta, l_doc_id_glavni ,'\\OTL1\Prevoznik\WinPro\Dokumenti_poslovanje\OMAHEN\LETO2016\NAKLADI\', Trim(:NEW.FILE_NAME),:new.doc_id, l_tip_dokumenta, :new.ORDER_TYPE,l_tip_dokumenta);
    EXCEPTION
        WHEN OTHERS THEN
        NULL;
    End;
    
    End if;
  
  
  
   INSERT INTO ferbit.LOG (  OPIS) VALUES ( 'Dodajne shiments '||:NEW.doc_id);       
end;
/
SHOW ERRORS;


CREATE OR REPLACE TRIGGER ART_UPDATE 
before insert or update 
on articles
for each row
begin
  :new.last_Date:=sysdate+1;
null;
end;
/
SHOW ERRORS;


CREATE OR REPLACE TRIGGER TBE_SHIPMENTS 
BEFORE INSERT or update
ON SHIPMENTS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
    loc_vez              NUMBER;
    loc_faza             NUMBER;
    l_error              NUMBER   := 0;                                
    l_procedure          CHAR(50) := 'TBE_SHIPMENTS';                       --- ali obracun obstaja
    l_napak              NUMBER   := 0;
    l_program            char(4); 
    Sql_error            varchar2(250);                                         --- opis napake
    sql_stmt             VARCHAR2(3900);
    l_nalogvez           number   := 0;
    l_prejemnica         Number   := 0;
    l_dobavnica          Number   := 0;
    l_pro_id             Number   := 0;                                     --vez glave dokumenta      
    L_VEZLOKACIJAVSKL    NUMBER   := 0; 
    l_dogodek            NUMBER   := 0;
    l_znak               CHAR(1);
    l_avtovez            Number   := 0;
    l_zapst              Number   := 0;
    l_xid                Number  :=0;
    l_projekt            Number     := 0;
l_stm                char(6);
l_SERIJSKA           CHAR(30);
l_product            Number   := 0;
l_dobavProjekt       Number   := 0;
l_ODOKUMENT         CHAR(15 BYTE);
l_type               number:=0;
l_idp                number:=0;
l_travel_id         number;
BEGIN
    
      IF INSERTING THEN
        If nvl(:NEW.ID,0)=0 OR :NEW.ID=0 THEN
         SELECT SHIP_AUTO.NEXTVAL INTO :NEW.ID FROM DUAL;
        END IF;
       IF(:NEW.QUANTITY_R=-1) THEN
        :NEW.CLIENT_ID := :NEW.ID;
       END IF;
        If nvl(:NEW.PRODUCT,0)=0  THEN
         :new.PRODUCT:=0;
        END IF;
      END IF;  
       INSERT INTO ferbit.LOG (  OPIS) VALUES ( 'Dodajne shiments '||:NEW.ID);   
         
      If inserting or updating then
      
      
       Begin
        select ord.otype,ord.pro_id into l_type,l_pro_id from orders ord where ord.order_id=:new.order_id;
       EXCEPTION 
       WHEN OTHERS THEN
        l_error   := SQLCODE;
        l_napak   := 1;
        Sql_error := SUBSTR(SQLERRM, 1, 250);
        ERROR_LOG(l_error,'SELECT PROGRAM '||:new.order_id,Sql_error,0,l_procedure,0);
       end;
       If (:new.pro_id is null  OR :NEW.PRO_ID=0) then
        :new.pro_id := l_pro_id;
       end if;
        
      End if; 
      If nvl(l_Type,0)=60 then
       begin
        --SELECT dodAUTO.NEXTVAL INTO :NEW.doc_id   FROM DUAL;
        null;
       EXCEPTION
        WHEN OTHERS THEN
        l_error   := SQLCODE;
        l_napak   := 1;
        Sql_error := SUBSTR(SQLERRM, 1, 250);
        ERROR_LOG(l_error,'UPDATE tehtalni list',Sql_error,0,l_procedure,0);
       End;
      
      
        Begin
        --SELECT PROOAUTO.NEXTVAL INTO :NEW.PRO_ID FROM DUAL;
        null;
         EXCEPTION 
       WHEN OTHERS THEN
         l_error   := SQLCODE;
         l_napak   := 1;
         Sql_error := SUBSTR(SQLERRM, 1, 250);
         ERROR_LOG(l_error,'SELECT SEQPMAAUTO',Sql_error,0,l_procedure,0);
       end;
      
      
      
        BEGIN
      /*  insert into prometna2 
        (IDP,voz,BLAGO,datum_s,leto,PLACNIK,STATUS,OPOMBA,OPOSTAJA,TIPVCP,DOCID)
         values
         (:NEW.PRO_id,trim(:new.title),SUBSTR(:NEW.NOTE1,1,20),:new.scaned_date,trim(to_char(:new.scaned_date,'YY')),1,'U',SUBSTR(:NEW.NOTE,1,20),'79' ||TRIM(TO_CHAR(:NEW.QUANTITY_O)),TRIM(TO_CHAR(:NEW.QUANTITY_R)),:NEW.doc_id );
       */
       null;
       EXCEPTION 
         WHEN OTHERS THEN
         l_error   := SQLCODE;
         l_napak   := 1;
         Sql_error := SUBSTR(SQLERRM, 1, 250);
         ERROR_LOG(l_error,'DODAJANE V PROMETNO',Sql_error,0,l_procedure,0);
        END ;
      end if;  
     
     /*POTNI NAKLADALNI*/
     if(inserting and trim(:new.stype)='55' and trim(:new.terminal)='PLACE' and :new.route=3 ) then 
    -- if(inserting) then 
        null;
       -- select pro_id into l_travel_id  from orders where order_id=:new.order_id and otype=55;    
       --  If nvl(:NEW.transaction_id,0) > 0  then
         /*   Begin
            INSERT INTO ferbit.POTNI_NAKLADALNI
            (NAR_VEZ,PON_VEZ,FVO_VEZ,VEZN)
             VALUES(:NEW.PRO_ID,l_travel_id,:NEW.transaction_id,:NEW.PRO_ID);
           
            EXCEPTION 
             WHEN OTHERS THEN
             l_error   := SQLCODE;
             l_napak   :=  l_napak+1;
             Sql_error := SUBSTR(SQLERRM, 1, 250);
             ERROR_LOG(l_error,'UPDATE NAKLADI'||TRIM(:NEW.ID),Sql_error,0,l_procedure,0);  
            end;*/
         -- end if;
  
     end if;
     /*POTNI NAKLADALNI*/
     
     
    END;
/
SHOW ERRORS;


GRANT INSERT ON ARTICLES TO FERBIT;

GRANT SELECT ON ATTACHMENTS TO FERBIT;

GRANT EXECUTE ON MORDERS_INSERT_ORDERS TO FERBIT;

GRANT EXECUTE ON MORDERS_SEND_WITH_JOB TO FERBIT;

GRANT INSERT, SELECT, UPDATE ON ORDERS TO FERBIT;

GRANT SELECT ON ORD_SEQ TO FERBIT;