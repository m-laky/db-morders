create or replace procedure create_doc_document(p_tip in number, p_Stevilka in number, p_xid in number) is


L_UPORABNIK   VARCHAR2(30);
L_DATUM       DATE;
l_nklvez      NUMBER;
l_napaka      NUMBER:=0;
l_napak       NUMBER:=0;
l_leto        date:=sysdate;
l_voznik      NUMBER:=0;
 
l_tip    number :=14;
l_veja   number :=0;
l_pot    VARCHAR2(299 BYTE);
l_part   number :=0;
l_avto   number :=0;
l_IME    VARCHAR2(30 BYTE); 
 
 
l_error                  NUMBER:=0;                                
l_procedure              CHAR(15):='create_doc';                                    --- ali obracun obstaja
Sql_error                varchar2(250);                                    --- opis napake
sql_stmt                 VARCHAR2(3900);
loc_steviloVin            NUMBER:=0;   
loc_avez                  NUMBER:=0;
loc_faza                  NUMBER:=0;    
loc_seq                   NUMBER:=0;     
p_transakcija             NUMBER:=0;   
begin
     If p_tip=55 then
     l_tip :=6;
       
      l_ime :='Pot:'||trim(to_char(sysdate,'YYYY'))||trim(to_char(p_stevilka,'09999'))||'.pdf';
    
    
      Begin 
       SELECT POT,VEJA into l_pot,l_veja  FROM DOC_TIP WHERE  vez=l_tip;
      EXCEPTION
         WHEN OTHERS THEN
           l_error   := 1;  
           l_napak   := l_napak+1; 
           Sql_error := SUBSTR(SQLERRM, 1, 250);
            ERROR_LOG(l_error,'SELECT TIP',Sql_error,0,l_procedure,0); 
       End;
    
       Begin 
       SELECT PART_ID,AVTO into l_part,l_avto  FROM DOC_RELACIJE WHERE avto = l_veja;
       EXCEPTION
         WHEN OTHERS THEN
           l_error   := 1;  
           l_napak   := l_napak+1; 
           Sql_error := SUBSTR(SQLERRM, 1, 250);
           ERROR_LOG(l_error,'SELECT RELACIJE',Sql_error,0,l_procedure,0); 
       End;
    
      Begin 
       INSERT INTO DOC_DOKUMENT
      (XID, XNAME, VRSTA, POT_DATOTEKE, IME_DATOTEKE, KRATKI_OPIS, DATUM_S, VELIKOST, TIP_DOKUMENTA, PARTNER, PROJEKT, DELAVEC, USERID)
       VALUES(p_xid, l_ime, 2, l_pot, l_ime, l_ime,TO_DATE(sysdate , 'DD.MM.YYYY HH24:MI:SS'),0, l_tip, 1, 0, 0, 9999) ;
       EXCEPTION
         WHEN OTHERS THEN
           l_error   := 1;  
           l_napak   := l_napak+1; 
           Sql_error := SUBSTR(SQLERRM, 1, 250);
            ERROR_LOG(l_error,'INSERT DOC',Sql_error,0,l_procedure,0); 
       End;
    
      Begin   
      INSERT INTO DOC_RELACIJE
      (ASSEMBLY_ID, AVTO, PART_ID, SEQ, TID,assembly_avto) VALUES(l_part, 0, p_xid , 0, 0,l_avto) ;
      EXCEPTION
         WHEN OTHERS THEN
           l_error   := 1;  
           l_napak   := l_napak+1; 
           Sql_error := SUBSTR(SQLERRM, 1, 250);
            ERROR_LOG(l_error,'INSERT RELACIJE',Sql_error,0,l_procedure,0); 
       End;
    
     end if; -- potni nalog
 end ;
 
 create or replace procedure        dodaj_dokument( 
tip_dokumenta       in number, -- ??
vez_dokumenta       in number, -- polje ORDER_ID iz ATTACHMENTS
mapa_dokumenta      in char,  -- pot na disku kjer je shranjena datoteka
ime_dokumenta       in char,
sekvenca            in number, -- sekvenca za DOC_DOKUMENT
vrsta               in int,
order_type in char,
lot number)   -- vrsta datoteke
as
t_xid int              :=0; 
t_ASSEMBLY_ID      int :=0; -- PART_ID glavnega dokumenta
t_ASSEMBLY_AVTO    int :=0; -- AVTO glavnega dokumenta 

stavek2            VARCHAR2(4000);
stavek3            VARCHAR2(4000);
stavek4            VARCHAR2(4000);
stavek5            VARCHAR2(4000);
stavek6            VARCHAR2(4000);
stavek7            VARCHAR2(4000);
napaka             int :=0;
l_QUANTITY_O    number :=0;
 -- iz PNALOG_MOBILE
 l_xid    number :=0;
l_tip    number :=0; -- mogoče ga je treba najt iz nastavitev !!
l_veja   number :=0;
l_pot    VARCHAR2(299 BYTE);
l_part   number :=0;
l_avto   number :=0;
l_error                  NUMBER:=0;                                
l_procedure              CHAR(15):='dodaj_dokument';                                    --- ali obracun obstaja
Sql_error                varchar2(250);
t_PRO_ID number :=0;

begin
    
  t_xid :=   sekvenca; 
   
   INSERT INTO ferbit.LOG (  OPIS) VALUES ( 'FDodaj dokument'||sekvenca||' vrsta:' ||vrsta); 
  if  vrsta = 55  then
        l_tip     := 6 ; --mogoče ga je treba najt iz nastavitev !!
  elsif vrsta = 50 then
        l_tip     :=16 ;
  elsif vrsta = 51 then
        l_tip     :=105 ;
  elsif vrsta = 52 then
        l_tip     :=17 ;
  elsif vrsta = 53 then
        l_tip     :=4 ;
   elsif vrsta = 54 then
        l_tip     :=18 ;
   elsif vrsta = 56 then
        l_tip     :=19 ;
  else 
        l_tip     :=14 ; --mogoče ga je treba najt iz nastavitev !!
  end if;   
     -- DBMS_OUTPUT.PUT_LINE('l_tip '||l_tip );
 if t_xid > 0 then --če obstaja sekvenca dodamo zapis v dokumente
 
       begin
            stavek3 :=  'INSERT INTO DOC_DOKUMENT (XID, XNAME, VRSTA, POT_DATOTEKE, IME_DATOTEKE, KRATKI_OPIS,TIP_DOKUMENTA ) values (:1,:2,:3,:4,:5,:6,:7)';
            execute immediate stavek3 using t_xid,ime_dokumenta,2, mapa_dokumenta, ime_dokumenta,ime_dokumenta,l_tip;
       EXCEPTION
            WHEN OTHERS THEN
            --DBMS_OUTPUT.PUT_LINE('Insert DOC_DOKUMENT: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
            Sql_error :=SQLERRM;
            INSERT INTO LOG (  OPIS) VALUES ( 'napaka Insert DOC_DOKUMENT'||SUBSTR(Sql_error,1,150)||' '||TRIM(ime_dokumenta) );
            
            napaka := 1;
    end;

    if  vrsta in (50,51,52,53,54,55,56,49)  then -- če so te vrste najdemo korenski id od vrste dokumenta v doc_relacijah
   
        Begin 
            SELECT POT,VEJA into l_pot,l_veja  FROM  DOC_TIP WHERE  vez=l_tip;
        EXCEPTION
         WHEN OTHERS THEN
           Sql_error := SUBSTR(SQLERRM, 1, 250);
           ERROR_LOG(l_error,'SELECT TIP '||l_tip,Sql_error,0,l_procedure,0); 
       End;
   -- DBMS_OUTPUT.PUT_LINE('l_veja '||l_veja );
       Begin 
       SELECT PART_ID,AVTO into l_part,l_avto  FROM  DOC_RELACIJE WHERE avto = l_veja;
       EXCEPTION
         WHEN OTHERS THEN
           Sql_error := SUBSTR(SQLERRM, 1, 250);
           ERROR_LOG(l_error,'SELECT RELACIJE '||l_veja,Sql_error,0,l_procedure,0); 
       End;
       INSERT INTO LOG (  OPIS) VALUES ( 'l_part,l_avto:' ||l_part||','||l_avto);
  
    end if;


   if  vrsta = 1 then   
    
     if napaka=0 then  
       begin
           stavek4 := 'SELECT DISTINCT PART_ID FROM ORDERS INNER JOIN NAKLADI ON (PRO_ID = VEZ) INNER JOIN DOC_RELACIJE ON (DOCID = PART_ID) where ORDERS.ORDER_ID = '||vez_dokumenta;  
           EXECUTE IMMEDIATE stavek4 into t_ASSEMBLY_ID   ;
       EXCEPTION
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select DOCID, avto: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
            INSERT INTO LOG (  OPIS) VALUES ( '1 napaka select DOCID, avto:' );
            napaka :=2;
       end;       
      end if;  
      
   elsif vrsta = 49 then   
    
     if napaka=0 then  
       begin
           stavek4 := 'SELECT PART_ID, avto FROM ORDERS INNER JOIN NAKLADI ON (PRO_ID = VEZ) INNER JOIN DOC_RELACIJE ON (DOCID = PART_ID) where ORDERS.ORDER_ID = '||vez_dokumenta;  
           EXECUTE IMMEDIATE stavek4 into t_ASSEMBLY_ID, t_ASSEMBLY_AVTO   ;
       EXCEPTION
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select DOCID, avto: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
         --   INSERT INTO LOG (  OPIS) VALUES ( '1 napaka select DOCID, avto:' );
            napaka :=2;
       end;       
      end if;      
   
      
   elsif  vrsta = 2 then  
      
     if napaka=0 then  
       begin
           if (trim(upper(order_type))='MP') then--ČE SO MALE POŠILJKE
              stavek4 := 'select PART_ID, avto from potovanje po inner join orders o on (o.pro_id=po.avez) INNER JOIN nakladi nkl ON (po.vezn = nkl.VEZ) INNER JOIN DOC_RELACIJE ON (nkl.DOCID = PART_ID) where o.order_id='||vez_dokumenta; 
           elsif(trim(upper(order_type))='MWP') then--ČE JE ŠPEDICIJA
              stavek4 := 'SELECT PART_ID, avto FROM ORDERS INNER JOIN NAKLADI ON (PRO_ID = VEZ) INNER JOIN DOC_RELACIJE ON (DOCID = PART_ID) where ORDERS.ORDER_ID = '||vez_dokumenta; 
           elsif(trim(upper(order_type))='0') then--VSE OSTALO
              stavek4 := 'SELECT PART_ID, avto FROM ORDERS INNER JOIN NAKLADI ON (PRO_ID = VEZ) INNER JOIN DOC_RELACIJE ON (DOCID = PART_ID) where ORDERS.ORDER_ID = '||vez_dokumenta; 
           else
              stavek4 := 'SELECT PART_ID, avto FROM ORDERS INNER JOIN NAKLADI ON (PRO_ID = VEZ) INNER JOIN DOC_RELACIJE ON (DOCID = PART_ID) where ORDERS.ORDER_ID = '||vez_dokumenta;   
           end if;
           --stavek4 := 'SELECT PART_ID, avto FROM ORDERS INNER JOIN NAKLADI ON (PRO_ID = VEZ) INNER JOIN DOC_RELACIJE ON (DOCID = PART_ID) where ORDERS.ORDER_ID = '||vez_dokumenta;  
           EXECUTE IMMEDIATE stavek4 into t_ASSEMBLY_ID, t_ASSEMBLY_AVTO   ;
       EXCEPTION
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select DOCID, avto: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
        --    INSERT INTO LOG (  OPIS) VALUES ( '2 napaka select DOCID, avto:' );
            napaka :=2;
       end;       
     end if;  
   elsif  vrsta in (50,54,56) then  
   
      if napaka=0 then  -- pogledamo, če zapis za ta dokument že obstaja
       begin
           stavek4 := 'SELECT PART_ID, avto FROM ORDERS INNER JOIN NALOGD ON (PRO_ID = VEZ) INNER JOIN DOC_RELACIJE ON (DOCID = PART_ID) where ORDERS.ORDER_ID = '||vez_dokumenta;  
           EXECUTE IMMEDIATE stavek4 into t_ASSEMBLY_ID, t_ASSEMBLY_AVTO   ;
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                t_ASSEMBLY_ID:=0;
                t_ASSEMBLY_AVTO :=0;
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select DOCID, avto: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
           -- INSERT INTO LOG (  OPIS) VALUES ( '50 napaka select DOCID, avto:' );
            napaka :=2;
        end;       
     end if;
     
       if napaka=0 then  -- vrnemo pro_id za ta dokument
       begin
           stavek4 := 'SELECT PRO_ID FROM ORDERS  where ORDERS.ORDER_ID = '||vez_dokumenta;  
           EXECUTE IMMEDIATE stavek4 into t_PRO_ID   ;
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                t_ASSEMBLY_ID:=0;
                t_ASSEMBLY_AVTO :=0;
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select DOCID, avto: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
          --  INSERT INTO LOG (  OPIS) VALUES ( '50 napaka select DOCID, avto:' );
            napaka :=2;
        end;       
     end if;
     
   elsif  vrsta = 51 then 
   
    if napaka=0 then  
       begin
           stavek4 := 'SELECT PART_ID, avto FROM ORDERS INNER JOIN PREJEM ON (PRO_ID = VEZ) INNER JOIN DOC_RELACIJE ON (DOCID = PART_ID) where ORDERS.ORDER_ID = '||vez_dokumenta;  
           EXECUTE IMMEDIATE stavek4 into t_ASSEMBLY_ID, t_ASSEMBLY_AVTO   ;
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                t_ASSEMBLY_ID:=0;
                t_ASSEMBLY_AVTO :=0;
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select DOCID, avto: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
         --   INSERT INTO LOG (  OPIS) VALUES ( '50 napaka select DOCID, avto:' );
            napaka :=2;
        end;       
     end if;
     
       if napaka=0 then  
       begin
           stavek4 := 'SELECT PRO_ID FROM ORDERS  where ORDERS.ORDER_ID = '||vez_dokumenta;  
           EXECUTE IMMEDIATE stavek4 into t_PRO_ID   ;
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                t_ASSEMBLY_ID:=0;
                t_ASSEMBLY_AVTO :=0;
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select DOCID, avto: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
            --INSERT INTO LOG (  OPIS) VALUES ( '50 napaka select DOCID, avto:' );
            napaka :=2;
        end;       
     end if;
   elsif  vrsta = 52 then 
    
  if napaka=0 then  
       begin
           stavek4 := 'SELECT PART_ID, avto FROM ORDERS INNER JOIN INVEN ON (PRO_ID = VEZ) INNER JOIN DOC_RELACIJE ON (DOCID = PART_ID) where ORDERS.ORDER_ID = '||vez_dokumenta;  
           EXECUTE IMMEDIATE stavek4 into t_ASSEMBLY_ID, t_ASSEMBLY_AVTO   ;
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                t_ASSEMBLY_ID:=0;
                t_ASSEMBLY_AVTO :=0;
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select DOCID, avto: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
         --   INSERT INTO LOG (  OPIS) VALUES ( '50 napaka select DOCID, avto:' );
            napaka :=2;
        end;       
     end if;
     
       if napaka=0 then  
       begin
           stavek4 := 'SELECT PRO_ID FROM ORDERS  where ORDERS.ORDER_ID = '||vez_dokumenta;  
           EXECUTE IMMEDIATE stavek4 into t_PRO_ID   ;
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                t_ASSEMBLY_ID:=0;
                t_ASSEMBLY_AVTO :=0;
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select DOCID, avto: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
          --  INSERT INTO LOG (  OPIS) VALUES ( '50 napaka select DOCID, avto:' );
            napaka :=2;
        end;       
     end if;
   elsif  vrsta = 53 then  
   
     if napaka=0 then  
       begin
           stavek4 := 'SELECT PART_ID, avto FROM ORDERS INNER JOIN DOBAVNICA ON (PRO_ID = VEZ) INNER JOIN DOC_RELACIJE ON (DOCID = PART_ID) where ORDERS.ORDER_ID = '||vez_dokumenta;  
           EXECUTE IMMEDIATE stavek4 into t_ASSEMBLY_ID, t_ASSEMBLY_AVTO   ;
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                t_ASSEMBLY_ID:=0;
                t_ASSEMBLY_AVTO :=0;
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select DOCID, avto: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
          --  INSERT INTO LOG (  OPIS) VALUES ( '50 napaka select DOCID, avto:' );
            napaka :=2;
        end;       
     end if;
     
       if napaka=0 then  
       begin
           stavek4 := 'SELECT PRO_ID FROM ORDERS  where ORDERS.ORDER_ID = '||vez_dokumenta;  
           EXECUTE IMMEDIATE stavek4 into t_PRO_ID   ;
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                t_ASSEMBLY_ID:=0;
                t_ASSEMBLY_AVTO :=0;
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select DOCID, avto: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
        --    INSERT INTO LOG (  OPIS) VALUES ( '50 napaka select DOCID, avto:' );
            napaka :=2;
        end;       
     end if;
   
  elsif  vrsta = 55 then 
  -- INSERT INTO LOG (  OPIS) VALUES ( '55 notri za dokument: '|| vez_dokumenta);
     if napaka=0 then  
       begin
           --stavek4 := 'SELECT PART_ID, avto FROM ORDERS INNER JOIN NAKLADI ON (PRO_ID = VEZ) INNER JOIN DOC_RELACIJE ON (DOCID = PART_ID) where ORDERS.ORDER_ID = '||vez_dokumenta;  
         stavek4 := 'SELECT PART_ID, avto FROM ORDERS INNER JOIN PNALOG ON (PRO_ID = VEZ) INNER JOIN DOC_RELACIJE ON (DOCID = PART_ID) where ORDERS.ORDER_ID = '||vez_dokumenta;   
        EXECUTE IMMEDIATE stavek4 into t_ASSEMBLY_ID, t_ASSEMBLY_AVTO   ;
       EXCEPTION
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select DOCID, avto: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
           -- INSERT INTO LOG (  OPIS) VALUES ( '55 napaka select DOCID, avto: '|| vez_dokumenta);
            napaka :=2;
       end;       
     end if;
     
     if napaka=0 then                  
       begin
            stavek5 :=  'INSERT INTO DOC_RELACIJE (ASSEMBLY_ID , PART_ID , ASSEMBLY_AVTO  ) values (:1,:2,:3)';
            execute immediate stavek5 using l_part,t_xid, l_avto;
                EXCEPTION
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Insert DOC_RELACIJE: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
           -- INSERT INTO LOG (  OPIS) VALUES ( 'napaka Insert DOC_RELACIJE:' );
            napaka :=2;
       end;  
    end if;
    
     If lot = -1 then
      begin
        --  INSERT INTO LOG (  OPIS) VALUES ( 'STAVEK5 INSERT DOC_RELACIJE: l_part'||l_part||' t_xid:'||t_xid||' l_avto:'||l_avto );
            stavek5 :=  'INSERT INTO DOC_RELACIJE (ASSEMBLY_ID , PART_ID , ASSEMBLY_AVTO  ) values (:1,:2,:3)';
            execute immediate stavek5 using t_ASSEMBLY_ID,t_xid,t_ASSEMBLY_AVTO;
                EXCEPTION
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Insert DOC_RELACIJE: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
           -- INSERT INTO LOG (  OPIS) VALUES ( 'napaka Insert DOC_RELACIJE:' );
            napaka :=2;
       end;  
     
     end if;
     
    
    
     if napaka=0 then                  
       begin
            stavek6 :=  'select QUANTITY_O from shipments where client_id= '||lot||' and order_id='||vez_dokumenta;
            execute immediate stavek6 into l_QUANTITY_O;
                EXCEPTION
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select QUANTITY_O from shipments: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
          --  INSERT INTO LOG (  OPIS) VALUES ( 'select QUANTITY_O from shipments:' );
            napaka :=2;
       end;  
    end if;
    
     if napaka=0 then                  
       begin
         --   INSERT INTO LOG (  OPIS) VALUES ( 'update PNALOGP:'||t_xid||','|| l_QUANTITY_O );
            stavek7 :=  'update PNALOGP set DOCID = :1 where ZAPST = :2'; 
            execute immediate stavek7 using t_xid, l_QUANTITY_O ;
                EXCEPTION
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('update PNALOGP: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
           -- INSERT INTO LOG (  OPIS) VALUES ( 'update PNALOGP:' );
            napaka :=2;
       end;  
    end if;
     
     /*
     select QUANTITY_O  from shipments where client_id= :1 and order_id=2962

    select docid from pnalogp where zapst=164111

    UPDATE PNALOGP SET DOC_ID = (DODAN DOKUMENT) WHERE  zapst=164111

    DODAJ ZAPIS V RELACIJE
     */
     
  /*else     
  
     if napaka=0 then  
       begin
           stavek4 := 'SELECT PART_ID, avto FROM ORDERS INNER JOIN NAKLADI ON (PRO_ID = VEZ) INNER JOIN DOC_RELACIJE ON (DOCID = PART_ID) where ORDERS.ORDER_ID = '||vez_dokumenta;  
           EXECUTE IMMEDIATE stavek4 into t_ASSEMBLY_ID, t_ASSEMBLY_AVTO   ;
       EXCEPTION
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('select DOCID, avto: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
            INSERT INTO LOG (  OPIS) VALUES ( 'OSTALO napaka select DOCID, avto:' );
            napaka :=2;
       end;       
     end if;  */        
  end if;                        
     
  if vrsta in (55,50,51,52,53,54,56) then
    if nvl(t_ASSEMBLY_ID,0) =0 then
         t_ASSEMBLY_ID := l_part;
        t_ASSEMBLY_AVTO := l_avto;
          if napaka=0 then                  
       begin
           -- INSERT INTO LOG (  OPIS) VALUES ( '!!update nalogd:'||t_xid||','|| t_PRO_ID );
            stavek7 :=  'update nalogd set DOCID = :1 where VEZ = :2'; 
            execute immediate stavek7 using t_xid, t_PRO_ID ;
                EXCEPTION
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('update nalogd: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
         --   INSERT INTO LOG (  OPIS) VALUES ( 'update nalogd:' );
            napaka :=2;
       end;  
    end if;
    end if;
  end if;  
  -- DBMS_OUTPUT.PUT_LINE('l_vcej'||l_tip );
     if napaka=0 then                  
       begin
            stavek5 :=  'INSERT INTO DOC_RELACIJE (ASSEMBLY_ID , PART_ID , ASSEMBLY_AVTO  ) values (:1,:2,:3)';
            execute immediate stavek5 using t_ASSEMBLY_ID,t_xid, t_ASSEMBLY_AVTO;
       EXCEPTION
            WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Insert DOC_RELACIJE: ' ||SQLCODE||' - '||SUBSTR(SQLERRM,1,4000) );
         --   INSERT INTO LOG (  OPIS) VALUES ( 'napaka Insert DOC_RELACIJE:' );
       end;  
    end if;


 end if; -- t_xid > 0


end;

create or replace FUNCTION         DocRelPisi(xDocIdGlave in Number,xDocIdDokumenta in Number )
RETURN NUMBER IS
LOC_AvtoGlave        NUMBER :=0;
l_error              NUMBER   := 0;
l_procedure          CHAR(50) := 'DocRelPisi';                       --- ali obracun obstaja
l_napak              NUMBER   := 0;
Sql_error            varchar2 (250);                                 --- opis napake
sql_stmt             VARCHAR2 (3900);
p_transakcija        NUMBER   :=0;  
BEGIN
  l_napak   := 0;
   Begin
    Select dor.avto into loc_avtoglave from doc_relacije dor where dor.part_id=xDocIdGlave;
  
   EXCEPTION 
     WHEN OTHERS THEN
     l_error   := SQLCODE;
     l_napak   :=  l_napak+1;
     Sql_error := SUBSTR(SQLERRM, 1, 250);
     ERROR_LOG(l_error,'ISKANJE DOR.AVTO' ,Sql_error,0,l_procedure,0);
    End;
    
   Begin
   Insert into Doc_Relacije
   (Assembly_Id,avto,Part_Id,Assembly_avto)
    Values(xDocIdGlave,0,xDocIdDokumenta,LOC_AvtoGlave);
   EXCEPTION 
         WHEN OTHERS THEN
          l_error   := SQLCODE;
          l_napak   :=  l_napak+1;
          Sql_error := SUBSTR(SQLERRM, 1, 250);
          ERROR_LOG(l_error,'INSERT RELACIJE' ,Sql_error,0,l_procedure,0);
   end;
   
   If l_napak = 0 then
     Return(1);
   else
     Return(0);
   End If;
                
END;


grant execute on  DocRelPisi to mobilno3;
grant execute on dodaj_dokument to mobilno3;
grant execute on create_doc_document to mobilno3;
grant update,select,insert on dobavskl to mobilno3;
grant update,select,insert on promets to mobilno3;
grant update,select,insert on nakladi to mobilno3 ;
grant update,select,insert on fvozniki to mobilno3 ;
grant update,select,insert on nastavi to mobilno3;
grant update,select,insert on klienti to mobilno3;
grant update,select,insert on izhodnisms to mobilno3;
grant update,select,insert on promets to mobilno3;
grant update,select,insert on dobavskl to mobilno3;
grant select on ARTSAUTO to mobilno3;
grant select on prmsauto to mobilno3;
GRANT SELECT ON preauto TO MOBILNO3;
grant select on blaauto to mobilno3;
grant select on nklauto to mobilno3;
grant update,select,insert on artikliskl to mobilno3; 
grant select on prmauto  to mobilno3;

grant select, insert, update on pomozna to mobilno3;
grant select on pomauto to mobilno3;
grant select on kliauto to mobilno3;
grant select on doc_tip to mobilno;
