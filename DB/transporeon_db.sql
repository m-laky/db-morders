
drop table TRANSPOREON_ATTACHMENT;
drop table TRANSPOREON_BOOKING;
drop table TRANSPOREON_DISPATCH_STATUS;
drop table TRANSPOREON_EXTENSION;
drop table TRANSPOREON_FIELD;
drop table TRANSPOREON_ITEM;
drop table TRANSPOREON_OFFER;
drop table TRANSPOREON_PARAMETER;
drop table TRANSPOREON_QUANTITY;
drop table transporeon_req_del_param;
drop table transporeon_req_off_param;
drop table transporeon_req_price_comp;
drop table TRANSPOREON_REQ_TRAN_PARAM;
drop table TRANSPOREON_SHIPMENT;
drop table TRANSPOREON_STATION;
drop table TRANSPOREON_TOur;


create table transporeon_attachment(
    t_id number,
    shipment_number varchar2(4000),
    a_description varchar2(3000),
    A_link varchar2(4000)
);

create table transporeon_booking(
    t_id number,
    booking_id varchar2(4000),
    b_location varchar2(4000),
    gate varchar2(4000),
    loading_type varchar2(4000),
    date_time_begin date,
    date_time_end date,
    licence_place varchar2(4000)
);

create table transporeon_dispatch_status(
    t_id number,
    qualifier varchar2(4000),
    d_timestamp date
);

create table transporeon_extension(
    t_id number,
    qualifier varchar2(4000),
    e_value varchar2(4000)
);

create table transporeon_field(
    t_id number,
    qualifier varchar2(4000)
);

create table transporeon_item(
    t_id number,
    pos_number varchar2(4000),
    pos_index varchar2(4000),
    i_description varchar2(4000),
    short_description varchar2(4000),
    material_number  varchar2(4000),
    id  varchar2(4000)
);

create table transporeon_offer(
    t_id number,
    tour_id varchar2(4000),
    t_timestamp date,
    disponent_name varchar2(4000),
    creditor_number varchar2(4000),
    price varchar2(4000),
    currency_id varchar2(4000),
    qualifier varchar2(4000),
    o_comment varchar2(4000),
    comment_plain_text varchar2(4000),
    binding_until_date date,
    binding_until_time varchar2(4000),
    additional_expenses_included varchar2(4000),
    id varchar2(4000)
);

create table transporeon_Parameter(
    t_id number,
    qualifier  varchar2(4000),
    p_value  varchar2(4000),
    p_description varchar2(4000),
    mandatory number
);

create table transporeon_quantity(
    t_id number,
    qualifier  varchar2(4000),
    unit  varchar2(4000),
    q_value varchar2(4000)
);

create table transporeon_req_del_param(
    editable varchar2(4000),
    t_id number,
    qualifier varchar2(4000),
    mandatory number
);

create table transporeon_req_off_param(
    t_id number,
    qualifier varchar2(4000),
    optional number
);

create table transporeon_req_price_comp(
    t_id number,
    qualifier  varchar2(4000),
    p_name  varchar2(4000),
    optional number
);

create table TRANSPOREON_REQ_TRAN_PARAM(
    t_id number,
    qualifier  varchar2(4000),
    EDITABLE varchar2(4000),
    mandatory number
);

create table transporeon_Shipment(
    t_id number,
    s_index varchar2(4000),
    tourId varchar2(4000),
    s_owner varchar2(4000),
    s_number varchar2(4000),
    customer_name varchar2(4000),
    customer_number varchar2(4000),
    faktura varchar2(4000),
    vehicle_id varchar2(4000),
    vehicle_name varchar2(4000),
    s_weight varchar2(4000),
    weight_unit varchar2(4000),
    s_length varchar2(4000),
    length_unit varchar2(4000),
    s_volume varchar2(4000),
    volume_unit varchar2(4000),
    s_route varchar2(4000),
    route_unit varchar2(4000),
    dlclass varchar2(4000),
    dlnumber varchar2(4000),
    storage_position_count varchar2(4000),
    s_comment varchar2(4000),
    comment_plain_text varchar2(4000),
    id varchar2(4000)
);

create table transporeon_Station(
    t_id number,
    shipmentId varchar2(4000),
    avis varchar2(4000),
    s_description varchar2(4000),
    company_name varchar2(4000),
    shipper_location_number varchar2(4000),
    s_address varchar2(4000),
    zip varchar2(4000),
    city varchar2(4000),
    region varchar2(4000),
    countryId varchar2(4000),
    loadingName varchar2(4000),
    loadingramp varchar2(4000),
    fromDate date,
    fromTime date,
    untilDate date,
    untilTime date,
    fixed_date_loading date,
    fixed_date_unloading date,
    transportunit_name varchar2(4000),
    transportunit_count varchar2(4000),
    transportunit_pileable varchar2(4000),
    transportunit_exchange number,
    s_comment varchar2(4000),
    comment_plain_text varchar2(4000),
    id varchar2(4000),
    s_type varchar2(4000)
);

create table transporeon_tour(
    t_id number,
    t_timestamp date,
    changed number,
    t_number varchar2(4000),
    external_Transport_Number varchar2(4000),
    tpwLink varchar2(4000),
    t_status varchar2(4000),
    qualifier varchar2(4000),
    partial_Accept number,
    company_Id varchar2(4000),
    company_Name varchar2(4000),
    company_Address varchar2(4000),
    company_Zip_Address varchar2(4000),
    company_City varchar2(4000),
    company_Region varchar2(4000),
    company_Country_Id varchar2(4000),
    disponent_Firstname varchar2(4000),
    disponent_Lastname varchar2(4000),
    disponent_Phone varchar2(4000),
    disponent_Id varchar2(4000),
    carrier_Disponent_Name varchar2(4000),
    carrier_Disponent_Id varchar2(4000),
    carrier_Disponent_Email varchar2(4000),
    vehicle_Id varchar2(4000),
    vehicle_Name varchar2(4000),
    start_Date date,
    start_Time date,
    end_Date date,
    end_Time Date,
    t_owner varchar2(4000),
    start_Country_Id varchar2(4000),
    start_Zip varchar2(4000),
    start_City varchar2(4000),
    start_Region varchar2(4000),
    end_Country_Id varchar2(4000),
    end_Zip varchar2(4000),
    end_City varchar2(4000),
    end_Region varchar2(4000),
    price_Basic varchar2(4000),
    price_Additional1 varchar2(4000),
    price_Additional2 varchar2(4000),
    price_Additional3 varchar2(4000),
    price_Additional1_Desc varchar2(4000),
    price_Additional2_Desc varchar2(4000),
    price_Additional3_Desc varchar2(4000),
    currency_Id varchar2(4000),
    offer_Price_Disabled number,
    assigned_Carrier_Id varchar2(4000),
    assigned_Carrier_Name varchar2(4000),
    assigned_Creditor_Number varchar2(4000),
    carrier_Number varchar2(4000),
    t_weight varchar2(4000),
    weight_Unit varchar2(4000),
    t_length varchar2(4000),
    length_Unit varchar2(4000),
    t_volume varchar2(4000),
    volume_Unit varchar2(4000),
    t_route varchar2(4000),
    route_Unit varchar2(4000),
    t_zone varchar2(4000),
    auction_Start_Date date,
    auction_Start_Time date,
    auction_End_Date varchar2(4000),
    auction_End_Time varchar2(4000),
    auction_Comment varchar2(4000),
    auction_Comment_Plain_Text varchar2(4000),
    plant varchar2(4000),
    delivery_Group_Number varchar2(4000),
    tkn_Loading_Station varchar2(4000),
    tkn_Weight varchar2(4000),
    storage_Position_Count varchar2(4000),
    pin varchar2(4000),
    tsm_Light_Login_Link varchar2(4000),
    t_comment varchar2(4000),
    comment_Plain_Text varchar2(4000),
    internal_Comment varchar2(4000),
    id number,
    tour_status number
);

/*
delete from TRANSPOREON_ATTACHMENT;
delete from TRANSPOREON_BOOKING;
delete from TRANSPOREON_DISPATCH_STATUS;
delete from TRANSPOREON_EXTENSION;
delete from TRANSPOREON_FIELD;
delete from TRANSPOREON_ITEM;
delete from TRANSPOREON_OFFER;
delete from TRANSPOREON_PARAMETER;
delete from TRANSPOREON_QUANTITY;
delete from TRANSPOREON_REQUIRED_DELIVERY_PARAMETER;
delete from TRANSPOREON_REQUIRED_OFFER_PARAMETER;
delete from TRANSPOREON_REQUIRED_PRICE_COMPONENT;
delete from TRANSPOREON_REQUIRED_TRANSPORT_PARAMETER;
delete from TRANSPOREON_SHIPMENT;
delete from TRANSPOREON_STATION;
delete from TRANSPOREON_TOur;
commit;




*/
create or replace  procedure transporeon_insert_nkl(pTid in number, l_firma in number) is 
    storitevId number := 19;
    stats number;
    p_procedure varchar2(200):='TRANSPOREON_INSERT_NKL';
    tour TRANSPOREON_TOUR%ROWTYPE; 
    
    /*loadingStation TRANSPOREON_STATION%ROWTYPE;
    unLoadingStation TRANSPOREON_STATION%ROWTYPE;
    */
    drzavaNakladSifra varchar2(1000);
    drzavaRazkladSifra varchar2(1000);
    cursor shipments is SELECT * FROM TRANSPOREON_SHIPMENT WHERE t_id=pTid;
    
    sumWeight number;
    sumVolume number;
    lvezn number := 0;
    lvez number :=0;
    lflag number := 0;
    
    
    loading_companyId          number :=0; 
    loading_SHIPMENTID         VARCHAR2(4000) ;
    loading_COMPANY_NAME       VARCHAR2(4000) ;
    loading_S_ADDRESS          VARCHAR2(4000) ;
    loading_ZIP                VARCHAR2(4000) ;
    loading_CITY               VARCHAR2(4000) ;
    loading_COUNTRYID          VARCHAR2(4000) ;
    loading_LOADINGRAMP        VARCHAR2(4000) ;
    loading_FROMDATE           DATE           ;
    loading_S_WEIGHT           VARCHAR2(4000) ;
    loading_S_TYPE             VARCHAR2(4000) ; 
    loading_s_volume           VARCHAR2(4000);
    
    unloading_companyId          number :=0;
    unloading_SHIPMENTID         VARCHAR2(4000) ;
    unloading_COMPANY_NAME       VARCHAR2(4000) ;
    unloading_S_ADDRESS          VARCHAR2(4000) ;
    unloading_ZIP                VARCHAR2(4000) ;
    unloading_CITY               VARCHAR2(4000) ;
    unloading_COUNTRYID          VARCHAR2(4000) ;
    unloading_LOADINGRAMP        VARCHAR2(4000) ;
    unloading_FROMDATE           DATE           ;
    unloading_S_WEIGHT           VARCHAR2(4000) ;
    unloading_S_TYPE             VARCHAR2(4000) ; 
    unloading_s_volume           VARCHAR2(4000) ;
    
    companyId                  NUMBER := 0;
    companyTitle                  VARCHAR2(4000):='' ;
    price                      number := 0;
    l_konsig                     number :=0;
   
begin
dbms_output.PUT_LINE('obdelujemo '||pTid);
    SELECT * INTO tour FROM TRANSPOREON_TOUR WHERE t_id=pTid; 
    /*SELECT * INTO loadingStation FROM TRANSPOREON_STATION WHERE t_id=pTid and s_type='loading' and rownum=1;
    SELECT * INTO unLoadingStation FROM TRANSPOREON_STATION WHERE t_id=pTid and s_type='unloading' and rownum=1;
*/

    begin
   --   select sifra,naslov1 into companyId,companyTitle from klienti where sifra =  (SELECT id from metatabela where stevilka1=tour.COMPANY_ID);
       select m.id, k.naslov1 into companyId,companyTitle from METATABELA m left join klienti k on m.id=k.sifra WHERE m.TABELA='KLIENTI' AND m.SIFRA=4 AND m.STEVILKA1=tour.COMPANY_ID;
    exception when others then 
        dbms_output.PUT_LINE(SQLERRM);
        ferbit.ERROR_LOG(1, '',SUBSTR(SQLERRM,1,250),0,'ISKANJE kupca',0);
        companyId :=0;
        companyTitle := tour.COMPANY_ID||'- '||tour.company_name;
    end;

   
    IF(tour.t_status='D') then
        --delete aka STORNO
          UPDATE ferbit.NAKLADI SET faza = 25, kamion='STORNO',cmr='STORNO' WHERE TRIM(EXNAROCILO)=to_char(tour.id);
          
    update transporeon_tour set tour_status=2 where t_id=pTid;
    else
        NULL;
    end if;
    
    
    sumWeight := 0;
    sumVolume := 0;
    begin
        FOR TMP_ship IN shipments LOOP 
            begin 
                begin
                    select vez into lvez from ferbit.nakladi nkl where trim(exoznaka)=to_char(tour.id) and nkl.firma=l_firma and nkl.vez=nkl.vezn;
                exception when others then
                ferbit.ERROR_LOG(1, '',SUBSTR(SQLERRM,1,250),0,'vez',0);
                    lvez:=0;
                end;
             
               if(lvez>0) then
                    begin
                        lvezn := lvez;
                        lflag:=0;
                        price := 0;
                        l_konsig := 0;
                        select ferbit.nklauto.nextval into lvez  from dual;
                    exception when others then
                        lvez:=0;
                    end;
                else
                    select ferbit.nklauto.nextval into lvez  from dual; --nov nalog
                    lvezn :=lvez;
                    lflag:=1;
                    l_konsig := 1;
                    price := to_number(nvl(tour.price_basic,'0'));
                end if;
            
                loading_companyId:=0;
                loading_SHIPMENTID    :='';
                loading_COMPANY_NAME  :='';
                loading_S_ADDRESS     :='';
                loading_ZIP           :='';
                loading_CITY          :='';
                loading_COUNTRYID     :='';
                loading_LOADINGRAMP   :='';
                loading_FROMDATE      :=sysdate;
                loading_S_WEIGHT      :='';
                loading_S_TYPE        :='';
                
                unloading_companyId:=0;
                unloading_SHIPMENTID  :='';
                unloading_COMPANY_NAME:='';
                unloading_S_ADDRESS   :='';
                unloading_ZIP         :='';
                unloading_CITY        :='';
                unloading_COUNTRYID   :='';
                unloading_LOADINGRAMP :='';
                unloading_FROMDATE    :=sysdate;
                unloading_S_WEIGHT    :='';
                unloading_S_TYPE      :=''; 
                
              begin
                select ts.shipmentid,ts.company_name,ts.s_address,ts.zip,ts.city,ts.countryid,ts.loadingramp,ts.fromdate,  ts.s_Type,s.s_weight,s.s_volume 
                into loading_SHIPMENTID,loading_COMPANY_NAME,loading_S_ADDRess,loading_ZIP,loading_CITY ,loading_COUNTRYID,loading_LOADINGRAMP ,loading_FROMDATE,loading_S_TYPE,loading_s_weight,loading_s_volume       
                from transporeon_shipment s, transporeon_station ts 
                where  s.t_id=pTid and ts.t_id=pTid and s.s_number=ts.shipmentid and ts.s_type='loading' and s.id=tmp_ship.id;
              exception when others then
                ferbit.ERROR_LOG(1, '',SUBSTR(SQLERRM,1,250),0,'get naklad',0);
              end;
               
              begin
                select ts.shipmentid, ts. company_name, ts.s_address,  ts.zip,ts.city, ts.countryid, ts.loadingramp, ts.fromdate, ts.s_Type ,s.s_weight,s.s_volume
                into unloading_SHIPMENTID, unloading_COMPANY_NAME, unloading_S_ADDRESS, unloading_ZIP, unloading_CITY, unloading_COUNTRYID,    unloading_LOADINGRAMP, unloading_FROMDATE, unloading_S_TYPE , unloading_S_WEIGHT, unloading_S_volume  
                from transporeon_shipment s, transporeon_station ts 
                where  s.t_id=pTid and ts.t_id=pTid and s.s_number=ts.shipmentid and ts.s_type='unloading' and s.id=tmp_ship.id;
               
              exception when others then
                ferbit.ERROR_LOG(1, '',SUBSTR(SQLERRM,1,250),0,'get razklad',0);
              end;
              --KONSIG=1
              
                 begin
                    select valuta into drzavaNakladSifra from ferbit.sifrant where trim(OZNAKA)=loading_COUNTRYID;
                exception when others then 
                    null;
                end;
            
                 begin
                    select valuta into drzavaRazkladSifra from ferbit.sifrant where trim(OZNAKA)=unloading_COUNTRYID;
                exception when others then 
                    null;
                end;
              
               
              begin
                select valuta into drzavaNakladSifra from ferbit.sifrant where trim(OZNAKA)=loading_COUNTRYID;
              exception when others then 
                    null;
                    
              end;
              
              
              --   dbms_output.PUT_LINE('pred 1');
              TMP_SHIP.comment_plain_text := substr(replace(REPLACE(regexp_replace(TMP_SHIP.comment_plain_text, '<.*?>'),'\n','  '),'&&nbsp;',' '),1,999) ;
                MERGE INTO ferbit.nakladi nkl USING (SELECT 1 FROM DUAL) m 
                     ON ( TRIM(NKL.exoznaka)=tour.id and trim(ext_nsifra1)=tmp_ship.id and nkl.firma=l_firma) 
                    When matched then                                      
                       update set 
                                leto=to_number(to_char(sysdate,'YY')),
                                datumnar_s=sysdate,
                                nnaslov1 = loading_COMPANY_NAME,--naklad
                                nulica= loading_S_ADDRess,
                                nposta=loading_CITY,
                                nstposta=loading_ZIP,
                                OPOMBE_NAKLADA='',--substr(loadingStation.s_description,1,999),
                                datumnak_s=loading_FROMDATE,--2020-05-22T09:45:00+02:00"
                                noznaka=loading_COUNTRYID,
                                ndrzava=drzavaNakladSifra,
                                pnaslov1 = unloading_COMPANY_NAME,--razklad
                                pulica= unloading_S_ADDRESS,
                                pposta=unloading_CITY,
                                pstposta=unloading_ZIP,
                                OPOMBE_RAZKLADA=TMP_SHIP.comment_plain_text,
                                PDATUMRAZ_S=UNLOADING_fromDate,--2020-05-22T09:45:00+02:00"
                                poznaka=UNLOADING_countryid,
                                pdrzava=drzavaRazkladSifra,
                                VOLUMEN=to_number(nvl(tmp_ship.s_volume,'0')),
                                TEZA=to_number(nvl(tmp_ship.s_weight,'0')),
                                STANJE=4,
                                narocnikNaziv=tour.company_name, 
                                kupec=companyId, 
                                narocnik= companyId,
                                KUPECNAZIV=companyTitle,
                                cena = price
                                --nakmeter=shipment.loadingmeters,

                                /*adr=shipment.isdangerousgoods,--is dangerous goods pomoje je adr. v opisu ni ni?
                                p_LATITUDE=to_number(replace(consignee.c_latitude,',','.'),'99999999999999.9999999999999999'),
                                p_LONGITUDE=to_number(replace(consignee.c_longitude,',','.'),'99999999999999.9999999999999999'), 
                                n_latitude=to_number(replace(pickup.p_latitude,',','.'),'99999999999999.9999999999999999'),
                                n_longitude=to_number(replace(pickup.p_longitude,',','.'),'99999999999999.9999999999999999'),

                                dokumenti=substr(shipment.r_reference,1,50), 
                                relacija=relationId,
                                relacijao=relationTitle,
                                blago = tmpOrder.GOODSDESCRIPTION */
                    WHEN NOT MATCHED THEN 
                      INSERT( 
                                vez,
                                vezn,
                                exoznaka,
                                ext_nsifra1,
                                leto,
                                datumnar_s,
                                firma,
                                nnaslov1 ,
                                nulica,
                                nposta,
                                nstposta,
                                OPOMBE_NAKLADA,
                                datumnak_s,
                                noznaka,
                                ndrzava,
                                pnaslov1,
                                pulica,
                                pposta,
                                pstposta,
                                OPOMBE_RAZKLADA,
                                PDATUMRAZ_S,
                                pdrzava,
                                poznaka, 
                                VOLUMEN,
                                TEZA,
                                STANJE ,
                                narocnikNaziv,
                                trenutna_smer,--API od kje je prislo
                                flag,
                                kupec, 
                                narocnik,
                                KUPECNAZIV,
                                konsig,
                                cena,
                                valuta,
                                storitev
                                ) 
                     values(
                     lvez,
                     lvezn,
                            tour.id,
                            tmp_ship.id,
                            to_number(to_char(sysdate,'YY')),
                            sysdate,
                            l_firma,
                            LOADING_company_name,--naklad
                            LOADING_s_address,
                            LOADING_city,
                            LOADING_zip,
                            '',
                            LOADING_FROMDATE,--2020-05-22T09:45:00+02:00"
                            LOADING_countryid,
                            drzavaNakladSifra,
                            UNLOADING_company_name,--razklad
                            UNLOADING_S_address,
                            UNLOADING_city,
                            UNLOADING_ZIP,
                            TMP_SHIP.comment_plain_text,
                            unloading_FROMDATE,--2020-05-22T09:45:00+02:00"
                            drzavaRazkladSifra,
                            unloading_COUNTRYID,
                            to_number(nvl(tmp_ship.s_weight,'0')),
                            to_number(nvl(tmp_ship.s_volume,'0')),
                            4,
                            tour.company_name,
                            2,--transporeon api
                            lflag,
                            companyId, 
                            companyId,
                            companyTitle,
                            l_konsig,
                            price,
                            978,
                            storitevId
                    );
                    
               exception 
                when others then
                dbms_output.PUT_LINE(SQLERRM);
                 ferbit.ERROR_LOG(1, '',SUBSTR(SQLERRM,1,250),0,'shipments loop',0);
             end ;
         END LOOP;
    exception when others then
    sumWeight:=0;
    sumVolume:=0;
        null;
    end;

end;
/

/*nakladi trigger*/
create or replace  TRIGGER "FERBIT"."TBE_NAKLADI" 
BEFORE INSERT OR UPDATE OR DELETE
ON FERBIT.NAKLADI
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
 NAKLAD_c NUMBER(10) := 0; 
 NAKLAD_p NUMBER(10) := 0;
 stdelitev NUMBER(10):= 0;
 l_tip                Char(1);   
 
  l_error        number;
  l_napak        number; 
  l_procedure    CHAR(15):='SEND_MESSAGE';
  sql_error      VARCHAR2(30000); 
BEGIN
 BEGIN
  if(updating) then
  
    if(:new.trenutna_Smer=2) then
        if(:new.kamion<>nvl(:old.kamion,'X')) then 
            TRANSPOREON_ADD_OUTBOUND(:new.exoznaka,TRIM(:new.kamion),'', sysdate);
        end if;
    else
        if(nvl(length(trim(:new.exoznaka)),0)>0) then 
            if(:new.kamion<>nvl(:old.kamion,'X')) then 
                SHIPPEO_ADD_OUTBOUND(:new.exoznaka,TRIM(:new.kamion),'COM/CFM', sysdate);--edi_reference, pVehicle IN VARCHAR2, pEvent IN VARCHAR2
            else
                if(:new.faza<>:old.faza) then -- če se je Faza zamenjala moramo poslati novo fazo
                    if(:new.faza=26) then---  PRISPEL NA NAKLAD
                        SHIPPEO_ADD_OUTBOUND(:new.exoznaka,TRIM(:new.kamion),'EML/ARS',:new.ZAKLJUCENO_S);
                    end if;
                    if(:new.faza=1) then --NALOŽEN
                        SHIPPEO_ADD_OUTBOUND(:new.exoznaka,TRIM(:new.kamion),'ECH/DES',:new.ZAKLJUCENO_S);
                    end if;
                    
                    if(:new.faza=27) then --čaka na razklad
                        SHIPPEO_ADD_OUTBOUND(:new.exoznaka,TRIM(:new.kamion),'MLV/ARS',:new.ZAKLJUCENO_S);
                    end if;
                    
                    if(:new.faza=2) then --razložen
                        SHIPPEO_ADD_OUTBOUND(:new.exoznaka,TRIM(:new.kamion),'LIV/DES',:new.ZAKLJUCENO_S);
                    end if; 
                    
                    if(:new.faza=2) then --razložen
                        SHIPPEO_ADD_OUTBOUND(:new.exoznaka,TRIM(:new.kamion),'LIV/DES',:new.ZAKLJUCENO_S);
                    end if; 
                    
                    if(:new.faza=23) then --prihaja na naklad
                        SHIPPEO_ADD_OUTBOUND(:new.exoznaka,TRIM(:new.kamion),'EML/CFM',:new.ZAKLJUCENO_S);
                    end if; 
                    if(:new.faza=24) then --prijaha na razklad
                        SHIPPEO_ADD_OUTBOUND(:new.exoznaka,TRIM(:new.kamion),'MLV/CFM',:new.ZAKLJUCENO_S);
                    end if; 
                end if;
            end if;
        end if; 
    end if;
  end if; 
 EXCEPTION
    WHEN OTHERS THEN
       l_error   := SQLCODE;
        l_napak   := 1;
         DBMS_OUTPUT.PUT_LINE('SQLERRM urlConnect: '||SQLERRM); 
        Sql_error := SUBSTR(SQLERRM, 1, 250);
        ERROR_LOG(l_error,'SHIPPEO_FAZA',Sql_error,0,l_procedure,0);
 END;
 
 If inserting or updating then 
    NakladiData.v_SteviloVnosov := NakladiData.v_SteviloVnosov + 1;
    NakladiData.v_vez(NakladiData.v_SteviloVnosov) := Nvl(:new.vez,0);
  end if;
 
  If  deleting then 
    NakladiData.v_SteviloVnosov := NakladiData.v_SteviloVnosov + 1;
    NakladiData.v_vez(NakladiData.v_SteviloVnosov) := Nvl(:old.vez,0);
  end if;     
  
 IF INSERTING OR UPDATING THEN
     
      If :new.nalog is null then
       :new.nalog:=0;
     end if;  
      
     -- If  LENGTH(TRIM(NVL(:new.NSTPOSTA,''))) > 0 then
       --:new.dokumenti := LTRIM(TRIM(:new.DOKUMENTI),0) ;
      --end if; 
  
      IF (NVL(:NEW.VEZ,0)=0 OR :NEW.VEZ=0) THEN
       SELECT NKLAUTO.NEXTVAL INTO :NEW.VEZ FROM DUAL;
      END IF;
      
      IF (NVL(:NEW.VEZN,0)=0 OR :NEW.VEZN=0) THEN
       :NEW.VEZN := :NEW.VEZ;
      END IF;
 
      IF :NEW.flag IS NULL  THEN
       :NEW.flag :=1;
      END IF;
     :NEW.FVODELITEV := stdelitev; 
      BEGIN
     SELECT COUNT(1)  INTO :NEW.FVODELITEV FROM FVOZNIKI WHERE vezn = :NEW.VEZ and zaporedje > 0;
      exception
              when others then
                 DBMS_OUTPUT.PUT_LINE('Napaka update'||sqlerrm );
     
    END;
    
       
    
      
      If inserting and nvl(:new.vozilo,0) > 0 then
         begin
             update ferbit.vozilo set status_xypos=0 ,
             OPIS_XYPOS='', DRZAVA_XYPOS= '',DATUM_XYPOS_S=sysdate 
             where sifra=:new.vozilo;     
             exception
              when others then
                 DBMS_OUTPUT.PUT_LINE('Napaka update'||sqlerrm );
            end;
      end if;
    
      If updating and ( :new.faza not in(2,3,8) and :new.faza <> :old.faza and :new.vozilo > 0) then
        begin
            begin
             update ferbit.vozilo set status_xypos=:new.faza,
             OPIS_XYPOS='', DRZAVA_XYPOS= '',DATUM_XYPOS_S=sysdate 
             where sifra=:new.vozilo;   
             exception
              when others then
                 DBMS_OUTPUT.PUT_LINE('Napaka update'||sqlerrm );
            end;
        
             exception
              when others then
                 DBMS_OUTPUT.PUT_LINE('Napaka update'||sqlerrm );
            end;
       
      
      
      end if;
            
      

/*
            SELECT COUNT(1)  INTO NAKLAD_c FROM geo_podatki WHERE geo_podatki.geo_tuj_id = :NEW.VEZ and geo_podatki.geo_tip = 2;
            IF :new.stanje = 1 then -- povpraĹˇevanje ( ga pokaĹľemo)
              
              IF NAKLAD_c = 0 THEN
                INSERT INTO geo_podatki VALUES
                (0,2,:NEW.VEZ ,:NEW.VEZ,SDO_GEOMETRY(2001, 8307,SDO_POINT_TYPE (:new.N_LATITUDE ,:new.N_LONGITUDE ,NULL), NULL, NULL));

              ELSIF NAKLAD_c = 1 THEN
                UPDATE geo_podatki
                SET geo_location = SDO_GEOMETRY(2001, 8307,SDO_POINT_TYPE(:new.N_LATITUDE ,:new.N_LONGITUDE ,NULL), NULL, NULL) 
                WHERE geo_podatki.geo_tuj_id = :NEW.VEZ and geo_podatki.geo_tip = 2;
                END IF;
                
            elsif :new.stanje = 4 then -- nalog ( ga briĹˇemo)
              
              IF NAKLAD_c = 1 THEN
                DELETE FROM GEO_PODATKI WHERE GEO_PODATKI.geo_tuj_id = :NEW.VEZ and geo_podatki.geo_tip = 2;
              END IF;
                
            end if;
            
            SELECT COUNT(1)  INTO NAKLAD_p FROM geo_podatki WHERE geo_podatki.geo_tuj_id = :NEW.VEZ and geo_podatki.geo_tip = 3;
            
            IF :new.stanje = 1 then -- povpraĹˇevanje ( ga pokaĹľemo)
              
              IF NAKLAD_p = 0 THEN
                INSERT INTO geo_podatki VALUES
                (0,3,:NEW.VEZ ,:NEW.VEZ,SDO_GEOMETRY(2001, 8307,SDO_POINT_TYPE (:new.p_LATITUDE ,:new.p_LONGITUDE ,NULL), NULL, NULL));

              ELSIF NAKLAD_p = 1 THEN
                UPDATE geo_podatki
                SET geo_location = SDO_GEOMETRY(2001, 8307,SDO_POINT_TYPE(:new.p_LATITUDE ,:new.p_LONGITUDE ,NULL), NULL, NULL) 
                WHERE geo_podatki.geo_tuj_id = :NEW.VEZ and geo_podatki.geo_tip = 3;
                END IF;
                
            elsif :new.stanje = 4 then -- nalog ( ga briĹˇemo)
              
              IF NAKLAD_p = 1 THEN
                DELETE FROM GEO_PODATKI WHERE GEO_PODATKI.geo_tuj_id = :NEW.VEZ and geo_podatki.geo_tip = 3;
              END IF;
                
            end if;     
            
           --nalogi in fakturirani nalogi  
           SELECT COUNT(1)  INTO NAKLAD_c FROM geo_podatki WHERE geo_podatki.geo_tuj_id = :NEW.VEZ and geo_podatki.geo_tip = 5;
            IF (:new.stanje = 4  or :new.stanje = 6) then -- nalog ( ga pokaĹľemo)
              
              IF NAKLAD_c = 0 THEN
                INSERT INTO geo_podatki VALUES
                (0,5,:NEW.VEZ ,:NEW.VEZ,SDO_GEOMETRY(2001, 8307,SDO_POINT_TYPE (:new.N_LATITUDE ,:new.N_LONGITUDE ,NULL), NULL, NULL));

              ELSIF NAKLAD_c = 1 THEN
                UPDATE geo_podatki
                SET geo_location = SDO_GEOMETRY(2001, 8307,SDO_POINT_TYPE(:new.N_LATITUDE ,:new.N_LONGITUDE ,NULL), NULL, NULL) 
                WHERE geo_podatki.geo_tuj_id = :NEW.VEZ and geo_podatki.geo_tip = 5;
              END IF;
                
            end if;
            
            SELECT COUNT(1)  INTO NAKLAD_p FROM geo_podatki WHERE geo_podatki.geo_tuj_id = :NEW.VEZ and geo_podatki.geo_tip = 6;
            
            IF :new.stanje = 1 then -- nalog ( ga pokaĹľemo)
              
              IF NAKLAD_p = 0 THEN
                INSERT INTO geo_podatki VALUES
                (0,6,:NEW.VEZ ,:NEW.VEZ,SDO_GEOMETRY(2001, 8307,SDO_POINT_TYPE (:new.p_LATITUDE ,:new.p_LONGITUDE ,NULL), NULL, NULL));

              ELSIF NAKLAD_p = 1 THEN
                UPDATE geo_podatki
                SET geo_location = SDO_GEOMETRY(2001, 8307,SDO_POINT_TYPE(:new.p_LATITUDE ,:new.p_LONGITUDE ,NULL), NULL, NULL) 
                WHERE geo_podatki.geo_tuj_id = :NEW.VEZ and geo_podatki.geo_tip = 6;
                 END IF;
               end if;
            */

 END IF; 
-- IF updating and NVL(:old.FAK_STEVEC,0) = 0 and :new.FAK_STEVEC > 0 THEN
--    :NEW.STANJE := 6;
-- END IF;

-- IF UPDATING AND NVL(LENGTH(TRIM(:OLD.FAK_FAKTURA)),0) = 0 AND NVL(LENGTH(TRIM(:NEW.FAK_FAKTURA)),0) > 0 THEN
--    :NEW.STANJE := 6;
-- END IF;
-- IF UPDATING AND NVL(LENGTH(TRIM(:OLD.FAK_FAKTURA)),0) > 0 AND NVL(LENGTH(TRIM(:NEW.FAK_FAKTURA)),0) = 0 THEN
--    :NEW.STANJE := 4;
--    :NEW.FAK_STEVEC := 0;
-- END IF;     
  If deleting then
  
   begin
    delete from fvozniki fvo where FVO.VEZN= :old.vez and fvo.zaporedje > 0;
   exception
              when others then
                 DBMS_OUTPUT.PUT_LINE('Napaka update'||sqlerrm );
            
   end;
  End if;
  
  IF UPDATING AND :NEW.STANJE=2 then
 
   begin
     delete from fvozniki fvo where fvo.vezn=:new.vez and fvo.zaporedje >0; 
     exception
     when others then
     DBMS_OUTPUT.PUT_LINE('Napaka update'||sqlerrm );
   end;
        
 END IF;
 
 
 l_tip := 'L';
 If Inserting then
  If :new.prevoznik = 0 then 
   :new.faza := -1;
  end if;
  
   If :new.prevoznik > 1 then 
   :new.faza := 0;
  end if;
  If :new.zaporedje = -1 then 
    :new.faza := 8;
    l_tip := 'F';
  end if;
  
 
   
  Begin
   INSERT INTO NAKLADILOG (
   AVEZ,TIP, DATUM, FIRMA, 
   FAZA, NKL_VEZ, PON_VEZ, 
   FVO_SIFRA, ZBI_VEZ, DOGODEK, 
   BOOKINGOPIS, FAKTURA, POSTAVKA, 
   VOZNIK, VOZNIKOPIS, PROCENT, 
   ZNESEK, VOZILO, PREVOZNIK, 
   VOZILOOPIS, PRIKOLICA, PRIKOLICAOPIS, 
   STATUS, KILOMETRI, PRAZNIKM, 
   DRZAVA, LATITUDE, LONGITUDE, 
   ZAPOREDJE, NAKLADA, KOLICINA, 
   TEZA, CMR,  SOVOZNIK, 
   FAZAP, BKGKLJUC, VEZPOG, 
   PGPVEZ, POGNACIN, PREJEMNIK, 
   PNASLOV1, SKLADISCE ) 
   VALUES (
    null,
    l_tip,
    sysdate,
   :new.FIRMA,
   :new.FAZA,
   :new.vez,
    0,
   0,
   0,
    Decode(:new.zaporedje,1,1,2),
    Decode(:new.faza,-1,'Nepokrit',0,'Pripravljen',8,'Fakturiran')||' - '||Decode(:new.zaporedje,1,'Zapis novega naloga',-1,'Zapis fakture','Dodajanje nove delitev za nalog'),
    0 ,  --faktura
    0, /* POSTAVKA */
   :new.voznik,       /*VOZNIK */
   :new.vozniko,   /* VOZNIKOPIS */
   0 ,     /* PROCENT */
   :new.cena,       /* ZNESEK */
   :new.vozilo,       /* VOZILO */
   :new.prevoznik,    /* PREVOZNIK */
   :new.kamion,   /* VOZILOOPIS */
   :new.prikolica,     /* PRIKOLICA */
   :new.PRIKOLICAO, /* PRIKOLICAOPIS    */
   :new.STATUS,        /* STATUS */
   :new.KILOMETRI,     /* KILOMETRI */
   :new.PRAZNIKM,      /* PRAZNIKM */
   :new.ndrzava,        /* DRZAVA */
   :new.n_LATITUDE,     /* LATITUDE */
   :new.n_LONGITUDE,      /* LONGITUDE */
    0,      /* ZAPOREDJE */
   :new.NAKLADA ,       /* NAKLADA */
   :new.KOLICINA,        /* KOLICINA */
   :new.TEZA,             /* TEZA */
   :new.CMR,          /* CMR */
   :new.sovoznik,  /* SOVOZNIK */
   :new.fazap ,    /* FAZAP */
   '',  /* BKGKLJUC */
   :new.vezpog,     /* VEZPOG */
   :new.pgpvez,     /* PGPVEZ */
   :new.pognacin,    /* POGNACIN */
   :new.prejemnik,   /* PREJEMNIK */
   :new.pnaslov1,    /* PNASLOV1 */
   :new.skladisce   /* SKLADISCE */
    );/* ZASKLADISCE */ 
    End;
 end if;
  
     
  END;
  /

  /*add data to out table*/
  create or replace  procedure TRANSPOREON_ADD_OUTBOUND(pEdiReference IN VARCHAR2, pVehicle IN VARCHAR2, pEvent IN VARCHAR2, pDate IN date) IS
BEGIN

/*

POD zahteva
{

    "inputDate":"2021-04-16T12:10:40+0200",
    "label":"/Users/matejl/Desktop/vozila.pdf", 
    "ediReference":"23W65M92",
    "token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2MTg4MTkzOTMsImhhc2hpZCI6IkcyWTdMWjMyIiwiZXhwIjoxNjE4OTkyMTkzfQ.SUP6JO8sp8SkiM4Aa51lIkCpfLjoO_xTEabj7l4ER5d3gH_VvxcQxQlG9gqooO-zS2EsBeDId6FDFDMH6qSGFmHwMviH5kMK_rKFxkwQkrTwcbhkx9AuumrvDV0Cab0iX0CL4434PwXUJn8CGsZWB0lPu47ilI4fe-VSYPWouKgRmfAE2GszUvvVcng_UvK6UHSjd2bNzlH-lpcRUe-Ed2E38o9UlWRMk2TmqVrVDsbrpTS8w59T7QsaSAj7h1PQMYiRa5K42KZiwww0MtTUMLmpY9VxQLElMlQbPx_cHsiofC_yNDBWn0lkG4QaYyO4hMNDmM_qv-YcHtXtLv9FBhwnreIMHS0HFBYOvVK9GGCFmzJOe5tcY6-fzhZovJ0BTIhNKG6f-nsIcNAKkG7peMDKCMkgsOztca59UNeKEDld0fdv9FRzFl9McvcR_QLWQ1XUEJyTufYIpMfSYOOzCOeu4eqt4WDB0HNe3PTsNriJMdU3DPsAVQNh_f9Y6PAo2AAPWQSRuZuEk67ORYo1r_5oY-EnhEL1q7_4FP_heP1ZWF2I5CscZ_nWofw9rX9em-Wv9UDdaOJlOV8VoWwWcqTqmWfEFDCqJ09hWqzEZWQ98CDODm5aso3Ea2C7cZTcYLviO96InefUcyXGvYA7pbKavgpmiuUxOnssuJayiJk"
} 

*/

    insert into SHIPPEO_OUTBOUND_DATA(
        input_date, 
        s_label, 
        event, 
        telematic_unit_id, 
        telematic_configuration, 
        edi_reference,
        status ,
        l_date,
        partner
    ) values(
        to_char(sysdate,'yyyy-mm-dd"T"hh24:mi:ss"+02:00"'),
        pVehicle,---label vozilo
        pEvent,--COM/CFM če je pair
        pVehicle,--vozilo
        pVehicle,--vozilo
        pEdiReference,--edireference
        0,
        to_char(pDate,'yyyy-mm-dd"T"hh24:mi:ss"+02:00"'),
        'TRANSPOREON'
    );
END;
/