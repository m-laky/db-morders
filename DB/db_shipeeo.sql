--------------------------------------------------------
--  DDL for Table SHIPEEO_AMOUNT
--------------------------------------------------------

  CREATE TABLE SHIPEEO_AMOUNT 
   (	T_ID NUMBER, 
	A_VALUE NUMBER, 
	CURRENCY VARCHAR2(200 BYTE), 
	QUALIFIER VARCHAR2(500 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table SHIPEEO_CARRIER
--------------------------------------------------------

  CREATE TABLE SHIPEEO_CARRIER 
   (	T_ID NUMBER, 
	C_IDENTIFIER VARCHAR2(200 BYTE), 
	QUALIFIER VARCHAR2(200 BYTE)
   ) ;
 
--------------------------------------------------------
--  DDL for Table SHIPEEO_CHARGES
--------------------------------------------------------

  CREATE TABLE SHIPEEO_CHARGES 
   (	T_ID NUMBER, 
	BILLING_ACCOUNT VARCHAR2(2000 BYTE), 
	SERVICECHARGE VARCHAR2(2000 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table SHIPEEO_CONSIGNEE
--------------------------------------------------------

  CREATE TABLE SHIPEEO_CONSIGNEE 
   (	T_ID NUMBER, 
	C_NAME VARCHAR2(4000 BYTE), 
	C_ADDRESS VARCHAR2(4000 BYTE), 
	C_COUNTRY VARCHAR2(4000 BYTE), 
	C_POSTAL_CODE VARCHAR2(4000 BYTE), 
	C_CITY VARCHAR2(4000 BYTE), 
	C_LATITUDE NUMBER, 
	C_LONGITUDE NUMBER, 
	C_INSTRUCTIONS VARCHAR2(4000 BYTE), 
	C_APPOINTMENT NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table SHIPEEO_DATE
--------------------------------------------------------

  CREATE TABLE SHIPEEO_DATE 
   (	T_ID NUMBER, 
	QUALIFIER VARCHAR2(4000 BYTE), 
	DATE_TIME VARCHAR2(4000 BYTE), 
	D_TABLE VARCHAR2(2 BYTE)
   );
--------------------------------------------------------
--  DDL for Table SHIPEEO_IDENTIFICATION
--------------------------------------------------------

  CREATE TABLE SHIPEEO_IDENTIFICATION 
   (	T_ID NUMBER, 
	I_IDENTIFIER VARCHAR2(4000 BYTE), 
	I_QUALIFIER VARCHAR2(4000 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table SHIPEEO_LINKEDSHIPMENT
--------------------------------------------------------

  CREATE TABLE SHIPEEO_LINKEDSHIPMENT 
   (	T_ID NUMBER, 
	L_IDENTIFIER VARCHAR2(4000 BYTE), 
	L_DIRECTION VARCHAR2(4000 BYTE), 
	L_QUALIFIER VARCHAR2(4000 BYTE)
   );
--------------------------------------------------------
--  DDL for Table SHIPEEO_META
--------------------------------------------------------

  CREATE TABLE SHIPEEO_META 
   (	T_ID NUMBER, 
	SENDERID VARCHAR2(4000 BYTE), 
	DUPLICATERECEIVERID VARCHAR2(4000 BYTE), 
	MESSAGETYPE VARCHAR2(4000 BYTE), 
	MESSAGEDATE VARCHAR2(4000 BYTE), 
	MESSAGEFUNCTION VARCHAR2(4000 BYTE), 
	MESSAGEREFERENCE VARCHAR2(4000 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table SHIPEEO_ORDER
--------------------------------------------------------

  CREATE TABLE SHIPEEO_ORDER 
   (	T_ID NUMBER, 
	GOODSDESCRIPTION VARCHAR2(4000 BYTE), 
	STATUS NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table SHIPEEO_PACKING
--------------------------------------------------------

  CREATE TABLE SHIPEEO_PACKING 
   (	T_ID NUMBER, 
	HANDLINGUNITNUMBER NUMBER, 
	P_TYPE VARCHAR2(4000 BYTE), 
	P_CUSTOMTYPE VARCHAR2(4000 BYTE), 
	P_GROSSWEIGHT VARCHAR2(4000 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table SHIPEEO_PICKUP
--------------------------------------------------------

  CREATE TABLE SHIPEEO_PICKUP 
   (	T_ID NUMBER, 
	P_NAME VARCHAR2(4000 BYTE), 
	P_ADDRESS1 VARCHAR2(4000 BYTE), 
	P_COUNTRY VARCHAR2(4000 BYTE), 
	P_POSTALCODE VARCHAR2(4000 BYTE), 
	P_CITY VARCHAR2(4000 BYTE), 
	P_LATITUDE VARCHAR2(4000 BYTE), 
	P_LONGITUDE NUMBER, 
	P_INSTRUCTIONS VARCHAR2(4000 BYTE), 
	P_APPOINTMENT NUMBER
   );
   COMMENT ON COLUMN SHIPEEO_PICKUP.P_APPOINTMENT IS '0-false,1-true';
--------------------------------------------------------
--  DDL for Table SHIPEEO_QUANTITY
--------------------------------------------------------

  CREATE TABLE SHIPEEO_QUANTITY 
   (	T_ID NUMBER, 
	GROSSWEIGHT NUMBER, 
	GROSSVOLUME NUMBER, 
	LOADINGMETERS NUMBER, 
	PALLETGROUND VARCHAR2(4000 BYTE), 
	FULLLOAD NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table SHIPEEO_REFERENCE
--------------------------------------------------------

  CREATE TABLE SHIPEEO_REFERENCE 
   (	T_ID NUMBER, 
	QUALIFIER VARCHAR2(4000 BYTE), 
	R_REFERENCE VARCHAR2(2000 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table SHIPEEO_SHIPMENT
--------------------------------------------------------

  CREATE TABLE SHIPEEO_SHIPMENT 
   (	T_ID NUMBER, 
	TECHNICALREFERENCE VARCHAR2(4000 BYTE), 
	R_REFERENCE VARCHAR2(4000 BYTE), 
	ISDANGEROUSGOODS NUMBER, 
	R_TYPE VARCHAR2(4000 BYTE), 
	TRANSPORTMODE VARCHAR2(4000 BYTE), 
	STATUS NUMBER, 
	STATUS_TEXT VARCHAR2(1000 BYTE), 
	INSERTED DATE DEFAULT SYSDATE, 
	ENDED_DATE DATE
   ) ;
--------------------------------------------------------
--  DDL for Table SHIPEEO_TRANSPORTMEAN
--------------------------------------------------------

  CREATE TABLE SHIPEEO_TRANSPORTMEAN 
   (	T_ID NUMBER, 
	CODE VARCHAR2(4000 BYTE), 
	QUALFIER VARCHAR2(4000 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table SHIPEEO_TRANSPORTSERVICEBUYER
--------------------------------------------------------

  CREATE TABLE SHIPEEO_TRANSPORTSERVICEBUYER 
   (	T_ID NUMBER, 
	T_IDENTIFIER VARCHAR2(4000 BYTE), 
	T_QUALIFIER VARCHAR2(4000 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table SHIPPEO_OUTBOUND_DATA
--------------------------------------------------------

  CREATE TABLE SHIPPEO_OUTBOUND_DATA 
   (	INPUT_DATE VARCHAR2(1000 BYTE), 
	S_LABEL VARCHAR2(1000 BYTE), 
	EVENT VARCHAR2(1000 BYTE), 
	TELEMATIC_UNIT_ID VARCHAR2(1000 BYTE), 
	TELEMATIC_CONFIGURATION VARCHAR2(1000 BYTE), 
	EDI_REFERENCE VARCHAR2(1000 BYTE), 
	STATUS NUMBER, 
	ID NUMBER, 
	L_DATE VARCHAR2(1000 BYTE),
	partner varchar2(200 byte)
   ) ;
--------------------------------------------------------
--  DDL for Index SHIPPEO_INBOUND_DATA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX SHIPPEO_INBOUND_DATA_PK ON SHIPPEO_OUTBOUND_DATA (ID);
--------------------------------------------------------
--  DDL for Trigger SHIPPEO_OUTBOUND_TGR
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER SHIPPEO_OUTBOUND_TGR 
   before insert ON SHIPPEO_OUTBOUND_DATA 
   for each row
begin  
   if inserting then 
      if :NEW.ID is null then 
         select SHIPPEO_OUTBOUND_SEQ.nextval into :NEW.ID from dual; 
      end if; 
   end if; 
   
   begin
   if( INSTR(:new.s_label, 'T:\WinPro') >0) then
        :new.s_label := replace(:new.s_label,'T:\WinPro','D:\FERBIT\WinPro');
        :new.TELEMATIC_UNIT_ID := replace(:new.s_label,'T:\WinPro','D:\FERBIT\WinPro');
   end if;
   exception 
   when others then
    null;
   end ;
end;
/
ALTER TRIGGER SHIPPEO_OUTBOUND_TGR ENABLE;
--------------------------------------------------------
--  Constraints for Table SHIPPEO_OUTBOUND_DATA
--------------------------------------------------------

  ALTER TABLE SHIPPEO_OUTBOUND_DATA ADD CONSTRAINT SHIPPEO_INBOUND_DATA_PK PRIMARY KEY (ID);
  ALTER TABLE SHIPPEO_OUTBOUND_DATA MODIFY (ID NOT NULL ENABLE);