
  CREATE TABLE PRONET_DOCUMENTS
   (	ID NUMBER, 
	   STATUS NUMBER, 
	   DATE_ADDED DATE, 
	   DATE_FINISHED DATE, 
	   PARTNER VARCHAR2(1000 BYTE), 
	   INVOICE_NUMBER VARCHAR2(1000 BYTE), 
	   INVOICE_DATE VARCHAR2(1000 BYTE), 
	   INVOICE_AMOUNT VARCHAR2(1000 BYTE), 
	   STATUS_DESCRIPTION VARCHAR2(1000 BYTE)
   ) 

create or replace function cloblengthb(p_clob in clob ) return number
as
  v_temp_blob BLOB;
  v_dest_offset NUMBER := 1;
  v_src_offset NUMBER := 1;
  v_amount INTEGER := dbms_lob.lobmaxsize;
  v_blob_csid NUMBER := dbms_lob.default_csid;
  v_lang_ctx INTEGER := dbms_lob.default_lang_ctx;
  v_warning INTEGER;
  v_total_size number := 0; -- Return total clob length in bytes
BEGIN
  IF p_clob is not null THEN
    DBMS_LOB.CREATETEMPORARY(lob_loc=>v_temp_blob, cache=>TRUE);
    DBMS_LOB.CONVERTTOBLOB(v_temp_blob, p_clob,v_amount,v_dest_offset,v_src_offset,v_blob_csid,v_lang_ctx,v_warning);
    v_total_size := DBMS_LOB.GETLENGTH(v_temp_blob);
    DBMS_LOB.FREETEMPORARY(v_temp_blob);
  ELSE
    v_total_size := NULL;
  END IF;
  return v_total_size;
end cloblengthb;
/
create or replace function  base64DecodeClobAsBlob_plsql(i_data_cl CLOB) 
return blob is
  v_out_bl blob;
  clob_size number;
  pos number;
  charBuff varchar2(32767);
  dBuffer RAW(32767);
  v_readSize_nr number;
  v_line_nr number;
begin
  dbms_lob.createTemporary
    (v_out_bl, true, dbms_lob.call);
  v_line_nr:=greatest(65, 
                 instr(i_data_cl,chr(10)),
                 instr(i_data_cl,chr(13)));
  v_readSize_nr:=
      floor(32767/v_line_nr)*v_line_nr;
  clob_size := dbms_lob.getLength(i_data_cl);
  pos := 1;

  WHILE (pos < clob_size) LOOP
    dbms_lob.read
      (i_data_cl, v_readSize_nr, pos, charBuff);
    dBuffer := UTL_ENCODE.base64_decode
      (utl_raw.cast_to_raw(charBuff));
    dbms_lob.writeAppend
     (v_out_bl,utl_raw.length(dBuffer),dBuffer);
    pos := pos + v_readSize_nr;
  end loop;
  return v_out_bl;
end;
/
   create or replace procedure url_connect_soap(pUrl in varchar2,pContent IN CLOB, pMethod IN VARCHAR2, pOut OUT CLOB, auth in varchar2 default '', contentType in varchar2 default 'application/json;charset=utf-8', soapAction in varchar2 )IS 
/*
      mmessage_id   -  id sporočila
      muser_id      -  za koga je sprocilo; lahko je id uporabnika ali vozila(odvisno kak mMessageType pošiljamo)
      mtype         -  tip sporocila; 0 sinhronizacija; 1 sporočilo
      mcontent      -  vsebina
      mfromuser     -  od koga je sporocilo 1-centrala
      msendeddate   -  datum posiljanja
      mMessageType  -  če gre za nalog 0; če je sporčilo 1
      */
  t_http_req     utl_http.req; 
  t_http_resp    utl_http.resp;  
  t_respond      VARCHAR2(30000); 
  t_start_pos    INTEGER := 1; 
  t_output       VARCHAR2(2000); 
  l_mcontent     varchar2(10000);
  t_device       VARCHAR2(2000); 
  t_data         VARCHAR(30000); 
  sql_error      VARCHAR2(30000); 
  l_error        number;
  l_napak        number; 
  l_procedure    CHAR(15):='SEND_MESSAGE';
  buffer varchar2(4000); 
  response_Text  varchar2(32000);
   l_buffer     varchar2(32767);
   l_length      number:=0;
   l_offset number:=1; 
   amount number :=32767;
BEGIN      
     l_length :=  DBMS_LOB.GETLENGTH(pContent);

    --    dbms_output.put_line('length '||cloblengthb(pContent));
  -- UTL_HTTP.SET_DETAILED_EXCP_SUPPORT(TRUE); 
    --utl_http.set_wallet ('file:/home/oracle/wallet', 'password123');
    utl_http.Set_body_charset('UTF-8'); 
    utl_http.Set_transfer_timeout(50); 
    t_http_req := utl_http.Begin_request(pUrl , pMethod); 
    utl_http.Set_header(t_http_req, 'Content-Type',contentType); 
    utl_http.Set_header(t_http_req, 'Connection', 'keep-alive');  
    utl_http.set_header(t_http_req,'SOAPAction',soapAction);
    if(nvl(length(trim(auth)),0)<>0) then
        utl_http.Set_header(t_http_req, 'Authorization', auth);  
    end if;


    if(trim(pMethod)='POST') then
        if(l_length>0) then 

            utl_http.Set_header(t_http_req, 'Content-Length',cloblengthb(pContent)); 
            --utl_http.Write_text(t_http_req, pContent); 
            l_length := DBMS_LOB.GETLENGTH(pContent);
            while(l_offset < l_length) 
                          loop
                             dbms_lob.read(pContent, amount, l_offset, l_buffer);
                             UTL_HTTP.WRITE_TEXT(r    => t_http_req, data => l_buffer);
                             l_offset := l_offset + amount;

                          end loop;

        else
            utl_http.Set_header(t_http_req, 'Content-Length',0); 
        end if;
    else
        utl_http.Set_header(t_http_req, 'Content-Length',0); 
    end if ;

    t_http_resp := utl_http.Get_response(t_http_req); 
    dbms_lob.createtemporary(pOut, false);

     begin
        loop
          utl_http.read_text(t_http_resp, l_buffer, 32000);
          dbms_lob.writeappend (pOut, length(l_buffer), l_buffer);
        end loop;
      exception
        when utl_http.end_of_body then
          utl_http.end_response(t_http_resp);
      end; 

   -- dbms_output.put_line('response raw '||pout);
  /*  EXCEPTION 
    WHEN OTHERS THEN 
         l_error   := SQLCODE;
        l_napak   := 1;
         DBMS_OUTPUT.PUT_LINE('SQLERRM urlConnect: '||SQLERRM); 
        Sql_error := SUBSTR(SQLERRM, 1, 250);
        ERROR_LOG(l_error,'getCVSVehicle',Sql_error,0,l_procedure,0);*/
END;

create or replace  procedure pronet_login (p_token out varchar2 , username in varchar2, pass in varchar2) is
    req clob := '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://analytics.program.pronet-kr.si/api/soap/">
   <soapenv:Header/>
   <soapenv:Body>
      <soap:Login soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <username xsi:type="xsd:string">'||username||'</username>
         <password xsi:type="xsd:string">'||pass||'</password>
      </soap:Login>
   </soapenv:Body>
</soapenv:Envelope>';

pout clob;
documentIds clob;
token varchar2(32767):='teest';
v_count number := 0;
begin 
    url_connect_soap('http://127.0.0.1/pronet/api/soap/knjiga-poste.php',req,'POST',pout,'','text/xml','http://analytics.program.pronet-kr.si/api/soap/#Login');
--dbms_output.put_line(pout);
     SELECT extractValue( xmltype(pout)
                ,'/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:LoginResponse/token'
                           ,'xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://analytics.program.pronet-kr.si/api/soap/"') into token FROM dual;
    
    p_token := token;
end;




create or replace  procedure pronet_get_documentsIds(ptoken in varchar2, p_taxNumber in varchar2) as 
req clob;
pout clob;
documentIds clob;
token varchar2(32767):='teest';
v_count number := 0;
begin  
    req := '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://analytics.program.pronet-kr.si/api/soap/">
           <soapenv:Header/>
           <soapenv:Body>
              <soap:GetDocumentIDs soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                 <token xsi:type="xsd:string">'||Ptoken||'</token>
                      <request>
                    <![CDATA[ 
                 <request> 
                 <requestID>1031</requestID>
                 <VATID>'||p_taxNumber||'</VATID>
                 <documentType>1</documentType>
                   <returnErrors>1</returnErrors>
                    <returnAttachments>0</returnAttachments>
                 <returnTexts>0</returnTexts>
                 <changeStatus>0</changeStatus>
              </request>]]>
                     </request>
              </soap:GetDocumentIDs>
           </soapenv:Body>
        </soapenv:Envelope>';

    url_connect_soap('http://127.0.0.1/pronet/api/soap/knjiga-poste.php',req,'POST',pout,'','text/xml','http://analytics.program.pronet-kr.si/api/soap/#GetDocumentIDs');


DBMS_OUTPUT.PUT_LINE(pout);
    SELECT extractValue( xmltype(pout)
            ,'/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:GetDocumentIDsResponse/ids'
                       ,'xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://analytics.program.pronet-kr.si/api/soap/"') into documentIds FROM dual;

DBMS_OUTPUT.PUT_LINE(documentIds);
    FOR r IN (SELECT *
         FROM   XMLTABLE('//answer/documents/document'
                         PASSING XMLTYPE(documentIds)
                         COLUMNS partner varchar2(4000) PATH './partner', 
                                 "number" varchar2(4000) PATH './number',
                                 "date" varchar2(4000) PATH './date',
                                 "amount"  varchar2(4000) PATH './amount',
                                  id_r varchar2(4000) PATH './@ID'
                         ))
    LOOP
        insert into pronet_documents(id, status, date_added, partner, invoice_number, invoice_date, invoice_amount) values(r.id_r, -1, sysdate, r.partner,r."number", r."date", r."amount"); 
    END LOOP;
commit;
end;



create or replace  procedure pronet_get_documents_for_download is
    p_token varchar2(4000);
    p_user varchar2(10000) := 'invoice@comark.si';
    p_pass varchar2(10000) := 'MDFpbnZvaWNlMTQwNA==';
    p_tax varchar2(10000) := 'SI35304405';
BEGIN
    pronet_login (p_token,p_user,p_pass);

    pronet_get_documentsIds(p_token,p_tax); 
END;



create or replace  procedure pronet_get_document(p_token in varchar2, p_number in number) is
    req clob := '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://analytics.program.pronet-kr.si/api/soap/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <soap:GetDocument soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                       <token>'||p_token||'</token>
                           <request>
                            <![CDATA[ 
                           <request>
                           <requestID>160023</requestID>
                           <documentID>'||p_number||'</documentID>
                        </request>]]>
                        </request>
                      </soap:GetDocument>
                   </soapenv:Body>
                </soapenv:Envelope>';

    pout clob;
    xmlDoc xmltype;
    l_document clob; 
    v_count number := 0;
    l_status number := -1;
    l_description varchar2(2000):='';
begin 
    url_connect_soap('http://127.0.0.1/pronet/api/soap/knjiga-poste.php',req,'POST',pout,'','text/xml','http://analytics.program.pronet-kr.si/api/soap/#GetDocument');
   --insert into clob_test (a ) values(pout);
     FOR r IN (SELECT *
         FROM   XMLTABLE( 
                 Xmlnamespaces(
                 'http://schemas.xmlsoap.org/soap/envelope/' as "SOAP-ENV"
               , 'http://analytics.program.pronet-kr.si/api/soap/' as "ns1"
               ),
          
         '//SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:GetDocumentResponse/document'
                         PASSING xmltype(pout)
                         COLUMNS textt clob PATH '.'
                         ))
                        
    LOOP
    
        xmlDoc  := xmltype(r.textt);
     
        FOR r IN (SELECT *
             FROM   XMLTABLE('//answer/documents/document/status'
                             PASSING xmlDoc
                             COLUMNS status number PATH './code',
                                     descriptio varchar2(2000) PATH './description'
                                    
                             ))
        LOOP
            if(r.status=0) then
                /*PDF DATOTEKA*/
                 FOR d IN (SELECT *
                     FROM   XMLTABLE('//answer/documents/document'
                                     PASSING xmlDoc
                                     COLUMNS eslog clob PATH './eslog',
                                             attach clob PATH './attachment',
                                             attach_name varchar2(1000) PATH './attachment/@name'
                                     ))
                    LOOP
                     dbms_output.put_line(replace(upper(d.attach_name),'.PDF','.XML'));
                     dbms_output.put_line(upper(d.attach_name));
                     write_to_file('ERACUNI', replace(upper(d.attach_name),'.PDF','.XML'),d.eslog);
                     write_to_file('ERACUNI', upper(d.attach_name),d.attach);
                    END LOOP;
            end if ;
            update pronet_documents set status=r.status, STATUS_DESCRIPTION=r.descriptio where id=p_number;
        END LOOP;
          
     end loop;
    
    
end;


create or replace  procedure pronet_download_documents is
    p_token varchar2(4000);
    cursor not_download is select * from pronet_documents where status=-1;
BEGIN
    pronet_login (p_token,'invoice@comark.si','MDFpbnZvaWNlMTQwNA==');
    for r in not_download 
    loop
        pronet_get_document(p_token,r.id);
    end loop;
END;