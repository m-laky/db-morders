BEGIN
  DBMS_NETWORK_ACL_ADMIN.create_acl (
    acl          => 'ferbit.xml',
    description  => 'A test of the ACL functionality',
    principal    => 'FERBIT',
    is_grant     => TRUE,
    privilege    => 'connect',
    start_date   => null,
    end_date     => NULL);
 
  COMMIT;
END;
/
 
BEGIN
  DBMS_NETWORK_ACL_ADMIN.add_privilege (
    acl         => 'ferbit.xml',
    principal   => 'FERBIT',
    is_grant    => true,
    privilege   => 'connect',
    position    => NULL,
    start_date  => sysdate,
    end_date    => NULL);
 
  COMMIT;
END;
/
 
BEGIN
  DBMS_NETWORK_ACL_ADMIN.assign_acl (
    acl         => 'ferbit.xml',
    host        => '*.ferbit.net');
    commit;
   
END;

40 teg
/*

select


'ALTER SYSTEM KILL SESSION '''||b.sid||','||b.serial#||''' IMMEDIATE;'

 
from
 v$locked_object a ,
 v$session b,
 dba_objects c
where
 b.sid = a.session_id
and
 a.object_id = c.object_id;
 
 
*/


SELECT PRINCIPAL, HOST, lower_port, upper_port, acl, 'connect' AS PRIVILEGE, 
    DECODE(DBMS_NETWORK_ACL_ADMIN.CHECK_PRIVILEGE_ACLID(aclid, PRINCIPAL, 'connect'), 1,'GRANTED', 0,'DENIED', NULL) PRIVILEGE_STATUS
FROM DBA_NETWORK_ACLS
    JOIN DBA_NETWORK_ACL_PRIVILEGES USING (ACL, ACLID)  
UNION ALL
SELECT PRINCIPAL, HOST, NULL lower_port, NULL upper_port, acl, 'resolve' AS PRIVILEGE, 
    DECODE(DBMS_NETWORK_ACL_ADMIN.CHECK_PRIVILEGE_ACLID(aclid, PRINCIPAL, 'resolve'), 1,'GRANTED', 0,'DENIED', NULL) PRIVILEGE_STATUS
FROM DBA_NETWORK_ACLS
    JOIN DBA_NETWORK_ACL_PRIVILEGES USING (ACL, ACLID);
 