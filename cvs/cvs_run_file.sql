create or replace procedure        cvs_run_files_sync AS
    l_mapa varchar(4000);
    dateFrom date;
    dateTo date;
    token varchar2(4000);
    l_tip_dokumenta number:=14;
  l_cvsDelay   number:=5*60;
l_runned date;
BEGIN  
    dbms_output.put_line('test');
     BEGIN
        select stevilo into l_cvsDelay from nastavi where trim(kljuc)='CVS_DELAY';
       -- dbms_output.put_line(to_char(l_date_sync,'dd.mm.yyy hh24:mi:ss'));
    EXCEPTION WHEN OTHERS THEN
        l_cvsdelay := 5*60;
        --dbms_output.put_line('napaka');
     END;
begin
   SYS.DBMS_SCHEDULER.DROP_JOB (job_name => 'cvs_get_files_job');
exception when others then  
null;
end;
   -- select pot into l_mapa from FERBIT.doc_tip where vez=141;  ---l_tip_dokumenta;
    l_mapa:='C:\WinPro\WinPro\PDF_IZPISI\CMR\';
    select to_date(tekst,'yyyy-mm-dd hh24:mi:ss') into DateFrom from FERBIT.nastavi where  kljuc='CVS_DOK_PRENOS_DATUM'; 
    DateFrom:= DateFrom - ((l_cvsDelay/60)/1440);
    select tekst into token from FERBIT.nastavi where  kljuc='CVSToken_doc'; 
    dateTo := sysdate;
   -- dateTo := to_date('03.03.2021','dd.mm.yyyy');
         dbms_output.put_line('"jdbc:oracle:thin:ferbit/ferbit@127.0.0.1:1521:XE;documents;'||token||';'||l_tip_dokumenta||';'||to_char(dateFrom,'yyyy-mm-dd"T"hh24:mi:ss')||';'||to_char(dateTo,'yyyy-mm-dd"T"hh24:mi:ss')||';'||l_mapa||';C:\WinPro\WinPro\PDF_IZPISI\CMR\;"');
    DBMS_SCHEDULER.create_job ('cvs_get_files_job',
                              job_action            => 'C:\WINDOWS\SYSTEM32\CMD.EXE',
                              number_of_arguments   => 4,
                              job_type              => 'executable',
                              enabled               => FALSE);

    DBMS_SCHEDULER.set_job_argument_value ('cvs_get_files_job', 1, '/q');

    DBMS_SCHEDULER.set_job_argument_value ('cvs_get_files_job', 2, '/c');

    DBMS_SCHEDULER.set_job_argument_value ('cvs_get_files_job', 3, 'C:\WinPro\WinPro\cvs\run.bat');

    DBMS_SCHEDULER.set_job_argument_value ('cvs_get_files_job', 4, '"jdbc:oracle:thin:ferbit/ferbit@127.0.0.1:1521:XE;documents;'||token||';'||l_tip_dokumenta||';'||to_char(dateFrom,'yyyy-mm-dd"T"hh24:mi:ss')||';'||to_char(dateTo,'yyyy-mm-dd"T"hh24:mi:ss')||';'||l_mapa||';C:\WinPro\WinPro\PDF_IZPISI\CMR\;"');

    DBMS_SCHEDULER.enable ('cvs_get_files_job');

    dbms_scheduler.run_job('cvs_get_files_job',TRUE);  
    --zakljui datum
    update FERBIT.nastavi set tekst= to_char(DateTo,'YYYY-MM-DD HH24:MI:SS') where kljuc='CVS_DOK_PRENOS_DATUM';
    commit;
END; 