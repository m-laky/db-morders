drop table orders;
CREATE TABLE ORDERS (
    ORDER_ID NUMBER DEFAULT 0, 
    WORKER_ID NUMBER DEFAULT 0, 
    ORDER_DATE DATE, 
    DELIVERY_DATE DATE, 
    COMPLETE_DATE DATE, 
    OTYPE NUMBER DEFAULT 0, 
    TYPE_D VARCHAR2(150 BYTE) DEFAULT 0, 
    CNT NUMBER DEFAULT 0, 
    VEHICLE_ID NUMBER DEFAULT 0, 
    VEHICLE VARCHAR2(20 BYTE) DEFAULT 0, 
    TERMINAL CHAR(1000 BYTE) DEFAULT 0, 
    SEQ NUMBER DEFAULT 0, 
    COMPANY NUMBER DEFAULT 0, 
    BUSSINES_UNIT NUMBER DEFAULT 0, 
    CUSTOMER NUMBER DEFAULT 0, 
    LOC VARCHAR2(40 BYTE) DEFAULT 0, 
    DESC_CODE VARCHAR2(30 BYTE) DEFAULT 0, 
    ACTIVE NUMBER DEFAULT 0, 
    SCANED_COUNT NUMBER DEFAULT 0, 
    ENDED NUMBER DEFAULT 0, 
    TRANSACTION_ID NUMBER DEFAULT 0, 
    SIGNATOR VARCHAR2(150 BYTE) DEFAULT 0, 
    SENDER VARCHAR2(100 BYTE) DEFAULT 0, 
    RECIVER VARCHAR2(100 BYTE) DEFAULT 0, 
    ROUTE NUMBER DEFAULT 0, 
    SCANED DATE, 
    OPROGRAM VARCHAR2(30 BYTE) DEFAULT 'MP', 
    NOTE1 VARCHAR2(200 BYTE) DEFAULT '-', 
    CART VARCHAR2(50 BYTE) DEFAULT 0, 
    WEIGHT NUMBER, 
    HEIGHT NUMBER, 
    WIDTH NUMBER, 
    ODEPTH NUMBER, 
    PRO_ID NUMBER, 
    STITLE VARCHAR2(200 BYTE) DEFAULT 0, 
    SADDRESS VARCHAR2(200 BYTE) DEFAULT 0, 
    SPOSTOFFICENUMBER VARCHAR2(150 BYTE) DEFAULT 0, 
    SCITY VARCHAR2(200 BYTE) DEFAULT 0, 
    RTITLE VARCHAR2(200 BYTE) DEFAULT 0, 
    RADDRESS VARCHAR2(200 BYTE) DEFAULT 0, 
    RPOSTOFFICENUMBER VARCHAR2(150 BYTE) DEFAULT 0, 
    RCITY VARCHAR2(200 BYTE) DEFAULT 0, 
    CMR VARCHAR2(150 BYTE) DEFAULT 0, 
    NOTE2 VARCHAR2(2000 BYTE) DEFAULT ' ', 
    NOTE3 VARCHAR2(2000 BYTE) DEFAULT ' ', 
    TEMPERATURE_1 NUMBER, 
    TEMPERATURE_2 NUMBER, 
    NR_PALETTES NUMBER, 
    NOTE4 VARCHAR2(1000 BYTE), 
    NOTE5 VARCHAR2(1000 BYTE), 
    NOTE6 VARCHAR2(1000 BYTE), 
    S_TELEPHONE VARCHAR2(50 BYTE), 
    R_TELEPHONE VARCHAR2(50 BYTE), 
    LOADING_LAT NUMBER, 
    LOADING_LNG NUMBER, 
    UNLOADING_LAT NUMBER, 
    UNLOADING_LNG NUMBER, 
    TRAILER VARCHAR2(1000 BYTE), 
    TRAILER_ID NUMBER, 
    RABAT1 NUMBER, 
    RABAT2 NUMBER, 
    DATE_MODIFIED DATE, 
    CASH NUMBER, 
    ZOI VARCHAR2(1000 BYTE), 
    EOR VARCHAR2(1000 BYTE), 
    BUSINESSPREMISE VARCHAR2(1000 BYTE), 
    ELECTRONICDEVICE VARCHAR2(1000 BYTE), 
    VAT22 NUMBER, 
    VAT95 NUMBER, 
    OSNOVA1 NUMBER, 
    OSNOVA2 NUMBER, 
    PRICE NUMBER, 
    FURS_ORDER VARCHAR2(1000 BYTE), 
    INVOICE_NUMBER NUMBER
   ) ;
   
   
   
   
   
 --------------------------------------------------------
--  DDL for Table SHIPMENTS
--------------------------------------------------------
drop table shipments;
CREATE TABLE SHIPMENTS(
    ID NUMBER, 
	BARCODE VARCHAR2(25 BYTE), 
    ORDER_ID NUMBER, 
    CNT NUMBER(22,5) DEFAULT 0, 
    SCANED_COUNT NUMBER DEFAULT 0, 
    SCANED NUMBER DEFAULT 0, 
    SCANED_DATE DATE, 
    WEREHOUSE CHAR(150 BYTE) DEFAULT 0, 
    LOC CHAR(60 BYTE) DEFAULT 0, 
    SOPTION NUMBER DEFAULT 0, 
    COMPLETED NUMBER DEFAULT 00, 
    SEQ NUMBER DEFAULT 0, 
    TITLE VARCHAR2(150 BYTE) DEFAULT 0, 
    CLIENT_ID NUMBER(*,0) DEFAULT 0, 
    PALETTE VARCHAR2(25 BYTE) DEFAULT 0, 
    STYPE CHAR(10 BYTE) DEFAULT 0, 
    PRODUCT NUMBER DEFAULT 0, 
    TERMINAL CHAR(10 BYTE) DEFAULT 1, 
    DAMAGE_ID NUMBER DEFAULT 0, 
    DAMAGE_DESC VARCHAR2(100 BYTE) DEFAULT 0, 
    LOADING_X NUMBER(22,10) DEFAULT 0, 
    LOADING_Y NUMBER(22,10) DEFAULT 0, 
    UNLOADING_X NUMBER(22,10) DEFAULT 0, 
    UNLOADING_Y NUMBER(22,10) DEFAULT 0, 
    ACTIVE NUMBER DEFAULT 1, 
    QUANTITY_O NUMBER DEFAULT 0, 
    LOT VARCHAR2(18 BYTE) DEFAULT 0, 
    NOTE VARCHAR2(500 BYTE) DEFAULT 0, 
    NOTE1 VARCHAR2(500 BYTE) DEFAULT 0, 
    NOTE2 VARCHAR2(500 BYTE) DEFAULT 0, 
    NOTE3 VARCHAR2(500 BYTE) DEFAULT 0, 
    NR_PACKAGE NUMBER DEFAULT 0, 
    NR_PALETS NUMBER DEFAULT 0, 
    TRANSACTION_ID NUMBER DEFAULT 0, 
    QUANTITY_R NUMBER DEFAULT 0, 
    CART NUMBER DEFAULT 0, 
    ROUTE NUMBER DEFAULT 0, 
    PRICE NUMBER DEFAULT 0, 
    WEIGHT NUMBER DEFAULT 0, 
    HEIGHT NUMBER DEFAULT 0, 
    WIDTH NUMBER DEFAULT 0, 
    SDEPTH NUMBER DEFAULT 0, 
    PRO_ID NUMBER, 
    DOC_ID NUMBER, 
    RABAT1 NUMBER, 
    RABAT2 NUMBER
); 



drop table attachments;


  CREATE TABLE ATTACHMENTS
   (	
  ID NUMBER NOT NULL ENABLE, 
	ORDER_ID NUMBER, 
	ATYPE NUMBER, 
	ATTACHMENT_DATE DATE, 
	DOC_ID NUMBER, 
	STATUS NUMBER DEFAULT 0, 
	ORDER_TYPE VARCHAR2(20 BYTE) DEFAULT 0, 
	LOT VARCHAR2(50 BYTE), 
	LINKID VARCHAR2(400 BYTE), 
	SENDED NUMBER DEFAULT 0, 
	WORKER_ID NUMBER, 
	IMAGE_COMMENT VARCHAR2(400 BYTE), 
	EMAIL VARCHAR2(150 BYTE) DEFAULT '-', 
	FILE_NAME VARCHAR2(4000 BYTE), 
	SHIPMENT_ID NUMBER
   )  ;

  CREATE OR REPLACE  TRIGGER TGR_CVS_DOCUMENTS
before insert or update 
on attachments
for each row
begin
  /*  if inserting then

        SELECT REGEXP_REPLACE(SUBSTR(STANDARD_HASH(SYS_GUID(), 'SHA1'), 0, 32), '(.{8})(.{4})(.{4})(.{4})(.{12})', '\1-\2-\3-\4-\5') into :new.linkid  FROM DUAL    ;

    end if;*/
    null;
end;
/
ALTER TRIGGER TGR_CVS_DOCUMENTS ENABLE;

  CREATE OR REPLACE  TRIGGER TBE_ATTACHMENTS
BEFORE INSERT 
ON ATTACHMENTS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH Row
declare
l_tip_dokumenta   number:=0;
l_doc_id_glavni   number:=0; 
l_pro_id          number:=0;     
BEGIN

 
  if(nvl(:new.id,0)=0) then
   select att_auto.nextval into :new.id from dual;
  end if;
  
  
  If :new.shipment_id = 0 then
   Begin
   Select ord.Otype,ORD.PRO_ID into :new.ORDER_TYPE,l_pro_id from orders Ord where ORD.ORDER_ID=:new.order_id;
    EXCEPTION
    WHEN OTHERS THEN
    NULL;
   End;
  End if;
  
  If :new.order_type = 70 then
   Begin
   select teh.docid into l_doc_id_glavni from tehtalnilist teh where teh.vez=l_pro_id;
      EXCEPTION
    WHEN OTHERS THEN
    NULL;
   End;
  
  End if;
  
  
  If :new.order_type = 21 then
   Begin
   select teh.docid into l_doc_id_glavni from dobavskl teh where teh.vez=l_pro_id;
      EXCEPTION
    WHEN OTHERS THEN
    NULL;
   End;
  
  End if;
  
  
  If :new.Order_Type = 60 then
   l_tip_dokumenta :=9;
  elsif  :new.Order_Type = 70 then
   l_tip_dokumenta :=8;
  end if;
  
  If :new.Order_Type = 60 then
   begin
     SELECT SHI.DOC_ID INTO :NEW.doc_id   FROM SHIPMENTS SHI WHERE   SHI.ID=:NEW.SHIPMENT_ID;
   EXCEPTION
    WHEN OTHERS THEN
    NULL;
   End;
  End if;
  
   If :new.Order_Type = 70 then
     begin
        SELECT dodAUTO.NEXTVAL INTO :NEW.doc_id   FROM DUAL;
     EXCEPTION
       WHEN OTHERS THEN
        NULL;
     End;
   end if;
   
    If :new.Order_Type = 21 then
     begin
        SELECT dodAUTO.NEXTVAL INTO :NEW.doc_id   FROM DUAL;
     EXCEPTION
       WHEN OTHERS THEN
        NULL;
     End;
   end if;
  dbms_output.put_line('dodajanje dokumenta '||Trim(:NEW.FILE_NAME) || ' '||:new.doc_id||' '||l_doc_id_glavni);
  begin
   DODAJ_DOKUMENT ( l_tip_dokumenta, l_doc_id_glavni ,'O:\WinPro\mOrders\', Trim(:NEW.FILE_NAME),:new.doc_id, l_tip_dokumenta, :new.ORDER_TYPE,0);
  EXCEPTION
    WHEN OTHERS THEN
    NULL;
   End;
  
  
    If :new.order_type in (10,11) then
    Begin
        l_tip_dokumenta := 4;
        SELECT dodAUTO.NEXTVAL INTO :NEW.doc_id   FROM DUAL;
        select pro_id into l_pro_id from orders where order_id = :new.order_id;
        select nkl.docid into l_doc_id_glavni from nakladi nkl where nkl.vez=l_pro_id;
        DODAJ_DOKUMENT ( l_tip_dokumenta, l_doc_id_glavni ,'O:\WinPro\mOrders\', Trim(:NEW.FILE_NAME),:new.doc_id, l_tip_dokumenta, :new.ORDER_TYPE,0);
    EXCEPTION
        WHEN OTHERS THEN
        NULL;
    End;
    
    End if;
  
end;
/
ALTER TRIGGER TBE_ATTACHMENTS ENABLE;


--------------------------------------------------
drop table articles;
  CREATE TABLE ARTICLES
   (ID NUMBER DEFAULT 1, 
    TITLE VARCHAR2(1000 BYTE) DEFAULT 'A', 
    TYPE VARCHAR2(200 BYTE) DEFAULT 1, 
    BARCODE VARCHAR2(50 BYTE) DEFAULT 'A', 
    TYPEART CHAR(100 BYTE) DEFAULT 'A', 
    LAST DATE, 
    COMPANY NUMBER, 
    NOTE1 VARCHAR2(2000 BYTE), 
    NOTE2 VARCHAR2(2000 BYTE), 
    NOTE3 VARCHAR2(2000 BYTE), 
    NOTE4 VARCHAR2(2000 BYTE), 
    NOTE5 VARCHAR2(2000 BYTE), 
    PRICE NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Trigger ART_UPDATE
--------------------------------------------------------

CREATE OR REPLACE TRIGGER ART_UPDATE
before insert or update
on articles
for each row
begin
  :new.last:=sysdate+1;
  
  IF(:NEW.TYPE<>'M') then
    :new.barcode := trim(:new.type)||TRIM(to_char(:new.id,'09999'));
  end if ;
null;
end;
/