
--------------------------------------------------------
--  DDL for Table OMNIOPTI_PROCESSES
--------------------------------------------------------

  CREATE TABLE "OMNIOPTI_PROCESSES" 
   (	"PROCCESS_ID" VARCHAR2(500 BYTE), 
	"STATUS" NUMBER, 
	"D_SYSDATE" DATE DEFAULT sysdate, 
	"END_SYSDATE" DATE
   )   ;


/


--------------------------------------------------------
--  DDL for Procedure OMNIOPTI_LOGIN
--------------------------------------------------------
set define off;

  CREATE OR REPLACE  PROCEDURE "OMNIOPTI_LOGIN" (USERNAME IN VARCHAR2, PASS IN VARCHAR2, P_TOKEN OUT VARCHAR2) AS 
   
    pOut CLOB;
    resp json;
    
BEGIN
     url_connect('http://127.0.0.1/omniopti/auth/login?password='||pass||'&username='||username ,'','POST',pOut,'','application/json;charset=utf-8');
    -- dbms_output.put_line(pOut);
     /*
     {"accessToken":"eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJtQ1VqeVNUN25TLUt3a0pnZXZjOFJNR0pTekxrTS1vRWF4ek1QaFBpWnNjIn0.eyJleHAiOjE2NjkwNDkzMDksImlhdCI6MTY2OTAxMzMwOSwianRpIjoiMTU3NTcyNTgtN2QzMS00NjMwLWE2YTgtM2VlM2JjMDYwMWIxIiwiaXNzIjoiaHR0cHM6Ly9sb2dpbi5vbW5pb3B0aS5ldS9hdXRoL3JlYWxtcy9tYXN0ZXIiLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiMDQ1YjI0NTUtNjE4Yi00MzVlLThhMTItMzRiYjc5OTczNzg0IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoidGVzdCIsInNlc3Npb25fc3RhdGUiOiJjYTFjYjQyZS0yNzQ2LTQ2YjctYWQ0Zi1mOTAyNzM5NTkwY2MiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwicHJlZmVycmVkX3VzZXJuYW1lIjoiZmVyYml0IiwiZ21zIjpbXX0.m36F3YYDoAShDvCh8M4sYqSONXHmzueOZzN5fW2s7Yrj1hXtnsbs_RBc3x5DkuTa33mA0WgFGw1LcdK-URHxhc39rjuJavAq17fRSDqwixdJTFJPvYExZh6WF5awMBk1-hKLQwcJo02N7O6OfT7Sx_iU9w93OR33JhI5__h6oLeL7YE90bg4Lt9J3PrTz8hMqyN88A2DEGyCcNU42IqSfhcvni8mPa-Sux9f3NsTcY07zGk--ONDZz5564vvteTDuo3wlH4a-6BICAtI0xAkCvieAYeMl4pZFXIfjVV99TMw5HxkK_Gmv2m3O0mRv0zDd_D3g8IxlnWKp-8-Xfon8g","expiresIn":36000,"refreshToken":"eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJjMzNmMmIzMS04MWE5LTRhODItOGFmOS1lNzFkMGJjY2U5Y2QifQ.eyJleHAiOjE2NjkwMTUxMDksImlhdCI6MTY2OTAxMzMwOSwianRpIjoiNGMyZDU3NWMtN2Y4Yi00MDg1LWE3N2QtYWU0NTQ2NWE5YTk4IiwiaXNzIjoiaHR0cHM6Ly9sb2dpbi5vbW5pb3B0aS5ldS9hdXRoL3JlYWxtcy9tYXN0ZXIiLCJhdWQiOiJodHRwczovL2xvZ2luLm9tbmlvcHRpLmV1L2F1dGgvcmVhbG1zL21hc3RlciIsInN1YiI6IjA0NWIyNDU1LTYxOGItNDM1ZS04YTEyLTM0YmI3OTk3Mzc4NCIsInR5cCI6IlJlZnJlc2giLCJhenA
        iOiJ0ZXN0Iiwic2Vzc2lvbl9zdGF0ZSI6ImNhMWNiNDJlLTI3NDYtNDZiNy1hZDRmLWY5MDI3Mzk1OTBjYyIsInNjb3BlIjoiZW1haWwgcHJvZmlsZSJ9.PcAxLTyoP8g9ttK_lnUUbwRmLtAfUII7tGvFu3jHz5g"}
     */
     resp := json(pout);
     if(NVL(LENGTH(TRIM(json_ext.get_string(resp, 'accessToken'))),0)<>0) then
        p_token := json_ext.get_string(resp, 'accessToken');
     else
        p_token := 'UNAUTHORIZED';
     end if;
END OMNIOPTI_LOGIN;

/
--------------------------------------------------------
--  DDL for Procedure OMNIOPTI_SEND_DATA
--------------------------------------------------------

  create or replace PROCEDURE OMNIOPTI_SEND_DATA(P_ID IN NUMBER, p_token in varchar2, p_processId OUT VARCHAR2) AS 
   
    pOut CLOB;
    resp json;
    mContent clob;
    l_request json;
    l_response json;
    cursor cData is select * From nakladi where zbirnik=p_id;
    l_order json;
    l_data json_list;
    l_delivery json;
    l_location json;
    l_time json; 
    l_timeWindow json_list;
    l_tmp_timeWindow json;
    l_quantities json_list;
    l_quantity json; 
    l_vehicle json;
    l_vehicels json_list;
    l_vehicle_start json;
    l_vehicle_stop json;
    l_vehicle_start_location json;
    l_vehicle_stop_location json;  
    l_vehicle_skils json_list;
    l_vehicle_cost json;
    l_vehicle_costs json_list;
    
    l_vehicle_capacity json;
    l_vehicle_capacity1 json;
    l_vehicle_capacity2 json;
    l_vehicle_capacitys json_list;
    
    start_latitude number;
    start_longitude number;
    start_address varchar2(1000);
    start_address_id varchar2(1000);
    
    datum_naklada_zbirnik date;
    vehicleId number;
    vehicle_title varchar2(500);
BEGIN
    begin
        select  s.latitude, s.longitude, s.naslov,s.skladisce into start_latitude,start_longitude,start_address,start_address_id from zbirnik z left join skladisca s on s.skladisce=z.skladisce where vez=p_id;
    exception when others then
        null;
    end;
    
     begin
        select DATUMNAK,SIFVOZILO,VOZILO into datum_naklada_zbirnik,vehicleId,vehicle_title from zbirnik where vez=p_id;
    exception when others then
        null;
    end;
    
    l_data := json_list();
    for c in cData 
    loop
        null;
        l_order := json();
        
        l_order.put('id',c.vez);
        l_order.put('name',c.pnaslov1);        
        
        l_location := json();
        l_location.put('latitude',c.p_latitude );
        l_location.put('longitude', c.p_longitude);
        l_location.put('address',trim(c.pulica)||trim(c.pstevilka));
        l_location.put('locationId',trim(c.pulica)||trim(c.pstevilka));
        
        
        l_tmp_timeWindow := json();
--        l_tmp_timeWindow.put('start',to_char(c.pldatumraz_s,'HH24:MI'));
 --       l_tmp_timeWindow.put('end',to_char(c.pdatumraz_s,'HH24:MI'));
        l_tmp_timeWindow.put('start','00:00:00');
        l_tmp_timeWindow.put('end','23:59:59');
         
        l_timeWindow := json_list();
        l_timeWindow.append(l_tmp_timeWindow.to_json_value);
        
        l_time:=json();
        l_time.put('timeWindows',l_timeWindow);
        l_time.put('serviceTime',600);
        
        l_delivery := json();
        l_delivery.put('location', l_location);
        l_delivery.put('time',l_time);
        l_order.put('delivery',l_delivery);
        
        l_quantity := json();
        l_quantity.put('name','0');
        l_quantity.put('quantity',1);
        l_quantities := json_list();
        l_quantities.append(l_quantity.to_json_value);
        
        l_order.put('quantity',l_quantities);
        l_order.put('useAllVehicles',true); 
        l_data.append(l_order.to_json_value);
    end loop;
    
    l_request := json();
    l_request.put('userId',1);
    l_request.put('planId',p_id);
    l_request.put('name','Zbirnik '||p_id);
    
    l_vehicle := json();
    l_vehicle.put('id',24);
    l_vehicle.put('name','KP DA 895');
    l_vehicle.put('vehicleType','utility_over_7t');
  
  
    l_vehicle_start := json();
    l_vehicle_start_location := json();
    l_vehicle_start_location.put('latitude',start_latitude);--TODO : popravi na pravo lokacijo
    l_vehicle_start_location.put('longitude',start_longitude);--TODO : popravi na pravo lokacijo
    l_vehicle_start_location.put('address',start_address);--TODO : popravi na pravo lokacijo
    l_vehicle_start_location.put('locationId',start_address_ID);--TODO : popravi na pravo lokacijo
    l_vehicle_start.put('location',l_vehicle_start_location);
    l_vehicle_start.put('time',to_char(datum_naklada_zbirnik, 'hh24:mi:ss'));--popravi na pravilno uro
    
    
    
    l_vehicle_stop := json();
    l_vehicle_stop_location := json();
    l_vehicle_stop_location.put('latitude',start_latitude);--TODO : popravi na pravo lokacijo
    l_vehicle_stop_location.put('longitude',start_longitude);--TODO : popravi na pravo lokacijo
    l_vehicle_stop_location.put('address',start_address);--TODO : popravi na pravo lokacijo
    l_vehicle_stop_location.put('locationId',start_address_ID);--TODO : popravi na pravo lokacijo
    l_vehicle_stop.put('location',l_vehicle_stop_location);
    l_vehicle_stop.put('time','18:00:00');--popravi na pravilno uro
   
    l_vehicle_skils := json_list();
    l_vehicle_skils.append('24');
    
    l_vehicle_cost := json();
    l_vehicle_cost.put('waiting',1);
    l_vehicle_cost.put('perKm',100);
    l_vehicle_cost.put('fixed',1000);
    l_vehicle_cost.put('perH',1); 
    
   
    l_vehicle_capacitys := json_list();
     l_vehicle_capacity := json();
    l_vehicle_capacity.put('name','0');
    l_vehicle_capacity.put('capacity',1000); 
    
      l_vehicle_capacity1 := json();
    l_vehicle_capacity1.put('name','1');
    l_vehicle_capacity1.put('capacity',0); 
    
      l_vehicle_capacity2 := json();
    l_vehicle_capacity2.put('name','2');
    l_vehicle_capacity2.put('capacity',0); 
    l_vehicle_capacitys.append(l_vehicle_capacity.to_json_value);
    l_vehicle_capacitys.append(l_vehicle_capacity1.to_json_value);
    l_vehicle_capacitys.append(l_vehicle_capacity2.to_json_value);
    
    l_vehicle := json();

    l_vehicle.put('id',vehicleId);
    l_vehicle.put('name',trim(vehicle_title));
    l_vehicle.put('vehicleType','utility');
    l_vehicle.put('start',l_vehicle_start);
    l_vehicle.put('end',l_vehicle_stop); 
    l_vehicle.put('capacity',l_vehicle_capacitys); 
    l_vehicle.put('skils',l_vehicle_skils);
    l_vehicle.put('cost',l_vehicle_cost);  
    l_vehicle.put('nrOfVehicles',1);

      
    l_vehicels:=json_list();
    l_vehicels.append(l_vehicle.to_json_value);
    
    l_request.put('stops', l_data);
    l_request.put('vehicles',l_vehicels);
    l_request.put('includePolylines', true);
    l_request.put('nrIterations', 1000);
    l_request.put('algorithmTimeTermination', 60);
  
    DBMS_LOB.CREATETEMPORARY(mContent,true); 
    l_request.to_clob(mContent,true); 
    --dbms_output.put_line(mContent);
    INSERT INTO CLOB_TEST(A,D) VALUES(McONTENT,SYSDATE);
    url_connect('http://127.0.0.1/omniopti/sendJsonData',mContent,'POST',pOut,'Bearer '||p_token,'application/json;charset=utf-8');
   -- dbms_output.put_line(pOut);
    l_response := json(pOut);
    if(json_ext.get_bool(l_response,'hasErrors',1)=false) then
        dbms_output.put_line('poslano');    
        p_processId := json_ext.get_string(l_response,'processId');
    else
        p_processId := '-1';
    end if;
END OMNIOPTI_SEND_DATA;

/
--------------------------------------------------------
--  DDL for Procedure OMNIOPTI_START_COMPUTATION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE  PROCEDURE OMNIOPTI_START_COMPUTATION (pToken IN VARCHAR2, pId in varchar2) AS 
    l_response json;
    pOut clob;
BEGIN
    null; 
    url_connect('http://127.0.0.1/omniopti/async/startComputation?numIterations=1000&originalMode=json&postToMQTT=true&processID='||trim(pId),'','POST',pOut,'Bearer '||pToken,'application/json;charset=utf-8');
   ----DBMS_OUTPUT.put_line('start computation');
  --  DBMS_OUTPUT.put_line(pout);
    l_response := json(pout);
    if(json_ext.get_bool(l_response, 'hasErrors',1)=false) then
         insert into omniopti_processes(proccess_id,status,d_sysdate) values(pId, 0, sysdate);
    end if;
    /* 
    {
      "timeStamp": "21-11-2022 11:30",
      "hasErrors": false,
      "processId": "1669028181-20",
      "finished": false,
      "message": "Computation started, use MQTT or /checkComputationStatus to see when it finishes",
      "currentTask": "Polyline computation"
    }*/
    
END;

/
 
--------------------------------------------------------
--  DDL for Procedure OMNIOPTI_DOWNLOAD
-------------------------------------------------------- 
  CREATE OR REPLACE  PROCEDURE "OMNIOPTI_DOWNLOAD" (p_token in varchar2, pId in varchar2) is
    pOut CLOB;
    resp json;
    stopOrder json_list;
    stops json_list;
    tmporder json;
    sifraNkl number;
    stopOrderTmp varchar2(1000);
begin
--
dbms_output.put_line('curl -X GET "http://127.0.0.1/omniopti/downloadResults?includePolylines=true&mode=1&processID='||pid||'" -H "accept: */*" -H "Authorization: Bearer '||p_token||'"');
    url_connect('http://127.0.0.1/omniopti/downloadResults?includePolylines=true&mode=1&processID='||pId,'','GET',pOut,'Bearer '||p_token,'application/json;charset=utf-8');
    --dbms_output.put_line(pout);
    resp := json(pout);
    stopOrder := json_ext.get_json_list(resp, 'stopOrder');
    
    for idx in 1..stopOrder.count 
    loop
        tmporder := json(stopOrder.get(idx));
        stops := json_ext.get_json_list(tmporder, 'stopOrder');
        for i in 1..stops.count
        loop
            stopOrderTmp := stops.get_string(i);
            sifraNkl := to_number(replace(stopOrderTmp,'Delivery ',''));
            update nakladi set pal2 = i where vez=to_number(trim(sifraNkl));
        end loop;
        update omniopti_processes set status=1,END_SYSDATE=sysdate where trim(proccess_id)=trim(pid);
    end loop; 
end;

/
--------------------------------------------------------
--  DDL for Procedure OMNIOPTI_CHECK_STATUS
--------------------------------------------------------
 
  CREATE OR REPLACE  PROCEDURE "OMNIOPTI_CHECK_STATUS" (pToken IN VARCHAR2) AS 
    l_response json;
    pOut clob;
     /*

    {
      "timeStamp": "21-11-2022 11:30",
      "hasErrors": false,
      "processId": "1669028181-20",
      "finished": false,
      "message": "Computation started, use MQTT or /checkComputationStatus to see when it finishes",
      "currentTask": "Polyline computation"
    }*/
    cursor proc_Data is select * from omniopti_processes where status=0;
    b boolean;
    V_RESULT varchar(15);
BEGIN
    null;
    --curl -X GET "http://127.0.0.1/omniopti/checkComputationStatus?processID=1&whichFile=1" -H "accept: */*" -H "Authorization: Bearer access_token"
    
    for p in proc_data 
    loop
            
        url_connect('http://127.0.0.1/omniopti/checkComputationStatus?processID='||p.PROCCESS_ID||'&whichFile=1','','GET',pOut,'Bearer '||pToken,'application/json;charset=utf-8');
        l_response := json(pout);
        b:=json_ext.get_bool(l_response,'finished',0);
        dbms_output.put_line(pout);    
    dbms_output.put_line('curl -X GET "http://127.0.0.1/omniopti/checkComputationStatus?processID='||p.PROCCESS_ID||'&whichFile=1" -H "accept: */*" -H "Authorization: Bearer '||pToken||'"'     );
    
         v_result := case when b then 'true' else 'false' end;
        dbms_output.put_line(v_result );
         if(json_ext.get_bool(l_response,'finished',0)=true) then 
           dbms_output.put_line('downloading');
            OMNIOPTI_DOWNLOAD(ptoken, p.PROCCESS_ID);
        else
            dbms_output.put_line('je koncalo false');
            null;
        end if;
    end loop;
  
    l_response := json(pout);
     
   
END;

/

--------------------------------------------------------
--  DDL for Procedure OMNIOPTI_RUN_DOWNLOAD
--------------------------------------------------------
create or replace PROCEDURE OMNIOPTI_RUN_DOWNLOAD(ZBIRNIK IN NUMBER) as 
    p_token varchar2(10000):='';
    p_processId varchar2(1000):='';

 begin
    OMNIOPTI_LOGIN('ferbit', 'Ferbitolo1234', p_token); 
    OMNIOPTI_check_status(p_token);
 end; 
/

--------------------------------------------------------
--  DDL for Procedure OMNIOPTI_RUN_OPTI
--------------------------------------------------------
create or replace PROCEDURE OMNIOPTI_RUN_OPTI(ZBIRNIK IN NUMBER) as 
    p_token varchar2(10000):='';
    p_processId varchar2(1000):='';
 begin
   OMNIOPTI_LOGIN('ferbit', 'Ferbitolo1234', p_token);
   --dbms_output.put_line('----SENDING DATA----');
   --dbms_output.put_line('TOKEN: '||p_token);
   OMNIOPTI_SEND_DATA(28,p_token, p_processId);
   --dbms_output.put_line('-----DATA SENT-----');
   --dbms_output.put_line('processId : ' ||p_processId);
   OMNIOPTI_START_COMPUTATION(p_token,p_processId);
 end;
