/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */


var lgpx;
var map;
var infowindow = new google.maps.InfoWindow({maxWidth: 850});
google.maps.event.addListener(infowindow, 'closeclick', function () {
    selectedLdm = 90000000;
    showLdmOverflow(true);
});
var mUsers = [];
var mUsersVectorLayer = [];
var mUsersPopUps = [];
var truckLocation = {};
var truckCircle;
var truckCircleShowed = false;
var truckUnloadingVisible = false;
var circleDistance = 5000;
var map;
var routePath;
var markers = [];
var shipMarkers = [];
var geocoder;
var infowindows = [];
var tmpMarker = 0;
var ship;
var markersIndex = 0;
var showLoading = true;
var truckVisible = true;
var startDragPosition = {};
var selectedLdm = 90000000;
var intervalTruck = 0, intervalTruckId = 0, intervalShips = 0, intervalShipsId = 0;

function menu_open() {
    $("#menu").animate({width: 'toggle'}, 350);
}
function initializeTruck() {
    var mapOptions = {
        center: new google.maps.LatLng(47.32722087487091, 8.010060296602507),
        zoom: 7
    };
    geocoder = new google.maps.Geocoder();
    map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);
    refreshTrucksLocation();
    refreshLoadingAreas();
    $("#loadingDetail").dialog({
        autoOpen: false,
        resizable: false, // prevent resize of the dialog
        width: $(window).width() > 600 ? 900 : 'auto',
        modal: true, // show a modal overlay in background
        buttons: {
            "Vredu": function () {
                $(this).dialog("close");
            }
        }
    });
}

/*
 * 
    Funkcija prikaže posamezno vozilo na mapi.
    MarkerWithLabel je costume knjižnica, ki omogoča, da lahko pod vsakim kamiončkom oz. nakladom piše nek tekst. 
    Ker google maps tega ne podpira v originalu.

 */
function showTruck(truck) {
    try {
//        if (parseFloat(truck.longitude) == 0 || parseFloat(truck.latitude) == 0)
//            return;
        var skupnildm = 0;
        if (truck.skupnildm != undefined) {
            skupnildm = truck.skupnildm;
        }

        var skupnateza = 0;
        if (truck.skupnateza != undefined) {
            skupnateza = truck.skupnateza;
        }

        var image = './style/images/Truck.png';
        if (truck.stevilo <= 0)
            image = './style/images/TruckRed.png';
        var marker = new MarkerWithLabel({
            id: truck.id,
            map: map,
            icon: image,
            title: truck.name,
            turckId: truck.id,
            truckLdm: truck.nakmeter, // truck.ldm,
            avaliableLdm: truck.nakmeter - skupnildm,
            weightLimit: parseFloat(truck.nosilnost),
            avaliableWeight: truck.nosilnost - skupnateza,
            positionDate: truck.position_date,
            position: {lat: parseFloat(truck.latitude), lng: parseFloat(truck.longitude)},
            labelContent: truck.name,
            labelAnchor: new google.maps.Point(18, 0),
            labelClass: "my-custom-class-for-label", // your desired CSS class
            labelInBackground: true,

        });
        
        google.maps.event.addListener(marker, 'click', function () {// ob vsakem kliku na vozilo se zgodi to spodaj. Pokličeš podatke o nakladu in jih prikažeš
            if (marker.positionDate == undefined) {
                alert("Vozilo nima prave lokacije");
                return;
            }
            $.post("../getOrdersForSelectedTruck", {truck: truck.id})
                    .done(function (data) {
                        selectedLdm = marker.truckLdm - data.ldmSum;
                        showLdmOverflow(false);
                        if (infowindow != undefined)
                            infowindow.close(); // Close previously opened infowindow
                        google.maps.event.addListener(infowindow, 'domready', function () {

                            var weightPercent = 100 - (marker.avaliableWeight * 100 / marker.weightLimit)
                            var ldmPercent = 100 - (marker.avaliableLdm * 100 / marker.truckLdm)

                            $("#progressbarLdm").progressbar({//progres bar za prikaz koliko je poln kamion. ProgressBar izhaja iz jQueryUi knjižnice
                                value: ldmPercent

                            });
                            $("#progressbarNosilnost").progressbar({
                                value: weightPercent

                            });
                            $("#progressbarLdm").find(".ui-progressbar-value").css({"background": '#0000ff'});
                            $("#progressbarNosilnost").find(".ui-progressbar-value").css({"background": '#0000ff'});
                        });
                        infowindow.setContent("<div class='infowindow' id='infowindow'>" + data.data + "</div>");
                        infowindow.open(map, marker);
                    });
        });
        markers.push(marker);
    } catch (err) {
        //  alert(err);
    }
}

function showLdmOverflow(show) {
    for (var i = 0; i < shipMarkers.length; i++) {
        shipMarkers[i].setMap(map);
    }
    if (!$("#considerLdm").is(':checked'))
        return;
    if (!show) {
        for (var i = 0; i < shipMarkers.length; i++) {
            if (shipMarkers[i].ldm > selectedLdm) {
                shipMarkers[i].setMap(null);
            }
        }
    }
}

/**
 * 
 * Prikaz vseh nakladov glede na filter na mapi
 */
function refreshLoadingAreas() {
    for (var i = 0; i < shipMarkers.length; i++) {
        try {
            shipMarkers[i].setMap(null);
        } catch (err) {
        }
    }
    shipMarkers = [];
    ;
    if (showLoading) {
        $.getJSON("../getLoadingLocations?leto=" + $("#yearValue").val() + "&input=" + $("#input_output_type").val() + "&vehicle=" + $("#vehicleLoadingValue").val(), function (json) {
            ship = json.data;
            for (var i = 0; i < ship.length; i++) {
                if (ship[i].nulica != undefined && ship[i].nposta != undefined)
                    codeAddress(ship[i].vez, ship[i].nulica.trim() + "," + ship[i].nposta.trim(), i, ship[i].nalog, parseFloat(ship[i].n_latitude), parseFloat(ship[i].n_longitude), "red", ship[i].nakmeter);
            }

//            for (var i = 0; i < shipMarkers.length; i++) {
//                shipMarkers[i].addListener('click', function () {
//                    openInfoWindow(this)
//                });
//            }
        });
    }

    $("#monday").text(("" + getMonday(new Date())).substring(0, 15));
    $("#saturday").text(("" + getSaturday(new Date())).substring(0, 15));
}

/*
 * 
 Funkcija osveži pozicije tovornjakov
 */
function refreshTrucksLocation() {
    for (var i = 0; i < markers.length; i++) {
        try {
            markers[i].setMap(null);
        } catch (err) {
//   alert(err);
        }
    }
    if (truckVisible == false)
        return;
    mUsers = [];
    markers = [];
    $.getJSON("../getTruckLocations", function (json) {
        console.log("osveževanje vozil")
        mUsers = json.data;
        for (var i = 0; i < mUsers.length; i++) {
            showTruck(mUsers[i]);

        }
        $("#updateDate").text(("" + new Date().toJSON().slice(0, 16).replace("T", "   ")));
    });
}

function hideTrucks() {
    for (var i = 0; i < markers.length; i++) {
        try {
            if ($("#vehicleLoadingValue").val() != markers[i].id) {
                if ($("#vehicleLoadingValue").val() != 0) {
                    markers[i].setMap(null);
                } else {
                    markers[i].setMap(map);
                }
            }

        } catch (err) {
//   alert(err);
        }
    }
}

function sleepFor(sleepDuration) {
    var now = new Date().getTime();
    while (new Date().getTime() < now + sleepDuration) { /* do nothing */
    }
}

/*
 * Funkcija prikaže podatke za posamezen naklad
 */
function getContent(finded, marker) {
    $.post("../getOrder", {order: finded.vez, year: finded.leto, nalog: finded.nalog})
            .done(function (data) {
                var contentString = data;
                if (infowindow != undefined)
                    infowindow.close();
                infowindow = new google.maps.InfoWindow({width: 950, maxWidth: 950});
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            });
}

function openOrder(id) {
    $.post("../getOrderData", {order: id})
            .done(function (data) {
                showDetails(data);
            });
}

function showDetails(i) {
    var data = {};
    if (i.vez != undefined) {
        data = i
    } else {
        for (var j = 0; j < ship.length; j++) {
            var item = ship[j];
            if (item.vez == i) {
                data = item;
            }
        }
    }

    $("#order_nalog").text(data.nalog);
    $("#order_datum_nar").text(data.datumnar_s.substr(0, 10));
    $("#order_narocilo").text(data.dokumenti);
    //  $("#order_narocilo_title").text(data.narocilo);
    $("#order_stranka").text(data.kupec);
    $("#order_stranka_title").text(data.kupecnaziv);
    $("#order_cmr").text(data.cmr);
    $("#order_narocnik").text(data.narocnik);
    $("#order_narocnik_title").text(data.narocniknaziv);
    $("#order_komercialist_title").text(data.osebao.trim());
    $("#order_prevoznik_title").text(data.prevnaziv.trim());
    $("#order_vozilo_title").text(data.kamion);
    $("#order_voznik_title").text(data.vozniko);
    $("#order_prikolica_title").text(data.prikolicao);
    $("#order_blago_title").text(data.blago);
    $("#order_koli_value").text(data.kolicina);
    $("#order_teza_val").text(data.teza);
    $("#order_ldm_val").text(data.nakmeter);
    $("#order_st_pal_val").text(data.stpalet);
    $("#order_volumen_val").text(data.volumen);
    $("#order_start_title").text(data.nnaslov1);
    $("#order_start_address").text(data.nulica);
    $("#order_start_post").text(data.nstposta + " " + data.nposta);
    $("#order_start_contry").text(data.ndrzava_opis);
    $("#order_end_title").text(data.pnaslov1);
    $("#order_end_saddress").text(data.pulica);
    $("#order_end_post").text(data.pstposta + " " + data.pposta);
    $("#order_end_contry").text(data.pdrzava_opis);
    $("#order_opomba_val").text(data.opomba);
    $("#loadingDetail").dialog("option", "title", "Nalog " + data.nalog + " - " + data.fazaopis).dialog("open");
}

/*
 * 
    Funkcija izračuna x in y naklada. TRUE pogoj if stavka lahko odmisliš, ker dobimo LONGITUDE IN LATITUDE že izpolnjen.
    dragstart je event da si zapomneš koordinate od kje si začel vleči, da v kolikor nisi dodelil naklada na vozilu, ga lahko vrneš nazaj na pravo mesto
    dragend je event ko se konča vlečenje markerja po zemljevidu. Tukaj se potem kliče funkcija za razdaljo do kamiona.
 */
function codeAddress(vez, address, i, nalog, lat, lng, icon, ldm, weight) {
    if (lat == 0 && lng == 0) {
        geocoder.geocode({'address': address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                console.log("zracunano");
                map.setCenter(results[0].geometry.location);
                var marker = new MarkerWithLabel({
                    map: map,
                    draggable: true,
                    //icon: 'http://maps.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png',
                    icon: './style/images/package.png',
                    vez: vez,
                    id: nalog,
                    ldm: ldm,
                    weight: weight,
                    title: 'NALOG : ' + nalog,
                    infowindow: markersIndex,
                    position: results[0].geometry.location,
                    labelContent: nalog,
                    labelAnchor: new google.maps.Point(18, 0),
                    labelClass: "my-custom-class-for-label", // your desired CSS class
                    labelInBackground: true
                });
                $.getJSON("saveLongitude.jsp?id=" + vez + "&lat=" + results[0].geometry.location.lat() + "&lng=" + results[0].geometry.location.lng());
                google.maps.event.addListener(marker, "dragstart", function (event) {
                    startDragPosition = event.latLng;
                });
                google.maps.event.addListener(marker, "dragend", function (event) {

                    var lat = event.latLng.lat();
                    var lng = event.latLng.lng();
                    markerDragEnd(lat, lng, this.id, this.vez, this);
                });
                marker.addListener('click', function () {
                    openInfoWindow(this)
                });
                shipMarkers.push(marker);
            } else {
                console.log("Geocode was not successful for(" + address + ") the following reason: " + status);
            }
        });
    } else {
//console.log("samo prikazano");
        var marker = new MarkerWithLabel({
            map: map,
            vez: vez,
            draggable: true,
            //icon: 'http://maps.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png',
            icon: './style/images/package.png',
            title: 'NALOG : ' + nalog,
            id: nalog,
            ldm: ldm,
            weight: weight,
            infowindow: markersIndex,
            position: {lat: lat, lng: lng},
            labelContent: nalog,
            labelAnchor: new google.maps.Point(18, 0),
            labelClass: "my-custom-class-for-label", // your desired CSS class
            labelInBackground: true
        });
        google.maps.event.addListener(marker, "dragstart", function (event) {
            startDragPosition = event.latLng;
        });
        google.maps.event.addListener(marker, "dragend", function (event) {
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();
            markerDragEnd(lat, lng, this.id, this.vez, this);
        });
        marker.addListener('click', function () {
            openInfoWindow(this)
        });
        shipMarkers.push(marker);
    }
}

function markerDragEnd(lat, lng, order, vez, marker) {
    marker.setPosition(startDragPosition);
    for (var i = 0; i < markers.length; i++) {
        if (distance(markers[i].position.lat(), markers[i].position.lng(), lat, lng) < 0.6) {
//ali je trenutni večji od kamiona
            vehicleSelected(order, markers[i].title, vez, marker.ldm > markers[i].avaliableLdm || marker.weight > markers.avaliableWeight, markers[i].id);
        }
    }
}

/*
 * Funkcija je proži ko naklad potegneš na vozilo. To pomeni, da si nakladu dodelil vozilo in se mora to osvežiti v bazi podatkov.
 * Kliče se servlet, ki naredi update v Oracle.
 * Ko se naredi update, moramo uporabnika vprašati ali bo nalog poslal na vozilo.
 */
function vehicleSelected(order, vehicle, vez, ldmOverflow, vehicleId) {
    var div = $('<div></div>');
    var content = "Nalogu številka <strong>" + order + "</strong> bo dodeljeno vozilo <strong>" + vehicle + ".</strong>";
    var img = "";
    var comment = "";
    if (ldmOverflow) {
        img = '<img src="../images/Alert.png" width="50px" height="50px;"/>'
        comment = '<strong style="color:red"> LDM ali teža so prekoračeni</strong>';
    }
    content = "<table>" +
            "<tr>" +
            "<td>" + img + "</td>" +
            "<td>" + content + '<br/>' + comment + '</td>' +
            "</tr>" +
            "</table>";
    div.html(content).dialog({
        title: "Opozorilo",
        resizable: false,
        modal: true,
        width: 500,
        buttons: {
            "Vredu": function () {
                $(this).dialog("close");
                //zahteva post za dodelitev vozila
                $.post("../setVehicleToOrder", {vehicle_id: vehicleId, vez: vez})
                        .done(function (data) {
                            if (data.status == 0) {
                                sendOrderToVehicle(order, vez, vehicle, vehicleId);
                            }
                        });
            },
            "Prekliči": function () {
                $(this).dialog("close");
            }
        }
    });
}

/**
 * 
    Opozorilo da je bil nalog poslan oziroma napak.
 */
function sendOrderToVehicle(vez, all) {
    $.post("../sendOrderToDevice", {all: all, id: vez})
            .done(function (data) {
                var message = "";
                if (data.status == 0) {
                    message = "Poslano";
                } else {
                    message = "Napaka";
                }
                var div = $('<div></div>');
                div.html(message).dialog({
                    title: "Opozorilo",
                    resizable: false,
                    modal: true,
                    width: 500,
                    buttons: {
                        "V redu": function () {
                            $(this).dialog("close");
                        },
                    }
                });
            });
}

/**
 * 
    Funkcija pošilja nalog na vozilo. Kliče se servlet, ki proži proceduro v oraclu.
    Ko boš rabil klic mi povej, ti bom poslal.
 */
function sendOrderToVehicle(order, vez, vehicle, vehicleId) {
    var div = $('<div></div>');
    div.html("Ali nalog <strong>" + order + "</strong> pošljem na vozilo <strong>" + vehicle + "</strong>?").dialog({
        title: "Opozorilo",
        resizable: false,
        modal: true,
        width: 500,
        buttons: {
            "DA brez podnakladov": function () {
                sendOrderToVehicle(false, vez);
                $(this).dialog("close");
            },
            "DA tudi podnaklade": function () {
                sendOrderToVehicle(true, vez);
                $(this).dialog("close");
            },
            "Prekliči": function () {
                refreshLoadingAreas()
                $(this).dialog("close");
            }
        }
    });
}

function openInfoWindow(marker) {
    var obj;
    for (var i = 0; i < ship.length; i++) {
        if (ship[i].vez == marker.vez) {
            obj = ship[i];
            break;
        }
    }
    getContent(obj, marker);
}

/**
 * 
 funkcija za osveževanje nakladov ko je spremenjen filter na ekranu
 */
function loadingAreasShow(e) {
    showLoading = e.checked;
    refreshLoadingAreas();
}

function truckShow(e) {
    truckVisible = e.checked;
    if (e.checked) {
        refreshTrucksLocation();
    } else {
        for (var i = 0; i < markers.length; i++) {
            try {
                markers[i].setMap(null);
            } catch (err) {
            }
        }
    }
}

function truckShowUnloading(e) {
    truckUnloadingVisible = e.checked;
}

function toogleControls() {
    $("#control").toggle();
}

function getMonday(d) {
    d = new Date(d);
    var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
    return new Date(d.setDate(diff));
}

function getSaturday(d) {
    d = new Date(d);
    var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6 : 6); // adjust when day is sunday
    return new Date(d.setDate(diff));
}


function cirlceDistnce(e) {
    circleDistance = parseInt(e.value) / 2;
    $("#circleDistanceSpan").text(circleDistance / 500);
    if (truckCircle != undefined) {
        truckCircle.setRadius(circleDistance);
        for (var i = 0; i < shipMarkers.length; i++) {
            shipMarkers[i].setMap(null);
            var distanceL = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(truckLocation.lat, truckLocation.lng), new google.maps.LatLng(shipMarkers[i].position.lat(), shipMarkers[i].position.lng()));
            if (distanceL <= circleDistance) {
                shipMarkers[i].setMap(map);
            }
        }
    }
}
/*
 * funkcija za izračun oddaljenosti ene točke od druge.
 * To se uporablja ko vlečeš naklad na vozilo, v kolikor je razladlja manjša od predvidene potem se naklad dodli vozilu
 */
function distance(lat1, lon1, lat2, lon2, unit) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    } else {
        var radlat1 = Math.PI * lat1 / 180;
        var radlat2 = Math.PI * lat2 / 180;
        var theta = lon1 - lon2;
        var radtheta = Math.PI * theta / 180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit == "K") {
            dist = dist * 1.609344
        }
        if (unit == "N") {
            dist = dist * 0.8684
        }
        return dist;
    }
}

/**
    resize zemljevida. Neka funkcija ki spreminja velikost google mape glede na velikost ekrana
 */
function resizeMap() {
    var windowWidth = $(window).width();
    var windowHeight = $(window).height();

    $("#map-canvas").css("height", (windowHeight - 40) + "px");
    $("#map-canvas").css("width", (windowWidth - 300) + "px");
}

function hideLoadedTruck() {
    for (var i = 0; i < markers.length; i++) {
        try {
            if ($("#onlyAvaliableVehicle").is(':checked')) {
                if (markers[i].avaliableLdm <= 0 || markers[i].avaliableWeight <= 0)
                    markers[i].setMap(null);
            } else {
                markers[i].setMap(map);
            }
        } catch (err) {
        }
    }
}

$(document).ready(function () {
    initializeTruck();
    loadSettings();
    $("#settingsDiv").dialog({
        autoOpen: false,
        resizable: false, // prevent resize of the dialog
        width: $(window).width() > 600 ? 900 : 'auto',
        modal: true, // show a modal overlay in background
        buttons: {
            "Vredu": function () {
                saveSettings();
                $(this).dialog("close");
            }
        }
    });


    $(window).resize(function () {
        resizeMap();
    });
    resizeMap();
});

