
--Najprej to poženeš z SYS 
begin


    DBMS_SCHEDULER.create_credential (credential_name  => 'MAILSERVER_CREDENTIAL_otl',
                                         username        => 'ferbit@omahen-transport.si',
                                           password        => '*****');
    
    grant execute on MAILSERVER_CREDENTIAL_otl to public;

    DBMS_SCHEDULER.set_scheduler_attribute('email_server', 'xx.xx.xx:25');
    DBMS_SCHEDULER.set_scheduler_attribute('email_sender', 'ferbit@omahen-transport.si'); 
    DBMS_SCHEDULER.set_scheduler_attribute ('email_server_credential', 'MAILSERVER_CREDENTIAL_otl');
end;
 


 -- potem pa spodnje z uporabnikom kjer imamo job


BEGIN
 DBMS_SCHEDULER.ADD_JOB_EMAIL_NOTIFICATION (
  job_name   =>  'GET_UPDATE_JOB',
  recipients =>  'matej@ferbit.net',
  sender     =>  'ferbit@omahen-transport.si',
  subject    =>  'Scheduler Job Notification-%job_owner%.%job_name%-%event_type%',
  body       =>   '%event_type% occurred at %event_timestamp%. %error_message%',
  events     =>  'JOB_FAILED, JOB_BROKEN, JOB_DISABLED, JOB_SCH_LIM_REACHED');-- tukaj obstaja še več parametrov ampak jih ne poznam
END;remote
/


--v kolikor želiš odstraniti opozorila potem pa spodnje
BEGIN

    DBMS_SCHEDULER.REMOVE_JOB_EMAIL_NOTIFICATION (    
            job_name => '"SCHEMA"."GET_UPDATE_JOB"'
             );

end;
/





declare
    token varchar2(1000):='';
    TYPE VRNI_rt IS RECORD(document_id varchar2(10000), path varchar2(10000), docType number);
    l_VRNI   VRNI_rt;
    b_kurzor        sys_refcursor;
    DateFrom date;
    DateTo date;
begin


        select trim(tekst) into token from ferbit.nastavi where kljuc='morders_token';
   --prevelika kolicina za id : 038312345600411197
   datefrom := to_Date( '2024-04-15 13:35','yyyy-mm-dd hh24:mi:ss');
   dateTo := to_Date( '2024-04-15 14:45:35','yyyy-mm-dd hh24:mi:ss');
   morders_get_data(token,DateFrom,DateTo);
  /*  OPEN b_kurzor FOR 'select linkid, file_name, ORDER_TYPE from attachments where nvl(status,0)=0  and attachment_date>sysdate-45';
                     LOOP
                        FETCH b_kurzor INTO l_VRNI;
                        EXIT WHEN b_kurzor%NOTFOUND; 
                         morders_GET_document(token, l_VRNI.document_id, l_vrni.path,l_vrni.docType);  
                    END LOOP;
        close b_kurzor;
*/

        MERGE INTO ferbit.nastavi n
            USING (select 1 from dual) m
            ON (n.kljuc='morders_PRENOS_DATUM')
          WHEN MATCHED THEN
            UPDATE SET n.tekst=to_char(DateTo,'YYYY-MM-DD HH24:MI:SS')
          WHEN NOT MATCHED THEN
            INSERT (kljuc, tekst)
            VALUES ('morders_PRENOS_DATUM', to_char(DateTo,'YYYY-MM-DD HH24:MI:SS'));


        --update ferbit.nastavi set tekst=to_char(DateTo,'YYYY-MM-DD HH24:MI:SS') where kljuc='morders_PRENOS_DATUM';
        commit;
         -- DBMS_OUTPUT.PUT_LINE('end '||to_char(sysdate,'yyyy-MM-dd hh24:mi:ss'));
end;