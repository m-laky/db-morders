NAME                         TYPE    VALUE                      
---------------------------- ------- -------------------------- 
audit_file_dest              string  /opt/oracle/admin/XE/adump 
audit_sys_operations         boolean TRUE                       
audit_syslog_level           string                             
audit_trail                  string  DB                         
unified_audit_sga_queue_size integer 1048576                    
unified_audit_systemlog      string                             

https://support.huawei.com/enterprise/en/doc/EDOC1100044373/e67af621/disabling-the-audit-service-for-the-oracle-database